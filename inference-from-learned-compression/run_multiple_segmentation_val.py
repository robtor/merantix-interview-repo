import glob
import os

import configargparse
import tensorflow as tf

import segmentation_evaluate
from networks.nets_class import networks
from utils import utils_run_multiple as utils

NETWORKS = networks

ENCODER_DECODER_BASEDIR = '/srv/glusterfs/robertto/imagenet/compression_runs'
DATA_DIR = '/srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012'
DATA_LIST = '/scratch_net/tractor/robertto/masters_thesis/dataset/val.txt'
MODEL_SCOPE = 'resnet'
NUM_STEPS = 1449

ENC_DEC_PATH_MAP = utils.get_enc_dec_map(ENCODER_DECODER_BASEDIR)


def check_valid_dir_name(dir_name, network_names=NETWORKS):
    network_name = get_network_name(dir_name)

    x1 = network_name in network_names
    x2 = 'segmentation' in network_name

    return x1 and x2


def get_network_name(dir_name):
    folder_name = os.path.basename(os.path.normpath(dir_name))
    return folder_name.split('_')[0]


def main(FLAGS):
    dir_names = glob.glob(os.path.join(FLAGS.base_dir, FLAGS.pattern))
    dir_names.sort()

    if FLAGS.print_dirs:
        utils.print_dir_names(dir_names)
        return 0

    for dir_name in dir_names:
        print(dir_name)

        if check_valid_dir_name(dir_name, network_names=NETWORKS):
            folder_name = os.path.basename(os.path.normpath(dir_name))
            output_filename = os.path.join(FLAGS.output_dir, folder_name + '_val.txt')

            models_finished = utils.parse_output_filename(output_filename)
            models = utils.get_models_list(dir_name,
                                           models_finished,
                                           model_basename=FLAGS.model_basename,
                                           start_point=FLAGS.start_point,
                                           end_point=FLAGS.end_point,
                                           steps=FLAGS.steps)

            key = utils.get_enc_dec_mapping_number('key-', folder_name, default_val=None)

            if key:
                enc_dec_dir = ENC_DEC_PATH_MAP[key]
            else:
                enc_dec_dir = None

            network_name = get_network_name(dir_name)

            save_per_class_accuracy_flag = '--no-save-per-class-accuracy'
            if FLAGS.save_per_class_accuracy:
                save_per_class_accuracy_flag = '--save-per-class-accuracy'

            for model in models:
                print(os.path.join(dir_name, model))
                parser = segmentation_evaluate.get_arguments()

                flags_eval = parser.parse_args(['--data-dir', DATA_DIR,
                                                '--data-list', DATA_LIST,
                                                '--num-steps', str(NUM_STEPS),
                                                '--model-scope', MODEL_SCOPE,
                                                '--encoder-decoder-dir', enc_dec_dir,
                                                '--network-name', network_name,
                                                '--restore-from', os.path.join(dir_name, model),
                                                '--output-filename', output_filename,
                                                save_per_class_accuracy_flag])

                # Exception handling here so that a tensorflow error doesn't kill the whole program
                try:
                    # Also possible to use a context manager
                    # with tf.Graph().as_default():
                    tf.reset_default_graph()
                    segmentation_evaluate.main(flags_eval)
                except Exception as e:
                    print('A tensorflow exception happened at {}'.format(os.path.join(dir_name, model)))
                    print(e)


if __name__ == '__main__':
    parser = configargparse.ArgumentParser()

    parser.add_argument("--base-dir",
                        type=str,
                        required=True,
                        help='Dir that has the validation runs. base_dir + pattern is globbed')
    parser.add_argument("--pattern",
                        type=str,
                        default='*',
                        help='Folder pattern to use. base_dir + pattern is globbed. Note that when using wildcard matching with * from the command-line, the argument must be surrounded by \', example: --pattern \'summary_*200_resnet*\' so that it doesn\'t get expanded in the command-line')
    parser.add_argument("--output-dir",
                        type=str,
                        help='Folder where the configs are stored')
    parser.add_argument("--model-basename",
                        type=str,
                        default='model.ckpt',
                        help='')
    parser.add_argument("--start-point",
                        type=int,
                        default=0,
                        help='starting point for model validation')
    parser.add_argument("--end-point",
                        type=int,
                        default=2000000,
                        help='end point for model validation')
    parser.add_argument("--steps",
                        type=int,
                        default=20000,
                        help='Steps for model validation')
    parser.add_argument('--print-dirs', dest='print_dirs', action='store_true',
                        help='If passed shows the folders that will be processed by the script')
    parser.set_defaults(print_dirs=False)
    parser.add_argument('--save-per-class-accuracy', dest='save_per_class_accuracy', action='store_true',
                        help="Weather to also save a file for each run with per class accuracy")
    parser.set_defaults(save_per_class_accuracy=False)
    FLAGS = parser.parse_args()

    main(FLAGS)
