import tensorflow as tf
import glob
"""
The fields used

  example = tf.train.Example(features=tf.train.Features(feature={
      'image/height': _int64_feature(height),
      'image/width': _int64_feature(width),
      'image/colorspace': _bytes_feature(colorspace),
      'image/channels': _int64_feature(channels),
      'image/class/label': _int64_feature(label),
      'image/class/synset': _bytes_feature(str.encode(synset)),
      'image/class/text': _bytes_feature(str.encode(human)),
      'image/object/bbox/xmin': _float_feature(xmin),
      'image/object/bbox/xmax': _float_feature(xmax),
      'image/object/bbox/ymin': _float_feature(ymin),
      'image/object/bbox/ymax': _float_feature(ymax),
      'image/object/bbox/label': _int64_feature([label] * len(xmin)),
      'image/format': _bytes_feature(str.encode(extension)),
      'image/filename': _bytes_feature(str.encode(os.path.basename(filename))),
      'image/encoded': _bytes_feature(image_buffer)}))

"""
def _parse_proto_example(serialized_example):
    features = tf.parse_single_example(
        serialized_example,
        features={
            'image/encoded': tf.FixedLenFeature([], tf.string),
            'image/class/label': tf.FixedLenFeature([], tf.int64),
            'image/channels': tf.FixedLenFeature([], tf.int64)
        })

    image_decoded = tf.image.decode_image(features['image/encoded'],
                                         channels=3)
    label_index = features['image/class/label']

    return image_decoded, label_index

list_of_files = glob.glob('/home/robertto/glusterfs_robertto/imagenet/imagenet_256/images_val_256_reduced_compressed_tfrecord/*')
#print(list_of_files)
list_of_files = list_of_files[0:2]

filename_queue = tf.train.string_input_producer(list_of_files,
                                                num_epochs=None,
                                                shuffle=True)

reader = tf.TFRecordReader()
_, serialized_example = reader.read(filename_queue)
image_decoded, label_index = _parse_proto_example(serialized_example)


with tf.Session() as sess:
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    for i in range(10000):
        l = sess.run(label_index)
        print(l)

    coord.request_stop()
    coord.join(threads)
