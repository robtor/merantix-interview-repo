import tensorflow as tf
import tensorflow.contrib.slim as slim

import models.slim.nets.resnet_v1 as resnet_v1
import utils
from networks.resnet_blocks_mapping import networks_map

num_classes = None
is_training = True
inputs = tf.placeholder(tf.float32, (64, 224, 224, 3))
#inputs = tf.placeholder(tf.float32, (64, 320, 320, 3))
#inputs = tf.placeholder(tf.float32, (64, 3, 224, 224))
#inputs = tf.placeholder(tf.float32, (64, 32, 28, 28))
data_format = 'NHWC'
#data_format = 'NCHW'
global_pool = True
output_stride = 8

summary_dir = 'resnet-18_graph'


#from resnet_blocks_mapping import networks_map
#
blocks = networks_map['resnet-50']
#
with slim.arg_scope(resnet_v1.resnet_arg_scope()):
    logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                            global_pool=False, output_stride=output_stride,
                                           include_root_block=True, reuse=None, scope='resnet',
                                           spatial_squeeze=False, data_format=data_format)

logits = logits + 0.1
#network_fn = nets_factory.get_network_fn('resnet-50',
#                                         num_classes=1000,
#                                         is_training=False,
#                                         scope='resnet',
#                                         data_format=data_format)


#logits, end_points = network_fn(inputs, reuse=None)

init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
init_op = tf.group(*init_list)

with utils.get_session() as sess:
    sess.run(init_op)
    train_writer = tf.summary.FileWriter(summary_dir, sess.graph)
