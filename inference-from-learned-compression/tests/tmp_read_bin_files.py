import csv
import os
from collections import namedtuple
from time import time

import numpy as np
import tensorflow as tf

import utils
from utils.symbols_to_feature_vol import conversion_op


def create_mapping(input_filename):
    d = {}
    with open(input_filename, 'r') as f:
        csvreader = csv.reader(f, delimiter=',')
        header = next(csvreader)

        for row in csvreader:
            d[row[0]] = (int(row[1]), int(row[2]))

    return d


def get_spec_tensor(op_point, shape, downscale_factor):
    C = op_point.C
    h = shape[0] // downscale_factor
    w = shape[1] // downscale_factor
    ph = tf.constant(op_point.ph)
    pw = tf.constant(op_point.pw)
    d = ph * pw
    n = h * w // (ph * pw)

    return tf.convert_to_tensor([C, h, w, n, ph, pw, d])


def get_paths_labels_and_shapes(dataset, map_name_to_dim_filename):
    d = create_mapping(map_name_to_dim_filename)

    filelist = dataset.get_file_list()
    label_list = dataset.get_labels_list()

    shapelist = []

    for f_name in filelist:
        key = os.path.splitext(os.path.basename(f_name))[0]
        key = key.replace('_sym', '')

        shapelist.append(d[key])

    return filelist, label_list, shapelist


def get_file_label_and_shape_queue(path_list, label_list, shape_list):
    shapes = np.array(shape_list, dtype=np.int32)
    filename_queue, label, shape = tf.train.slice_input_producer([path_list, label_list, shapes])

    np_file = tf.read_file(filename_queue)

    return tf.decode_raw(np_file, tf.int16), label, shape


def process_symbols_tensor(symbols_tensor, shape, centers_tensor, op_point, downscale_factor):
    symbols_tensor_reshape = tf.reshape(symbols_tensor, shape=(op_point.C, -1))
    symbols_tensor_reshape = tf.cast(symbols_tensor_reshape, tf.int32)

    # TODO Remove this mapping when code is fixed (see todo below)
    map_idx = {'C': 0, 'h': 1, 'w': 2, 'n': 3, 'ph': 4, 'pw': 5, 'd': 6}
    spec_tensor = get_spec_tensor(op_point, shape, downscale_factor)

    feature_vol = conversion_op(symbols_tensor_reshape, centers_tensor, spec_tensor, d=map_idx, num_channels=op_point.C)
    feature_vol = tf.squeeze(feature_vol, axis=0)

    # TODO: Move crop to before the reshape stuff. 2 benefits, change some
    #       methods from dynamic to constant and should reduce the computations
    #       a little bit since it's always working with a fixed size
    #       discuss w. Fabian.
    #       Before cropping (conservative estimate) they are
    #       (32 * 16 * 20) / (32 * 14 * 14) - 1 ~= 63% larger
    feature_vol_crop = tf.random_crop(feature_vol, [28, 28, op_point.C])
    feature_vol_crop.set_shape([28, 28, op_point.C])

    return feature_vol_crop


def get_symbols():
    OperatingPoint = namedtuple('OperatingPoint', ['C', 'ph', 'pw'])
    op_point = OperatingPoint(C=32, ph=2, pw=2)

    BATCH_SIZE = 64
    DOWNSCALE_FACTOR = 8

    map_name_to_dim_filename = './input/filename_shape_map_val.txt'
    DATA_DIR = '/home/robert/code/masters_thesis/tmp_bin_files'
    SYNSET_FILENAME = './input/imagenet_lsvrc_2015_synsets.txt'
    EXTENSION = 'bin'
    centers_filename = './tmp_bin_files/centers_32_b5e_4_prog_LR6e-6_ckpt1162357.npy'

    centers = np.load(centers_filename)
    centers_tensor = tf.constant(centers)

    dataset = utils.ImagenetData(DATA_DIR, SYNSET_FILENAME, extension=EXTENSION)

    path_list, label_list, shape_list = get_paths_labels_and_shapes(dataset, map_name_to_dim_filename)
    symbols_tensor, label, shape = get_file_label_and_shape_queue(path_list, label_list, shape_list)
    feature_vol_crop = process_symbols_tensor(symbols_tensor, shape, centers_tensor, op_point, DOWNSCALE_FACTOR)

    # TODO Use shuffle batch and proper capacity etc. Add labels
    feature_vol_batch, label_batch = tf.train.batch([feature_vol_crop, label], batch_size=BATCH_SIZE, capacity=100)

    return feature_vol_batch, label_batch


def main():
    BATCH_SIZE = 64
    feature_vol_batch, label_batch = get_symbols()

    init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
    init_op = tf.group(*init_list)

    with tf.Session() as sess:
        sess.run(init_op)

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        for i in range(10000):
            start_time = time()
            feat_out, label_out = sess.run([feature_vol_batch, label_batch])

            duration = time() - start_time

            print(BATCH_SIZE / duration)
            print(feat_out.shape)
            print(label_out.shape)

        coord.request_stop()
        coord.join(threads)


if __name__ == '__main__':
    main()
