import tensorflow as tf

import utils
from networks import nets_factory

num_classes = 1000
is_training = True
#inputs = tf.placeholder(tf.float32, (64, 224, 224, 3))
#inputs = tf.placeholder(tf.float32, (64, 3, 224, 224))
inputs = tf.placeholder(tf.float32, (64, 8, 56, 56))
#data_format = 'NHWC'
data_format = 'NCHW'
global_pool = True
output_stride = None

summary_dir = 'resnet-18_graph'


#from resnet_blocks_mapping import networks_map
#
#blocks = networks_map['resnet-18']
#
#with slim.arg_scope(resnet_v1.resnet_arg_scope()):
#    logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
#                                            global_pool=global_pool, output_stride=output_stride,
#                                           include_root_block=True, reuse=None, scope='resnet',
#                                           data_format=data_format)
network_fn = nets_factory.get_network_fn('chopped-48-root',
                                         num_classes=1000,
                                         is_training=False,
                                         scope='resnet',
                                         data_format=data_format)


logits, end_points = network_fn(inputs, reuse=None)

init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
init_op = tf.group(*init_list)

with utils.get_session() as sess:
    sess.run(init_op)
    train_writer = tf.summary.FileWriter(summary_dir, sess.graph)
