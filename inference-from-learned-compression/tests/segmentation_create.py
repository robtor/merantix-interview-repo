import sys
sys.path.append('.')
import tensorflow as tf
import models.slim.nets.resnet_v1 as resnet_v1
import utils
from networks import nets_image_segmentation

inputs = tf.placeholder(tf.float32, (64, 321, 321, 3))
data_format = 'NHWC'
#data_format = 'NCHW'
global_pool = True
output_stride = None

summary_dir = 'segmentation_graph'


#from resnet_blocks_mapping import networks_map
#
#blocks = networks_map['resnet-18']
#
#with slim.arg_scope(resnet_v1.resnet_arg_scope()):
#    logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
#                                            global_pool=global_pool, output_stride=output_stride,
#                                           include_root_block=True, reuse=None, scope='resnet',
#                                           data_format=data_format)
arg_scope = resnet_v1.resnet_arg_scope()
logits, end_points = nets_image_segmentation.resnet_50(inputs,
                                                       arg_scope,
                                                       is_training=True,
                                                       global_pool=False,
                                                       num_classes=None,
                                                       output_stride=8,
                                                       reuse=None,
                                                       data_format='NHWC')


restore_var = [v for v in tf.global_variables() if 'fc' not in v.name or not True]
all_trainable = [v for v in tf.trainable_variables() if 'beta' not in v.name and 'gamma' not in v.name]
fc_trainable = [v for v in all_trainable if 'fc' in v.name]
conv_trainable = [v for v in all_trainable if 'fc' not in v.name] # lr * 1.0
fc_w_trainable = [v for v in fc_trainable if 'weights' in v.name] # lr * 10.0
fc_b_trainable = [v for v in fc_trainable if 'biases' in v.name] # lr * 20.0


for v in fc_w_trainable:
    print(v)


init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
init_op = tf.group(*init_list)

with utils.get_session() as sess:
    sess.run(init_op)
    train_writer = tf.summary.FileWriter(summary_dir, sess.graph)

