import sys
sys.path.append('./models/slim')

import configargparse

FLAGS = None

def main():
    print('END IS HERE')



if __name__ == '__main__':
    parser = configargparse.ArgParser(default_config_files=['./configs/default.config'])
    parser.add('-c0', '--config-file0', required=False, is_config_file=True, help='config file path')
    parser.add('-c1', '--config-file1', required=False, is_config_file=True, help='config file path')
    parser.add('-c2', '--config-file2', required=False, is_config_file=True, help='config file path')
    parser.add_argument("--batch_size",
                        type=int,
                        default=64,
                        help="Minibatch size fed into network")
    parser.add_argument("--num_examples",
                        type=int,
                        default=1281167,
                        help="Number of examples used in the run")
    parser.add_argument("--directory",
                        type=str,
                        default="/scratch_net/tractor/robertto/imagenet_256/images_train_compress_tfrecord",
                        help="Default directory to get data from")
    parser.add_argument("--summary_dir",
                        type=str,
                        default='./train_summary',
                        help="The directory to save tensorboard summary and checkpoints")
    parser.add_argument("--checkpoint_dir",
                        type=str,
                        help="Directory to load checkpoint from")
    parser.add_argument("--checkpoint_name",
                        type=str,
                        help="""
                        Name of checkpoint to restore. Defaults to latest checkpoint saved in
                        the folder""")
    parser.add_argument('--synsets_filename',
                        type=str,
                        default='input/imagenet_lsvrc_2015_synsets.txt',
                        help="File that includes all synset names")
    parser.add_argument('--extension',
                        type=str,
                        default='tfrecord',
                        help='Extension of the imagefiles. For example png, jpeg, tfrecord, etc')
    parser.add_argument('--learning_rate',
                        type=float,
                        default=0.1,
                        help="""
                        Learning rate of the optimizer. If the learning_rate_type is \'schedule\'
                        this is the initial learning rate""")
    parser.add_argument('--learning_rate_type',
                        type=str,
                        default='constant',
                        help='Type of learning rate, \'constant\' or \'schedule\'')
    parser.add_argument('--label_offset',
                        type=int,
                        default=0,
                        help='Offset of the labels from 0')
    parser.add_argument('--save_checkpoint_steps',
                        type=int,
                        default=2000,
                        help='Checkpoints are saved every save_checkpoint_steps')
    parser.add_argument('--num_classes',
                        type=int,
                        default=1000,
                        help='Checkpoints are saved every save_checkpoint_steps')
    parser.add_argument('--epochs_decay',
                        type=int,
                        default=30,
                        help='After how many epochs the learning rate is reduced by 1/10')
    parser.add_argument('--variables_to_train',
                        type=str,
                        default='all',
                        help="""
                        The variables that are passed to the optimizer and therefore
                        updated. Options: \'all\' and \'finetune\'
                        """)
    parser.add_argument('--variables_to_restore',
                        type=str,
                        default='all',
                        help="""
                        The variables that are loaded from the given checkpoint.
                        Options: \'all\' and \'finetune\'
                        """)

    parser.add_argument('--from_scratch', dest='from_scratch', action='store_true',
                        help="""
                        Flag that ensures that no variables are loaded and model
                        is initialized from scratch""")
    parser.add_argument('--print_variable_origin', dest='print_variable_origin', action='store_true',
                        help="""
                        Flag that ensures that no variables are loaded and model
                        is initialized from scratch""")
    parser.set_defaults(from_scratch=False)

    FLAGS = parser.parse_args()

    
    
    if FLAGS.print_variable_origin:
        left_side_len = 25
        d = vars(FLAGS)
        for key in sorted(d.keys()):
            spaces = ' ' * (left_side_len - len(key))
            print('{}:{}{}'.format(key, spaces, d[key]))
        print("----------")
        print(parser.format_values())    # useful for logging where different settings came from
    else:
        main()
