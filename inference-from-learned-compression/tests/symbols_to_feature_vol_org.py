import os
import argparse
import tensorflow as tf
from PIL import Image
import numpy as np


def convert(symbols_path, centers_path, operating_point):
    corresponding_image_path = symbols_path.replace('_sym.bin', '.png')
    assert os.path.exists(corresponding_image_path), 'Cannot find image at {}'.format(corresponding_image_path)

    spec = Spec.from_image(corresponding_image_path, operating_point)

    symbols = np.fromfile(symbols_path, dtype=np.uint16).reshape((spec.C, -1))
    assert symbols.shape == (spec.C, spec.n), 'Unexpected shape: {} != {}'.format(symbols.shape, (spec.C, spec.n))

    centers = np.load(centers_path)

    with create_session() as sess:
        symbols_tensor = tf.constant(symbols, dtype=tf.int32)
        centers_tensor = tf.constant(centers)
        feature_volume_op = conversion_op(symbols_tensor, centers_tensor, spec)

        feature_volume = sess.run(feature_volume_op)

        print(feature_volume.shape)


def create_session():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # Do not assign whole gpu memory, just use it on the go
    config.allow_soft_placement = True

    return tf.Session(config=config)


class Spec(object):
    """
    A Spec stores meta information about the operating point: See __init__
    """

    # Mapping str -> (C, ph, pw)
    OPERATING_POINTS = {
        '16': (16, 2, 2),
        '32': (32, 2, 2)
    }

    @staticmethod
    def from_image(img_p, operating_point):
        if operating_point not in Spec.OPERATING_POINTS.keys():
            raise ValueError('Invalid operating point {}, must be in {}'.format(operating_point, Spec.OPERATING_POINTS))

        im = Image.open(img_p)
        img_w, img_h = im.size
        num_channels, ph, pw = Spec.OPERATING_POINTS[operating_point]
        return Spec(num_channels, img_w, img_h, ph, pw)

    def __init__(self, num_channels, img_w, img_h, ph, pw):
        """
        C:          number of channels
        n:          number of patches per channel
        w, h:       size of the bottlneck
        pw, ph:     patch size
        d:          dimension of the patch, i.e., pw * ph
        """
        self.C = num_channels
        self.w, self.h = img_w // 8, img_h // 8
        self.n = self.w * self.h // (ph * pw)
        self.ph = ph
        self.pw = pw
        self.d = ph * pw


def conversion_op(symbols_tensor, centers_tensor, spec: Spec):
    assert symbols_tensor.dtype == tf.int32, 'symbols must be int32'
    assert len(symbols_tensor.get_shape()) == 2, 'symbols must be (C, n) dimensional, got {}'.format(
        symbols_tensor.get_shape().as_list())
    assert len(centers_tensor.get_shape()) == 3, 'centers must be (C, d, num_centers)-dimensional, got {}'.format(
        centers_tensor.get_shape().as_list())

    C, n = symbols_tensor.get_shape().as_list()
    C_c, d_c, num_centers = centers_tensor.get_shape().as_list()
    assert n == spec.n, 'Expected n == {}, got {}'.format(spec.n, n)
    assert C == C_c, 'Number of channels of symbols and centers must match, {} != {}'.format(C, C_c)
    assert d_c == spec.d, 'Expected d == {}, got {}'.format(spec.d, d_c)

    # convert symbols tensor to (C, n, d)-dimensional patches tensor, where the i-th patch of the c-th channel is
    # given by patches[c, i, :] \in R^d
    patches = _symbols_to_patches(symbols_tensor, centers_tensor, num_centers, spec)

    # convert to feature volume by rearanging the patches to a (1, w, h, C) tensor
    feature_volume = _inverse_of_extract_patches(patches, spec)

    return feature_volume


def _symbols_to_patches(symbols, centers, num_centers, spec: Spec):
    # symbols is (C, n), each element is in {0, ..., num_centers-1}
    # we add to each row symbols[c, :] the integer c * num_centers, to make a unique index into
    # the centers matrix, because the centers matrix is (C, d, num_centers), one center is given by
    # centers[chan, :, l], so given some index i = (p + c * num_centers), p in {0, ..., num_centers-1},
    # c in {0, ..., C}, there is a unique center given by chan = i // num_centers, l = i % num_centers = p
    indices = symbols + tf.expand_dims(tf.range(spec.C) * num_centers, axis=1)

    # we reshape these unique indices to a (C*n, 1) matrix
    indices = tf.reshape(indices, (-1, spec.n))

    # transposing the centers results in it becoming of shape (C, num_centers, d), so...
    centers_t = tf.transpose(centers, perm=(0, 2, 1))
    # ... we can reshape to have a (C*num_centers, d) matrix, where row i is the center (i % num_centers) of
    # channel i // num_centers
    centers_ = tf.reshape(centers_t, (-1, spec.d))

    # Thus, we can pick from this centers matrix the centers we want
    hard_out = tf.gather(centers_, indices)

    hard_out_reshaped = tf.reshape(hard_out, (spec.C, spec.n, spec.d))

    return hard_out_reshaped


def _inverse_of_extract_patches(patches_tensor, spec: Spec):
    """
    This returns an op that is the inverse of the following:

    # Note: with this setup, patches of one image are stored as consecutive rows
    # in the patches array of one channel, i.e., consecutive rows of out[c, :, :]
    patches = tf.reshape(
        tf.transpose(

            # reshape such that the patches reside in dimensions 2 and 4.
            # I.e. the (i, j)-th patch of batch b and channel c is at [b, i, :, j, :, c]
            tf.reshape(
                feature_volume,
                #      0           1        2   3        4   5
                shape=(batch_size, h // ph, ph, w // pw, pw, C)),

            # permute dimensions such that the patch dimensions 2 and 4 are at the end,
            # and channel dimension is at the beginning, i.e.
            # has shape (C, bs, h // ph, w // pw, ph, pw)
            # i.e., permutation:
            #  0 -> 5, 1 -> 0, 2 -> 1, 3 -> 3, 4 -> 2, 5 -> 4
            perm=(5, 0, 1, 3, 2, 4)),

        # merge the patch dimensions (which are now at the end).
        # Results in a 3D matrix (c, batch_size * (h / ph) * (w / pw), pw * ph)
        shape=(C, -1, pw * ph)
    )
    """
    assert patches_tensor.get_shape() == (spec.C, spec.n, spec.d), \
        'Cannot merge patches of tensor of shape {}'.format(patches_tensor.get_shape().as_list())

    batch_size = 1  # we only have batches of one image here

    return tf.reshape(
        tf.transpose(
            tf.reshape(
                patches_tensor,
                # undo reshape(…, (-1, pw * ph))
                shape=(spec.C, batch_size, spec.h // spec.ph, spec.w // spec.pw, spec.ph, spec.pw)),
            # permute it back to shape
            # (bs, h // ph, ph, w // pw, pw, C)
            # i.e., inverse of above permutation, i.e.,
            #  0 <- 5, 1 <- 0, 2 <- 1, 3 <- 3, 4 <- 2, 5 <- 4
            perm=(1, 2, 4, 3, 5, 0)),
        shape=(batch_size, spec.h, spec.w, spec.C)
    )


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--symbols_path', type=str, help='Path to a binary file containing the uint16-encoded symbols')
    parser.add_argument('--centers_path', type=str, help='Path to .npy file containing the centers matrix')
    parser.add_argument('--operating_point', type=str, choices=Spec.OPERATING_POINTS.keys())
    flags = parser.parse_args()
    convert(flags.symbols_path, flags.centers_path, flags.operating_point)
