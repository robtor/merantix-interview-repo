import tensorflow as tf
import imgcompress_arch_dec as imgdec
import numpy as np
import PIL
import utils
import glob

def _parse_proto_example(serialized_example, input_type='symbol'):
    features = tf.parse_single_example(
        serialized_example,
        features={
            'image/encoded': tf.FixedLenFeature([], tf.string),
            'image/class/label': tf.FixedLenFeature([], tf.int64),
            'image/width': tf.FixedLenFeature([], tf.int64),
            'image/height': tf.FixedLenFeature([], tf.int64),
            'image/channels': tf.FixedLenFeature([], tf.int64),
            'image/filename': tf.FixedLenFeature([], tf.string)
        })

    if input_type == 'symbol':
        symbols_tensor = tf.decode_raw(features['image/encoded'], tf.int16)
        symbols_tensor = tf.cast(symbols_tensor, tf.int32)
    elif input_type == 'feature_volume':
        symbols_tensor = tf.decode_raw(features['image/encoded'], tf.float32)
    else:
        raise ValueError('Unknown input_type {}'.format(input_type))
    
    label = features['image/class/label']

    height = features['image/height']
    width = features['image/width']

    name = features['image/filename']
    #channels = features['image/channels']

    #shape = tf.convert_to_tensor([height, width, channels])
    shape = tf.convert_to_tensor([height, width])
    shape = tf.cast(shape, tf.int32)

    return symbols_tensor, label, shape, name

record_filename = glob.glob('/home/robertto/glusterfs_robertto/imagenet/imagenet_256/images_train_256_bottleneck-0808_tfrecord/train-00000-of-01024.tfrecord')

filename_queue = tf.train.string_input_producer(record_filename,
                                                num_epochs=None,
                                                shuffle=False)

reader = tf.TFRecordReader()
_, serialized_example = reader.read(filename_queue)
symbols_tensor, label, shape, name  = _parse_proto_example(serialized_example,
                                                    input_type='symbol')

# Paths
config_name = 'shared_centers_c12'
#config_name = 'shared_centers_c12_ss4deep'
centers_path, ckpt_path, symbols_train_root_dir, symbols_val_root_dir = imgdec.get_paths(config_name)
#
# Get bottleneck
centers = np.load(centers_path)  # (L,) matrix, where L is number of centers
centers_tensor = tf.convert_to_tensor(centers, dtype=tf.float32)
#
d = utils.create_mapping('/home/robertto/masters_thesis/input/filename_to_shape_map_train_0808.txt')
symbols_path = '/srv/glusterfs/mentzerf/scalar/roberto_train_0808_1054/n02807133/n02807133_1446_sym.bin'
#
h, w = d['n02807133_1446']
#
symbols_raw = np.fromfile(symbols_path, dtype=np.int16)
symbols_raw = symbols_raw.reshape((h // 4, w // 4, 8))

symbols_tensor = tf.reshape(symbols_tensor, shape=(shape[0] // 4, shape[1] // 4, 8))
symbols_tensor = tf.reshape(symbols_tensor, shape=(8, shape[0] // 4, shape[1] // 4))
symbols_tensor = tf.gather(centers_tensor, symbols_tensor)
#
#print(symbols)
#print(symbols.shape)
#print(np.max(symbols))
#print(final)
#print(final.shape)


BATCH_SIZE = 64

init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
init_op = tf.group(*init_list)

with utils.get_session() as sess:
    sess.run(init_op)

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    for i in range(1):
        feat_out, label_out, name_out = sess.run([symbols_tensor, label, name])

        #feat_out[0,0,0] = 20
        print(feat_out.shape)
        #print(label_out)
        #print(name_out)
        #print(symbols_raw.shape)
        print(feat_out[5,:,10])

        print(np.all(feat_out == symbols_raw))

    coord.request_stop()
    coord.join(threads)

#symbols = ... # get symbols volume, from your input pipeline or sth
#bottleneck = tf.gather(centers, symbols)  # same dims as `symbols`
