import utils
from utils import parsers_definitions

parser = parsers_definitions.get_parser_train()
FLAGS = parser.parse_args()

# summary_dir: 'string'
# summary_basedir: None
FLAGS.summary_dir = '/home/robertto/masters_thesis/summaries/myrun'

x0 = utils.get_summary_dir(FLAGS)

# summary_dir: 'string'
# summary_basedir: 'string'
FLAGS.summary_basedir = '/home/robertto/masters_thesis/summaries'

x1 = utils.get_summary_dir(FLAGS)

# summary_dir: None
# summary_basedir: 'string'
FLAGS.summary_dir = None

x2 = utils.get_summary_dir(FLAGS)

FLAGS.epochs_decay = 38
FLAGS.num_classes = 200
x3 = utils.get_summary_dir(FLAGS)

print('0:')
print(x0)
print('1:')
print(x1)
print('2:')
print(x2)
print('3:')
print(x3)

FLAGS.num_classes = 150
x4 = utils.get_summary_dir(FLAGS)

# summary_dir: None
# summary_basedir: None
FLAGS.summary_dir = None
FLAGS.summary_basedir = None

x5 = utils.get_summary_dir(FLAGS)
