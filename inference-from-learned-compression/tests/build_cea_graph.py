import sys
sys.path.append('./models/slim')

import tensorflow as tf

import utils

import tensorflow.contrib.slim as slim

from datetime import datetime
from models.slim.nets import resnet_v1

from networks import cae_network_decoder
from networks.resnet_blocks_mapping import networks_map



def main():
    summary_dir = './summary_cae_network_decoder'
    checkpoint_path_dec = './decoder_ckpt/ckpt-996314'
    checkpoint_path_res = './decoder_ckpt/model.ckpt-1900000'

    #for var_name, _ in tf.contrib.framework.list_variables(checkpoint_path):
    #    print(var_name)

    map_from = 'decoder'
    map_to = 'printemps/cae_net/decoder'

    processed_images = tf.placeholder(dtype=tf.float32, shape=[64, 28, 28, 32])

    net = cae_network_decoder.decoder(processed_images)


    variables_to_restore_dec = cae_network_decoder.get_weights() + cae_network_decoder.get_biases()
    variables_to_restore_dec = {utils.map_variable_names(var, map_from=map_from, map_to=map_to):var for var in variables_to_restore_dec}

    saver_dec = tf.train.Saver(var_list=variables_to_restore_dec)

    network_name = 'resnet-50'
    blocks = networks_map[network_name]

    with slim.arg_scope(resnet_v1.resnet_arg_scope()):
        logits, end_points = resnet_v1.resnet_v1(net,
                                                 blocks,
                                                 1000,
                                                 is_training=True,
                                                 global_pool=True,
                                                 output_stride=None,
                                                 include_root_block=True,
                                                 reuse=None,
                                                 scope='resnet',
                                                 data_format='NHWC')

    variables_to_restore_res = slim.get_model_variables(scope='resnet')

    for var in tf.trainable_variables():
        print(var)

    saver_res = tf.train.Saver(var_list=variables_to_restore_res)

    init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
    init_op = tf.group(*init_list)


    with utils.get_session() as sess:
        sess.run(init_op)

        saver_dec.restore(sess, checkpoint_path_dec)
        saver_res.restore(sess, checkpoint_path_res)

        train_writer = tf.summary.FileWriter(summary_dir, sess.graph)
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        try:
            print('%s: starting evaluation on (%s).' % (datetime.now(), 'training'))
            step = 0

            #while True and not coord.should_stop():
            #        _, total_loss_out, summary, _, _, global_step = sess.run([train_op, total_loss, merged, top_1_op, top_5_op, slim.get_global_step()])
            #    step += 1


        except Exception as e:  # pylint: disable=broad-except
            coord.request_stop(e)

        coord.request_stop()
        coord.join(threads, stop_grace_period_secs=10)


if __name__ == '__main__':
    main()

