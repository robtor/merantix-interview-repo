import tensorflow as tf
import tensorflow.contrib.slim as slim

import utils
from utils import image_processing, parsers_definitions


def main(FLAGS):
    config = image_processing.get_image_processing_config_from_flags(True, FLAGS)
    config.print_preprocessing()
    images_batch, labels_batch = image_processing.get_images(FLAGS.directory,
                                                             FLAGS.batch_size,
                                                             num_preprocess_threads=FLAGS.num_preprocess_threads,
                                                             config=config)

    with utils.get_session() as sess:
        with slim.queues.QueueRunners(sess):
            while True:
                try:
                    images_out, labels_out = sess.run([images_batch, labels_batch])
                    print(images_out.shape)
                    print(labels_batch.shape)
                except tf.errors.OutOfRangeError:
                    break


if __name__ == '__main__':
    parser = parsers_definitions.get_parser_train()
    FLAGS = parser.parse_args()
    main(FLAGS)
