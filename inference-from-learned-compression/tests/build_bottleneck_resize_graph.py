import sys
sys.path.append('./models/slim')

import tensorflow as tf

import utils

import tensorflow.contrib.slim as slim

from datetime import datetime
from models.slim.nets import resnet_v1
from networks.resnet_blocks_mapping import networks_map


def main():
    blocks = networks_map['chopped-48']
    summary_dir = './summary_bottleneck_resize_graph'

    processed_images = tf.placeholder(dtype=tf.float32, shape=[64, 32, 28, 28])

    with slim.arg_scope(resnet_v1.resnet_arg_scope()):
        logits, end_points = resnet_v1.resnet_v1(processed_images,
                                                 blocks,
                                                 1000,
                                                 is_training=True,
                                                 global_pool=True,
                                                 output_stride=None,
                                                 include_root_block=False,
                                                 reuse=None,
                                                 scope='resnet',
                                                 data_format='NCHW')

    init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
    init_op = tf.group(*init_list)

    with utils.get_session() as sess:
        sess.run(init_op)

        train_writer = tf.summary.FileWriter(summary_dir, sess.graph)

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        try:
            print('%s: starting evaluation on (%s).' % (datetime.now(), 'training'))
            step = 0

            #while True and not coord.should_stop():
            #        _, total_loss_out, summary, _, _, global_step = sess.run([train_op, total_loss, merged, top_1_op, top_5_op, slim.get_global_step()])
            #    step += 1


        except Exception as e:  # pylint: disable=broad-except
            coord.request_stop(e)

        coord.request_stop()
        coord.join(threads, stop_grace_period_secs=10)


if __name__ == '__main__':
    main()

