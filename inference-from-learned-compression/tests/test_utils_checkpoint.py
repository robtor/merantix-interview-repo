import utils

summary_dir = '/home/robertto/masters_thesis/summaries/summary_original_200_scratch_0.0125'
checkpoint_dir = '/home/robertto/masters_thesis/summaries/summary_original_200_scratch_0.025'


x0 = utils.get_checkpoint_path(None, None, summary_dir)
x1 = utils.get_checkpoint_path(checkpoint_dir, None, summary_dir)
x2 = utils.get_checkpoint_path(None, 'model.ckpt-10000', summary_dir)
x3 = utils.get_checkpoint_path(checkpoint_dir, 'mymodel.ckpt', summary_dir)

print('0:')
print(x0)
print('1:')
print(x1)
print('2:')
print(x2)
print('3:')
print(x3)
