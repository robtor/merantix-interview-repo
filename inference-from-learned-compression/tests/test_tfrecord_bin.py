import utils
import tensorflow as tf
import numpy as np

def _parse_proto_example(serialized_example):
    features = tf.parse_single_example(
        serialized_example,
        features={
            'image/encoded': tf.FixedLenFeature([], tf.string),
            'image/class/label': tf.FixedLenFeature([], tf.int64),
            'image/width': tf.FixedLenFeature([], tf.int64),
            'image/height': tf.FixedLenFeature([], tf.int64),
            'image/channels': tf.FixedLenFeature([], tf.int64),
            'image/filename': tf.FixedLenFeature([], tf.string)
        })

    image_decoded = tf.decode_raw(features['image/encoded'],
                                         tf.int16)
    label_index = features['image/class/label']

    height = features['image/height']
    width = features['image/width']
    channels = features['image/channels']

    f_name = features['image/filename']

    return image_decoded, label_index, height, width, channels, f_name


data_dir = '/scratch/robertto/imagenet_256/tmp_bin'
extension = 'tfrecord'
synsets_filename = ''
dataset = utils.ImagenetData(data_dir, synsets_filename, extension=extension)

filename_queue = tf.train.string_input_producer(dataset.get_file_list(),
                                                num_epochs=None,
                                                shuffle=False)



reader = tf.TFRecordReader()
_, serialized_example = reader.read(filename_queue)
image_decoded, label_index, height, width, channels, f_name = _parse_proto_example(serialized_example)

dims = tf.convert_to_tensor([channels, height, width])
dims = tf.cast(dims, tf.int32)


shape_full = tf.convert_to_tensor([32, dims[1] // (2 * 8), dims[2] // (2 * 8)])
#shape_full = tf.convert_to_tensor([32, dims[0] // (2 * 8), dims[1] // (2 * 8)])
#symbols_tensor_reshape_first = tf.reshape(image_decoded, shape=[32, 16, 22])
symbols_tensor_reshape_first = tf.reshape(image_decoded, shape=shape_full)

#print(shape_full)
#print(symbols_tensor_reshape_first)

with tf.Session() as sess:
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    img, lab, dims_out, f_name_out, symb_out = sess.run([image_decoded, label_index, dims, f_name, symbols_tensor_reshape_first])

    x = np.fromfile('/srv/glusterfs/mentzerf/roberto_train/n07873807/n07873807_14015_sym.bin', dtype=np.uint16)
    print(img)
    print(np.sum(img - x))
    print(img.shape)
    print(lab)
    print(dims_out)
    print(f_name_out)
    #print(symb_out)
    #print(symb_out.shape)
    #print(shape_full_out)
#x =np.fromfile('n07873807_14015_sym.bin', dtype=np.uint16)
    #try:
    #    while True and not coord.should_stop():


    #except Exception as e:  # pylint: disable=broad-except
    #    coord.request_stop(e)

    coord.request_stop()
    coord.join(threads, stop_grace_period_secs=10)
