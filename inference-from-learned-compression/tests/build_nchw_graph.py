import sys
sys.path.append('./models/slim')

import tensorflow as tf

import utils
from networks import resnet_model

from datetime import datetime


def main():
    summary_dir = './summary_nchw_graph'

    processed_images = tf.placeholder(dtype=tf.float32, shape=[32, 3, 224, 224])

    logits, end_points = resnet_model.inference(processed_images,
                                                num_classes=1000,
                                                is_training=True)

    init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
    init_op = tf.group(*init_list)

    with utils.get_session() as sess:
        sess.run(init_op)

        train_writer = tf.summary.FileWriter(summary_dir, sess.graph)

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        try:
            print('%s: starting evaluation on (%s).' % (datetime.now(), 'training'))
            step = 0

            #while True and not coord.should_stop():
            #        _, total_loss_out, summary, _, _, global_step = sess.run([train_op, total_loss, merged, top_1_op, top_5_op, slim.get_global_step()])
            #    step += 1


        except Exception as e:  # pylint: disable=broad-except
            coord.request_stop(e)

        coord.request_stop()
        coord.join(threads, stop_grace_period_secs=10)


if __name__ == '__main__':
    main()

