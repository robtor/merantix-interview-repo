import tensorflow as tf

checkpoint_paths = ['/home/robertto/masters_thesis/summaries/test_delete_bottleneck_stuff/model_bn-1800000.ckpt-201']
checkpoint_paths.append('/home/robertto/glusterfs_summaries/summary_bottleneck_1000_chopped-1_lr-0.025_bs-64_gpu-1_1x/model.ckpt-1800000')

var_list_list = []
for checkpoint_path in checkpoint_paths:
    var_list_list.append({var_name for var_name, _ in tf.contrib.framework.list_variables(checkpoint_path)})

for s in var_list_list[0].intersection(var_list_list[1]):
    print(s)

for s in var_list_list[1].difference(var_list_list[0]):
    print(s)


print(len(var_list_list[0].intersection(var_list_list[1])))
print(len(var_list_list[1].difference(var_list_list[0])))

print(len(var_list_list[0]))
print(len(var_list_list[1]))

count = 0
for s in var_list_list[0]:
    if ('moving_mean' in s) or ('moving_variance' in s):
        count += 1
print(count)

count = 0

for s in var_list_list[1]:
    if ('moving_mean' in s) or ('moving_variance' in s):
        count += 1
print(count)
