import sys
sys.path.append('./vidcompress')

import tensorflow as tf
import utils
from networks import nets_class

filepath = '/srv/glusterfs/mentzerf/out_tb_pushpsnr/1209_0941 pp:shared_centers_c12_nips'

network_class = nets_class.get_network_by_name('joint-network')
network = network_class(num_classes=1000,
                        is_training=False,
                        data_format='NCHW',
                        model_scope='resnet',
                        weight_decay=0.0001,
                        batch_norm_decay=0.9,
                        input_network_log_dir=filepath)

# inputs = tf.placeholder(tf.float32, (64, 3, 224, 224))
# inputs = tf.placeholder(tf.float32, (64, 3, 256, 256))
# inputs = tf.placeholder(tf.float32, (64, 32, 28, 28))
inputs = tf.placeholder(tf.float32, (64, 3, 256, 256))

net = network.build(inputs, reuse=None)
#net = net + 1

with utils.get_session() as sess:
    train_writer = tf.summary.FileWriter('output_test_network_class', sess.graph)
