import tensorflow as tf

import tensorflow.contrib.slim as slim

from models.slim.nets import resnet_v1
from models.slim.nets import resnet_utils

def get_blocks():
    """
    blocks = [
        resnet_utils.Block(
            'block1', resnet_v1.bottleneck, [(256, 64, 1)] * 2 + [(256, 64, 2)]),
        resnet_utils.Block(
            'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 3 + [(512, 128, 2)]),
        resnet_utils.Block(
            'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 5 + [(1024, 256, 2)]),
        resnet_utils.Block(
            'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
    ]
    """

    blocks = [
        resnet_utils.Block(
            'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 3 + [(512, 128, 2)]),
        resnet_utils.Block(
            'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 5 + [(1024, 256, 2)]),
        resnet_utils.Block(
            'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
    ]

    return blocks

def main():

    input_tensor = tf.placeholder(tf.float32, shape=[1, 28, 28, 32])
    #input_tensor = tf.placeholder(tf.float32, shape=[1, 224, 224, 3])
    num_classes = 1000
    is_training = True

    with slim.arg_scope(resnet_v1.resnet_arg_scope()):
        # original version
        #logits, end_points = resnet_v1.resnet_v1_50(input_tensor, num_classes=num_classes, is_training=is_training)

        blocks = get_blocks()
        logits, end_points = resnet_v1.resnet_v1(input_tensor, blocks, num_classes, is_training,
                                                global_pool=True, output_stride=None,
                                                include_root_block=False, reuse=None, scope='resnet_v1_50_chop')


    with tf.Session() as sess:
        writer = tf.summary.FileWriter('./resnet_graph_build', sess.graph)

if __name__ == '__main__':
    main()