#def decoder(quantized_features_vol):
#    with tf.variable_scope(DECODER_VARIABLE_SCOPE):
#        c = 32
#        h8 = deconv_layer_from(quantized_features_vol, 'SAME',
#                               shape=3, in_c=c, out_c=128, upscale=2, nl=None, name='h8')
#        h9 = residual_block(h8, 'dec1')
#        h10 = residual_block(h9, 'dec2')
#        h11 = residual_block(h10, 'dec3')
#        h12 = deconv_layer_from(h11, 'SAME', shape=3, in_c=128, out_c=64, upscale=2, nl=lrelu, name='h12')
#        h13 = deconv_layer_from(h12, 'SAME', shape=3, in_c=64, out_c=3, upscale=2, nl=None, name='h13')
#        h13 = denormalize(h13)
#        h_13_clipped = forced_clipper(h13, 0., 255.)
#        return h_13_clipped
#
#def residual_block(h, name, nl_for_next_conv=None):
#    name = 'residual_' + name
#    h_into_hr_1 = h if nl_for_next_conv is None else nl_for_next_conv(h)
#    hr_1 = conv_layer_from(h_into_hr_1, 'VALID',
#                           shape=3, in_c=128, out_c=128, stride=1, nl=lrelu, name=name + '_hr1')
#    hr_2 = conv_layer_from(hr_1, 'VALID',
#                           shape=3, in_c=128, out_c=128, stride=1, nl=None, name=name + '_hr2')
#    # bring hr_2 to the same dimension as h via padding, so that addition is defined.
#    # Not sure if this is something that we want.
#    hr_2_pad = tf.pad(hr_2, [[0, 0], [2, 2], [2, 2], [0, 0]], mode='REFLECT', name=name+'_hr2_pad')
#    return h + hr_2_pad

# Format: kernel-w * kernel-h * depth-in * height-in * width-in * num_out_channels
c = 32
h_in = 28
w_in = 28

num_out_channels = 128

h8 = 3 * 3 * c * h_in * w_in * num_out_channels
h9 = 3 * 3 * num_out_channels * (2 * h_in) * (2 * w_in) * num_out_channels * 2
h10 = 3 * 3 * num_out_channels * (2 * h_in) * (2 * w_in) * num_out_channels * 2
h11 = 3 * 3 * num_out_channels * (2 * h_in) * (2 * w_in) * num_out_channels * 2
h12 = 3 * 3 * num_out_channels * (2 * h_in) * (2 * w_in) * 64
h13 = 3 * 3 * 64 * (4 * h_in) * (4 * w_in) * 3

result = h8 + h9 + h10 + h11 + h12 + h13

print(result / 10**9)
#print(h9 / 10**9)

#print(3 * 3 * 64 * 56 * 56 * 64 * 2 / 10**9)



#def sub_sample_4_dec(bottleneck, config, reuse=False):
#    with tf.variable_scope(NETWORK_SCOPE_NAME, reuse=reuse):
#        with slim.arg_scope([slim.conv2d, slim.conv2d_transpose],
#                            weights_regularizer=slim.l2_regularizer(config.regularization_factor)):
#            net = slim.conv2d_transpose(bottleneck, 24, [5, 5], stride=2, scope='upscale2_1')
#            net = slim.conv2d_transpose(net, 24, [5, 5], stride=2, scope='upscale2_2')
#            net = slim.conv2d(net, 24, [5, 5], stride=1, scope='conv5')
#            net = slim.conv2d(net, 3, [5, 5], stride=1, activation_fn=None, scope='conv6')
#            net = denormalize(net, config.normalization)
#            net = clip_to_image_range(net)
#            return net


input_size = 64
h0 = 5 * 5 * 8 * input_size * input_size * 24
h1 = 5 * 5 * 24 * (2*input_size) * (2*input_size) * 24
h2 = 5 * 5 * 24 * (4*input_size) * (4*input_size) * 24
h3 = 5 * 5 * 24 * (4*input_size) * (4*input_size) * 3

result = h0 + h1 + h2 + h3
print(result / 10**9)



#def sub_sample_4_deep_dec(bottleneck, config, reuse=False):
#    with tf.variable_scope(NETWORK_SCOPE_NAME, reuse=reuse):
#        with slim.arg_scope([slim.conv2d, slim.conv2d_transpose],
#                            weights_regularizer=slim.l2_regularizer(config.regularization_factor)):
#            net = residual_block(bottleneck, num_conv2d=2, kernel_size=[5, 5], scope='res3')
#            net = slim.conv2d_transpose(net, 24, [5, 5], stride=2, scope='upscale2_1')
#            net = residual_block(net, num_conv2d=2, kernel_size=[5, 5], scope='res4')
#            net = slim.conv2d_transpose(net, 24, [5, 5], stride=2, scope='upscale2_2')
#            net = slim.conv2d(net, 24, [5, 5], stride=1, scope='conv5')
#            net = slim.conv2d(net, 3, [5, 5], stride=1, activation_fn=None, scope='conv6')
#            net = denormalize(net, config.normalization)
#            net = clip_to_image_range(net)
#            return net

input_size = 56


h0 = 5 * 5 * 8 * input_size * input_size * 8 * 2
h1 = 5 * 5 * 8 * input_size * input_size * 24
h2 = 5 * 5 * 24 * (2*input_size) * (2*input_size) * 24 * 2
h3 = 5 * 5 * 24 * (2*input_size) * (2*input_size) * 24
h4 = 5 * 5 * 24 * (4*input_size) * (4*input_size) * 24
h5 = 5 * 5 * 24 * (4*input_size) * (4*input_size) * 3

result = h0 + h1 + h2 + h3 + h4 + h5
print(result / 10**9)
