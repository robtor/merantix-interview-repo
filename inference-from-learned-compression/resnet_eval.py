import math
import os
import sys
import time

sys.path.append('./vidcompress')

import tensorflow as tf
import utils.utils_general as utils_general
from utils import image_processing, read_bin_files, parsers_definitions, metrics_wrapper

from datetime import datetime

from networks import nets_class, nets_loader, resnet_model

try:
    os.environ["CUDA_VISIBLE_DEVICES"] = os.environ['SGE_GPU']
except KeyError:
    print('Getting gpu number manually')
    os.environ["CUDA_VISIBLE_DEVICES"] = utils_general.get_gpu_number()


def main(FLAGS):
    # TODO Load the model in the same way as in resnet_train
    num_classes_in_file = utils_general.get_num_classes(FLAGS.synsets_filename)

    # this assertion only makes sense when loading from png/jpeg files
    # not tfrecords, but still a nice sanity check
    assert num_classes_in_file == FLAGS.num_classes, 'Number of classes given ({}) must match the number of synsets in {}'.format(
        FLAGS.num_classes, FLAGS.synsets_filename)

    checkpoint_path = utils_general.get_checkpoint_path(FLAGS.checkpoint_dir,
                                                FLAGS.checkpoint_name)
    model_scope = utils_general.get_checkpoint_scope(checkpoint_path)

    if ('image' in FLAGS.model_type) or ('compressed' in FLAGS.model_type):
        config = image_processing.get_image_processing_config_from_flags(False, FLAGS)
        config.print_preprocessing()

        processed_symbols, labels = image_processing.get_images(FLAGS.directory,
                                                                FLAGS.batch_size,
                                                                num_preprocess_threads=1,
                                                                config=config)
    elif 'bottleneck' in FLAGS.model_type:
        processed_symbols, labels = read_bin_files.get_symbols(FLAGS.directory,
                                                               FLAGS.synsets_filename,
                                                               FLAGS.map_name_to_dim_filename,
                                                               FLAGS.centers_filename,
                                                               operating_point=FLAGS.operating_point,
                                                               batch_size=FLAGS.batch_size,
                                                               extension=FLAGS.extension,
                                                               is_training=False,
                                                               input_type=FLAGS.input_type,
                                                               feat_vol_input_size=FLAGS.feat_vol_input_size)

    else:
        raise ValueError('unknown model type {}'.format(FLAGS.model_type))

    network_class = nets_class.get_network_by_name(FLAGS.network_name)

    network = network_class(num_classes=1000,
                            is_training=False,
                            model_scope=model_scope,
                            data_format=FLAGS.data_format,
                            input_network_log_dir=FLAGS.encoder_decoder_dir,
                            network_name=FLAGS.network_name)
    output_dict = network.build(processed_symbols, reuse=None)
    logits = output_dict['logits']

    labels = tf.subtract(labels, FLAGS.label_offset)

    num_classes_in_file = utils_general.get_num_classes(FLAGS.synsets_filename)

    # this assertion only makes sense when loading from png/jpeg files
    # not tfrecords, but still a nice sanity check
    assert num_classes_in_file == FLAGS.num_classes, 'Number of classes given ({}) must match the number of synsets ' \
                                                     'in {}'.format(FLAGS.num_classes, FLAGS.synsets_filename)

    top_1_op, top_5_op = resnet_model.get_top_k(logits, labels)
    predicted_label = tf.argmax(logits, axis=1)

    (_, acc_per_class), acc_per_class_update_op = metrics_wrapper.mean_per_class_accuracy(labels,
                                                                                          predicted_label,
                                                                                          num_classes=FLAGS.num_classes)

    print('loading from {}'.format(checkpoint_path))

    if FLAGS.load_from_encoder_decoder_dir:
        checkpoint_dir_encoder_decoder = os.path.join(FLAGS.encoder_decoder_dir, 'ckpts')

        if FLAGS.encoder_decoder_checkpoint_name:
            checkpoint_path_encoder_decoder = os.path.join(checkpoint_dir_encoder_decoder,
                                                           FLAGS.encoder_decoder_checkpoint_name)
        else:
            checkpoint_path_encoder_decoder = tf.train.latest_checkpoint(checkpoint_dir_encoder_decoder)
        assert checkpoint_path_encoder_decoder, 'Found no checkpoint in folder, is the folder correct or did you ' \
                                                'forget to mount the folder? '
    else:
        checkpoint_path_encoder_decoder = checkpoint_path

    checkpoint_paths_dict = {'output': checkpoint_path,
                             'input': checkpoint_path_encoder_decoder}

    init_fn = nets_loader.get_init_fn(checkpoint_paths_dict,
                                      network.get_variables_to_restore())

    with utils_general.get_session() as sess:
        init_fn(sess)
        sess.run(tf.local_variables_initializer())

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        try:
            num_iter = int(math.ceil(FLAGS.num_examples / FLAGS.batch_size))
            # Counts the number of correct predictions.
            count_top_1 = 0.0
            count_top_5 = 0.0
            total_sample_count = num_iter * FLAGS.batch_size
            step = 0

            print('%s: starting evaluation on (%s).' % (datetime.now(), 'validation'))

            start_time = time.time()
            while step < num_iter and not coord.should_stop():
                top_1, top_5, _ = sess.run([top_1_op, top_5_op, acc_per_class_update_op])

                count_top_1 += top_1
                count_top_5 += top_5
                step += 1

                print_per_steps = 20

                if step % print_per_steps == 0:
                    duration = time.time() - start_time
                    sec_per_batch = duration / print_per_steps
                    examples_per_sec = FLAGS.batch_size / sec_per_batch
                    print('%s: [%d batches out of %d] (%.1f examples/sec; %.3f'
                          'sec/batch)' % (datetime.now(), step, num_iter,
                                          examples_per_sec, sec_per_batch))
                    start_time = time.time()

            # Compute precision @ 1.
            precision_at_1 = count_top_1 / num_iter
            recall_at_5 = count_top_5 / num_iter

            print('%s: precision @ 1 = %.4f recall @ 5 = %.4f [%d examples]' %
                  (datetime.now(), precision_at_1, recall_at_5, total_sample_count))
            output_str = '%s,%.4f,%.4f,%d' % (os.path.basename(checkpoint_path), precision_at_1, recall_at_5, total_sample_count)

            acc_per_class_out = acc_per_class.eval(session=sess)

            with open(FLAGS.output_filename, 'a') as f:
                f.write(output_str + '\n')

            if FLAGS.save_per_class_accuracy:
                with open(utils_general.append_to_filename(FLAGS.output_filename, suffix='1000_classes'), 'a') as f:
                    acc_per_class_str = ','.join(['%.5f' % num for num in acc_per_class_out])
                    output_str_per_class = os.path.basename(checkpoint_path) + ',' + acc_per_class_str
                    f.write(output_str_per_class + '\n')

        except Exception as e:  # pylint: disable=broad-except
            coord.request_stop(e)

        coord.request_stop()
        coord.join(threads, stop_grace_period_secs=10)


if __name__ == '__main__':
    parser = parsers_definitions.get_parser_validation()
    FLAGS = parser.parse_args()

    if FLAGS.print_variable_origin:
        utils_general.print_variable_values_and_origin(FLAGS, parser)
    else:
        main(FLAGS)

