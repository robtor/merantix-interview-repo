import glob
import os

import configargparse
import tensorflow as tf

import vidcompress.imgcompress_val as imgcompress_val
from utils import utils_run_multiple as utils

ENCODER_DECODER_BASEDIR = '/srv/glusterfs/robertto/imagenet/compression_runs'
VALIDATION_DIR = '/srv/glusterfs/mentzerf/imagenet_256_train_val_128x128'
VAL_DATASET_NAME = 'roberto_val'

ENC_DEC_PATH_MAP = utils.get_enc_dec_map(ENCODER_DECODER_BASEDIR)


def main(FLAGS):
    dir_names = glob.glob(os.path.join(FLAGS.base_dir, FLAGS.pattern))
    dir_names.sort()

    if FLAGS.print_dirs:
        utils.print_dir_names(dir_names)
        return 0

    for dir_name in dir_names:
        print(dir_name)
        folder_name = os.path.basename(os.path.normpath(dir_name))
        output_filename = os.path.join(FLAGS.output_dir, folder_name + '_val.txt')

        models_finished = utils.parse_output_filename(output_filename)
        models = utils.get_models_list(dir_name,
                                       models_finished,
                                       model_basename=FLAGS.model_basename,
                                       start_point=FLAGS.start_point,
                                       end_point=FLAGS.end_point,
                                       steps=FLAGS.steps)

        key = utils.get_enc_dec_mapping_number('key-', folder_name, default_val=None)
        enc_dec_dir = ENC_DEC_PATH_MAP[key]

        for model in models:
            print(os.path.join(dir_name, model))
            parser = imgcompress_val.get_parser()
            flags_eval = parser.parse_args(['--ckpt-path', os.path.join(dir_name, model),
                                            '--encoder-decoder-dir', enc_dec_dir,
                                            '--val-images-dir', VALIDATION_DIR,
                                            '--val-dataset-name', VAL_DATASET_NAME,
                                            '--output-filename', output_filename])

            # Exception handling here so that a tensorflow error doesn't kill the whole program
            try:
                # Also possible to use a context manager
                # with tf.Graph().as_default():
                tf.reset_default_graph()
                imgcompress_val.main(flags_eval)
            except Exception as e:
                print('A tensorflow exception happened at {}'.format(os.path.join(dir_name, model)))
                print(e)


if __name__ == '__main__':
    parser = configargparse.ArgumentParser()

    parser.add_argument("--base_dir",
                        type=str,
                        required=True,
                        help='Dir that has the validation runs. base_dir + pattern is globbed')
    parser.add_argument("--pattern",
                        type=str,
                        default='*',
                        help='Folder pattern to use. base_dir + pattern is globbed. Note that when using wildcard matching with * from the command-line, the argument must be surrounded by \', example: --pattern \'summary_*200_resnet*\' so that it doesn\'t get expanded in the command-line')
    parser.add_argument("--output_dir",
                        type=str,
                        help='Folder where the configs are stored')
    parser.add_argument("--model_basename",
                        type=str,
                        default='model.ckpt',
                        help='')
    parser.add_argument("--start_point",
                        type=int,
                        default=0,
                        help='starting point for model validation')
    parser.add_argument("--end_point",
                        type=int,
                        default=2000000,
                        help='end point for model validation')
    parser.add_argument("--steps",
                        type=int,
                        default=20000,
                        help='Steps for model validation')
    parser.add_argument('--print_dirs', dest='print_dirs', action='store_true',
                        help='If passed shows the folders that will be processed by the script')
    parser.set_defaults(print_dirs=False)
    FLAGS = parser.parse_args()

    main(FLAGS)
