import sys
from collections import namedtuple

sys.path.append('./models/slim')
sys.path.append('./vidcompress')

import os
import time
import tensorflow as tf

import utils.utils_general as utils_general
from utils import image_processing, read_bin_files, parsers_definitions
from networks import nets_class, nets_loader, resnet_model

from tensorflow.python.ops import math_ops
from tensorflow.python.framework import ops
import tensorflow.contrib.slim as slim

from tensorflow.contrib.framework.python.ops import variables
from datetime import datetime
import vidcompress.entropy as entropy
import vidcompress.utils_prediction as utils_prediction


# TODO Fix this, doesn't work with the batch scripts when logging on
if not ('CUDA_VISIBLE_DEVICES' in os.environ):
    try:
        os.environ["CUDA_VISIBLE_DEVICES"] = os.environ['SGE_GPU']
    except KeyError:
        os.environ["CUDA_VISIBLE_DEVICES"] = utils_general.get_gpu_number()

FLAGS = None

OptimizerParameters = namedtuple('OptimizerParameters', ['opt', 'scope_name'])
BETA = 50
MAX_ENTROPY = 1.1


def _average_gradients(tower_grads):
    """Calculate the average gradient for each shared variable across all towers.

    Note that this function provides a synchronization point across all towers.

    Args:
    tower_grads: List of lists of (gradient, variable) tuples. The outer list
      is over individual gradients. The inner list is over the gradient
      calculation for each tower.
    Returns:
     List of pairs of (gradient, variable) where the gradient has been averaged
     across all towers.
    """

    average_grads = []
    for grad_and_vars in zip(*tower_grads):
        # Note that each grad_and_vars looks like the following:
        #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
        grads = []
        for g, _ in grad_and_vars:
            # Add 0 dimension to the gradients to represent the tower.
            expanded_g = tf.expand_dims(g, 0)

            # Append on a 'tower' dimension which we will average over below.
            grads.append(expanded_g)

        # Average over the 'tower' dimension.
        grad = tf.concat(axis=0, values=grads)
        grad = tf.reduce_mean(grad, 0)

        # Keep in mind that the Variables are redundant because they are shared
        # across towers. So .. we will just return the first tower's pointer to
        # the Variable.
        v = grad_and_vars[0][1]
        grad_and_var = (grad, v)
        average_grads.append(grad_and_var)

    return average_grads


def _tower_loss(inputs,
                labels,
                network,
                num_classes,
                scope,
                model_scope,
                add_joint_network_loss=False,
                loss_scale_factor=1.0,
                reuse_variables=None,
                only_encoder_decoder=False,
                beta=BETA,
                max_entropy=MAX_ENTROPY):

    """Calculate the total loss on a single tower running the ImageNet model.

    We perform 'batch splitting'. This means that we cut up a batch across
    multiple GPUs. For instance, if the batch size = 32 and num_gpus = 2,
    then each tower will operate on an batch of 16 images.

    Args:
    images: Images. 4D tensor of size [batch_size, FLAGS.image_size,
                                       FLAGS.image_size, 3].
    labels: 1-D integer Tensor of [batch_size].
    num_classes: number of classes
    scope: unique prefix string identifying the ImageNet tower, e.g.
      'tower_0'.

    Returns:
     Tensor of shape [] containing the total loss for a batch of data
    """

    output_dict = network.build(inputs, reuse=reuse_variables)
    logits = output_dict['logits']

    if add_joint_network_loss:
        assert ('output_image' in output_dict) and ('q' in output_dict) and ('input_image' in output_dict), 'Must have access to all outputs to construct the joint loss'
        # Todo: Add loss here
        mse, psnr = utils_prediction.mse_psnr(output_dict['input_image'], output_dict['output_image'], 'mse', loss_scale_factor=loss_scale_factor)

        soft_entropy_per_channel = entropy.soft_entropy_per_channel(output_dict['q'])
        soft_entropy = tf.reduce_mean(soft_entropy_per_channel)

        Config = namedtuple('Config', ['beta', 'max_entropy'])
        config = Config(beta=beta/loss_scale_factor, max_entropy=max_entropy)
        print(config)

        hard_entropy = entropy.add_entropy_loss(soft_entropy, config)

        tf.summary.scalar('mse', mse)
        tf.summary.scalar('psnr', psnr)
        tf.summary.scalar('soft_entropy', soft_entropy)
        tf.summary.scalar('hard_entropy', hard_entropy)

    # Build the portion of the Graph calculating the losses. Note that we will
    # assemble the total_loss using a custom function below.
    if not only_encoder_decoder:
        one_hot_labels = slim.one_hot_encoding(labels, num_classes)
        tf.losses.softmax_cross_entropy(one_hot_labels, logits)

    losses = tf.losses.get_losses(scope=scope)

    # This scoping is temporary before I start training the whole thing
    # probably simplest to split into softmax loss and reg-loss and get
    # all regularization losses
    # THIS CODE BREAKS OLDER THAN 1.3 TF

    if only_encoder_decoder:
        losses_reg = []
    else:
        losses_reg = tf.losses.get_regularization_losses(scope=scope + model_scope)

    losses_all = losses + losses_reg

    total_loss = math_ops.add_n(losses_all, name='total_loss')
    softmax_loss = math_ops.add_n(losses, name='softmax_loss')

    tf.summary.scalar('loss', total_loss)
    tf.summary.scalar('loss_softmax', softmax_loss)

    if not only_encoder_decoder:
        regularizers_loss = math_ops.add_n(losses_reg, name='regularizers_loss')
        tf.summary.scalar('loss_regularizers', regularizers_loss)

    top_1_op, top_5_op = resnet_model.get_top_k(logits, labels)

    tf.summary.scalar('top_1', top_1_op)
    tf.summary.scalar('top_5', top_5_op)

    return total_loss


def save_checkpoint(sess, saver, summary_dir, global_step, suffix=''):
    start_time = time.time()

    model_name = 'model{}.ckpt'.format(suffix)
    filepath = os.path.join(summary_dir, model_name)
    save_path = saver.save(sess, filepath, global_step=global_step)

    duration = time.time() - start_time
    print("Model saved in file: {}, saving took {:.2f} s".format(save_path, duration))


def main():
    # TODO Make sure number of CUDA_VISIBLE_DEVICES and FLAGS.num_gpus
    #      is compatible
    # TODO Add name scopes to make graph nicer.
    #if ((FLAGS.model_type == 'image' or FLAGS.model_type == 'compressed') and FLAGS.network_name != 'resnet-50') or (FLAGS.model_type == 'bottleneck' and FLAGS.network_name == 'resnet-50'):
    #    raise ValueError('Incompatible model type: {} with network name: {}'.format(FLAGS.model_type, FLAGS.network_name))

    num_classes_in_file = utils_general.get_num_classes(FLAGS.synsets_filename)
    summary_dir_parsed = utils_general.get_summary_dir(FLAGS)
    utils_general.check_for_checkpoint_overwrite(FLAGS.from_scratch, FLAGS.overwrite, summary_dir_parsed)

    if FLAGS.from_scratch:
        model_scope = 'resnet'
        checkpoint_path = ''
    else:
        checkpoint_path = utils_general.get_checkpoint_path(FLAGS.checkpoint_dir,
                                                            FLAGS.checkpoint_name,
                                                            summary_dir=summary_dir_parsed)
        model_scope = utils_general.get_checkpoint_scope(checkpoint_path)

    assert num_classes_in_file == FLAGS.num_classes, 'Number of classes given ({}) must match the number of synsets in {}'.format(
        FLAGS.num_classes, FLAGS.synsets_filename)
    # this assertion only makes sense when loading from png/jpeg files
    # not tfrecords, but still a nice sanity check

    # Scale num_preprocess_threads with number of gpus
    num_preprocess_threads_scaled = FLAGS.num_gpus * FLAGS.num_preprocess_threads

    if FLAGS.num_preprocess_threads > 4:
        tf.logging.warning('{} preprocessing threads per gpu'.format(FLAGS.num_preprocess_threads))

    if ('image' in FLAGS.model_type) or ('compressed' in FLAGS.model_type):
        config = image_processing.get_image_processing_config_from_flags(True, FLAGS)
        config.print_preprocessing()

        processed_tensors_batch, labels_batch = image_processing.get_images(
                                                            FLAGS.directory,
                                                            FLAGS.batch_size,
                                                            num_preprocess_threads=num_preprocess_threads_scaled,
                                                            config=config)
    elif 'bottleneck' in FLAGS.model_type:
        processed_tensors_batch, labels_batch = read_bin_files.get_symbols(
                                                            FLAGS.directory,
                                                            FLAGS.synsets_filename,
                                                            FLAGS.map_name_to_dim_filename,
                                                            FLAGS.centers_filename,
                                                            operating_point=FLAGS.operating_point,
                                                            batch_size=FLAGS.batch_size,
                                                            extension=FLAGS.extension,
                                                            is_training=True,
                                                            input_type=FLAGS.input_type,
                                                            num_preprocess_threads=num_preprocess_threads_scaled,
                                                            data_format=FLAGS.data_format,
                                                            rand_flip=FLAGS.rand_flip,
                                                            feat_vol_input_size=FLAGS.feat_vol_input_size)
    else:
        raise ValueError('unknown model type {}'.format(FLAGS.model_type))

    # Is needed for the adaptive learning rate
    variables.get_or_create_global_step()

    # If the label is offset, only special cases
    labels_batch = tf.subtract(labels_batch, FLAGS.label_offset)

    learn_rate = utils_general.get_learning_rate(FLAGS.learning_rate,
                                                 learning_rate_type=FLAGS.learning_rate_type,
                                                 epochs_decay=FLAGS.epochs_decay,
                                                 num_examples=FLAGS.num_examples,
                                                 batch_size=FLAGS.batch_size)

    tf.summary.scalar('learning_rate', learn_rate)

    tensors_splits = tf.split(axis=0,
                              num_or_size_splits=FLAGS.num_gpus,
                              value=processed_tensors_batch)
    labels_splits = tf.split(axis=0,
                             num_or_size_splits=FLAGS.num_gpus,
                             value=labels_batch)

    # Make more general/nicer?
    # Needs to account for the fact that not all networks need 2 sets of optimizers
    # A hack is to just add frozen_encoder_decoder flag
    if FLAGS.only_encoder_decoder:
        opt_class = tf.train.MomentumOptimizer(learning_rate=learn_rate,
                                               momentum=0.9,
                                               use_nesterov=FLAGS.nesterov)

        opts = [OptimizerParameters(opt=opt_class, scope_name='network')]
    elif FLAGS.frozen_encoder_decoder:
        opt_class = tf.train.MomentumOptimizer(learning_rate=learn_rate,
                                               momentum=0.9,
                                               use_nesterov=FLAGS.nesterov)

        opts = [OptimizerParameters(opt=opt_class, scope_name=model_scope)]
    else:
        opt_enc_dec = tf.train.MomentumOptimizer(learning_rate=learn_rate,
                                                 momentum=0.9,
                                                 use_nesterov=FLAGS.nesterov)

        opt_class = tf.train.MomentumOptimizer(learning_rate=learn_rate,
                                               momentum=0.9,
                                               use_nesterov=FLAGS.nesterov)

        opts = [OptimizerParameters(opt=opt_enc_dec, scope_name='network'),
                OptimizerParameters(opt=opt_class, scope_name=model_scope)]

    tower_gradients_all = [[] for _ in opts]
    apply_gradients_ops_all = []

    # Grab the input-summaries here before the towers are added
    # Could also do it in other places using a scope, but
    # This is simpler
    summaries = tf.get_collection(tf.GraphKeys.SUMMARIES)
    summaries_histograms = []

    network_class = nets_class.get_network_by_name(FLAGS.network_name)
    network = network_class(num_classes=FLAGS.num_classes,
                            is_training=True,
                            batch_norm_decay=FLAGS.batch_norm_decay,
                            weight_decay=FLAGS.weight_decay,
                            model_scope=model_scope,
                            data_format=FLAGS.data_format,
                            input_network_log_dir=FLAGS.encoder_decoder_dir,
                            frozen_encoder_decoder=FLAGS.frozen_encoder_decoder,
                            network_name=FLAGS.network_name)

    # Calculate the gradients for each model tower.
    reuse_variables = None
    for i in range(FLAGS.num_gpus):
        with tf.device('/gpu:%d' % i):
            with tf.name_scope('%s_%d' % ('tower', i)) as scope:
                # Force all Variables to reside on the CPU.
                # TODO Make sure this is equivalent to the inception version
                with slim.arg_scope([slim.variable, slim.model_variable], device='/cpu:0'):
                    # Calculate the loss for one tower of the ImageNet model. This
                    # function constructs the entire ImageNet model but shares the
                    # variables across all towers.

                    loss = _tower_loss(tensors_splits[i],
                                       labels_splits[i],
                                       network,
                                       FLAGS.num_classes,
                                       scope,
                                       model_scope,
                                       add_joint_network_loss=FLAGS.add_joint_network_loss,
                                       loss_scale_factor=FLAGS.loss_scale_factor,
                                       reuse_variables=reuse_variables,
                                       only_encoder_decoder=FLAGS.only_encoder_decoder,
                                       beta=FLAGS.beta,
                                       max_entropy=FLAGS.max_entropy)

                # Reuse variables for the next tower.
                reuse_variables = True

                # Retain the summaries from the first tower so that runs with
                # different number of gpus have the same scope
                if i == 0:
                    summaries.extend(tf.get_collection(tf.GraphKeys.SUMMARIES, scope))
                    if FLAGS.add_histograms:
                        summaries_histograms.extend(tf.get_collection('SUMMARIES_HISTOGRAMS'))

                # Retain the Batch Normalization updates operations only from the
                # final tower. Ideally, we should grab the updates from all towers
                # but these stats accumulate extremely fast so we can ignore the
                # other stats from the other towers without significant detriment.
                if FLAGS.only_encoder_decoder:
                    batchnorm_list = ops.get_collection(ops.GraphKeys.UPDATE_OPS, scope=scope)
                    batchnorm_updates = [t for t in batchnorm_list if model_scope not in t.name]
                    batchnorm_updates = set(batchnorm_updates)
                else:
                    batchnorm_updates = set(ops.get_collection(ops.GraphKeys.UPDATE_OPS, scope=scope))

                # Calculate the gradients for the batch of data on this ImageNet
                # tower.

                variables_to_train = [v for v in tf.trainable_variables() if 'centers' not in v.name]

                for j, opt_params in enumerate(opts):
                    grads_curr = opt_params.opt.compute_gradients(loss,
                                                                  var_list=[v for v in variables_to_train if opt_params.scope_name in v.name])
                    tower_gradients_all[j].append(grads_curr)

    # We must calculate the mean of each gradient. Note that this is the
    # synchronization point across all towers.

    for j, (tower_gradients, opt_params) in enumerate(zip(tower_gradients_all, opts)):
        if j == 0:
            global_step_update = slim.get_global_step()
        else:
            global_step_update = None

        apply_grad_op_curr = opt_params.opt.apply_gradients(_average_gradients(tower_gradients),
                                                            global_step=global_step_update)
        apply_gradients_ops_all.append(apply_grad_op_curr)

    # Group all updates to into a single train op.
    batchnorm_updates_op = tf.group(*batchnorm_updates)
    gradient_updates_op = tf.group(*apply_gradients_ops_all)
    train_op = tf.group(gradient_updates_op, batchnorm_updates_op)

    if FLAGS.add_histograms:
        # TODO Needs refactoring and fixing.
        # # Add histograms for gradients.
        # for grad, var in grads:
        #     if grad is not None:
        #         summaries_histograms.append(
        #             tf.summary.histogram(var.op.name + '/gradients', grad))

        for opt_params in opts:
            variables_to_train_curr = [v for v in variables_to_train if opt_params.scope_name in v.name]
            for var in variables_to_train_curr:
                summaries_histograms.append(tf.summary.histogram(var.op.name, var))

    # TODO make nicer
    # Fix logic
    # Should probably be: If path is passed, load that part (unless from_scratch)
    #                     and very explicitly tell which part was loaded/not loaded
    #                     and where from

    if FLAGS.load_from_encoder_decoder_dir:
        checkpoint_dir_encoder_decoder = os.path.join(FLAGS.encoder_decoder_dir, 'ckpts')

        if FLAGS.encoder_decoder_checkpoint_name:
            checkpoint_path_encoder_decoder = os.path.join(checkpoint_dir_encoder_decoder,
                                                           FLAGS.encoder_decoder_checkpoint_name)
        else:
            checkpoint_path_encoder_decoder = tf.train.latest_checkpoint(checkpoint_dir_encoder_decoder)
        assert checkpoint_path_encoder_decoder, 'Found no checkpoint in folder, is the folder correct or did you ' \
                                                'forget to mount the folder? '
    else:
        checkpoint_path_encoder_decoder = checkpoint_path

    checkpoint_paths_dict = {'output': checkpoint_path,
                             'input': checkpoint_path_encoder_decoder}

    init_fn = nets_loader.get_init_fn(checkpoint_paths_dict,
                                      network.get_variables_to_restore(restore_global_step=FLAGS.restore_global_step))

    # TODO Look into this with respect to my own checkpoint saver
    saver = tf.train.Saver(max_to_keep=200, keep_checkpoint_every_n_hours=1)

    merged = tf.summary.merge(summaries)

    if FLAGS.add_histograms:
        merged_histograms = tf.summary.merge(summaries_histograms)

    init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
    init_op = tf.group(*init_list)

    losses = tf.losses.get_losses(scope='tower_0')
    tensors_debug_names, tensors_debug = utils_general.get_tensors_to_print()

    with utils_general.get_session() as sess:
        sess.run(init_op)
        init_fn(sess)

        train_writer = tf.summary.FileWriter(summary_dir_parsed, sess.graph)
        utils_general.save_run_parameters(summary_dir_parsed, FLAGS, checkpoint_path, model_scope)

        global_step = sess.run(slim.get_global_step())

        train_writer.add_session_log(tf.SessionLog(status=tf.SessionLog.START), global_step=global_step)

        with slim.queues.QueueRunners(sess):
            print('%s: starting evaluation on (%s).' % (datetime.now(), 'training'))
            step = 0

            if sess.run(slim.get_global_step()) == 0:
                save_checkpoint(sess, saver, summary_dir_parsed, slim.get_global_step())

            while True:
                if step % 20 == 0:
                    start_time = time.time()
                    if step % 400 == 0 and FLAGS.add_histograms:
                        _, summary_out, summary_hist_out, loss_out, global_step = sess.run([train_op, merged, merged_histograms, loss, slim.get_global_step()])
                        train_writer.add_summary(summary_hist_out, global_step)
                    else:
                        _, summary_out, loss_out, global_step = sess.run([train_op, merged, loss, slim.get_global_step()])
                    duration = time.time() - start_time
                    print('{:.2f} examples/sec'.format(FLAGS.batch_size / duration))

                    train_writer.add_summary(summary_out, global_step)
                else:
                    if FLAGS.debug_print:
                        tensors_out, losses_out, _, loss_out, global_step = sess.run([tensors_debug, losses, train_op, loss, slim.get_global_step()])
                        print('losses: {}'.format(losses_out))
                        for t_name, tensor in zip(tensors_debug_names, tensors_out):
                            print('{}: {}'.format(t_name, tensor.item(0)))
                    else:
                        _, loss_out, global_step = sess.run([train_op, loss, slim.get_global_step()])

                if step % 100 == 0:
                    print('{}: Step: {}, Loss: {:.2f}'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), global_step, loss_out))

                if global_step % FLAGS.save_checkpoint_steps == 0:
                    save_checkpoint(sess, saver, summary_dir_parsed, slim.get_global_step())

                step += 1


if __name__ == '__main__':
    parser = parsers_definitions.get_parser_train()
    FLAGS = parser.parse_args()

    if FLAGS.print_variable_origin:
        utils_general.print_variable_values_and_origin(FLAGS, parser)
    else:
        main()
