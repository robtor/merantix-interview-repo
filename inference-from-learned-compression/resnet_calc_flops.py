from networks.resnet_blocks_mapping import networks_map

def calc_number_of_flops(spatial_size_in, num_inputs, num_outputs, ker_size, stride=1):
    return spatial_size_in**2 * num_inputs * num_outputs * ker_size**2 / stride**2


# Assuming square image for simplification
# and because that's always the use-case
input_size = 224
#input_size = 28
#input_size = 112

num_classes = 1000
include_root_block = False
include_root_block = True

tot_flops = 0

if include_root_block:
    tot_flops += calc_number_of_flops(input_size, 3, 64, 7, 2)
    input_size = input_size / 2
    input_size = input_size / 2

#last_output_depth = -1
#last_output_depth = 64
last_output_depth = 32

isfirst = True

for block in networks_map['resnet-50']:
#for block in networks_map['chopped-match']:
    for unit in block.args:
        bottleneck_size, depth, stride = unit

        print('========')
        print(isfirst)

        unit_flops = 0.0
        if isfirst:
            # Check this
            unit_flops += calc_number_of_flops(2 * input_size, last_output_depth, bottleneck_size, 1, stride=2)
            unit_flops += calc_number_of_flops(2 * input_size, last_output_depth, depth, 1, stride=2)
            isfirst = False
        else:
            unit_flops += calc_number_of_flops(input_size, bottleneck_size, depth, 1)

        #unit_flops += calc_number_of_flops(input_size, bottleneck_size, depth, 1)
        unit_flops += calc_number_of_flops(input_size, depth, depth, 3)
        unit_flops += calc_number_of_flops(input_size, depth, bottleneck_size, 1)

        print(unit)
        print(unit_flops)

        #bottleneck_size = bottleneck_size * stride

        #if stride == 2:
        #    print(unit)
        #    print(unit_flops / 10**9)
        #    unit_flops = 0.19 * 10**9

        input_size = input_size / stride

        tot_flops += unit_flops

        last_output_depth = bottleneck_size

        if stride == 2:
            isfirst = True

tot_flops += last_output_depth*num_classes


print(tot_flops / 10**9)


