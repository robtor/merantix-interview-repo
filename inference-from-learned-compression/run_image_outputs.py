import tensorflow as tf
import os
import glob

import segmentation_mask_output
import encoder_decoder_output

BASEDIR = '/srv/glusterfs/robertto/imagenet/compression_runs/'
JOINT_FOLDER_1 = '/srv/glusterfs/robertto/imagenet/summaries_backup/joint-training/'
JOINT_FOLDER_2 = '/srv/glusterfs/robertto/imagenet/summaries_backup/joint-training_max-entropy-beta/'

DATA_DIR = '/srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012'
MODEL_SCOPE = 'resnet'


def get_op_point_mapping():
    return {'low': BASEDIR + '1005_1344 pp:shared_centers_c12_nips_lr1e*3_bn',
            'low-medium': BASEDIR + '1014_1605 pp:shared_centers_c12_nips16_lr1e*5_bpp0.37_bn',
            'medium': BASEDIR + '1006_0749 pp:rob_x0.5'}


def get_op_point_key():
    return {'low': '1005-1344',
            'low-medium': '1014-1605',
            'medium': '1006-0749'}


def process_image(filename, output_dir):
    parser = encoder_decoder_output.get_parser()

    architectures = {'low': ('low', BASEDIR + '1005_1344 pp:shared_centers_c12_nips_lr1e*3_bn/ckpts/ckpt-736000'),
                     'low-medium': ('low-medium', BASEDIR + '1014_1605 pp:shared_centers_c12_nips16_lr1e*5_bpp0.37_bn/ckpts/ckpt-904000'),
                     'medium': ('medium', BASEDIR + '1006_0749 pp:rob_x0.5/ckpts/ckpt-102000'),
                     'medium-joint': ('medium', JOINT_FOLDER_2 + 'summary_image_1000_10x_joint-network-chopped-match_lr-0.0025_bs-64_no-scale-aug_key-1006-0749_loss-scale-factor-1000_max-ent-1.265_beta-150/model.ckpt-160000'),
                     'medium-joint-control': ('medium', JOINT_FOLDER_1 + 'summary_image_1000_10x_joint-network-chopped-match_lr-0.0025_bs-64_no-scale-aug_key-1006-0749_loss-scale-factor-1000_only-enc-dec/model.ckpt-160000'),
                     }

    #filename = '/srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012/JPEGImages/2009_003379.jpg'
    #filename = '/srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012/JPEGImages/2010_004930.jpg'
    #filename = '/srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012/JPEGImages/2009_004536.jpg'
    #filename = '/srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012/JPEGImages/2012_000938.jpg'

    op_point_map = get_op_point_mapping()

    for key, (op_point, ckpt_path) in architectures.items():
        parser_args = ['--output-dir', output_dir,
                       '--filename', filename,
                       '--encoder-decoder-dir', op_point_map[op_point],
                       '--checkpoint-path', ckpt_path,
                       '--op-point-name', key]

        flags_eval = parser.parse_args(parser_args)

        # Exception handling here so that a tensorflow error doesn't kill the whole program
        try:
            # Also possible to use a context manager
            # with tf.Graph().as_default():
            tf.reset_default_graph()
            encoder_decoder_output.main(flags_eval)
        except Exception as e:
            print(e)

    op_point_names = ['low', 'low-medium', 'medium']
    network_names = ['segmentation-encoder-chopped-match', 'segmentation-encoder-chopped-1', 'segmentation-encoder-decoder-resnet-50']
    op_point_to_key = get_op_point_key()

    seg_basename = '/srv/glusterfs/robertto/imagenet/summaries_backup/segmentation/{}_key-{}_320x320/model.ckpt-20000'

    for op_point in op_point_names:
        for network_name in network_names:
            print(op_point)
            print(network_name)
            ckpt_segmentation_path = seg_basename.format(network_name, op_point_to_key[op_point])
            parser_args = ['--data-dir', DATA_DIR,
                           '--model-scope', MODEL_SCOPE,
                           '--output-dir', output_dir,
                           '--filename', os.path.basename(filename),
                           '--encoder-decoder-dir', op_point_map[op_point],
                           '--restore-from', ckpt_segmentation_path,
                           '--network-name', network_name,
                           '--op-point-name', op_point + '_' + network_name]

            parser = segmentation_mask_output.get_arguments()
            flags_eval = parser.parse_args(parser_args)

            tf.reset_default_graph()
            segmentation_mask_output.main(flags_eval)


def main():
    image_folder = '/srv/glusterfs/robertto/imagenet/imagenet_256/images_selection_for_paper_segmentation'
    filenames = glob.glob(os.path.join(image_folder, '*.jpg'))

    output_dir = 'images_test/'

    for f_name in filenames:
        f_name_input = os.path.join('/srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012/JPEGImages/', os.path.basename(f_name))
        print(f_name_input)
        process_image(f_name, output_dir)


if __name__ == '__main__':
    main()
