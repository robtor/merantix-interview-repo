import sys
sys.path.append('./models/slim')

import os
import time
import tensorflow as tf
import utils
from utils import image_processing, read_bin_files, parsers_definitions
from networks import nets_factory

from tensorflow.python.framework import ops
import tensorflow.contrib.slim as slim

from tensorflow.contrib.framework.python.ops import variables

from datetime import datetime

# TODO Fix this, doesn't work with the batch scripts when logging on
if not ('CUDA_VISIBLE_DEVICES' in os.environ):
    try:
        os.environ["CUDA_VISIBLE_DEVICES"] = os.environ['SGE_GPU']
    except KeyError:
        os.environ["CUDA_VISIBLE_DEVICES"] = utils.get_gpu_number()


def save_checkpoint(sess, saver, summary_dir, global_step, suffix=''):
    start_time = time.time()

    model_name = 'model{}.ckpt'.format(suffix)
    filepath = os.path.join(summary_dir, model_name)
    save_path = saver.save(sess, filepath, global_step=global_step)

    duration = time.time() - start_time
    print("Model saved in file: {}, saving took {}".format(save_path, duration))


def main(FLAGS, MODEL_NAMES):
    #if ((FLAGS.model_type == 'image' or FLAGS.model_type == 'compressed') and FLAGS.network_name != 'resnet-50') or (FLAGS.model_type == 'bottleneck' and FLAGS.network_name == 'resnet-50'):
        #raise ValueError('Incompatable model type: {} with network name: {}'.format(FLAGS.model_type, FLAGS.network_name))

    # this assertion only makes sense when loading from png/jpeg files
    # not tfrecords, but still a nice sanity check
    num_classes_in_file = utils.get_num_classes(FLAGS.synsets_filename)
    assert num_classes_in_file == FLAGS.num_classes, 'Number of classes given ({}) must match the number of synsets in {}'.format(
        FLAGS.num_classes, FLAGS.synsets_filename)

    if not FLAGS.checkpoint_dir:
        raise ValueError('A checkpoint_dir must be supplied. Got {}'.format(FLAGS.checkpoint_dir))

    checkpoint_path = os.path.join(FLAGS.checkpoint_dir, MODEL_NAMES[0])

    if FLAGS.summary_dir:
        summary_dir_parsed = FLAGS.summary_dir
    else:
        # TODO Add custom output dir
        summary_dir_parsed = os.path.normpath(FLAGS.checkpoint_dir) + '_bn-global_' + str(FLAGS.batch_norm_decay) + FLAGS.suffix

    if not os.path.exists(summary_dir_parsed):
        os.makedirs(summary_dir_parsed)

    model_scope = utils.get_checkpoint_scope(checkpoint_path)

    if ('image' in FLAGS.model_type) or ('compressed' in FLAGS.model_type):
        processed_tensors_batch, labels_batch = image_processing.get_images(FLAGS.directory,
                                                                            FLAGS.synsets_filename,
                                                                            batch_size=FLAGS.batch_size,
                                                                            extension=FLAGS.extension,
                                                                            num_preprocess_threads=FLAGS.num_preprocess_threads,
                                                                            scale_augmentation=FLAGS.scale_augmentation,
                                                                            is_training=True,
                                                                            data_format=FLAGS.data_format,
                                                                            input_resize_factor=FLAGS.input_resize_factor)
    elif 'bottleneck' in FLAGS.model_type:
        processed_tensors_batch, labels_batch = read_bin_files.get_symbols(FLAGS.directory,
                                                                           FLAGS.synsets_filename,
                                                                           FLAGS.map_name_to_dim_filename,
                                                                           FLAGS.centers_filename,
                                                                           operating_point=FLAGS.operating_point,
                                                                           batch_size=FLAGS.batch_size,
                                                                           extension=FLAGS.extension,
                                                                           is_training=True,
                                                                           input_type=FLAGS.input_type,
                                                                           num_preprocess_threads=FLAGS.num_preprocess_threads,
                                                                           rand_flip=FLAGS.rand_flip,
                                                                           data_format=FLAGS.data_format,
                                                                           feat_vol_input_size=FLAGS.feat_vol_input_size)
    else:
        raise ValueError('unknown model type {}'.format(FLAGS.model_type))

    # Is needed for the adaptive learning rate
    variables.get_or_create_global_step()

    network_fn = nets_factory.get_network_fn(FLAGS.network_name,
                                             num_classes=FLAGS.num_classes,
                                             batch_norm_decay=FLAGS.batch_norm_decay,
                                             is_training=True,
                                             scope=model_scope,
                                             data_format=FLAGS.data_format)

    with slim.arg_scope([slim.variable, slim.model_variable], device='/cpu:0'):
        logits, end_points = network_fn(processed_tensors_batch, reuse=None)

        batchnorm_updates = set(ops.get_collection(ops.GraphKeys.UPDATE_OPS, scope=None))

    # Group all updates to into a single train op.
    batchnorm_updates_op = tf.group(*batchnorm_updates)
    train_op = batchnorm_updates_op

    #variables_to_restore = utils.get_variables_to_restore(subset_type=FLAGS.variables_to_restore,
                                                          #model_scope=model_scope)
    variables_to_restore = slim.get_model_variables()
    variables_to_restore.append(slim.get_global_step())
    #init_fn = slim.assign_from_checkpoint_fn(checkpoint_path, variables_to_restore)

    saver = tf.train.Saver(max_to_keep=10000, keep_checkpoint_every_n_hours=1)
    loader = tf.train.Saver(variables_to_restore)

    init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
    init_op = tf.group(*init_list)

    # TODO: For loop for different checkpoints
    #       Perhaps only save relevant variables?

    with utils.get_session() as sess:
        sess.run(init_op)

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        try:
            for model_name in MODEL_NAMES:
                checkpoint_path = os.path.join(FLAGS.checkpoint_dir, model_name)

                start = time.time()
                #init_fn(sess)
                loader.restore(sess, checkpoint_path)
                print('Loading took: ')
                print(time.time() - start)
                print('Loaded checkpoint from: {}'.format(checkpoint_path))


                global_step = sess.run(slim.get_global_step())
                global_step_beginning = global_step

                print('Global step {}'.format(global_step))

                print('%s: starting evaluation on (%s).' % (datetime.now(), 'batch_norm'))
                step = 0

                #save_checkpoint(sess,
                #                saver,
                #                summary_dir_parsed,
                #                global_step_beginning,
                #                suffix='_start')

                while not coord.should_stop():
                    start_time = time.time()
                    sess.run([train_op])
                    duration = time.time() - start_time

                    if step % 20 == 0:
                        print('{:.2f} examples/sec'.format(FLAGS.batch_size / duration))

                    step += 1

                    if (step % FLAGS.save_steps == 0) and FLAGS.save_intermittent:
                        save_checkpoint(sess,
                                        saver,
                                        summary_dir_parsed,
                                        step,
                                        suffix='_bn-{}'.format(global_step_beginning))

                    if step >= FLAGS.max_steps:
                        break

                save_checkpoint(sess,
                                saver,
                                summary_dir_parsed,
                                global_step_beginning)

                if coord.should_stop():
                    break

        except Exception as e:  # pylint: disable=broad-except
            coord.request_stop(e)

        coord.request_stop()
        coord.join(threads, stop_grace_period_secs=10)


if __name__ == '__main__':
    parser = parsers_definitions.get_parser_batch_norm()
    FLAGS = parser.parse_args()

    if FLAGS.print_variable_origin:
        utils.print_variable_values_and_origin(FLAGS, parser)
    else:
        main()

