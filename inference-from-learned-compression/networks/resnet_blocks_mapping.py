from models.slim.nets import resnet_utils
from models.slim.nets import resnet_v1

networks_map = {'resnet-50':
        [
            resnet_utils.Block(
                'block1', resnet_v1.bottleneck, [(256, 64, 1)] * 2 + [(256, 64, 2)]),
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 3 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 5 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
        ],
            'resnet-71':
        [
            resnet_utils.Block(
                'block1', resnet_v1.bottleneck, [(256, 64, 1)] * 2 + [(256, 64, 2)]),
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 3 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 12 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
        ],
            'resnet-59':
        [
            resnet_utils.Block(
                'block1', resnet_v1.bottleneck, [(256, 64, 1)] * 2 + [(256, 64, 2)]),
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 3 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 8 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
        ],
            'resnet-48':
        [
            resnet_utils.Block(
                'block1', resnet_v1.bottleneck, [(256, 64, 1)] * 2 + [(256, 64, 2)]),
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 3 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 5 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
        ],
            'resnet-101':
        [
            resnet_utils.Block(
                'block1', resnet_v1.bottleneck, [(256, 64, 1)] * 2 + [(256, 64, 2)]),
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 3 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 22 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
        ],
            'chopped-1':
        [
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 3 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 5 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
        ],

            'chopped-2':
        [
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 5 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
        ],

            'resnet-152':
        [
            resnet_utils.Block(
                'block1', resnet_v1.bottleneck, [(256, 64, 1)] * 2 + [(256, 64, 2)]),
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 7 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 35 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
        ],
            'chopped-match':
        [
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 3 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 9 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
        ],
            'chopped-match-72':
        [
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 3 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 16 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
        ],
            'equal-1':
        [
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 5 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 5 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 5)
        ],
            'equal-2':
        [
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(256, 64, 1)] * 22 + [(256, 64, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(512, 128, 1)] * 23 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(1024, 256, 1)] * 23)
        ],
            'chopped-48':
        [
            resnet_utils.Block(
                'block1', resnet_v1.bottleneck, [(256, 64, 1)] * 2 + [(256, 64, 2)]),
            resnet_utils.Block(
                'block2', resnet_v1.bottleneck, [(512, 128, 1)] * 3 + [(512, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.bottleneck, [(1024, 256, 1)] * 5 + [(1024, 256, 2)]),
            resnet_utils.Block(
                'block4', resnet_v1.bottleneck, [(2048, 512, 1)] * 3)
        ],
            'resnet-18':
        [
          resnet_utils.Block(
              'block1', resnet_v1.basicblock, [(64, 64, 1)] * 2),
          resnet_utils.Block(
              'block2', resnet_v1.basicblock, [(128, 128, 2)] + [(128, 128, 1)]),
          resnet_utils.Block(
              'block3', resnet_v1.basicblock, [(256, 256, 2)] + [(256, 256, 1)]),
          resnet_utils.Block(
              'block4', resnet_v1.basicblock, [(512, 512, 2)] + [(512, 512, 1)])
        ],
            'resnet-34':
        [
          resnet_utils.Block(
              'block1', resnet_v1.basicblock, [(64, 64, 1)] * 3),
          resnet_utils.Block(
              'block2', resnet_v1.basicblock, [(128, 128, 2)] + [(128, 128, 1)] * 3),
          resnet_utils.Block(
              'block3', resnet_v1.basicblock, [(256, 256, 2)] + [(256, 256, 1)] * 5),
          resnet_utils.Block(
              'block4', resnet_v1.basicblock, [(512, 512, 2)] + [(512, 512, 1)] * 2)
        ],
            'resnet-18-match':
        [
          resnet_utils.Block(
              'block2', resnet_v1.basicblock, [(128, 128, 1)] + [(128, 128, 1)] * 2),
          resnet_utils.Block(
              'block3', resnet_v1.basicblock, [(256, 256, 2)] + [(256, 256, 1)] * 2),
          resnet_utils.Block(
              'block4', resnet_v1.basicblock, [(512, 512, 2)] + [(512, 512, 1)])
        ],
            'resnet-34-match':
        [
          resnet_utils.Block(
              'block2', resnet_v1.basicblock, [(128, 128, 1)] + [(128, 128, 1)] * 4),
          resnet_utils.Block(
              'block3', resnet_v1.basicblock, [(256, 256, 2)] + [(256, 256, 1)] * 6),
          resnet_utils.Block(
              'block4', resnet_v1.basicblock, [(512, 512, 2)] + [(512, 512, 1)] * 3)
        ],
            'resnet-6':
        [
            resnet_utils.Block(
                'block1', resnet_v1.basicblock, [(64, 64, 1)] * 1),
            resnet_utils.Block(
                'block2', resnet_v1.basicblock, [(128, 128, 2)]),
        ],
            'resnet-6-match':
        [
            resnet_utils.Block(
                'block1', resnet_v1.basicblock, [(128, 128, 1)] * 2)
        ],
            'resnet-4-match':
        [
            resnet_utils.Block(
                'block1', resnet_v1.basicblock, [(128, 128, 1)])
        ],
            'resnet-8':
        [
            resnet_utils.Block(
                'block1', resnet_v1.basicblock, [(64, 64, 1)] * 1),
            resnet_utils.Block(
                'block2', resnet_v1.basicblock, [(128, 128, 2)]),
            resnet_utils.Block(
                'block3', resnet_v1.basicblock, [(256, 256, 2)])
        ],
            'resnet-8-match':
        [
            resnet_utils.Block(
                'block1', resnet_v1.basicblock, [(128, 128, 1)] * 2),
            resnet_utils.Block(
                'block2', resnet_v1.basicblock, [(256, 256, 2)])
        ],
              }

# Equal-2 probaly has way too many parameters


def main():
    for key in networks_map:
        print(key)
        for block in networks_map[key]:
            print(block)


if __name__ == '__main__':
    main()
