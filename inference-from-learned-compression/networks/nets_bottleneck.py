import imgcompress_frozen
import tensorflow as tf

from models.slim.nets import resnet_v1
from networks import cae_network_decoder
from networks.resnet_blocks_mapping import networks_map

slim = tf.contrib.slim

INCLUDE_ROOT_BLOCK = False
SCOPE = 'resnet'
DATA_FORMAT = 'NHWC'
OUTPUT_STRIDE = None
GLOBAL_POOL = True

def chopped_1(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):

    blocks = networks_map['chopped-1']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)
    return logits, end_points


def chopped_2(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):

    blocks = networks_map['chopped-2']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)
    return logits, end_points

def equal_1(inputs,
            arg_scope,
            num_classes=1000,
            is_training=True,
            global_pool=GLOBAL_POOL,
            output_stride=OUTPUT_STRIDE,
            reuse=None,
            scope=SCOPE,
            data_format=DATA_FORMAT):

    blocks = networks_map['equal-1']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)

    return logits, end_points

def equal_2(inputs,
            arg_scope,
            num_classes=1000,
            is_training=True,
            global_pool=GLOBAL_POOL,
            output_stride=OUTPUT_STRIDE,
            reuse=None,
            scope=SCOPE,
            data_format=DATA_FORMAT):

    blocks = networks_map['equal-2']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)

    return logits, end_points

def chopped_match(inputs,
                  arg_scope,
                  num_classes=1000,
                  is_training=True,
                  global_pool=GLOBAL_POOL,
                  output_stride=OUTPUT_STRIDE,
                  reuse=None,
                  scope=SCOPE,
                  data_format=DATA_FORMAT):

    blocks = networks_map['chopped-match']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)

    return logits, end_points


def chopped_48_root(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):

    blocks = networks_map['chopped-48']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, include_root_block_bottleneck=True,
                                    reuse=reuse, scope=scope, data_format=data_format)

    return logits, end_points


def chopped_48(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):

    blocks = networks_map['chopped-48']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)

    return logits, end_points


def resnet_34_match(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):

    blocks = networks_map['resnet-34-match']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)

    return logits, end_points


def resnet_18_match(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):

    blocks = networks_map['resnet-18-match']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)

    return logits, end_points


def combined(inputs,
             arg_scope,
             num_classes=1000,
             is_training=True,
             global_pool=GLOBAL_POOL,
             output_stride=OUTPUT_STRIDE,
             reuse=None,
             scope=SCOPE,
             data_format=DATA_FORMAT):

    decoded_image = cae_network_decoder.decoder(inputs)

    output_size = 224

    batch_size = decoded_image.get_shape().as_list()[0]
    offset1 = int((decoded_image.get_shape().as_list()[1] - output_size) / 2 )
    offset2 = int((decoded_image.get_shape().as_list()[2] - output_size) / 2)

    output_channels = decoded_image.get_shape().as_list()[3]

    offsets = [0, offset1, offset2, 0]

    output_shape = [batch_size, output_size, output_size, output_channels]

    decoded_image = tf.slice(decoded_image, offsets, output_shape)

    mean = tf.convert_to_tensor([123.68, 116.779, 103.939], dtype=tf.float32)
    decoded_image = decoded_image - mean

    blocks = networks_map['resnet-50']
    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(decoded_image,
                                                 blocks,
                                                 num_classes,
                                                 is_training=is_training,
                                                 global_pool=global_pool,
                                                 output_stride=output_stride,
                                                 include_root_block=True,
                                                 reuse=reuse,
                                                 scope=scope,
                                                 data_format=data_format)

    return logits, end_points


def frozen_encoder(inputs,
             arg_scope,
             num_classes=1000,
             is_training=True,
             global_pool=GLOBAL_POOL,
             output_stride=OUTPUT_STRIDE,
             reuse=None,
             scope=SCOPE,
             data_format=DATA_FORMAT):

    vidcompress_repo = '/scratch_net/tractor/robertto/vidcompress/'
    log_dir = '/srv/glusterfs/mentzerf/out_tb_pushpsnr/1209_0941 pp:shared_centers_c12_nips'
    
    net = imgcompress_frozen.get_network(log_dir, vidcompress_repo, inputs, encoder_only=True, data_format=data_format)
    encoded_feat_vol = net.Qhard
    #encoded_feat_vol = inputs

    encoded_feat_vol = tf.random_crop(encoded_feat_vol, [64, 8, 28, 28], seed=None, name=None)

    blocks = networks_map['chopped-match']
    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(encoded_feat_vol,
                                                 blocks,
                                                 num_classes,
                                                 is_training=is_training,
                                                 global_pool=global_pool,
                                                 output_stride=output_stride,
                                                 include_root_block=INCLUDE_ROOT_BLOCK,
                                                 reuse=reuse,
                                                 scope=scope,
                                                 data_format=data_format)


    return logits, end_points
