import functools

import nets_bottleneck
import tensorflow as tf

import models.slim.nets.resnet_v1 as resnet_v1
from networks import nets_image

slim = tf.contrib.slim

networks_map = {'resnet-50': nets_image.resnet_50,
                'resnet-34': nets_image.resnet_34,
                'resnet-18': nets_image.resnet_18,
                'chopped-1': nets_bottleneck.chopped_1,
                'chopped-2': nets_bottleneck.chopped_2,
                'chopped-48': nets_bottleneck.chopped_48,
                'chopped-48-root': nets_bottleneck.chopped_48_root,
                'chopped-match': nets_bottleneck.chopped_match,
                'equal-1': nets_bottleneck.equal_1,
                'equal-2': nets_bottleneck.equal_2,
                'resnet-34-match': nets_bottleneck.resnet_34_match,
                'resnet-18-match': nets_bottleneck.resnet_18_match,
                'combined': nets_bottleneck.combined,
                'frozen-encoder': nets_bottleneck.frozen_encoder,
                'frozen-encoder-decoder': nets_image.frozen_encoder_decoder,
               }

arg_scopes_map = {'resnet-50': resnet_v1.resnet_arg_scope,
                  'resnet-18': resnet_v1.resnet_arg_scope,
                  'resnet-34': resnet_v1.resnet_arg_scope,
                  'chopped-1': resnet_v1.resnet_arg_scope,
                  'chopped-2': resnet_v1.resnet_arg_scope,
                  'chopped-48': resnet_v1.resnet_arg_scope,
                  'chopped-48-root': resnet_v1.resnet_arg_scope,
                  'chopped-match': resnet_v1.resnet_arg_scope,
                  'equal-1': resnet_v1.resnet_arg_scope,
                  'equal-2': resnet_v1.resnet_arg_scope,
                  'resnet-34-match': resnet_v1.resnet_arg_scope,
                  'resnet-18-match': resnet_v1.resnet_arg_scope,
                  'combined': resnet_v1.resnet_arg_scope,
                  'frozen-encoder': resnet_v1.resnet_arg_scope,
                  'frozen-encoder-decoder': resnet_v1.resnet_arg_scope,
                 }


def get_network_fn(name,
                   num_classes=1000,
                   weight_decay=0.0001,
                   batch_norm_decay=0.9,
                   is_training=False,
                   data_format='NHWC',
                   scope='resnet'
                   ):
  """Returns a network_fn such as `logits, end_points = network_fn(images)`.

  Args:
    name: The name of the network.
    num_classes: The number of classes to use for classification.
    weight_decay: The l2 coefficient for the model weights.
    is_training: `True` if the model is being used for training and `False`
      otherwise.

  Returns:
    network_fn: A function that applies the model to a batch of images. It has
      the following signature:
        logits, end_points = network_fn(images)
  Raises:
    ValueError: If network `name` is not recognized.
  """
  if name not in networks_map:
    raise ValueError('Name of network unknown %s' % name)
  func = networks_map[name]
  @functools.wraps(func)
  def network_fn(inputs, reuse=None):
    arg_scope = arg_scopes_map[name](weight_decay=weight_decay, batch_norm_decay=batch_norm_decay)
    #with slim.arg_scope(arg_scope):
    # pass the arg_scope to the function, because in some cases for the network
    # the scope is 'split up'
    return func(inputs,
                arg_scope,
                num_classes=num_classes,
                is_training=is_training,
                data_format=data_format,
                scope=scope,
                reuse=reuse)

  return network_fn
