from collections import namedtuple

import tensorflow as tf
from tensorflow.contrib import slim as slim
from tensorflow.python.ops import math_ops

import models.slim.nets.resnet_v1 as resnet_v1
import utils.utils_general as utils_general
import vidcompress.imgcompress_frozen
from networks.resnet_blocks_mapping import networks_map

image_networks_kwargs = {'include_root_block': True,
                         'output_stride': None,
                         'global_pool': True,
                         'spatial_squeeze': True,
                         }
bottleneck_networks_kwargs = {'include_root_block': False,
                              'output_stride': None,
                              'global_pool': True,
                              'spatial_squeeze': True,
                              }

image_segmentation_networks_kwargs = {'include_root_block': True,
                                      'output_stride': 8,
                                      'global_pool': False,
                                      'spatial_squeeze': False,
                                      }
bottleneck_segmentation_networks_kwargs = {'include_root_block': False,
                                           'output_stride': 1,
                                           'global_pool': False,
                                           'spatial_squeeze': False,
                                           }

NetworkElements = namedtuple('NetworkElements', ['network_class', 'network_kwargs', 'default_input_size'])

# FIX
networks = None

if utils_general.is_on_biwi() or utils_general.is_on_tractor():
    VIDCOMPRESS_REPO = '/scratch_net/tractor/robertto/masters_thesis/vidcompress/'
else:
    VIDCOMPRESS_REPO = '/raid/robertto/masters_thesis/vidcompress/'


def get_network_by_name(network_name):
    """ Returns a class that is a subclass of _Network. """
    return networks[network_name].network_class


class _Network(object):
    def __init__(self,
                 num_classes,
                 is_training,
                 data_format='NHWC',
                 model_scope='resnet',
                 weight_decay=0.0001,
                 batch_norm_decay=0.9,
                 input_network_log_dir=None,
                 frozen_encoder_decoder=False,
                 network_name=None):

        assert data_format in ('NHWC', 'NCHW')
        if 'segmentation' in network_name:
            assert not num_classes, 'For segmentation number of classes should be None. Got {}'.format(num_classes)
            assert data_format == 'NHWC', 'Atrous convolution only works with NHWC. Got {}'.format(data_format)

        self.arg_scope = resnet_v1.resnet_arg_scope(weight_decay=weight_decay,
                                                    batch_norm_decay=batch_norm_decay,
                                                    batch_norm_fused='segmentation' not in network_name)
        self.num_classes = num_classes
        self.is_training = is_training
        self.data_format = data_format
        self.model_scope = model_scope
        self.input_network_log_dir = input_network_log_dir
        self.frozen_encoder_decoder = frozen_encoder_decoder
        self.network_name = network_name
        self.blocks_network_name = self.get_blocks_network_name(network_name)

    def _build_from_blocks(self, inputs, reuse):
        blocks = networks_map[self.blocks_network_name]
        logits, _ = resnet_v1.resnet_v1(inputs,
                                        blocks,
                                        self.num_classes,
                                        self.is_training,
                                        reuse=reuse,
                                        scope=self.model_scope,
                                        data_format=self.data_format,
                                        **networks[self.network_name].network_kwargs)
        return logits

    def build(self, inputs, reuse):
        raise NotImplementedError()

    def _build_network(self, inputs, reuse=None):
        raise NotImplementedError()

    def get_variables_to_restore(self, restore_global_step=True):
        raise NotImplementedError()

    def get_blocks_network_name(self, network_name):
        # A bit hacky, but should always follow the same pattern
        blocks_network_name = network_name.replace('encoder-decoder-', '')
        blocks_network_name = blocks_network_name.replace('encoder-upsample-', '')
        blocks_network_name = blocks_network_name.replace('encoder-', '')
        blocks_network_name = blocks_network_name.replace('joint-network-', '')
        blocks_network_name = blocks_network_name.replace('segmentation-', '')

        return blocks_network_name


class _SimpleNetwork(_Network):
    def build(self, inputs, reuse=None):
        with slim.arg_scope(self.arg_scope):
            net = self._build_network(inputs, reuse=reuse)
        return net

    def _build_network(self, inputs, reuse=None):
        raise NotImplementedError()

    def get_variables_to_restore(self, restore_global_step=True):
        variables_to_restore = slim.get_model_variables(scope=self.model_scope)
        if self.is_training and restore_global_step:
            variables_to_restore.append(slim.get_global_step())

        return {'output': variables_to_restore}


class _EncoderDecoderNetwork(_Network):
    def build(self, inputs, reuse=None):
        net = self._build_network(inputs, reuse=reuse)
        return net

    def _build_network(self, inputs, reuse=None):
        raise NotImplementedError()

    def get_variables_to_restore(self, restore_global_step=True):
        variables_to_restore = slim.get_model_variables()
        variables_to_restore.extend(slim.get_variables_by_name('centers'))

        if self.is_training and restore_global_step:
            variables_to_restore.append(slim.get_or_create_global_step())

        return {'output': variables_to_restore}


class _TwoPartNetwork(_Network):
    def build(self, inputs, reuse=None):
        return self._build_network(inputs, reuse=reuse)

    def _get_connection_crop_size(self):
        raise NotImplementedError()

    def get_variables_to_restore(self, restore_global_step=True):
        variables_to_restore_input = slim.get_model_variables(scope='network')
        variables_to_restore_input.extend(slim.get_variables_by_name('centers'))

        variables_to_restore_output = slim.get_model_variables(scope=self.model_scope)
        if self.is_training and restore_global_step:
            variables_to_restore_output.append(slim.get_global_step())

        return {'input': variables_to_restore_input, 'output': variables_to_restore_output}

    def _build_input(self, inputs, reuse=None):
        raise NotImplementedError()

    def _build_output(self, inputs, reuse=None):
        raise NotImplementedError()

    def _build_network(self, inputs, reuse=None):
        raise NotImplementedError()

    def _connection(self, inputs):

        with tf.name_scope('connection'):
            curr_shape = inputs.get_shape().as_list()

            img_dim = self._get_connection_crop_size()

            output_shape = [curr_shape[0], img_dim, img_dim, curr_shape[-1]]

            if self.data_format == 'NCHW':
                output_shape = [curr_shape[0], curr_shape[1], img_dim, img_dim]

            if self.is_training:
                net = tf.random_crop(inputs, output_shape, seed=None, name=None)
            else:
                if self.data_format == 'NCHW':
                    offset_height = (curr_shape[2] - output_shape[2]) // 2
                    offset_width = (curr_shape[3] - output_shape[3]) // 2
                    offsets = [0, 0, offset_height, offset_width]
                else:
                    offset_height = (curr_shape[1] - output_shape[1]) // 2
                    offset_width = (curr_shape[2] - output_shape[2]) // 2
                    offsets = [0, offset_height, offset_width, 0]

                net = tf.slice(inputs, offsets, output_shape)

        return net


class _Resnet(_SimpleNetwork):
    def _build_network(self, inputs, reuse=None):
        return {'logits': self._build_from_blocks(inputs, reuse)}


class _Chopped(_SimpleNetwork):
    def _build_network(self, inputs, reuse=None):
        return {'logits': self._build_from_blocks(inputs, reuse)}


class _EncoderDecoder(_EncoderDecoderNetwork):
    def _build_network(self, inputs, reuse=None):
        net = vidcompress.imgcompress_frozen.get_network(self.input_network_log_dir,
                                                         VIDCOMPRESS_REPO,
                                                         inputs,
                                                         encoder_only=False,
                                                         is_training=self.is_training,
                                                         reuse_variable=reuse,
                                                         data_format=self.data_format)

        return {'output_image': net.output, 'symbols_volume': net.symbols}


class _SegmentationResnet(_SimpleNetwork):
    def _build_network(self, inputs, reuse=None):
        net = self._build_from_blocks(inputs, reuse)

        num_classes_seg = 21
        outputs = []
        dilation_rates = [6, 12, 18, 24]

        # Experiment with the l2 and other stuff on the last layer
        for i, d_rate in enumerate(dilation_rates):
            current_name = 'fc1_voc12_c{}'.format(i)
            outputs.append(slim.conv2d(net, num_classes_seg, [3, 3],
                                       stride=1, padding='SAME',
                                       rate=d_rate, normalizer_fn=None,
                                       activation_fn=None,
                                       data_format=self.data_format,
                                       scope=current_name))

        output = math_ops.add_n(outputs, name='fc1_voc12')

        return {'logits': output}


class _EncoderChopped(_TwoPartNetwork):
    def _get_connection_crop_size(self):
        return 28

    def _build_network(self, inputs, reuse=None):
        net = self._build_input(inputs, reuse=reuse)
        net = self._connection(net)

        with slim.arg_scope(self.arg_scope):
            net = self._build_output(net, reuse=reuse)

        return {'logits': net}

    def _build_input(self, inputs, reuse=None):
        training_val = self.is_training and (not self.frozen_encoder_decoder)

        net = vidcompress.imgcompress_frozen.get_network(self.input_network_log_dir,
                                                         VIDCOMPRESS_REPO,
                                                         inputs,
                                                         encoder_only=True,
                                                         is_training=training_val,
                                                         reuse_variable=reuse,
                                                         data_format=self.data_format,
                                                         fused='segmentation' not in self.network_name)
        return net.Qbar

    def _build_output(self, inputs, reuse=None):
        return self._build_from_blocks(inputs, reuse)


class _EncoderChoppedUpsample(_EncoderChopped):
    def _get_connection_crop_size(self):
        return 56

    def _upscale_factor(self):
        return 2

    def _build_network(self, inputs, reuse=None):
        net = self._build_input(inputs, reuse=reuse)

        assert self.data_format == 'NHWC', 'Upsampling only works with NHWC data_format, got {}'.format(self.data_format)
        shape_bottleneck = tf.shape(net)
        # Note, only works for NHWC, so shapes are assumed to be consistent with that
        # net = tf.image.resize_images(net,
        #                              [self._upscale_factor() * shape_bottleneck[1], self._upscale_factor() * shape_bottleneck[2]],
        #                              method=tf.image.ResizeMethod.BICUBIC)

        print('=' * 50)
        print('Note: The resize of this function assumes spatial dimensions of 32x32. If this is not the case the implementation needs to be fixed')
        bottleneck_size = 32
        net = tf.image.resize_images(net,
                                     [self._upscale_factor() * bottleneck_size, self._upscale_factor() * bottleneck_size],
                                     method=tf.image.ResizeMethod.BICUBIC)

        net = self._connection(net)

        with slim.arg_scope(self.arg_scope):
            net = self._build_output(net, reuse=reuse)

        return {'logits': net}



_MEAN_RGB = [123.680, 116.779, 103.939]
class _EncoderDecoderResnet(_TwoPartNetwork):
    def _get_connection_crop_size(self):
        return 224

    def _build_network(self, inputs, reuse=None):
        net = self._build_input(inputs, reuse=reuse)
        net = self._connection(net)

        if self.data_format == 'NHWC':
            mean_val = tf.convert_to_tensor(_MEAN_RGB)
            net = tf.subtract(net, mean_val, name='mean_img')
        else:
            # Must be a better way
            mean_val = tf.convert_to_tensor(_MEAN_RGB)
            mean_val = tf.expand_dims(mean_val, axis=1)
            mean_val = tf.expand_dims(mean_val, axis=1)
            # Now has shape (3, 1, 1) and can be broadcast to NCHW

            net = tf.subtract(net, mean_val, name='mean_img')

        with slim.arg_scope(self.arg_scope):
            net = self._build_output(net, reuse=reuse)

        return {'logits': net}

    def _build_input(self, inputs, reuse=None):
        # Training_val controls
        # i)
        # is_training == True  => quantization == Qhat
        # is_training == False => quantization == Qhard, see function quantize
        # ii)
        # Whether batch_norm moving averages are updated, so if it is frozen this
        # is not supposed to be updated even if the other part is training

        training_val = self.is_training and (not self.frozen_encoder_decoder)

        net = vidcompress.imgcompress_frozen.get_network(self.input_network_log_dir,
                                                         VIDCOMPRESS_REPO,
                                                         inputs,
                                                         encoder_only=False,
                                                         is_training=training_val,
                                                         reuse_variable=reuse,
                                                         data_format=self.data_format,
                                                         fused='segmentation' not in self.network_name)
        return net.output

    def _build_output(self, inputs, reuse=None):
        return self._build_from_blocks(inputs, reuse)


class _SegmentationEncoderDecoderResnet(_EncoderDecoderResnet):
    def _connection(self, inputs):
        with tf.name_scope('connection'):
            net = inputs

        return net

    def _build_output(self, inputs, reuse=None):
        net = self._build_from_blocks(inputs, reuse)

        with tf.variable_scope('fc1_voc12', values=[net], reuse=None) as sc:
            num_classes_seg = 21
            outputs = []
            dilation_rates = [6, 12, 18, 24]

            # Experiment with the l2 and other stuff on the last layer
            for i, d_rate in enumerate(dilation_rates):
                current_name = 'fc1_voc12_c{}'.format(i)
                outputs.append(slim.conv2d(net, num_classes_seg, [3, 3],
                                           stride=1, padding='SAME',
                                           rate=d_rate, normalizer_fn=None,
                                           activation_fn=None,
                                           data_format=self.data_format,
                                           scope=current_name))

            output = math_ops.add_n(outputs, name='fc1_voc12')

        return output


class _SegmentationEncoderChopped(_EncoderChopped):
    def _connection(self, inputs):
        with tf.name_scope('connection'):
            net = inputs

        return net

    def _build_output(self, inputs, reuse=None):
        net = self._build_from_blocks(inputs, reuse)

        with tf.variable_scope('fc1_voc12', values=[net], reuse=None) as sc:
            num_classes_seg = 21
            outputs = []
            dilation_rates = [6, 12, 18, 24]

            # Experiment with the l2 and other stuff on the last layer
            for i, d_rate in enumerate(dilation_rates):
                current_name = 'fc1_voc12_c{}'.format(i)
                outputs.append(slim.conv2d(net, num_classes_seg, [3, 3],
                                           stride=1, padding='SAME',
                                           rate=d_rate, normalizer_fn=None,
                                           activation_fn=None,
                                           data_format=self.data_format,
                                           scope=current_name))

            output = math_ops.add_n(outputs, name='fc1_voc12')

        return output


class _JointNetwork(_TwoPartNetwork):
    def _get_connection_crop_size(self):
        return 28

    def _build_network(self, inputs, reuse=None):
        net = self._build_input(inputs, reuse=reuse)
        net_output = net.output
        net_logits = self._connection(net.Qbar)

        with slim.arg_scope(self.arg_scope):
            net_logits = self._build_output(net_logits, reuse=reuse)

        return {'logits': net_logits, 'output_image': net_output, 'q': net.q, 'input_image': inputs}

    def _build_input(self, inputs, reuse=None):
        training_val = self.is_training and (not self.frozen_encoder_decoder)

        net = vidcompress.imgcompress_frozen.get_network(self.input_network_log_dir,
                                                         VIDCOMPRESS_REPO,
                                                         inputs,
                                                         encoder_only=False,
                                                         is_training=training_val,
                                                         reuse_variable=reuse,
                                                         data_format=self.data_format)
        return net

    def _build_output(self, inputs, reuse=None):
        return self._build_from_blocks(inputs, reuse)


networks = {'resnet-50': NetworkElements(_Resnet, image_networks_kwargs, 224),
            'resnet-34': NetworkElements(_Resnet, image_networks_kwargs, 224),
            'resnet-18': NetworkElements(_Resnet, image_networks_kwargs, 224),
            'resnet-8': NetworkElements(_Resnet, image_networks_kwargs, 224),
            'resnet-6': NetworkElements(_Resnet, image_networks_kwargs, 224),
            'resnet-8-match': NetworkElements(_Chopped, bottleneck_networks_kwargs, 28),
            'resnet-6-match': NetworkElements(_Chopped, bottleneck_networks_kwargs, 28),
            'chopped-match': NetworkElements(_Chopped, bottleneck_networks_kwargs, 28),
            'encoder-chopped-match': NetworkElements(_EncoderChopped,
                                                     bottleneck_networks_kwargs,
                                                     256),
            'encoder-chopped-match-72': NetworkElements(_EncoderChopped,
                                                        bottleneck_networks_kwargs,
                                                        256),
            'encoder-chopped-1': NetworkElements(_EncoderChopped,
                                                 bottleneck_networks_kwargs,
                                                 256),
            'encoder-decoder-resnet-50': NetworkElements(_EncoderDecoderResnet,
                                                         image_networks_kwargs,
                                                         256),
            'encoder-decoder-resnet-71': NetworkElements(_EncoderDecoderResnet,
                                                         image_networks_kwargs,
                                                         256),
            'encoder-decoder-resnet-59': NetworkElements(_EncoderDecoderResnet,
                                                         image_networks_kwargs,
                                                         256),
            'encoder-resnet-6-match': NetworkElements(_EncoderChopped,
                                                      bottleneck_networks_kwargs,
                                                      256),
            'encoder-resnet-4-match': NetworkElements(_EncoderChopped,
                                                      bottleneck_networks_kwargs,
                                                      256),
            'encoder-decoder-resnet-6': NetworkElements(_EncoderDecoderResnet,
                                                        image_networks_kwargs,
                                                        256),
            'joint-network-chopped-match': NetworkElements(_JointNetwork,
                                                           bottleneck_networks_kwargs,
                                                           256),
            'encoder-decoder': NetworkElements(_EncoderDecoder, None, 256),
            'segmentation-encoder-decoder-resnet-50': NetworkElements(_SegmentationEncoderDecoderResnet,
                                                                      image_segmentation_networks_kwargs,
                                                                      256),
            'segmentation-encoder-decoder-resnet-71': NetworkElements(_SegmentationEncoderDecoderResnet,
                                                                      image_segmentation_networks_kwargs,
                                                                      256),
            'segmentation-encoder-chopped-match': NetworkElements(_SegmentationEncoderChopped,
                                                                  bottleneck_segmentation_networks_kwargs,
                                                                  256),
            'segmentation-encoder-chopped-match-72': NetworkElements(_SegmentationEncoderChopped,
                                                                     bottleneck_segmentation_networks_kwargs,
                                                                     256),
            'segmentation-encoder-chopped-1': NetworkElements(_SegmentationEncoderChopped,
                                                                  bottleneck_segmentation_networks_kwargs,
                                                                  256),
            'segmentation-resnet-50': NetworkElements(_SegmentationResnet,
                                                      image_segmentation_networks_kwargs,
                                                      256),
            'encoder-upsample-resnet-48': NetworkElements(_EncoderChoppedUpsample,
                                                          bottleneck_networks_kwargs,
                                                          256),
            }
