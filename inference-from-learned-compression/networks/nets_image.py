import imgcompress_frozen
import tensorflow as tf

from models.slim.nets import resnet_v1
from networks.resnet_blocks_mapping import networks_map

slim = tf.contrib.slim

INCLUDE_ROOT_BLOCK = True
SCOPE = 'resnet'
DATA_FORMAT = 'NHWC'
OUTPUT_STRIDE = None
GLOBAL_POOL = True

def resnet_50(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):
    """ResNet-50 model of [1]. See resnet_v1() for arg and return description."""
    blocks = networks_map['resnet-50']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)
    return logits, end_points

def resnet_18(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):
    """ResNet-50 model of [1]. See resnet_v1() for arg and return description."""
    blocks = networks_map['resnet-18']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)
    return logits, end_points


def resnet_34(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):
    """ResNet-50 model of [1]. See resnet_v1() for arg and return description."""
    blocks = networks_map['resnet-34']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)
    return logits, end_points


def frozen_encoder_decoder(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):
    """ResNet-50 model of [1]. See resnet_v1() for arg and return description."""
    vidcompress_repo = '/scratch_net/tractor/robertto/vidcompress/'
    log_dir = '/srv/glusterfs/mentzerf/out_tb_pushpsnr/1209_0941 pp:shared_centers_c12_nips'
    
    net = imgcompress_frozen.get_network(log_dir, vidcompress_repo, inputs, encoder_only=False, data_format=data_format)
    encoded_feat_vol = net.output

    encoded_feat_vol = tf.random_crop(encoded_feat_vol, [64, 3, 224, 224], seed=None, name=None)

    blocks = networks_map['resnet-50']
    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(encoded_feat_vol,
                                                 blocks,
                                                 num_classes,
                                                 is_training=is_training,
                                                 global_pool=global_pool,
                                                 output_stride=output_stride,
                                                 include_root_block=INCLUDE_ROOT_BLOCK,
                                                 reuse=reuse,
                                                 scope=scope,
                                                 data_format=data_format)


    return logits, end_points
    blocks = networks_map['resnet-50']

    with slim.arg_scope(arg_scope):
        logits, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    data_format=data_format)

    return logits, end_points
