import tensorflow as tf


def get_var_and_checkpoint_list(checkpoint_paths_dict, var_list_dict):
    """
    A function that takes 2 dictionaries, checkpoint_paths_dict and var_list_dict
    and return a list of tuples of variable_lists and associated checkpoint_paths.
    The checkpoints_paths are filtered by the keys in var_list_dict, that is if the
    key doesn't exist in var_list_dict then it's not added to the result

    For example, if the model is trained from scratch, both checkpoint_paths are empty
    and are therefore not loaded

    :param checkpoint_paths_dict:
    :param var_list_dict:
    :return: A list of tuples (key, vars_list, checkpoint_path)
    """

    assert set(var_list_dict.keys()).issubset(set(checkpoint_paths_dict.keys())),\
        'The keys for var_list_dict must be a subset of the keys for checkpoint_paths'

    return [(key, var_list_dict[key], checkpoint_paths_dict[key]) for key in var_list_dict if checkpoint_paths_dict[key]]


def get_init_fn(checkpoint_paths_dict, var_list_dict):
    # var_list_dict is a dictionary of namedtuples that each has the fields allow_restore_from_scratch
    # and vars_list.
    # Maybe change the semantics of 'from_scratch' to mean that everything is loaded completely from scratch
    # and then set a flag for the ones you need each time of we need to train like that

    saver_list = []
    for key, vars_list, checkpoint_path in get_var_and_checkpoint_list(checkpoint_paths_dict, var_list_dict):
            saver_list.append((key, tf.train.Saver(vars_list), checkpoint_path))

    def callback(session):
        for k, saver, model_path in saver_list:
            saver.restore(session, model_path)
            print('Model ({}) loaded from {}'.format(k, model_path))

    return callback

