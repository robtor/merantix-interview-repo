import tensorflow as tf
import numpy as np

ENCODER_VARIABLE_SCOPE = 'encoder'
DECODER_VARIABLE_SCOPE = 'decoder'

_VAR_EPS = 1e-1


# CODE from network.py ---------------------------------------------------------


WEIGHTS_COLLECTION_NAME = 'weights_coll'
BIASES_COLLECTION_NAME = 'biases_coll'


def lrelu(x, leak=0.01):
    """ Leaky ReLu """
    return tf.maximum(leak * x, x, name="lrelu")


def set_backwards_pass(op, backwards):
    """
    Returns new operation op' which behaves like op in the forward pass but
    like backwards in the backwards pass.
    """
    return backwards + tf.stop_gradient(op - backwards)


def forced_clipper(t, clip_value_min, clip_value_max):
    """ Clip t to range, but make sure gradient is 1 outside the range. """
    clip = tf.clip_by_value(t, clip_value_min, clip_value_max)
    return set_backwards_pass(clip, tf.identity(t))


def conv_layer_from(x, pad, shape, in_c, out_c, stride, nl, name):
    W = _weight_variable([shape, shape, in_c, out_c], name)
    b = _bias_variable([out_c], name)
    op = _conv2d(x, W, stride, pad) + b
    return op if nl is None else nl(op)


def _conv2d(x, W, stride, padding='SAME'):
    return tf.nn.conv2d(x, W, strides=[1, stride, stride, 1], padding=padding)


def deconv_layer_from(x, pad, shape, in_c, out_c, upscale, nl, name):
    """ r is the upscaling ratio """
    assert isinstance(upscale, int), 'Expected integer for upscale factor'

    # the filter shape for the deconv operation is supposed to be in a different order than the one for the conv
    # operation, namely [w, h, out_c, in_c] as opposed to [w, h, in_c, out_c]
    W = _weight_variable([shape, shape, out_c, in_c], name)
    b = _bias_variable([out_c], name)

    op = _deconv2d(x, W, out_c, upscale, padding=pad) + b
    return op if nl is None else nl(op)


def _deconv2d(x, W, out_c, upscaling_ratio, padding='SAME'):
    # NOTE: this prevents it from being fully convolutional
    # could probably be:
    #  x_shape = tf.shape(x)
    #  batch_size, w, h = x_shape[0], x_shape[1], x_shape[2]
    batch_size, w, h, _ = x.shape.as_list()  
    output_shape = [batch_size, w * upscaling_ratio, h * upscaling_ratio, out_c]
    return tf.nn.conv2d_transpose(x, W, output_shape, strides=[1, upscaling_ratio, upscaling_ratio, 1], padding=padding)


def _weight_variable(shape, name):
    with tf.variable_scope(name):
        # Hacky, but need to change this in the future anyway
        with tf.device('/cpu:0'):
            initializer = tf.contrib.layers.xavier_initializer(uniform=False)
            return tf.get_variable(
                    'weights', shape,
                    initializer=initializer,
                    collections=[tf.GraphKeys.GLOBAL_VARIABLES, 
                                 WEIGHTS_COLLECTION_NAME])


def _bias_variable(shape, name):
    with tf.variable_scope(name):
        # Hacky, but need to change this in the future anyway
        with tf.device('/cpu:0'):
            return tf.get_variable('biases', shape,
                                initializer=tf.constant_initializer(0.1),
                                collections=[tf.GraphKeys.GLOBAL_VARIABLES,    
                                             BIASES_COLLECTION_NAME])


def get_weights():
    return tf.get_collection(WEIGHTS_COLLECTION_NAME)


def get_biases():
    return tf.get_collection(BIASES_COLLECTION_NAME)


# DECODER code -----------------------------------------------------------------


def decoder(quantized_features_vol):
    with tf.variable_scope(DECODER_VARIABLE_SCOPE):
        c = 32
        h8 = deconv_layer_from(quantized_features_vol, 'SAME',
                               shape=3, in_c=c, out_c=128, upscale=2, nl=None, name='h8')
        h9 = residual_block(h8, 'dec1')
        h10 = residual_block(h9, 'dec2')
        h11 = residual_block(h10, 'dec3')
        h12 = deconv_layer_from(h11, 'SAME', shape=3, in_c=128, out_c=64, upscale=2, nl=lrelu, name='h12')
        h13 = deconv_layer_from(h12, 'SAME', shape=3, in_c=64, out_c=3, upscale=2, nl=None, name='h13')
        h13 = denormalize(h13)
        h_13_clipped = forced_clipper(h13, 0., 255.)
        return h_13_clipped


def residual_block(h, name, nl_for_next_conv=None):
    name = 'residual_' + name
    h_into_hr_1 = h if nl_for_next_conv is None else nl_for_next_conv(h)
    hr_1 = conv_layer_from(h_into_hr_1, 'VALID',
                           shape=3, in_c=128, out_c=128, stride=1, nl=lrelu, name=name + '_hr1')
    hr_2 = conv_layer_from(hr_1, 'VALID',
                           shape=3, in_c=128, out_c=128, stride=1, nl=None, name=name + '_hr2')
    # bring hr_2 to the same dimension as h via padding, so that addition is defined.
    # Not sure if this is something that we want.
    hr_2_pad = tf.pad(hr_2, [[0, 0], [2, 2], [2, 2], [0, 0]], mode='REFLECT', name=name+'_hr2_pad')
    return h + hr_2_pad


def denormalize(image_batch):
    mean = np.array([122.4, 110.2, 94.2], dtype=np.float32)
    var = np.array([4702.01626, 4235., 4715.], dtype=np.float32)
    return image_batch * tf.sqrt(var + _VAR_EPS) + mean
