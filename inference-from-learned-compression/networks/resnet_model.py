import tensorflow as tf

import tensorflow.contrib.slim as slim

from models.slim.nets import resnet_v1
from models.slim.nets import resnet_utils

def inference(processed_image, num_classes=1000, reuse=None, is_training=True, scope='resnet'):
    # Create the model, use the default arg scope to configure the batch norm parameters.
    with slim.arg_scope(resnet_v1.resnet_arg_scope()):
        # 1000 classes instead of 1001.
        logits, end_points = resnet_v1.resnet_v1_50(processed_image,
                                            num_classes=num_classes,
                                            is_training=is_training,
                                            reuse=reuse,
                                            scope=scope)

    return logits, end_points


def inference_chopped(processed_image, blocks, num_classes=1000, reuse=None, is_training=True, scope='resnet'):
    # Create the model, use the default arg scope to configure the batch norm parameters.

    with slim.arg_scope(resnet_v1.resnet_arg_scope()):
        # original version
        #logits, end_points = resnet_v1.resnet_v1_50(input_tensor, num_classes=num_classes, is_training=is_training)

        logits, end_points = resnet_v1.resnet_v1(processed_image,
                                                blocks, num_classes, is_training,
                                                global_pool=True, output_stride=None,
                                                include_root_block=False, reuse=reuse,
                                                scope=scope)

    return logits, end_points


def get_total_loss(logits, labels, num_classes=1000):
    with tf.name_scope("my_scope"):
        one_hot_labels = slim.one_hot_encoding(labels, num_classes)
        slim.losses.softmax_cross_entropy(logits, one_hot_labels)
        total_loss = slim.losses.get_total_loss()

        tf.summary.scalar('loss', total_loss)

    return total_loss

def get_train_ops(total_loss, variables_to_train=None, learn_rate=0.0001, momentum=0.9):
    optimizer = tf.train.MomentumOptimizer(learning_rate=learn_rate, momentum=momentum)

    # By default, slim.learning.create_train_op includes all update ops that are
    # part of the `tf.GraphKeys.UPDATE_OPS` collection
    train_op = slim.learning.create_train_op(total_loss, optimizer, variables_to_train=variables_to_train)

    return train_op


def get_top_k(logits, labels):
    with tf.name_scope('top_k'):
        top_1_op = tf.nn.in_top_k(logits, labels, 1)
        top_5_op = tf.nn.in_top_k(logits, labels, 5)

        top_1_op = tf.reduce_mean(tf.cast(top_1_op, tf.float32))
        top_5_op = tf.reduce_mean(tf.cast(top_5_op, tf.float32))

        #tf.summary.scalar('top1', top_1_op)
        #tf.summary.scalar('top5', top_5_op)

    return top_1_op, top_5_op
