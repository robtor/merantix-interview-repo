import sys
sys.path.append('./models/slim')

import tensorflow as tf

from tensorflow.python.ops import math_ops
from models.slim.nets import resnet_v1
from networks.resnet_blocks_mapping import networks_map

slim = tf.contrib.slim

INCLUDE_ROOT_BLOCK = True
SCOPE = 'resnet'
DATA_FORMAT = 'NHWC'
OUTPUT_STRIDE = 8
GLOBAL_POOL = False


def make_var(name, shape):
    '''Creates a new TensorFlow variable.'''
    return tf.get_variable(name, shape, trainable=True)


def validate_padding(padding):
    '''Verifies that the padding is one of the supported ones.'''
    assert padding in ('SAME', 'VALID')

def atrous_conv(input,
                k_h,
                k_w,
                c_o,
                dilation,
                name,
                relu=True,
                padding='SAME',
                group=1,
                biased=True):
    # Verify that the padding is acceptable
    validate_padding(padding)
    # Get the number of channels in the input
    c_i = input.get_shape()[-1]
    # Verify that the grouping parameter is valid
    assert c_i % group == 0
    assert c_o % group == 0
    # Convolution for a given input and kernel
    convolve = lambda i, k: tf.nn.atrous_conv2d(i, k, dilation, padding=padding)
    with tf.variable_scope(name) as scope:
        kernel = make_var('weights', shape=[k_h, k_w, c_i // group, c_o])
        if group == 1:
            # This is the common-case. Convolve the input without any further complications.
            output = convolve(input, kernel)
        else:
            # Split the input into groups and then convolve each of them independently
            input_groups = tf.split(3, group, input)
            kernel_groups = tf.split(3, group, kernel)
            output_groups = [convolve(i, k) for i, k in zip(input_groups, kernel_groups)]
            # Concatenate the groups
            output = tf.concat(3, output_groups)
        # Add the biases
        if biased:
            biases = make_var('biases', [c_o])
            output = tf.nn.bias_add(output, biases)
        if relu:
            # ReLU non-linearity
            output = tf.nn.relu(output, name=scope.name)
        return output


def resnet_50_basic(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):
    """ResNet-50 model of [1]. See resnet_v1() for arg and return description."""
    blocks = networks_map['resnet-50']

    with slim.arg_scope(arg_scope):
        net, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    spatial_squeeze=False, data_format=data_format)

        outputs = []
        num_classes_seg = 21
        current_name = 'fc1_voc12_c0'
        
        outputs.append(slim.conv2d(net, num_classes_seg, [1, 1],
                                   stride=1, padding='SAME',
                                   rate=1, normalizer_fn=None,
                                   activation_fn=None,
                                   scope=current_name))

        #for i, d_rate in enumerate(dilation_rates):
        #    current_name = 'fc1_voc12_org_c{}'.format(i)
        #    outputs.append(atrous_conv(net, 3, 3, num_classes_seg, d_rate, padding='SAME', relu=False, name=current_name))


        output = math_ops.add_n(outputs, name='fc1_voc12')

    return output, end_points



def resnet_50_aspp(inputs,
              arg_scope,
              num_classes=1000,
              is_training=True,
              global_pool=GLOBAL_POOL,
              output_stride=OUTPUT_STRIDE,
              reuse=None,
              scope=SCOPE,
              data_format=DATA_FORMAT):
    """ResNet-50 model of [1]. See resnet_v1() for arg and return description."""
    blocks = networks_map['resnet-50']

    with slim.arg_scope(arg_scope):
        net, end_points = resnet_v1.resnet_v1(inputs, blocks, num_classes, is_training,
                                    global_pool=global_pool, output_stride=output_stride,
                                    include_root_block=INCLUDE_ROOT_BLOCK, reuse=reuse, scope=scope,
                                    spatial_squeeze=False, data_format=data_format)

        num_classes_seg = 21
        outputs = []
        dilation_rates = [6, 12, 18, 24]

        # Experiment with the l2 and other stuff on the last layer
        for i, d_rate in enumerate(dilation_rates):
            current_name = 'fc1_voc12_c{}'.format(i)
            outputs.append(slim.conv2d(net, num_classes_seg, [3, 3],
                                       stride=1, padding='SAME',
                                       rate=d_rate, normalizer_fn=None,
                                       activation_fn=None,
                                       scope=current_name))

        #for i, d_rate in enumerate(dilation_rates):
        #    current_name = 'fc1_voc12_org_c{}'.format(i)
        #    outputs.append(atrous_conv(net, 3, 3, num_classes_seg, d_rate, padding='SAME', relu=False, name=current_name))


        output = math_ops.add_n(outputs, name='fc1_voc12')

    return output, end_points

