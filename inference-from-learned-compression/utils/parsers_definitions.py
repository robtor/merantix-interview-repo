import configargparse


def get_parser_train():
    parser = configargparse.ArgumentParser()

    # TODO A bit hacky, find a way to pass a list
    parser.add('-c0', '--config-file0',
               required=False,
               is_config_file=True,
               help='config file path')
    parser.add('-c1', '--config-file1',
               required=False,
               is_config_file=True,
               help='config file path')
    parser.add('-c2', '--config-file2',
               required=False,
               is_config_file=True,
               help='config file path')

    parser.add_argument("--batch-size",
                        type=int,
                        default=64,
                        help="Minibatch size fed into network")
    parser.add_argument("--num-examples",
                        type=int,
                        default=1281167,
                        help="Number of examples used in the run")
    parser.add_argument("--directory",
                        type=str,
                        help="Default directory to get data from")
    parser.add_argument("--summary-dir",
                        type=str,
                        default=None,
                        help="If supplied overrides the automatic folder-naming of summary_basedir. The directory to save tensorboard summary and checkpoints")
    parser.add_argument("--summary-basedir",
                        type=str,
                        default=None,
                        help="If summary_basedir is supplied and summary_dir is not supplied, the summary will be placed in an automatically generated folder named from hyperparameters in the summary_basedir. Target directory is then $summary_basedir/summary_{model_type}_{num_classes}_{network_name}_lr-{learning_rate}_bs-{batch_size}_gpu-{num_gpus}_{schedule}x_{suffix}")
    parser.add_argument('--suffix',
                        type=str,
                        default='',
                        help='Additional suffix for the automatically generated name for the summary dir')
    parser.add_argument("--checkpoint-dir",
                        type=str,
                        help="Directory to load checkpoint from")
    parser.add_argument("--checkpoint-name",
                        type=str,
                        help="""
                        Name of checkpoint to restore. Defaults to latest checkpoint saved in
                        the folder""")
    parser.add_argument('--synsets-filename',
                        type=str,
                        default='./input/imagenet_lsvrc_2015_synsets.txt',
                        help="File that includes all synset names")
    parser.add_argument('--num-classes',
                        type=int,
                        default=1000,
                        help="Number of classes to predict")
    parser.add_argument('--extension',
                        type=str,
                        default='tfrecord',
                        help='Extension of the imagefiles. For example png, jpeg, tfrecord, etc')
    parser.add_argument('--learning-rate',
                        type=float,
                        default=0.1,
                        help="""
                        Learning rate of the optimizer. If the learning_rate_type is \'schedule\'
                        this is the initial learning rate""")
    parser.add_argument('--batch-norm-decay',
                        type=float,
                        default=0.997,
                        help='')
    parser.add_argument('--weight-decay',
                        type=float,
                        default=0.0001,
                        help='')
    parser.add_argument('--learning-rate-type',
                        type=str,
                        default='constant',
                        help='Type of learning rate, \'constant\' or \'schedule\'')
    parser.add_argument('--label-offset',
                        type=int,
                        default=0,
                        help='Offset of the labels from 0')
    parser.add_argument('--save-checkpoint-steps',
                        type=int,
                        default=2000,
                        help='Checkpoints are saved every save_checkpoint_steps')
    parser.add_argument('--epochs-decay',
                        type=int,
                        default=30,
                        help='After how many epochs the learning rate is reduced by 1/10')
    parser.add_argument('--variables-to-train',
                        type=str,
                        default='all',
                        help="""
                        The variables that are passed to the optimizer and therefore
                        updated. Options: \'all\' and \'finetune\'
                        """)
    parser.add_argument('--variables-to-restore',
                        type=str,
                        default='all',
                        help="""
                        The variables that are loaded from the given checkpoint.
                        Options: \'all\' and \'finetune\'
                        """)
    parser.add_argument('--map-name-to-dim-filename',
                        type=str,
                        default='./input/filename_shape_map_train.txt',
                        help='')
    parser.add_argument('--print-variable-origin',
                        dest='print_variable_origin',
                        action='store_true',
                        help='Prints all input variables value and their source')
    parser.add_argument('--centers-filename',
                        type=str,
                        help='Path to the file containing the \'centers\' of the compressed model')
    parser.add_argument('--operating-point',
                        type=int,
                        default=32,
                        help='Path to the file containing the \'centers\' of the compressed model')
    parser.add_argument('--input-type',
                        type=str,
                        default='symbols',
                        help='Only applicable to bottleneck. Values: feature_volume or symbols')
    parser.add_argument('--model-type',
                        type=str,
                        default='image',
                        help='Whether the input is bottlenecks or images or compressed images. Values: bottleneck, compressed and image')
    parser.add_argument('--num-gpus',
                        type=int,
                        default=1,
                        help='Number of gpus to use')
    parser.add_argument('--network-name',
                        type=str,
                        default='resnet-50',
                        help='Name of the architecture to use. Valid strings: chopped-1, chopped-2, resnet-50')
    parser.add_argument('--num-preprocess-threads',
                        type=int,
                        default=4,
                        help='Whether the input is bottlenecks or images. Values: bottleneck and image')
    parser.add_argument('--no-scale-augmentation', dest='scale_augmentation', action='store_false',
                        help="Sets no scale augmentation")
    parser.add_argument('--scale-augmentation', dest='scale_augmentation', action='store_true',
                        help="Sets scale augmentation")
    parser.set_defaults(scale_augmentation=True)

    parser.add_argument('--no-rand-flip', dest='rand_flip', action='store_false',
                        help="Sets random flip for bottlenecks")
    parser.add_argument('--rand-flip', dest='rand_flip', action='store_true',
                        help="Sets random flip for bottlenecks")
    parser.set_defaults(rand_flip=False)
    parser.add_argument('--no-rand-crop', dest='rand_crop', action='store_false',
                        help="Sets random flip for bottlenecks")
    parser.add_argument('--rand-crop', dest='rand_crop', action='store_true',
                        help="If the preprocessing does random cropping of input images")
    parser.set_defaults(rand_crop=True)
    parser.add_argument('--no-subtract-mean', dest='subtract_mean', action='store_false',
                        help="If preprocessing subtracts the image mean")
    parser.add_argument('--subtract-mean', dest='subtract_mean', action='store_true',
                        help="If preprocessing subtracts the image mean")
    parser.set_defaults(subtract_mean=True)
    parser.add_argument('--image-output-dim',
                        type=int,
                        default=224,
                        help='Input spatial dim of the feature volume')

    parser.add_argument('--input-resize-factor',
                        type=float,
                        default=1,
                        help='If the input to the network is supposed to be scaled.')
    parser.add_argument('--feat-vol-input-size',
                        type=int,
                        default=28,
                        help='Input spatial dim of the feature volume')

    parser.add_argument('--nesterov', dest='nesterov', action='store_true',
                        help="Sets type of momentum to nesterov")
    parser.add_argument('--no-nesterov', dest='nesterov', action='store_false',
                        help="Sets type of momentum to regular (no nesterov)")
    parser.set_defaults(nesterov=False)

    parser.add_argument('--from-scratch', dest='from_scratch', action='store_true',
                        help="""
                        Flag that ensures that no variables are loaded and model
                        is initialized from scratch""")
    parser.set_defaults(from_scratch=False)
    parser.add_argument('--overwrite', dest='overwrite', action='store_true',
                        help="""
                        If user wants run training from scratch and overwrite old
                        checkpoints""")
    parser.set_defaults(overwrite=False)
    parser.add_argument('--data-format',
                        type=str,
                        default='NHWC',
                        help='Format of the inputs. Possibilities NHWC and NCHW (faster)')
    parser.add_argument('--encoder-decoder-dir',
                        type=str,
                        help='')
    parser.add_argument('--encoder-decoder-checkpoint-name',
                        type=str,
                        help='')
    parser.add_argument('--load-from-encoder-decoder-dir', dest='load_from_encoder_decoder_dir', action='store_true',
                        help="Sets random flip for bottlenecks")
    parser.set_defaults(load_from_encoder_decoder_dir=False)
    parser.add_argument('--add-histograms', dest='add_histograms', action='store_true',
                        help="Adds histograms of gradients and variables to summaries")
    parser.set_defaults(add_histograms=False)
    parser.add_argument('--add-joint-network-loss', dest='add_joint_network_loss', action='store_true',
                        help="Adds the loss of the joint network")
    parser.set_defaults(add_joint_network_loss=False)
    parser.add_argument('--frozen-encoder-decoder', dest='frozen_encoder_decoder', action='store_true',
                        help="Adds the loss of the joint network")
    parser.set_defaults(frozen_encoder_decoder=False)
    parser.add_argument('--only-encoder-decoder', dest='only_encoder_decoder', action='store_true',
                        help="Only train the encoder and the decoder for the joint-training")
    parser.set_defaults(only_encoder_decoder=False)
    parser.add_argument('--restore-global-step', dest='restore_global_step', action='store_true',
                        help='')
    parser.add_argument('--not-restore-global-step', dest='restore_global_step', action='store_false',
                        help='')
    parser.set_defaults(restore_global_step=True)
    parser.add_argument('--debug-print', dest='debug_print', action='store_true',
                        help='')
    parser.set_defaults(debug_print=False)
    parser.add_argument('--loss-scale-factor',
                        type=float,
                        default=1.0,
                        help="""
                             The scale between mse loss (compression) and softmax loss (classification).
                             tot_loss = mse / loss_scale_factor + softmax
                             """)
    parser.add_argument('--beta',
                        type=float,
                        default=50.0,
                        help='Scaling factor on entropy loss')
    parser.add_argument('--max-entropy',
                        type=float,
                        default=1.1,
                        help='Scaling factor on entropy loss')


    return parser


def get_parser_validation():
    parser = configargparse.ArgumentParser()


    # TODO A bit hacky, find a way to pass a list
    parser.add('-c0', '--config-file0',
               required=False,
               is_config_file=True,
               help='config file path')
    parser.add('-c1', '--config-file1',
               required=False,
               is_config_file=True,
               help='config file path')
    parser.add('-c2', '--config-file2',
               required=False,
               is_config_file=True,
               help='config file path')

    parser.add_argument("--batch-size",
                        type=int,
                        default=100,
                        help="Minibatch size fed into network")
    parser.add_argument("--num-examples",
                        type=int,
                        default=50000,
                        help="Number of examples used in the run")
    parser.add_argument("--directory",
                        type=str,
                        default="/scratch_net/tractor/robertto/imagenet_256/roberto",
                        help="Default directory to get data from")
    parser.add_argument("--summary-dir",
                        type=str,
                        default='./val_summary',
                        help="The directory to save tensorboard summary")
    parser.add_argument("--checkpoint-dir",
                        type=str,
                        help="Folder that contains the checkpoint")
    parser.add_argument("--checkpoint-name",
                        type=str,
                        help="Checkpoint name. If empty selects the newest checkpoint in folder")
    parser.add_argument('--extension',
                        type=str,
                        default='tfrecord')
    parser.add_argument('--synsets-filename',
                        type=str,
                        default='input/imagenet_lsvrc_2015_synsets.txt')
    parser.add_argument('--output-filename',
                        type=str,
                        default='output_val_results.txt')
    parser.add_argument('--label-offset',
                        type=int,
                        default=0,
                        help='Offset of the labels from 0')
    parser.add_argument('--num-classes',
                        type=int,
                        default=1000,
                        help="Number of classes to predict")
    parser.add_argument('--centers-filename',
                        type=str,
                        help='Path to the file containing the \'centers\' of the compressed model')
    parser.add_argument('--operating-point',
                        type=int,
                        default=32,
                        help='Path to the file containing the \'centers\' of the compressed model')
    parser.add_argument('--input-type',
                        type=str,
                        default='symbols',
                        help='Only applicable to bottleneck. Values: feature_volume or symbols')
    parser.add_argument('--model-type',
                        type=str,
                        default='image',
                        help='Whether the input is bottlenecks or images. Values: bottleneck and image')
    parser.add_argument('--network-name',
                        type=str,
                        default='chopped-1',
                        help='Name of the architecture to use for bottlenecks')
    parser.add_argument('--num-preprocess-threads',
                        type=int,
                        default=1,
                        help='Whether the input is bottlenecks or images. Values: bottleneck and image')
    parser.add_argument('--print-variable-origin',
                        dest='print_variable_origin',
                        action='store_true',
                        help='Prints all input variables value and their source')
    parser.add_argument('--map-name-to-dim-filename',
                        type=str,
                        default='./input/filename_shape_map_val.txt',
                        help='')
    parser.add_argument('--input-resize-factor',
                        type=float,
                        default=1,
                        help='If the input to the network is supposed to be scaled.')
    parser.add_argument('--feat-vol-input-size',
                        type=int,
                        default=28,
                        help='Input spatial dim of the feature volume')
    parser.add_argument('--image-output-dim',
                        type=int,
                        default=224,
                        help='Input spatial dim of the feature volume')
    parser.add_argument('--data-format',
                        type=str,
                        default='NHWC',
                        help='Format of the inputs. Possibilities NHWC and NCHW (faster)')
    parser.add_argument('--no-subtract-mean', dest='subtract_mean', action='store_false',
                        help="If preprocessing subtracts the image mean")
    parser.add_argument('--subtract-mean', dest='subtract_mean', action='store_true',
                        help="If preprocessing subtracts the image mean")
    parser.set_defaults(subtract_mean=True)
    parser.add_argument('--load-from-encoder-decoder-dir', dest='load_from_encoder_decoder_dir', action='store_true',
                        help="Sets random flip for bottlenecks")
    parser.set_defaults(load_from_encoder_decoder_dir=False)
    parser.add_argument('--encoder-decoder-dir',
                        type=str,
                        help='')
    parser.add_argument('--encoder-decoder-checkpoint-name',
                        type=str,
                        help='')
    parser.add_argument('--save-per-class-accuracy', dest='save_per_class_accuracy', action='store_true',
                        help="Weather to also save a file for each run with per class accuracy")
    parser.add_argument('--no-save-per-class-accuracy', dest='save_per_class_accuracy', action='store_false',
                        help="Not to also save a file for each run with per class accuracy")
    parser.set_defaults(save_per_class_accuracy=False)

    return parser


def get_parser_batch_norm():
    parser = configargparse.ArgumentParser()

    # TODO A bit hacky, find a way to pass a list
    parser.add('-c0', '--config-file0',
               required=False,
               is_config_file=True,
               help='config file path')
    parser.add('-c1', '--config-file1',
               required=False,
               is_config_file=True,
               help='config file path')
    parser.add('-c2', '--config-file2',
               required=False,
               is_config_file=True,
               help='config file path')

    parser.add_argument("--batch-size",
                        type=int,
                        default=64,
                        help="Minibatch size fed into network")
    parser.add_argument("--directory",
                        type=str,
                        help="Default directory to get data from")
    parser.add_argument("--summary-dir",
                        type=str,
                        default=None,
                        help="If supplied overrides the automatic folder-naming of summary_basedir. The directory to save tensorboard summary and checkpoints")
    parser.add_argument("--summary-basedir",
                        type=str,
                        default=None,
                        help="If summary_basedir is supplied and summary_dir is not supplied, the summary will be placed in an automatically generated folder named from hyperparameters in the summary_basedir. Target directory is then $summary_basedir/summary_{model_type}_{num_classes}_{network_name}_lr-{learning_rate}_bs-{batch_size}_gpu-{num_gpus}_{schedule}x_{suffix}")
    parser.add_argument('--suffix',
                        type=str,
                        default='',
                        help='Additional suffix for the automatically generated name for the summary dir')
    parser.add_argument("--checkpoint-dir",
                        type=str,
                        help="Directory to load checkpoint from")
    parser.add_argument("--checkpoint-name",
                        type=str,
                        help="""
                        Name of checkpoint to restore. Defaults to latest checkpoint saved in
                        the folder""")
    parser.add_argument('--synsets-filename',
                        type=str,
                        default='./input/imagenet_lsvrc_2015_synsets.txt',
                        help="File that includes all synset names")
    parser.add_argument('--num-classes',
                        type=int,
                        default=1000,
                        help="Number of classes to predict")
    parser.add_argument('--extension',
                        type=str,
                        default='tfrecord',
                        help='Extension of the imagefiles. For example png, jpeg, tfrecord, etc')
    parser.add_argument('--batch-norm-decay',
                        type=float,
                        default=0.9,
                        help='')
    parser.add_argument('--label-offset',
                        type=int,
                        default=0,
                        help='Offset of the labels from 0')
    parser.add_argument('--variables-to-restore',
                        type=str,
                        default='all',
                        help="""
                        The variables that are loaded from the given checkpoint.
                        Options: \'all\' and \'finetune\'
                        """)
    parser.add_argument('--map-name-to-dim-filename',
                        type=str,
                        default='./input/filename_shape_map_train.txt',
                        help='')
    parser.add_argument('--print-variable-origin',
                        dest='print_variable_origin',
                        action='store_true',
                        help='Prints all input variables value and their source')
    parser.add_argument('--centers-filename',
                        type=str,
                        help='Path to the file containing the \'centers\' of the compressed model')
    parser.add_argument('--operating-point',
                        type=int,
                        default=32,
                        help='Path to the file containing the \'centers\' of the compressed model')
    parser.add_argument('--input-type',
                        type=str,
                        default='symbols',
                        help='Only applicable to bottleneck. Values: feature_volume or symbols')
    parser.add_argument('--model-type',
                        type=str,
                        default='image',
                        help='Whether the input is bottlenecks or images or compressed images. Values: bottleneck, compressed and image')
    parser.add_argument('--save-steps',
                        type=int,
                        default=-1,
                        help='How many steps to perform when calculating the batch_norm_average for a fixed model')
    parser.add_argument('--max-steps',
                        type=int,
                        default=-1,
                        help='How many steps to perform when calculating the batch_norm_average for a fixed model')
    parser.add_argument('--network-name',
                        type=str,
                        default='resnet-50',
                        help='Name of the architecture to use. Valid strings: chopped-1, chopped-2, resnet-50')
    parser.add_argument('--num-preprocess-threads',
                        type=int,
                        default=1,
                        help='Whether the input is bottlenecks or images. Values: bottleneck and image')
    parser.add_argument('--data-format',
                        type=str,
                        default='NHWC',
                        help='Format of the inputs. Possibilities NHWC and NCHW (faster)')
    parser.add_argument('--no-scale-augmentation', dest='scale_augmentation', action='store_false',
                        help="Sets no scale augmentation")
    parser.add_argument('--scale-augmentation', dest='scale_augmentation', action='store_true',
                        help="Sets scale augmentation")
    parser.set_defaults(scale_augmentation=True)

    parser.add_argument('--no-rand-flip', dest='rand_flip', action='store_false',
                        help="Sets random flip for bottlenecks")
    parser.add_argument('--rand-flip', dest='rand_flip', action='store_true',
                        help="Sets random flip for bottlenecks")
    parser.set_defaults(rand_flip=False)
    parser.add_argument('--save-intermittent', dest='save_intermittent', action='store_true',
                        help="Set if the script is supposed to save intermittent checkpoints")
    parser.set_defaults(save_intermittent=False)
    parser.add_argument('--input-resize-factor',
                        type=float,
                        default=1,
                        help='If the input to the network is supposed to be scaled.')
    parser.add_argument('--feat-vol-input-size',
                        type=int,
                        default=28,
                        help='Input spatial dim of the feature volume')
    return parser
