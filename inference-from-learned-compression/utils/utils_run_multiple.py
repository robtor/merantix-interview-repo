import glob
import os
import re
import sys

sys.path.append('./vidcompress')

from networks.nets_class import networks

NETWORK_NAMES = networks


def get_enc_dec_map(dir_path):
    # Assumes that the folder has subfolders on the form
    # /srv/glusterfs/robertto/imagenet/compression_runs/0915_1533 pp:shared_centers_c12_nips_lr1e*3_bn
    # and returns a dictionary that has mappings such as:
    # {'0915-1533': '/srv/glusterfs/robertto/imagenet/compression_runs/0915_1533 pp:shared_centers_c12_nips_lr1e*3_bn'}

    filenames = glob.glob(os.path.join(dir_path, '*'))
    return {os.path.basename(f_name).split(' ')[0].replace('_', '-'): f_name for f_name in filenames}


def check_valid_dir_name(dir_name, config_mapping, network_names=NETWORK_NAMES):
    folder_name = os.path.basename(os.path.normpath(dir_name))

    if len(folder_name.split('_')) < 5:
        return False

    model_type, num_classes, schedule, network_name = folder_name.split('_')[1:5]
    x1 = network_name in network_names
    x2 = model_type + '_' + num_classes in config_mapping

    return x1 and x2


def get_config_mapping_key_and_network_name(folder_name):
    model_type, num_classes, _, network_name = folder_name.split('_')[1:5]

    # This all depends on the exact format for the filename
    mapping_key = model_type + '_' + num_classes

    return mapping_key, network_name


def get_data_from_folder_name(folder_name, config_dir, config_mapping):
    mapping_key, network_name = get_config_mapping_key_and_network_name(folder_name)

    config_path = os.path.join(config_dir, config_mapping[mapping_key])

    return config_path, network_name


def get_jp2k_data_path(folder_name):
    bpp = get_number_with_prefix('bpp-', folder_name, default_val=-1)

    if bpp == -1:
        raise ValueError('Must return a bpp- number from jp2k folder. Got {}'.format(bpp))

    return '/srv/glusterfs/robertto/imagenet/imagenet_256/images_val_256_jp2k_bpp-target-{}_tfrecord'.format(bpp)


def check_if_model_exists(dir_name, index, model_basename='model.ckpt'):

    file_endings = ['-{}.data-00000-of-00001',
                    '-{}.index',
                    '-{}.meta']
    filenames = [model_basename + ending for ending in file_endings]

    x = True
    for f_name in filenames:
        x = x and os.path.exists(os.path.join(dir_name, f_name.format(index)))

    return x


def get_models_list(dir_name,
                    models_finished,
                    model_basename='model.ckpt',
                    start_point=0,
                    end_point=2000000,
                    steps=20000):
    """
    end_point is inclusive

    possibilities for some more complex logic, such as
    all between start_point and end_point. Good enough 
    fow now
    """

    models = []

    for i in range(start_point, end_point+1, steps):
        # FIX W BASENAME
        model_name = model_basename + '-{}'
        model_name = model_name.format(i)

        model_is_finished = model_name in models_finished
        model_files_exist = check_if_model_exists(dir_name, i,
                                             model_basename=model_basename) 

        if model_files_exist and (not model_is_finished):
            models.append(model_name)

    return models

def parse_output_filename(output_filename, delimiter=','):
    """
    Parses the output file to see which runs have
    already been performed and can therefore be skipped    

    Built in assumptions: Comma seperated and first column
    has elements on the format model.ckpt-{$idx}
    """

    models_finished = []

    if os.path.exists(output_filename):
        with open(output_filename, 'r') as f:
            for line in f:
                models_finished.append(line.strip().split(delimiter)[0])

    return models_finished


def parse_output_folder(output_filename, delimiter=','):
    """
    Parses the output folder for files to see which runs have
    already been performed and can therefore be skipped

    Built in assumptions: Basic name is enough to describe
    the output files
    """
    pass


def get_number_with_prefix(prefix, input_string, default_val=1):
    val = default_val
    re_pattern = '{}\d*\.\d+|{}\d+'.format(prefix, prefix)
    match = re.search(re_pattern, input_string)

    if match:
        offset = len(prefix)
        val = float(input_string[match.start()+offset:match.end()])

    return val


def get_enc_dec_mapping_number(prefix, input_string, default_val=None):
    val = default_val
    re_pattern = '%s[0-9]{4}-[0-9]{4}' % prefix
    match = re.search(re_pattern, input_string)

    if match:
        offset = len(prefix)
        val = input_string[match.start()+offset:match.end()]

    return val


def get_preprocessing_flags(folder_name):
    if 'no-scale-aug' in folder_name:
        scale_aug_flag = '--no_scale_augmentation'
    else:
        scale_aug_flag = '--scale_augmentation'

    if 'w-rand-flip' in folder_name:
        rand_flip_flag = '--rand_flip'
    else:
        rand_flip_flag = '--no_rand_flip'

    input_resize_factor = get_number_with_prefix('resize-', folder_name)

    return scale_aug_flag, rand_flip_flag, input_resize_factor

def print_dir_names(dir_names, config_mapping):

    for dir_name in dir_names:
        if check_valid_dir_name(dir_name, config_mapping):
            print(dir_name)


def main(FLAGS):
    pass


if __name__ == '__main__':
    main()
