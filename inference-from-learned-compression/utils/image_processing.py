import glob
import os

import tensorflow as tf

from utils import utils_image_processing as utils

_MEAN_RGB = [123.680, 116.779, 103.939]
_BASE_IMAGE_HEIGHT = 256
_IMAGE_OUTPUT_DIM = 224

_RESIZE_SIDE_MIN = 256
_RESIZE_SIDE_MAX = 512


class ImageProcessingConfig(object):

    def __init__(self,
                 is_training,
                 data_format='NHWC',
                 scale_augmentation=False,
                 rand_flip=True,
                 rand_crop=True,
                 subtract_mean=True,
                 input_resize_factor=1,
                 image_output_dim=_IMAGE_OUTPUT_DIM):

        assert data_format in ('NHWC', 'NCHW')

        # Use a dictionary if these values keep changing
        self.is_training = is_training
        self.data_format = data_format
        self.scale_augmentation = scale_augmentation
        self.rand_flip = rand_flip
        self.rand_crop = rand_crop
        self.subtract_mean = subtract_mean
        self.input_resize_factor = input_resize_factor
        self.image_output_dim = image_output_dim
        self.rescale_input = input_resize_factor != 1
        self.mean_rgb = _MEAN_RGB

        self.resize_side_min = int(round(input_resize_factor * _RESIZE_SIDE_MIN))
        self.resize_side_max = int(round(input_resize_factor * _RESIZE_SIDE_MAX))

        self.input_target_size = int(round(input_resize_factor * _BASE_IMAGE_HEIGHT))
        self.output_height = int(image_output_dim * input_resize_factor)
        self.output_width = int(image_output_dim * input_resize_factor)
        self.output_shape = [self.output_height, self.output_width, 3]

    def print_preprocessing(self):
        print('====== Preprocessing ======')
        if self.is_training:
            print('is_training: {}'.format(self.is_training))
            print('data_format: {}'.format(self.data_format))
            print('rand_flip: {}'.format(self.rand_flip))
            print('rand_crop: {}'.format(self.rand_crop))
            print('subtract_mean: {}'.format(self.subtract_mean))
            print('scale_augmentation: {}'.format(self.scale_augmentation))
            print('input_resize_factor: {}'.format(self.input_resize_factor))
            print('input_target_size: {}'.format(self.input_target_size))
            print('image_output_dim: {}'.format(self.image_output_dim))
            print('output_shape: {}'.format(self.output_shape))
        else:
            print('is_training: {}'.format(self.is_training))
            print('data_format: {}'.format(self.data_format))
            print('subtract_mean: {}'.format(self.subtract_mean))
            print('input_resize_factor: {}'.format(self.input_resize_factor))
            print('input_target_size: {}'.format(self.input_target_size))
            print('image_output_dim: {}'.format(self.image_output_dim))
            print('output_shape: {}'.format(self.output_shape))

        print('=======================')


def get_image_processing_config_from_flags(is_training, flags):
    if is_training:
        return ImageProcessingConfig(is_training,
                                     data_format=flags.data_format,
                                     scale_augmentation=flags.scale_augmentation,
                                     rand_flip=flags.rand_flip,
                                     rand_crop=flags.rand_crop,
                                     subtract_mean=flags.subtract_mean,
                                     input_resize_factor=flags.input_resize_factor,
                                     image_output_dim=flags.image_output_dim)
    else:
        return ImageProcessingConfig(is_training,
                                     data_format=flags.data_format,
                                     subtract_mean=flags.subtract_mean,
                                     input_resize_factor=flags.input_resize_factor,
                                     image_output_dim=flags.image_output_dim,
                                     scale_augmentation=False,
                                     rand_flip=False,
                                     rand_crop=False)


def _parse_proto_example(serialized_example):
    features_dict = {
                    'image/encoded': tf.FixedLenFeature([], tf.string),
                    'image/class/label': tf.FixedLenFeature([], tf.int64),
                    'image/channels': tf.FixedLenFeature([], tf.int64)
                    }

    features = tf.parse_single_example(serialized_example, features=features_dict)

    image_decoded = tf.image.decode_image(features['image/encoded'], channels=3)
    label = features['image/class/label']

    return image_decoded, label


def preprocess_for_train(image, config):
    """Preprocesses the given image for training.

    Note that the actual resizing scale is sampled from
      [`resize_size_min`, `resize_size_max`].

    Args:
      image: A `Tensor` representing an image of arbitrary size.
      output_height: The height of the image after preprocessing.
      output_width: The width of the image after preprocessing.
      resize_side_min: The lower bound for the smallest side of the image for
        aspect-preserving resizing.
      resize_side_max: The upper bound for the smallest side of the image for
        aspect-preserving resizing.

    Returns:
      A preprocessed image.
    """

    # TODO Print all image_preprocessing flags at the beginning

    if config.rescale_input:
        image = utils.aspect_preserving_resize(image, config.input_target_size, resize_func=tf.image.resize_bicubic)

    if config.scale_augmentation:
        resize_side = tf.random_uniform([],
                                        minval=config.resize_side_min,
                                        maxval=config.resize_side_max+1,
                                        dtype=tf.int32)

        image = utils.aspect_preserving_resize(image, resize_side)

    if config.rand_crop:
        image = tf.random_crop(image, config.output_shape)
    else:
        image = utils.central_crop(image, config.output_height, config.output_width)

    image.set_shape(config.output_shape)

    if config.rand_flip:
        image = tf.image.random_flip_left_right(image)

    image = tf.to_float(image)

    if config.subtract_mean:
        image = image - tf.convert_to_tensor(config.mean_rgb)

    if config.data_format == 'NCHW':
        image = tf.transpose(image, [2, 0, 1])

    return image


def preprocess_for_eval(image, config):
    """Preprocesses the given image for evaluation.

    Args:
      image: A `Tensor` representing an image of arbitrary size.
      output_height: The height of the image after preprocessing.
      output_width: The width of the image after preprocessing.
      resize_side: The smallest side of the image for aspect-preserving resizing.

    Returns:
      A preprocessed image.
    """

    if config.rescale_input:
        image = utils.aspect_preserving_resize(image, config.input_target_size, resize_func=tf.image.resize_bicubic)

    image = utils.central_crop(image, config.output_height, config.output_width)
    image.set_shape(config.output_shape)
    image = tf.to_float(image)

    if config.subtract_mean:
        image = image - tf.convert_to_tensor(config.mean_rgb)

    if config.data_format == 'NCHW':
        image = tf.transpose(image, [2, 0, 1])

    return image


def parse_and_process(serialized_example, config):
    image, label = _parse_proto_example(serialized_example)

    if config.is_training:
        image = preprocess_for_train(image, config)
    else:
        image = preprocess_for_eval(image, config)

    return image, label


def parse_and_process_with_config(config):
    # Should maybe be a method in the class?
    def callback(serialized_example):
        return parse_and_process(serialized_example, config)
    return callback


def get_serialized_examples(filelist, num_epochs=None, shuffle=True):

    filename_queue = tf.train.string_input_producer(filelist,
                                                    num_epochs=num_epochs,
                                                    shuffle=shuffle)

    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)

    return serialized_example


def get_images(data_dir,
               batch_size=64,
               num_preprocess_threads=4,
               num_epochs=None,
               config=ImageProcessingConfig(True)):

    filenames = glob.glob(os.path.join(data_dir, '*.tfrecord'))
    filenames.sort()
    with tf.name_scope('preprocessing'):
        with tf.device('/cpu:0'):
            images_and_labels = []

            serialized_example = get_serialized_examples(filenames,
                                                         num_epochs=num_epochs,
                                                         shuffle=config.is_training)

            for thread_id in range(num_preprocess_threads):
                image, label = parse_and_process(serialized_example, config=config)
                images_and_labels.append([image, label])

            if config.is_training:
                images_batch, labels_batch = tf.train.shuffle_batch_join(images_and_labels,
                                                                         batch_size=batch_size,
                                                                         capacity=8000,
                                                                         min_after_dequeue=4000)
            else:
                images_batch, labels_batch = tf.train.batch_join(images_and_labels,
                                                                 batch_size=batch_size,
                                                                 capacity=500)

            # Dataset class does not allow to shuffle the input filenames so it's useless
            # parse_and_process_fn = parse_and_process_with_config(config)
            # buffer_size = 0
            # if config.is_training:
            #     buffer_size = 6000
            # dataset = tf.contrib.data.TFRecordDataset(filenames)
            # dataset = dataset.map(parse_and_process_fn, num_threads=num_preprocess_threads)
            # dataset = dataset.shuffle(buffer_size=buffer_size)
            # dataset = dataset.batch(batch_size)
            # dataset = dataset.repeat(num_epochs)
            # dataset_iterator = dataset.make_one_shot_iterator()
            # images_batch, labels_batch = dataset_iterator.get_next()

    return images_batch, labels_batch

