import os
import argparse
import tensorflow as tf
import numpy as np

NAME_TO_IDX_MAP = {'C': 0, 'h': 1, 'w': 2, 'n': 3, 'ph': 4, 'pw': 5, 'd': 6}
NUM_CHANNELS = 32


def create_session():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # Do not assign whole gpu memory, just use it on the go
    config.allow_soft_placement = True

    return tf.Session(config=config)


def conversion_op(symbols_tensor, centers_tensor, spec_tensor, d=NAME_TO_IDX_MAP, num_channels=NUM_CHANNELS):
    """Conversion op for compressed indexed bottlenecks to feature maps

    This function takes a bottleneck from the compression network and transforms
    it into a feature map by mapping indices of the symbols_tensor to the
    corresponding centers in the centers_tensor

    Args:
      symbols_tensor: The tensor to convert to feature volume.
        Has the size size [batch, h, w, num_channels].
      centers_tensor: A tensor that contains information used to map the
        indices to centers
      spec_tensor: A tensor that contains the information needed to reshape the
        symbols_tensor into a feature_map. Has the shape (7,) and has the elements
        correspond to the values

        C:          number of channels
        n:          number of patches per channel
        w, h:       size of the bottlneck
        pw, ph:     patch size
        d:          dimension of the patch, i.e., pw * ph

        using the mapping d to map values to indices

      d: A mapping that maps the names of the spec_tensor values to indices. Default
        to NAME_TO_IDX_MAP = {'C': 0, 'h': 1, 'w': 2, 'n': 3, 'ph': 4, 'pw': 5, 'd': 6}
      num_channels: Number of channels of the operating point (C). Needs to be
        passed as an int because it's used to create the graph

    Returns:
      feature_volume: A tensor of shape [batch_size, ?, ?, num_channels] that contains the
       feature map from symbols to feature volume
    """

    assert symbols_tensor.dtype == tf.int32, 'symbols must be int32'
    assert len(symbols_tensor.get_shape()) == 2, 'symbols must be (C, n) dimensional, got {}'.format(
        symbols_tensor.get_shape().as_list())
    assert len(centers_tensor.get_shape()) == 3, 'centers must be (C, d, num_centers)-dimensional, got {}'.format(
        centers_tensor.get_shape().as_list())

    C, n = symbols_tensor.get_shape().as_list()
    C_c, d_c, num_centers = centers_tensor.get_shape().as_list()
    # assert n == spec.n, 'Expected n == {}, got {}'.format(spec.n, n)
    assert C == C_c, 'Number of channels of symbols and centers must match, {} != {}'.format(C, C_c)
    # assert d_c == spec.d, 'Expected d == {}, got {}'.format(spec.d, d_c)

    # convert symbols tensor to (C, n, d)-dimensional patches tensor, where the i-th patch of the c-th channel is
    # given by patches[c, i, :] \in R^d
    patches = _symbols_to_patches(symbols_tensor, centers_tensor, num_centers, spec_tensor, d, num_channels)

    # convert to feature volume by rearanging the patches to a (1, w, h, C) tensor
    feature_volume = _inverse_of_extract_patches(patches, spec_tensor, d)

    return feature_volume


def _symbols_to_patches(symbols, centers, num_centers, spec_tensor, d, num_channels):
    # symbols is (C, n), each element is in {0, ..., num_centers-1}
    # we add to each row symbols[c, :] the integer c * num_centers, to make a unique index into
    # the centers matrix, because the centers matrix is (C, d, num_centers), one center is given by
    # centers[chan, :, l], so given some index i = (p + c * num_centers), p in {0, ..., num_centers-1},
    # c in {0, ..., C}, there is a unique center given by chan = i // num_centers, l = i % num_centers = p
    indices = symbols + tf.expand_dims(tf.range(num_channels) * num_centers, axis=1)

    # we reshape these unique indices to a (C*n, 1) matrix
    indices = tf.reshape(indices, (-1, spec_tensor[d['n']]))

    # transposing the centers results in it becoming of shape (C, num_centers, d), so...
    centers_t = tf.transpose(centers, perm=(0, 2, 1))
    # ... we can reshape to have a (C*num_centers, d) matrix, where row i is the center (i % num_centers) of
    # channel i // num_centers
    centers_ = tf.reshape(centers_t, (-1, spec_tensor[d['d']]))

    # Thus, we can pick from this centers matrix the centers we want
    hard_out = tf.gather(centers_, indices)

    hard_out_reshaped = tf.reshape(
        hard_out, (spec_tensor[d['C']], spec_tensor[d['n']], spec_tensor[d['d']]))

    # hard_out_reshaped = tf.reshape(
    #            hard_out, (spec_tensor[D['C']], spec_tensor[D['n']], spec_tensor[D['d']]))
    return hard_out_reshaped


def _inverse_of_extract_patches(patches_tensor, spec_tensor, d):
    """
    This returns an op that is the inverse of the following:

    # Note: with this setup, patches of one image are stored as consecutive rows
    # in the patches array of one channel, i.e., consecutive rows of out[c, :, :]
    patches = tf.reshape(
        tf.transpose(

            # reshape such that the patches reside in dimensions 2 and 4.
            # I.e. the (i, j)-th patch of batch b and channel c is at [b, i, :, j, :, c]
            tf.reshape(
                feature_volume,
                #      0           1        2   3        4   5
                shape=(batch_size, h // ph, ph, w // pw, pw, C)),

            # permute dimensions such that the patch dimensions 2 and 4 are at the end,
            # and channel dimension is at the beginning, i.e.
            # has shape (C, bs, h // ph, w // pw, ph, pw)
            # i.e., permutation:
            #  0 -> 5, 1 -> 0, 2 -> 1, 3 -> 3, 4 -> 2, 5 -> 4
            perm=(5, 0, 1, 3, 2, 4)),

        # merge the patch dimensions (which are now at the end).
        # Results in a 3D matrix (c, batch_size * (h / ph) * (w / pw), pw * ph)
        shape=(C, -1, pw * ph)
    )
    """
    # assert patches_tensor.get_shape() == (spec.C, spec.n, spec.d), \
    #    'Cannot merge patches of tensor of shape {}'.format(patches_tensor.get_shape().as_list())

    batch_size = 1  # we only have batches of one image here

    # For readability
    C = spec_tensor[d['C']]
    h = spec_tensor[d['h']]
    w = spec_tensor[d['w']]
    ph = spec_tensor[d['ph']]
    pw = spec_tensor[d['pw']]

    return tf.reshape(
        tf.transpose(
            tf.reshape(
                patches_tensor,
                # undo reshape(…, (-1, pw * ph))
                shape=(C, batch_size, h // ph, w // pw, ph, pw)),
            # permute it back to shape
            # (bs, h // ph, ph, w // pw, pw, C)
            # i.e., inverse of above permutation, i.e.,
            #  0 <- 5, 1 <- 0, 2 <- 1, 3 <- 3, 4 <- 2, 5 <- 4
            perm=(1, 2, 4, 3, 5, 0)),
        shape=(batch_size, h, w, C)
    )


def convert(symbols_path, centers_path, operating_point):
    corresponding_image_path = symbols_path.replace('_sym.bin', '.png')
    assert os.path.exists(corresponding_image_path), 'Cannot find image at {}'.format(corresponding_image_path)

    org_stuff = tf.constant((32, 16, 22), dtype=tf.int32)

    """
        Rules for calculating the variables

        C:          number of channels
        n:          number of patches per channel
        w, h:       size of the bottlneck
        pw, ph:     patch size
        d:          dimension of the patch, i.e., pw * ph

        self.C = num_channels
        self.w, self.h = img_w // 8, img_h // 8
        self.n = self.w * self.h // (ph * pw)
        self.ph = ph
        self.pw = pw
        self.d = ph * pw
    """
    C = org_stuff[0]
    h = org_stuff[1] * 2
    w = org_stuff[2] * 2
    ph = tf.constant(2)
    pw = tf.constant(2)
    d = ph * pw
    n = h * w // (ph * pw)

    spec_tensor = tf.convert_to_tensor([C, h, w, n, ph, pw, d])
    print(spec_tensor)

    # translate to tf
    symbols = np.fromfile(symbols_path, dtype=np.uint16).reshape((32, -1))
    centers = np.load(centers_path)

    symbols_tensor = tf.constant(symbols, dtype=tf.int32)
    centers_tensor = tf.constant(centers)

    map_idx = {'C': 0, 'h': 1, 'w': 2, 'n': 3, 'ph': 4, 'pw': 5, 'd': 6}

    feature_volume_op = conversion_op(symbols_tensor, centers_tensor, spec_tensor, d=map_idx, num_channels=32)

    with create_session() as sess:
        print(sess.run(spec_tensor))

        feature_volume = sess.run(feature_volume_op)

        print(feature_volume.shape)
        print(np.max(feature_volume))
        print(np.min(feature_volume))
        print(np.mean(feature_volume, axis=(1, 2)))
        print(np.mean(feature_volume, axis=(1, 2)).shape)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--symbols_path', type=str, help='Path to a binary file containing the uint16-encoded symbols')
    parser.add_argument('--centers_path', type=str, help='Path to .npy file containing the centers matrix')
    parser.add_argument('--operating_point', type=int, default=32)
    flags = parser.parse_args()
    convert(flags.symbols_path, flags.centers_path, flags.operating_point)
