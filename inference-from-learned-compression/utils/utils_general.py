import csv
import datetime
import fnmatch
import glob
import math
import os
import random
import re
import socket
import time
import warnings
from collections import Counter
from collections import namedtuple

import tensorflow as tf

from networks import cae_network_decoder

slim = tf.contrib.slim


GLOB_PATTERN = '/tmp/lock-gpu*/info.txt'
USERNAME = 'robertto'

IMAGE_HEIGHT = 224
IMAGE_WIDTH = 224
DOWNSCALE_FACTOR = 8


def append_to_filename(filename, suffix=''):
    name, ext = os.path.splitext(filename)
    return "{name}_{suffix}{ext}".format(name=name, suffix=suffix, ext=ext)


def get_tensors_to_print():
    var_name_list = []
    for v in slim.get_model_variables():
        var_name_list.append(v.name)

    names = ['network/enc?/conv?/weights:0',
             'network/enc?/conv?/BatchNorm/moving_mean:0',
             'network/enc?/conv?/BatchNorm/moving_variance:0',
             'network/enc?/conv?/BatchNorm/gamma:0',
             'network/enc?/conv?/BatchNorm/beta:0',
             'network/dec?/conv?/weights:0',
             'network/dec?/conv?/BatchNorm/moving_mean:0',
             'network/dec?/conv?/BatchNorm/moving_variance:0',
             'network/dec?/conv?/BatchNorm/gamma:0',
             'network/dec?/conv?/BatchNorm/beta:0',
             'resnet*/block?/unit_?/*/conv?/weights:0',
             'resnet*/block?/unit_?/*/conv?/BatchNorm/beta:0',
             'resnet*/block?/unit_?/*/conv?/BatchNorm/gamma:0',
             'resnet*/block?/unit_?/*/conv?/BatchNorm/moving_mean:0',
             'resnet*/block?/unit_?/*/conv?/BatchNorm/moving_variance:0']

    random.shuffle(var_name_list)
    tensor_list_names = []
    for name in names:
        filtered_names = fnmatch.filter(var_name_list, name)
        if len(filtered_names) > 0:
            tensor_list_names.append(filtered_names[0])

    tensor_list = [tf.get_default_graph().get_tensor_by_name(t_name) for t_name in tensor_list_names]

    return tensor_list_names, tensor_list

def is_on_biwi():
    return 'biwi' in socket.gethostname()


def is_on_tractor():
    return 'tractor' in socket.gethostname()


def get_effective_batch_norm(batch_norm_decay, batch_size, batch_size_gpu):
    # Function that gets returns a new batch norm for multi-gpu settings
    # Because the batch-norm moving averages are only calculated for
    # a single GPU that has a smaller batch-norm size we need to
    # increase the batch norm to have the same effect. This ONLY
    # WORKS if the model is fixed when doing the batch norm, if
    # the model is not fixed and the regular batch-norm process is
    # used this will have a bad effect

    # Formula derived so that the variance of the moving average mean
    # for the new batch size matches the variance of the moving average
    # mean for the new batch size. The moving average for the variance is
    # not equivalent but probably very close

    # In the normal range for the batch_norm_decay we get approximately
    # batch_norm_decay == batch_norm_decay_effective^ratio_batch
    # which is a nice intuative result (the decay is ratio_batch 'slower')

    ratio_batch = float(batch_size_gpu) / float(batch_size)
    ratio_alpha = (1 - batch_norm_decay) / (1 + batch_norm_decay)

    batch_norm_decay_effective = (1 - ratio_batch * ratio_alpha) / (1 + ratio_batch * ratio_alpha)

    return batch_norm_decay_effective


def get_number_of_batch_norm_iterations(batch_norm_decay, threshold=0.01):
    # Calculate how many iterations of batch norm are needed for the
    # effect of the current value to be ~1%

    return math.ceil(math.log(threshold) / math.log(batch_norm_decay))


def check_for_checkpoint_overwrite(from_scratch, overwrite, summary_dir_parsed):
    """
    Make sure all input FLAGS are consistent
    and other checks
    """

    if (from_scratch and os.path.exists(summary_dir_parsed)) and (not overwrite):
        raise ValueError('Path {} exists, with flag from_scratch {} which would cause an overwrite of previous checkpoints. To proceed anyway use the --overwrite flag'.format(summary_dir_parsed, from_scratch))


def get_spec_tensor(op_point, shape, downscale_factor):
    C = op_point.C
    h = shape[0] // downscale_factor
    w = shape[1] // downscale_factor
    ph = tf.constant(op_point.ph)
    pw = tf.constant(op_point.pw)
    d = ph * pw
    n = h * w // (ph * pw)

    return tf.convert_to_tensor([C, h, w, n, ph, pw, d])


def create_mapping(input_filename):

    d = {}
    with open(input_filename, 'r') as f:
        csvreader = csv.reader(f, delimiter=',')
        header = next(csvreader)

        for row in csvreader:
            d[row[0]] = (int(row[1]), int(row[2]))

    return d

def get_checkpoint_scope(checkpoint_path):
    # This could also be done in a more general way
    # that is not only apllicable to scopes. 
    # Using code such as

    ## map_from: Names of variables in checkpoint
    ## map_to: Names of variables of the loaded model
    # map_from = 'resnet_v1'
    # map_to = 'resnet'

    # variables_to_restore = slim.get_model_variables()
    # variables_to_restore = {utils.map_variable_names(var, map_from=map_from, map_to=map_to):var for var in variables_to_restore}

    #init_fn = slim.assign_from_checkpoint_fn(
    #    checkpoint_path,
    #    variables_to_restore)

    # Assume that the most common prefix/scope is the correct one
    # hacky, but works

    cn = Counter()
    for var_name, _ in tf.contrib.framework.list_variables(checkpoint_path):
        cn[var_name.split('/')[0]] += 1

    result = ''
    for name, count in cn.most_common():
        if 'resnet' in name:
            result = name

    print('Model_scope is: {}'.format(result))

    return result


def map_variable_names(var, map_from='', map_to=''):
    if map_from in var.op.name:
        return var.op.name.replace(map_from, map_to)

def get_op_point(op_point, downscale_factor=DOWNSCALE_FACTOR, image_height=IMAGE_HEIGHT, image_width=IMAGE_WIDTH):
    """
        Rules for calculating the variables

        C:          number of channels
        n:          number of patches per channel
        w, h:       size of the bottlneck
        pw, ph:     patch size
        d:          dimension of the patch, i.e., pw * ph

        self.C = num_channels
        self.w, self.h = img_w // 8, img_h // 8
        self.n = self.w * self.h // (ph * pw)
        self.ph = ph
        self.pw = pw
        self.d = ph * pw
    """

    OperatingPoint = namedtuple('OperatingPoint', ['C', 'h', 'w', 'd', 'ph', 'pw', 'n', 'downscale_factor'])
    if op_point == 32:
        C = 32
        ph = 2
        pw = 2

        h = image_height // downscale_factor
        w = image_width // downscale_factor
        d = ph * pw
        n = h * w // (ph * pw)

        op_point_tuple = OperatingPoint(C=C, h=h, w=w, ph=ph, pw=pw, d=d, n=n, downscale_factor=downscale_factor)
    elif op_point == 8:
        # Dummy data for new symbols. Refactor into num_channels
        C = 8
        ph = 2
        pw = 2

        h = image_height // downscale_factor
        w = image_width // downscale_factor
        d = ph * pw
        n = h * w // (ph * pw)

        op_point_tuple = OperatingPoint(C=C, h=h, w=w, ph=ph, pw=pw, d=d, n=n, downscale_factor=downscale_factor)
    else:
        raise ValueError('Unknown operating point: {}'.format(op_point))

    return op_point_tuple


def get_num_classes(synsets_filename):
    i = 0

    with open(synsets_filename, 'r') as f:
        for line in f:
            i = i + 1

    return i


def get_gpu_number():
    #TODO Make general so if there are more than one on the same render it
    #     gets the correct number
    sge_gpu = ''
    filelist = glob.glob(GLOB_PATTERN)

    for filename in filelist:
        with open(filename, 'r') as f:
            for line in f:
                if USERNAME in line:
                    sge_gpu = line.split()[1]

    if not sge_gpu:
        warnings.warn('No GPU reserved', UserWarning)

    return sge_gpu


def get_session(log_device_place=False):
    config = tf.ConfigProto()

    config.gpu_options.allow_growth = True
    config.allow_soft_placement = True
    config.log_device_placement = log_device_place

    sess = tf.Session(config=config)

    return sess

def get_summary_dir_name(FLAGS):
    s = ('summary_{model_type}_{num_classes}_{schedule}x_{network_name}_'
         'lr-{learning_rate}_bs-{batch_size}_{preprocess_flags}{suffix}')

    preprocess_flags = ''
    if 'bottleneck' in FLAGS.model_type and FLAGS.rand_flip:
        preprocess_flags += 'w-rand-flip'
    if (('compressed' in FLAGS.model_type) or ('image' in FLAGS.model_type)) and (not FLAGS.scale_augmentation):
        preprocess_flags += 'no-scale-aug'

    # TODO A bit of a hack, make general
    if FLAGS.num_classes == 1000 or FLAGS.num_classes == 982:
        schedule = int(round(30.0 / FLAGS.epochs_decay))
    elif FLAGS.num_classes == 200:
        schedule = int(round(150.0 / FLAGS.epochs_decay))
    else:
        raise ValueError('Schedule for {} classes not supported'.format(FLAGS.num_classes))

    return s.format(model_type=FLAGS.model_type,
                    num_classes=FLAGS.num_classes,
                    network_name=FLAGS.network_name,
                    learning_rate=FLAGS.learning_rate,
                    batch_size=FLAGS.batch_size,
                    schedule=schedule,
                    suffix=FLAGS.suffix,
                    preprocess_flags=preprocess_flags)

def get_summary_dir(FLAGS):
    if not (FLAGS.summary_dir or FLAGS.summary_basedir):
        raise ValueError('Have to supply either a summary_dir or summary_basedir. Got {} and {}'.format(FLAGS.summary_dir ,FLAGS.summary_basedir))

    summary_dir = FLAGS.summary_dir
    
    if not summary_dir:
        summary_dir = os.path.join(FLAGS.summary_basedir, get_summary_dir_name(FLAGS))

    return summary_dir
        

def get_checkpoint_path(checkpoint_dir, checkpoint_name, summary_dir=None):

    if not (checkpoint_dir or summary_dir):
        raise ValueError('Have to supply either a checkpoint_dir or summary_dir. Got {} and {}'.format(checkpoint_dir, summary_dir))


    checkpoint_path = checkpoint_dir
    if not checkpoint_dir:
        checkpoint_path = summary_dir 

    if not checkpoint_name:
        checkpoint_path = tf.train.latest_checkpoint(checkpoint_path)
    else:
        checkpoint_path = os.path.join(checkpoint_path, checkpoint_name)

    assert checkpoint_path is not None, 'Did not find checkpoint file in path'

    return checkpoint_path


def get_learning_rate(learning_rate, learning_rate_type='schedule', epochs_decay=30, num_examples=1281167, batch_size=64):
    if learning_rate_type == 'schedule':
        decay_steps = int(epochs_decay * num_examples / batch_size)

        learn_rate = tf.train.exponential_decay(learning_rate, slim.get_global_step(),
                                                decay_steps, decay_rate=0.1,
                                                staircase=True)
    elif learning_rate_type == 'constant':
        learn_rate = tf.constant(learning_rate)
    else:
        raise ValueError('Not a valid learning_rate_type: {}'.format(learning_rate_type))

    return learn_rate


def get_basic_learning_rate(learning_rate_type):
    if learning_rate_type == 'default':
        learning_rate = tf.constant(0.1)
    elif learning_rate_type == 'finetune':
        learning_rate = tf.constant(0.0001)
    else:
        try:
            learning_rate = tf.constant(float(learning_rate_type))
        except ValueError:
            print('Unknown learning rate type: {}'.format(learning_rate_type))
            raise

    return learning_rate


def get_variables_to_train(subset_type='all', model_scope='resnet'):
    collection = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=model_scope)
    variable_list = []

    if subset_type == 'all':
        exclusions = []
        variable_list = _get_variables_subset(exclusions,
                                            collection,
                                            select_by_exclusion=True)
    elif subset_type == 'finetune':
        # TODO Fix this to a general variable scope
        inclusions = [model_scope + '/logits']

        variable_list = _get_variables_subset(inclusions,
                                            collection,
                                            select_by_exclusion=False)
    else:
        raise ValueError('Not a valid subset_type: {}'.format(subset_type))

    return variable_list

def get_variables_to_restore(subset_type='all', model_scope='resnet'):
    collection = slim.get_model_variables()
    variable_list = []

    if subset_type == 'all':
        exclusions = []
        variable_list = _get_variables_subset(exclusions,
                                            collection,
                                            select_by_exclusion=True)
    elif subset_type == 'finetune':
        exclusions = [model_scope + '/logits']

        variable_list = _get_variables_subset(exclusions,
                                            collection,
                                            select_by_exclusion=True)
    else:
        raise ValueError('Not a valid subset_type: {}'.format(subset_type))

    return variable_list


def _get_variables_subset(exclusions, variable_collection, select_by_exclusion=True):
    variable_list = []
    variable_list_rest = []

    for var in variable_collection:
        excluded = False
        for exclusion in exclusions:
            if var.op.name.startswith(exclusion):
                excluded = True
                break
        if not excluded:
            variable_list.append(var)
        else:
            variable_list_rest.append(var)

    if not select_by_exclusion:
        variable_list = variable_list_rest

    return variable_list


def get_challenge_synsets(synsets_filename):
    return [l.strip() for l in open(synsets_filename, 'r')]

def get_current_time_str():
    return datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

def save_run_parameters(output_dir, flags, checkpoint_path, model_scope, basename='parameters'):
    filename = '{}_{}.csv'.format(basename, get_current_time_str())
    output_filepath = os.path.join(output_dir, filename)

    d = vars(flags)

    with open(output_filepath, 'w') as f:
        f.write('loaded_checkpoint,{}\n'.format(checkpoint_path))

        for key in sorted(d.keys()):
            f.write('{},{}\n'.format(key, d[key]))

        f.write('model_scope,{}'.format(model_scope))


def print_variable_values_and_origin(flags, parser, left_side_len=25):
    d = vars(flags)

    print('-'*50)
    print('Values')
    print('-'*50)
    for key in sorted(d.keys()):
        spaces = ' ' * (left_side_len - len(key))
        print('{}:{}{}'.format(key, spaces, d[key]))

    print('-'*50)
    print('Values origin')
    print('-'*50)
    print(parser.format_values())    # useful for logging where different settings came from

def assign_from_2_checkpoints(checkpoint_path_res, checkpoint_path_dec):
    # Tmp helper function

    map_from = 'decoder'
    map_to = 'printemps/cae_net/decoder'

    variables_to_restore_dec = cae_network_decoder.get_weights() + cae_network_decoder.get_biases()
    variables_to_restore_dec = {map_variable_names(var, map_from=map_from, map_to=map_to):var for var in variables_to_restore_dec}

    saver_dec = tf.train.Saver(var_list=variables_to_restore_dec)

    variables_to_restore_res = slim.get_model_variables(scope='resnet')

    saver_res = tf.train.Saver(var_list=variables_to_restore_res)

    def callback(session):
        saver_dec.restore(session, checkpoint_path_dec)
        saver_res.restore(session, checkpoint_path_res)

    return callback


# TODO Add shuffling to the class so that input data,
#      when not contained in tfrecords, is properly shuffled
class ImagenetData():


    def __init__(self,
                 data_path,
                 synsets_filename,
                 extension='tfrecord',
                 subset='train'):

        self.extension = extension
        self.data_path = data_path
        self.subset = subset
        self.filelist = []
        self.SYNSETS_FILENAME = synsets_filename

    def num_classes(self):
        """Returns the number of classes in the data set."""
        return 1000

    def set_data_dir(self, data_path_in):
        self.data_path = data_path_in

    def num_examples_per_epoch(self):
        """Returns the number of examples in the data set."""
        # Bounding box data consists of 615299 bounding boxes for 544546 images.
        if self.subset == 'train':
            return 1281167
        if self.subset == 'validation':
            return 50000

    def get_reader(self):
      if self.extension == 'tfrecord':
        return tf.TFRecordReader()
      else:
        return tf.WholeFileReader()

    def set_data_path(self, data_path_in):
        self.data_path = data_path_in

    def file_list(self):
        filelist = []

        if os.path.isdir(self.data_path):
            if self.extension == 'tfrecord':
                filelist = glob.glob(os.path.join(self.data_path, '*.{}'.format(self.extension)))
            else:
                challenge_synsets = [l.strip() for l in open(self.SYNSETS_FILENAME, 'r')]

                for synset in challenge_synsets:
                    image_file_pattern = '%s/%s/*.%s' % (self.data_path,
                                                         synset,
                                                         self.extension)

                    filelist.extend(glob.glob(image_file_pattern))

        elif os.path.isfile(self.data_path):
            with open(self.data_path, 'r') as f:
                for line in f:
                    filelist.append(line.strip())
        else:
            # If doesn't work, perhaps it's a symlink. No check for that
            raise ValueError('Neither a directory nor file: {}'.format(self.data_path))

        return filelist

    def get_file_list(self):
        if not self.filelist:
            print("Listing {} files in {}".format(self.extension, self.data_path))

            start_time = time.time()
            self.filelist = self.file_list()
            duration = time.time() - start_time

            print("Took {:.6f} sec".format(duration))
            print('Found {} files'.format(len(self.filelist)))

            if not self.filelist:
                raise ValueError('Didn\'t find any files. Does dir {} correspond to extension {}?'.format(self.data_path, self.extension))

        return self.filelist

    def filename_to_synset(self, filename):
        return re.search(r'(n\d+)', filename).group(1)

    def synset_label_to_idx(self):
        # The synsets in the file must be sorted!
        synset_list = [s.strip() for s in open(self.SYNSETS_FILENAME).readlines()]
        num_synsets_in_ilsvrc = len(synset_list)
        assert (num_synsets_in_ilsvrc == 1000) or (num_synsets_in_ilsvrc == 200)

        synset_to_idx_dict = {}
        label_num = 0
        for syn in synset_list:
            synset_to_idx_dict[syn] = label_num
            label_num += 1

        # Watch out when there is a 0 background class
        return synset_to_idx_dict

    def get_labels_list(self):

        if self.extension == 'tfrecord':
            raise ValueError('No labels list for extension: {}'.format(self.extension))

        d = self.synset_label_to_idx()

        return [d[self.filename_to_synset(f)] for f in self.get_file_list()]

    def is_tfrecord(self):
        return self.extension == 'tfrecord'


# prefix = 'network/dec1/conv1/'
# tensor_names = ['weights:0', 'BatchNorm/beta:0', 'BatchNorm/gamma:0', 'BatchNorm/moving_mean:0', 'BatchNorm/moving_variance:0']
# tensors_out = [prefix + t for t in tensor_names]
# tensors_out = [tf.get_default_graph().get_tensor_by_name(t) for t in tensors_out]

