import os
from time import time

import numpy as np
import utils.symbols_to_feature_vol_dynamic as s2f_dynamic
import tensorflow as tf

import utils
from utils import symbols_to_feature_vol as s2f

#FEAT_VOL_HEIGHT = 28
#FEAT_VOL_WIDTH = 28

#FEAT_VOL_HEIGHT = 32
#FEAT_VOL_WIDTH = 32

# Note: This assumes that height == width,
#       which is (almost) always the case
FEAT_VOL_INPUT_SIZE = 28


def _crop(feature_vol_reshape, tensor_shape, output_shape, is_training):
    if is_training:
        feature_vol_crop = tf.random_crop(feature_vol_reshape, output_shape)
    else:
        offset_height = (tensor_shape[0] - output_shape[0]) // 2
        offset_width = (tensor_shape[1] - output_shape[1]) // 2

        offsets = [offset_height, offset_width, 0]

        feature_vol_crop = tf.slice(feature_vol_reshape, offsets, output_shape)

    return feature_vol_crop

def _parse_proto_example(serialized_example, input_type='symbol'):
    features = tf.parse_single_example(
        serialized_example,
        features={
            'image/encoded': tf.FixedLenFeature([], tf.string),
            'image/class/label': tf.FixedLenFeature([], tf.int64),
            'image/width': tf.FixedLenFeature([], tf.int64),
            'image/height': tf.FixedLenFeature([], tf.int64),
            'image/channels': tf.FixedLenFeature([], tf.int64),
            'image/filename': tf.FixedLenFeature([], tf.string)
        })

    if input_type == 'symbol' or input_type == 'symbol-new':
        symbols_tensor = tf.decode_raw(features['image/encoded'], tf.int16)
        symbols_tensor = tf.cast(symbols_tensor, tf.int32)
    elif input_type == 'feature_volume':
        symbols_tensor = tf.decode_raw(features['image/encoded'], tf.float32)
    else:
        raise ValueError('Unknown input_type {}'.format(input_type))
    
    label = features['image/class/label']

    height = features['image/height']
    width = features['image/width']
    #channels = features['image/channels']

    #shape = tf.convert_to_tensor([height, width, channels])
    shape = tf.convert_to_tensor([height, width])
    shape = tf.cast(shape, tf.int32)

    return symbols_tensor, label, shape


def get_paths_labels_and_shapes(dataset, map_name_to_dim_filename):
    d = utils.create_mapping(map_name_to_dim_filename)

    filelist = dataset.get_file_list()
    label_list = dataset.get_labels_list()

    shapelist = []

    for f_name in filelist:
        key = os.path.splitext(os.path.basename(f_name))[0]
        key = key.replace('_sym', '')

        shapelist.append(d[key])

    return filelist, label_list, shapelist


def get_file_label_and_shape_queue(path_list, label_list, shape_list, shuffle=True):
    shapes = np.array(shape_list, dtype=np.int32)
    filename_queue, label, shape = tf.train.slice_input_producer([path_list, label_list, shapes], shuffle=shuffle)

    np_file = tf.read_file(filename_queue)

    return tf.decode_raw(np_file, tf.int16), label, shape


def process_symbols_tensor_dynamic(symbols_tensor, shape, centers_tensor, op_point):
    """
    Bit of a mixup, here 'op_point' is different from the one used in other places,
    should have the same fields though and thereforw works

    """
    symbols_tensor_reshape = tf.reshape(symbols_tensor, shape=(op_point.C, -1))
    symbols_tensor_reshape = tf.cast(symbols_tensor_reshape, tf.int32)

    # TODO Remove this mapping when code is fixed (see todo below)
    map_idx = {'C': 0, 'h': 1, 'w': 2, 'n': 3, 'ph': 4, 'pw': 5, 'd': 6}
    spec_tensor = utils.get_spec_tensor(op_point, shape, op_point.downscale_factor)

    feature_vol = s2f_dynamic.conversion_op(symbols_tensor_reshape, centers_tensor, spec_tensor, d=map_idx, num_channels=op_point.C)
    feature_vol = tf.squeeze(feature_vol, axis=0)

    feature_vol_crop = tf.random_crop(feature_vol, [FEAT_VOL_HEIGHT, FEAT_VOL_WIDTH, op_point.C])
    feature_vol_crop.set_shape([FEAT_VOL_HEIGHT, FEAT_VOL_WIDTH, op_point.C])

    return feature_vol_crop


def process_symbol_new_tensor(symbol_vol, shape, centers_tensor, op_point, feat_vol_input_size=FEAT_VOL_INPUT_SIZE, is_training=True, rand_flip=True):
    tensor_shape_nhwc = (shape[0] // 4, shape[1] // 4, op_point.C)
    tensor_shape = (op_point.C, shape[0] // 4, shape[1] // 4)

    feature_vol_reshape = tf.reshape(symbol_vol, shape=tensor_shape)
    # CHECK EFFICIENCY
    feature_vol_reshape = tf.transpose(feature_vol_reshape, [1, 2, 0])

    output_shape = (feat_vol_input_size, feat_vol_input_size, op_point.C)

    feature_vol_crop = _crop(feature_vol_reshape, tensor_shape_nhwc, output_shape, is_training)
    feature_vol_crop.set_shape([feat_vol_input_size, feat_vol_input_size, op_point.C])

    if is_training and rand_flip:
        print('DEBUG: HAS RAND FLIP')
        feature_vol_crop = tf.image.random_flip_left_right(feature_vol_crop)

    feature_vol_crop = tf.gather(centers_tensor, feature_vol_crop)

    return feature_vol_crop


def process_feat_vol(feature_vol, shape, op_point, feat_vol_input_size=FEAT_VOL_INPUT_SIZE, is_training=True, rand_flip=True):
    tensor_shape = (shape[0] // 8, shape[1] // 8, op_point.C)
    feature_vol_reshape  = tf.reshape(feature_vol, shape=tensor_shape)

    output_shape = (feat_vol_input_size, feat_vol_input_size, op_point.C)

    feature_vol_crop = _crop(feature_vol_reshape, tensor_shape, output_shape, is_training)
    feature_vol_crop.set_shape([feat_vol_input_size, feat_vol_input_size, op_point.C])

    if is_training and rand_flip:
        print('DEBUG: HAS RAND FLIP')
        feature_vol_crop = tf.image.random_flip_left_right(feature_vol_crop)

    return feature_vol_crop

def process_symbols_tensor(symbols_tensor, shape, centers_tensor, op_point):
    shape_full = tf.convert_to_tensor([op_point.C, shape[0] // (2 * op_point.downscale_factor), shape[1] // (2 * op_point.downscale_factor)])
    symbols_tensor_reshape_first = tf.reshape(symbols_tensor, shape=shape_full)

    output_shape = [op_point.C, op_point.h // 2, op_point.w // 2]

    symbols_tensor_crop = tf.random_crop(symbols_tensor_reshape_first, output_shape)
    symbols_tensor_crop.set_shape(output_shape)

    #symbols_tensor_crop = tf.image.crop_to_bounding_box(symbols_tensor_reshape_first, 0, 0, op_point.h // 2, op_point.w // 2)
    #symbols_tensor_crop = tf.slice(symbols_tensor_reshape_first, (0,0,0), (32,14,14))
    #symbols_tensor_crop.set_shape([op_point.C, op_point.h // 2, op_point.w // 2])

    symbols_tensor_reshape = tf.reshape(symbols_tensor_crop, shape=(op_point.C, -1))
    symbols_tensor_reshape = tf.cast(symbols_tensor_reshape, tf.int32)

    feature_vol = s2f.conversion_op(symbols_tensor_reshape, centers_tensor, op_point)
    feature_vol = tf.squeeze(feature_vol, axis=0)

    return feature_vol


def get_symbols(data_dir,
                synsets_filename,
                map_name_to_dim_filename,
                centers_filename,
                operating_point=32,
                batch_size=64,
                extension='bin',
                num_epochs=None,
                is_training=True,
                input_type='symbol',
                num_preprocess_threads=4,
                shuffle=True,
                data_format='NHWC',
                rand_flip=True,
                feat_vol_input_size=28):

    op_point = utils.get_op_point(operating_point)

    dataset = utils.ImagenetData(data_dir, synsets_filename, extension=extension)

    # TODO NEED EVAL PREPROCESSING WITHOUT RANDOM CROP BEFORE DOING VALIDATION!
    if dataset.is_tfrecord():
        filename_queue = tf.train.string_input_producer(dataset.get_file_list(),
                                                        num_epochs=num_epochs,
                                                        shuffle=shuffle)

        reader = tf.TFRecordReader()
        _, serialized_example = reader.read(filename_queue)
        symbols_tensor, label, shape = _parse_proto_example(serialized_example,
                                                            input_type=input_type)
    else:
        path_list, label_list, shape_list = get_paths_labels_and_shapes(dataset, map_name_to_dim_filename)
        symbols_tensor, label, shape = get_file_label_and_shape_queue(path_list, label_list, shape_list, shuffle=shuffle)

    feat_vols_and_labels = []

    for thread_id in range(num_preprocess_threads):
        if input_type == 'symbol':
            # Does not have differeng input dimensions
            centers = np.load(centers_filename)
            centers_tensor = tf.constant(centers)

            feature_vol_crop = process_symbols_tensor(symbols_tensor,
                                                      shape,
                                                      centers_tensor,
                                                      op_point,
                                                      is_training=is_training)
        elif input_type == 'symbol-new':
            # Does not have differeng input dimensions
            centers = np.load(centers_filename)
            centers_tensor = tf.constant(centers)

            feature_vol_crop = process_symbol_new_tensor(symbols_tensor,
                                                          shape,
                                                          centers_tensor,
                                                          op_point,
                                                          is_training=is_training,
                                                          rand_flip=rand_flip,
                                                          feat_vol_input_size=feat_vol_input_size)
        elif input_type == 'feature_volume':
            feature_vol_crop = process_feat_vol(symbols_tensor,
                                                shape,
                                                op_point,
                                                is_training=is_training,
                                                rand_flip=rand_flip,
                                                feat_vol_input_size=feat_vol_input_size)
        else:
            raise ValueError('Unknown input_type {}'.format(input_type))

        if data_format == 'NCHW':
            feature_vol_crop = tf.transpose(feature_vol_crop, [2, 0, 1])

        feat_vols_and_labels.append([feature_vol_crop, label])

    min_after_dequeue = 0
    if is_training:
        min_after_dequeue = 4000

    feature_vol_batch, label_batch = tf.train.shuffle_batch_join(
        feat_vols_and_labels,
        batch_size=batch_size,
        capacity=8000,
        min_after_dequeue=min_after_dequeue)

    return feature_vol_batch, label_batch


def main():

    BATCH_SIZE = 64
    DOWNSCALE_FACTOR = 8

    MAP_NAME_TO_DIM_FILENAME = './input/filename_shape_map_train.txt'
    #MAP_NAME_TO_DIM_FILENAME = './input/filename_shape_map_val.txt'

    #DATA_DIR = '/home/robert/code/masters_thesis/tmp_bin_files'
    #DATA_DIR = '/home/robertto/scratch_tractor/imagenet_256/roberto_val'
    #DATA_DIR = '/srv/glusterfs/mentzerf/roberto_train'
    #DATA_DIR = './feature_maps_3_bak/imagenet_3_files.txt'
    #DATA_DIR = './feature_maps_3_bak/imagenet_3_files.txt'
    DATA_DIR = '/scratch/robertto/imagenet_256/tmp_bin'
    DATA_DIR = '/srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_feat_vol_tfrecord'

    SYNSET_FILENAME = './input/imagenet_lsvrc_2015_synsets.txt'
    #EXTENSION = 'bin'
    EXTENSION = 'tfrecord'
    INPUT_TYPE = 'feature_volume'

    #CENTERS_FILENAME = './tmp_bin_files/centers_32_b5e_4_prog_LR6e-6_ckpt1162357.npy'
    CENTERS_FILENAME = '/srv/glusterfs/mentzerf/roberto_train/centers_32_b5e_4_prog_LR6e-6_ckpt1162357.npy'
    #CENTERS_FILENAME = 'centers_structured.npy'

    SHUFFLE = True

    OP_POINT = 32
    BATCH_SIZE = 64
    BATCH_SIZE = 1

    feature_vol_batch, label_batch = get_symbols(DATA_DIR,
                                               SYNSET_FILENAME,
                                               MAP_NAME_TO_DIM_FILENAME,
                                               CENTERS_FILENAME,
                                               operating_point=OP_POINT,
                                               batch_size=BATCH_SIZE,
                                               extension=EXTENSION,
                                               input_type=INPUT_TYPE,
                                               is_training=True,
                                               num_preprocess_threads=1,
                                               shuffle=SHUFFLE)

    init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
    init_op = tf.group(*init_list)

    with tf.Session() as sess:
    
        sess.run(init_op)
    
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        for i in range(1):
            start_time = time()
            feat_out, label_out = sess.run([feature_vol_batch, label_batch])

            duration = time() - start_time

            print(BATCH_SIZE / duration)
            print(feat_out.shape)
            print(label_out.shape)
            print(label_out)

            print(feat_out[0, 0:3, 0:3, 1])
            print(feat_out[0, 0:3, -3:, 1])

            np.save('feature_maps_3_bak/feature_map_crop_before_structured_{}'.format(label_out[0]), np.squeeze(feat_out))

        coord.request_stop()
        coord.join(threads)

if __name__ == '__main__':
    main()
