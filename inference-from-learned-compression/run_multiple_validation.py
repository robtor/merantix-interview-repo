import glob
import os

import configargparse
import tensorflow as tf

import resnet_eval
from networks.nets_class import networks
from utils import utils_run_multiple as utils, parsers_definitions

NETWORKS = networks
ENCODER_DECODER_BASEDIR = '/srv/glusterfs/robertto/imagenet/compression_runs'
ENC_DEC_PATH_MAP = utils.get_enc_dec_map(ENCODER_DECODER_BASEDIR)

CONFIG_MAPPING = {'bottleneck_200': 'bottleneck_200_val.config',
                  'bottleneck_1000': 'bottleneck_1000_val.config',
                  'compressed_200': 'compressed_200_val.config',
                  'compressed_1000': 'compressed_1000_val.config',
                  'original_200': 'image_200_val.config',
                  'original_1000': 'image_1000_val.config',
                  'image_200': 'image_200_val.config',
                  'image_1000': 'image_1000_val.config',
                  'bottleneck-0808_982': 'bottleneck-0808_1000_val.config',
                  'bottleneck-2308_982': 'bottleneck-2308_1000_val.config',
                  'compressed-0808_982': 'compressed-0808_1000_val.config',
                  'compressed-2308_982': 'compressed-2308_1000_val.config',
                  }


def main(FLAGS):
    dir_names = glob.glob(os.path.join(FLAGS.base_dir, FLAGS.pattern))
    dir_names.sort()

    if FLAGS.print_dirs:
        utils.print_dir_names(dir_names)
        return 0

    models = []

    for dir_name in dir_names:
        print(dir_name)
        if utils.check_valid_dir_name(dir_name, CONFIG_MAPPING, network_names=NETWORKS):
            folder_name = os.path.basename(os.path.normpath(dir_name))
            output_filename = os.path.join(FLAGS.output_dir, folder_name + '_val.txt')

            config_path, network_name = utils.get_data_from_folder_name(folder_name,
                                                                        FLAGS.config_dir,
                                                                        CONFIG_MAPPING)

            models_finished = utils.parse_output_filename(output_filename)
            models = utils.get_models_list(dir_name,
                                           models_finished,
                                           model_basename=FLAGS.model_basename,
                                           start_point=FLAGS.start_point,
                                           end_point=FLAGS.end_point,
                                           steps=FLAGS.steps)

            # TODO Refactor this code below to be nicer
            input_resize_factor = utils.get_number_with_prefix('resize-', folder_name)
            image_output_dim = NETWORKS[network_name].default_input_size
            key = utils.get_enc_dec_mapping_number('key-', folder_name, default_val=None)

            enc_dec_dir = None
            subtraction_flag = '--subtract-mean'
            if key:
                # Key implies that it's one of the encoder-decoder networks
                enc_dec_dir = ENC_DEC_PATH_MAP[key]
                subtraction_flag = '--no-subtract-mean'

            save_per_class_accuracy_flag = '--no-save-per-class-accuracy'
            if FLAGS.save_per_class_accuracy:
                save_per_class_accuracy_flag = '--save-per-class-accuracy'

            for model in models:
                print(model)
                parser = parsers_definitions.get_parser_validation()

                parser_args = ['-c0', config_path,
                               '--network-name', network_name,
                               '--output-filename', output_filename,
                               '--checkpoint-dir', dir_name,
                               '--checkpoint-name', model,
                               '--input-resize-factor', str(input_resize_factor),
                               '--image-output-dim', str(image_output_dim),
                               '--encoder-decoder-dir', enc_dec_dir,
                               save_per_class_accuracy_flag,
                               subtraction_flag]

                if 'jp2k' in folder_name:
                    parser_args.append('--directory')
                    parser_args.append(utils.get_jp2k_data_path(folder_name))

                flags_eval = parser.parse_args(parser_args)

                # Exception handling here so that a tensorflow error doesn't kill the whole program
                try:
                    # Also possible to use a context manager
                    # with tf.Graph().as_default():
                    tf.reset_default_graph()
                    resnet_eval.main(flags_eval)
                except Exception as e:
                    print('A tensorflow exception happened at {}'.format(os.path.join(dir_name, model)))
                    print(e)
        else:
            print('{} does not have the correct folder format'.format(dir_name))


if __name__ == '__main__':
    parser = configargparse.ArgumentParser()

    parser.add_argument("--base-dir",
                        type=str,
                        required=True,
                        help='Dir that has the validation runs. base_dir + pattern is globbed')
    parser.add_argument("--pattern",
                        type=str,
                        default='*',
                        help='Folder pattern to use. base_dir + pattern is globbed. Note that when using wildcard matching with * from the command-line, the argument must be surrounded by \', example: --pattern \'summary_*200_resnet*\' so that it doesn\'t get expanded in the command-line')
    parser.add_argument("--output-dir",
                        type=str,
                        default= '/scratch_net/tractor/robertto/masters_thesis/output/output_new',
                        help='Folder where the configs are stored')
    parser.add_argument("--config-dir",
                        type=str,
                        default= '/scratch_net/tractor/robertto/masters_thesis/configs',
                        help='Folder where the configs are stored')
    parser.add_argument("--model-basename",
                        type=str,
                        default= 'model.ckpt',
                        help='')
    parser.add_argument("--start-point",
                        type=int,
                        default=0,
                        help='starting point for model validation')
    parser.add_argument("--end-point",
                        type=int,
                        default=2000000,
                        help='end point for model validation')
    parser.add_argument("--steps",
                        type=int,
                        default=20000,
                        help='Steps for model validation')
    parser.add_argument('--print-dirs', dest='print_dirs', action='store_true',
                        help='If passed shows the folders that will be processed by the script')
    parser.add_argument('--image-dim',
                        type=int,
                        default=224,
                        help='Input spatial dim of the feature volume')
    parser.set_defaults(print_dirs=False)
    parser.add_argument('--save-per-class-accuracy', dest='save_per_class_accuracy', action='store_true',
                        help="Weather to also save a file for each run with per class accuracy")
    parser.set_defaults(save_per_class_accuracy=False)
    FLAGS = parser.parse_args()

    main(FLAGS)
