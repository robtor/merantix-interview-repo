import glob
import os

import configargparse
import tensorflow as tf

import resnet_calc_bn
from utils import utils_run_multiple as utils, parsers_definitions

CONFIG_MAPPING = {'bottleneck_200': ('bottleneck_bn.config', 'bottleneck_200_bn.config'),
                  'bottleneck_1000': ('bottleneck_bn.config', 'bottleneck_1000_bn.config'),
                  'compressed_200': ('image_compressed_bn.config', 'compressed_200_bn.config'),
                  'compressed_1000': ('image_compressed_bn.config', 'compressed_1000_bn.config'),
                  'original_200': ('image_compressed_bn.config', 'image_200_bn.config'),
                  'original_1000': ('image_compressed_bn.config', 'image_1000_bn.config'),
                  'image_200': ('image_compressed_bn.config', 'image_200_bn.config'),
                  'image_1000': ('image_compressed_bn.config', 'image_1000_bn.config'),
                  'bottleneck-0808_982': ('bottleneck-0808_bn.config', 'bottleneck-0808_1000_bn.config'),
                  'bottleneck-2308_982': ('bottleneck-2308_bn.config', 'bottleneck-2308_1000_bn.config'),
                  'compressed-0808_982': ('image_compressed_bn.config', 'compressed-0808_1000_bn.config'),
                  'compressed-2308_982': ('image_compressed_bn.config', 'compressed-2308_1000_bn.config'),
                  }


def main(FLAGS):
    dir_names = glob.glob(os.path.join(FLAGS.base_dir, FLAGS.pattern))
    dir_names.sort()

    if FLAGS.print_dirs:
        utils.print_dir_names(dir_names, CONFIG_MAPPING)
        return 0

    for dir_name in dir_names:
        print(dir_name)
        if utils.check_valid_dir_name(dir_name, CONFIG_MAPPING):
            folder_name = os.path.basename(os.path.normpath(dir_name))
            output_folder_name = '{}_bn-average_{}_{}'.format(folder_name, FLAGS.batch_norm_decay, FLAGS.max_steps)

            output_dir = os.path.join(FLAGS.output_basedir, output_folder_name)
            print(output_dir)

            # Refactor so it returns a list of config_paths, or change the
            # configs to be a single one for each setting
            mapping_key, network_name = utils.get_config_mapping_key_and_network_name(folder_name)

            config_path0 = os.path.join(FLAGS.config_dir, CONFIG_MAPPING[mapping_key][0])
            config_path1 = os.path.join(FLAGS.config_dir, CONFIG_MAPPING[mapping_key][1])

            models_finished = utils.get_models_list(output_dir,
                                                   [],
                                                   model_basename=FLAGS.model_basename,
                                                   start_point=FLAGS.start_point,
                                                   end_point=FLAGS.end_point,
                                                   steps=FLAGS.steps)

            models = utils.get_models_list(dir_name,
                                           models_finished,
                                           model_basename=FLAGS.model_basename,
                                           start_point=FLAGS.start_point,
                                           end_point=FLAGS.end_point,
                                           steps=FLAGS.steps)

            if not models:
                continue

            scale_aug_flag, rand_flip_flag, input_resize_factor = utils.get_preprocessing_flags(folder_name)

            parser = parsers_definitions.get_parser_batch_norm()
            flags_bn = parser.parse_args(['-c0', config_path0,
                                          '-c1', config_path1,
                                          '--batch_norm_decay', str(FLAGS.batch_norm_decay),
                                          '--max_steps', str(FLAGS.max_steps),
                                          '--network_name', network_name,
                                          '--summary_dir', output_dir,
                                          '--checkpoint_dir', dir_name,
                                          '--data_format', FLAGS.data_format,
                                          '--input_resize_factor', str(input_resize_factor),
                                          scale_aug_flag,
                                          rand_flip_flag])

            # Exception handling here so that a tensorflow error doesn't kill the whole program
            # Also possible to use a context manager
            # with tf.Graph().as_default():
            tf.reset_default_graph()
            resnet_calc_bn.main(flags_bn, models)
            try:
                pass
            except Exception as e:
                    print('A tensorflow exception happened at {}'.format(dir_name))
                    print(e)
        else:
            print('{} does not have the correct folder format'.format(dir_name))


if __name__ == '__main__':
    parser = configargparse.ArgumentParser()

    parser.add_argument("--base_dir",
                        type=str,
                        required=True,
                        help='Dir that has the validation runs. base_dir + pattern is globbed')
    parser.add_argument("--pattern",
                        type=str,
                        default='*',
                        help='Folder pattern to use. base_dir + pattern is globbed. Note that when using wildcard matching with * from the command-line, the argument must be surrounded by \', example: --pattern \'summary_*200_resnet*\' so that it doesn\'t get expanded in the command-line')
    parser.add_argument("--output_basedir",
                        type=str,
                        help='Folder where the configs are stored')
    parser.add_argument("--config_dir",
                        type=str,
                        default= '/scratch_net/tractor/robertto/masters_thesis/configs',
                        help='Folder where the configs are stored')
    parser.add_argument("--model_basename",
                        type=str,
                        default= 'model.ckpt',
                        help='')
    parser.add_argument("--start_point",
                        type=int,
                        default=0,
                        help='starting point for model validation')
    parser.add_argument("--end_point",
                        type=int,
                        default=2000000,
                        help='end point for model validation')
    parser.add_argument("--steps",
                        type=int,
                        default=20000,
                        help='Steps for model validation')
    parser.add_argument("--max_steps",
                        type=int,
                        default=200,
                        help='Steps for model validation')
    parser.add_argument("--batch_norm_decay",
                        type=float,
                        default=0.9,
                        help='Steps for model validation')
    parser.add_argument("--data_format",
                        type=str,
                        default='NHWC',
                        help='Steps for model validation')
    parser.add_argument('--print_dirs', dest='print_dirs', action='store_true',
                        help='If passed shows the folders that will be processed by the script')
    parser.set_defaults(print_dirs=False)
    FLAGS = parser.parse_args()

    main(FLAGS)
