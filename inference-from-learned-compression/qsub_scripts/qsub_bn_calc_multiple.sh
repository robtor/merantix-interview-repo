#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

#BRANCH=master
##BRANCH=calc_batch_norm_average
#BASE=/scratch_net/tractor/robertto/git_repos_tmp_runs
#TEMPLATE="${TAG}_XXXXXX"
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE}  masters_thesis_repo_XXXXXX)
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/masters_thesis/ .
#pwd

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf_gpu

python -u run_multiple_bn_calc.py \
--base_dir /srv/glusterfs/robertto/imagenet/summaries_backup \
--pattern 'summary_*-2308_982_*' \
--output_basedir /srv/glusterfs/robertto/imagenet/summaries_backup_bn-average \
--batch_norm_decay 0.9 \
--max_steps 40 \
--data_format NHWC \
--steps 20000

