#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=48:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

pyenv activate tf_gpu

python3 -u /home/robertto/masters_thesis/resnet_train.py \
-c0 /home/robertto/masters_thesis/configs/image_compressed.config \
-c1 /home/robertto/masters_thesis/configs/image_1000.config \
--summary_basedir /home/robertto/masters_thesis/summaries \
--epochs_decay 8 \
--no_scale_augmentation \
--no_rand_flip \
--data_format NCHW \
--batch_norm_decay 0.9 \
--network_name frozen-encoder-decoder \
--suffix _frozen-encoder-test \
--image_dim 256 \
--from_scratch \
--overwrite
