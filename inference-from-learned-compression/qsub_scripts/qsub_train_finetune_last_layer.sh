#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

pyenv activate tf_gpu

python -u resnet_train.py \
--directory /srv/glusterfs/robertto/imagenet/imagenet_256/images_train_compress_reduced_tfrecord \
--summary_dir ./summaries/finetune_last_layer \
--checkpoint_dir ./summaries/finetune_last_layer \
--learning_rate 0.00025 \
--learning_rate_type constant \
--synsets_filename ./input/imagenet_200_class_synsets.txt \
--label_offset 0 \
--variables_to_train finetune \
--variables_to_restore all \
--extension tfrecord


#--checkpoint_dir ./checkpoints \
#--checkpoint_name resnet_v1_50.ckpt \
