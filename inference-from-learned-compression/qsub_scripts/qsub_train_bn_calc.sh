#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

pyenv activate tf_gpu

python -u resnet_calc_bn.py \
-c0 ./configs/bottleneck_bn.config \
-c1 ./configs/bottleneck_1000_bn.config \
--directory /srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_feat_vol_tfrecord \
--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup/summary_bottleneck_1000_chopped-1_lr-0.025_bs-64_gpu-1_1x \
--checkpoint_name model.ckpt-1720000 \
--summary_dir ./summaries/tmp_bn/summary_bottleneck_1000_chopped-1_lr-0.025_bs-64_gpu-1_1x_test_bn_script \
--network_name chopped-1 \
--no_rand_flip \
--variables_to_restore all \
--max_steps 200 \
--batch_norm_decay 0.9 \
--suffix _test-bn-script-round2
