#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

pyenv activate tf_gpu

python -u resnet_train.py \
--directory /srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_feat_vol_tfrecord \
--summary_dir ./summaries/test_delete_bottleneck_stuff \
--learning_rate 0.025 \
--learning_rate_type constant \
--extension tfrecord \
--map_name_to_dim_filename ./input/filename_shape_map_train.txt \
--centers_filename /srv/glusterfs/mentzerf/roberto_train/centers_32_b5e_4_prog_LR6e-6_ckpt1162357.npy \
--operating_point 32 \
--input_type feature_volume \
--model_type bottleneck \
--network_name chopped_1 \
--from_scratch

#--directory /srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_bottleneck_tfrecord \
