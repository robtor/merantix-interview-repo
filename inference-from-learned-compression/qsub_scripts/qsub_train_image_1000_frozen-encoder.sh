#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


#echo "----------Info----------"
#current_host_name=$(uname -n)
#echo "Hostname: $current_host_name"
#echo "Job ID: $JOB_ID"
#echo "Arguments: $@"
#echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
#echo "------------------------"
#echo ""
#
#export CUDA_VISIBLE_DEVICES=7

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

pyenv activate tf-gpu-3.6.2

python3 -u /home/robertto/masters_thesis/resnet_train.py \
-c0 /home/robertto/masters_thesis/configs/image_compressed.config \
-c1 /home/robertto/masters_thesis/configs/image_1000.config \
-c2 /home/robertto/masters_thesis/configs/image_processing_encoder_decoder.config \
--encoder-decoder-dir '/srv/glusterfs/mentzerf/out_tb_pushpsnr/1209_0941 pp:shared_centers_c12_nips' \
--summary_basedir /home/robertto/masters_thesis/summaries \
--epochs_decay 8 \
--image-output-dim 256 \
--num_gpus 1 \
--data_format NCHW \
--network_name encoder-frozen-chopped-match \
--batch_norm_decay 0.9 \
--suffix _NEW_GRAPH \
--learning_rate 0.025 \
--from_scratch \
--overwrite \

#--encoder-decoder-dir '/home/robertto/glusterfs_r/imagenet/0915_1533 pp:shared_centers_c12_nips_lr1e*3_bn' \
