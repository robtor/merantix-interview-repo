#!/bin/bash
N=$1
shift

PREV=$(qsub  "$@")
PREV=`echo $PREV | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}'`

echo "SUBMITTED: $PREV"
for jobno in $(seq 2 $N)
do
        NEXT=$(qsub  -hold_jid $PREV "$@")
        NEXT=`echo $NEXT | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}'`
        PREV=$NEXT
        echo "SUBMITTED: $PREV"
done
