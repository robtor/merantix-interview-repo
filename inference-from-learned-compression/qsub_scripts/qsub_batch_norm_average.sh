#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

pyenv activate tf_gpu

for i in 500000 860000; do
  for j in 9; do
    date
    echo $i
    echo 0.$j
    python -u resnet_train.py \
    --num_examples 258758 \
    --directory /srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_reduced_compressed_tfrecord \
    --checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup/summary_compressed_200_resnet-50_lr-0.025_bs-64_gpu-2_2x_no-scale-aug_wd-0.0004 \
    --checkpoint_name model.ckpt-$i \
    --summary_basedir ./summaries \
    --extension tfrecord \
    --num_classes 200 \
    --no_scale_augmentation \
    --synsets_filename ./input/imagenet_200_class_synsets.txt \
    --model_type compressed \
    --batch_norm_average \
    --batch_norm_decay 0.$j \
    --batch_norm_average_max_steps 200 \
    --batch_norm_average_save_steps 20 \
    --suffix _no-mismatch-check-converge-0.$j
  done
done
