#!/bin/bash
#
#
# Calls enfleurs/runner.py with NETWORK_REPO set. You need to pass the other arguments.
#
#
#
## otherwise the default shell would be used
#$ -S /bin/bash
#
## the maximum memory usage of this job, (below 4G does not make much sense)
####$ -l h_vmem=20G
#
## only Titans
###$ -l h="biwirender11"
#$ -l h="biwirender0[6-9]|biwirender[1-9][0-9]"
###$ -l h="biwirender0[6-9]|biwirender[1-9][0-9]|bmicgpu0[0-9]"
###$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
###$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]|bmicgpu0[0-9]"
#
## GPU
####$ -l gpu=1
#
## stderr and stdout are merged together to stdout
#$ -j y
#
# logging directory. preferrably on your scratch
#$ -o /srv/glusterfs/mentzerf/sge_logs
#

echo "- submittor.sh -------------------------"
uname -n  # Show hostname
echo "Job ID: $JOB_ID"
echo "Arguments: $*"

# setup CUDA & TensorFlow
if [[ $(whoami) == "aeirikur" ]]; then
        source ~/cudarc_fabian
        source ~/pyenvrc_fabian
        RUNNER_DIR=~/projects/printemps
        KDU_DIR=~/projects/kdu_fabian
else
        source ~/cudarc
        source ~/pyenvrc
        RUNNER_DIR=~/code/printemps
        KDU_DIR=~/kdu
fi

# setup kducompress
PATH=$KDU_DIR:$PATH
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$KDU_DIR

# setup python
pyenv activate ma_env

#
# Run Script --
#
NETWORK_REPO_BASE=/scratch/$(whoami)/
PARAMS_REPO=/scratch/$(whoami)/printemps_params

CUDA_VISIBLE_DEVICES=$SGE_GPU python -u ${RUNNER_DIR}/runner.py \
    --network_repo_base $NETWORK_REPO_BASE --config_repo $PARAMS_REPO "$@"
