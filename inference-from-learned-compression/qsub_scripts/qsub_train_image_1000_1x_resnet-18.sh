#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=100:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf-gpu-3.6.2

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

python3 -u /scratch_net/tractor/robertto/masters_thesis/resnet_train.py \
-c0 /scratch_net/tractor/robertto/masters_thesis/configs/image_compressed.config \
-c1 /scratch_net/tractor/robertto/masters_thesis/configs/image_1000.config \
--summary-basedir /scratch_net/tractor/robertto/masters_thesis/summaries \
--frozen-encoder-decoder \
--epochs-decay 30 \
--num-gpus 1 \
--model-type image \
--network-name resnet-18 \
--batch-norm-decay 0.9 \
--no-scale-augmentation \
--rand-flip \
--rand-crop \
--image-output-dim 224 \
--data-format NHWC

