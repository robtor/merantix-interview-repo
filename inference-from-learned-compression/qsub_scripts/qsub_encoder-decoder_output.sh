#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


#echo "----------Info----------"
#current_host_name=$(uname -n)
#echo "Hostname: $current_host_name"
#echo "Job ID: $JOB_ID"
#echo "Arguments: $@"
#echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
#echo "------------------------"
#echo ""
#
#export CUDA_VISIBLE_DEVICES=7

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

#pyenv activate tf-gpu-3.6.2
pyenv activate tf-cpu-3.6.2

python3 -u /home/robertto/masters_thesis/encoder_decoder_output.py \
--directory /srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_tfrecord \
--encoder-decoder-dir '/srv/glusterfs/mentzerf/out_tb_pushpsnr/1209_0941 pp:shared_centers_c12_nips' \
--checkpoint_name ckpt-229000 \
--image-output-dim 256 \
--num_gpus 1 \
--data_format NHWC \
--network_name encoder-decoder \
--no_scale_augmentation \
--no_rand_flip \
--batch_size 1 \
--rand-crop \

#--encoder-decoder-dir '/srv/glusterfs/robertto/imagenet/0915_1533 pp:shared_centers_c12_nips_lr1e*3_bn' \
#--checkpoint_name ckpt-748000 \
