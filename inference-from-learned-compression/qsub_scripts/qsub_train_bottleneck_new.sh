#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

pyenv activate tf_gpu

python -u resnet_train.py \
-c0 /scratch_net/tractor/robertto/masters_thesis/configs/bottleneck.config \
-c1 /scratch_net/tractor/robertto/masters_thesis/configs/bottleneck_200.config \
--summary_dir ./summaries/testing_summaries_delete \
--epochs_decay 75 \
--network_name chopped-1 \
--rand_flip \
--from_scratch \
--overwrite

#--directory /srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_bottleneck_tfrecord \
