#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

#BRANCH=combined_model
#
##BRANCH=calc_batch_norm_average
#BASE=/scratch_net/tractor/robertto/git_repos_tmp_runs
#TEMPLATE="${TAG}_XXXXXX"
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE}  masters_thesis_repo_XXXXXX)
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/masters_thesis/ .
#pwd

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf_gpu

python -u resnet_eval.py \
--directory /srv/glusterfs/robertto/imagenet/imagenet_256/images_val_256_bottleneck-0808_tfrecord \
--num_examples 49100 \
--extension tfrecord \
--model_type bottleneck \
--centers_filename /srv/glusterfs/mentzerf/scalar/roberto_train_0808_1054/centers.npy \
--num_classes 982 \
--synsets_filename /home/robertto/masters_thesis/input/synsets_2308.txt \
--network_name chopped-48 \
--output_filename /scratch_net/tractor/robertto/masters_thesis/output/tmp_output_delete.txt \
--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup_bn-average/summary_bottleneck-0808_982_chopped-48_lr-0.025_bs-64_gpu-2_4x_w-rand-flip_bn-average_0.9_40 \
--checkpoint_name model.ckpt-440000 \
--input_type symbol-new \
--operating_point 8 \
--feat_vol_input_size 56

#--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup/summary_compressed_1000_resnet-50_lr-0.025_bs-64_gpu-2_1x_no-scale-aug \
#--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup/summary_compressed_1000_resnet-18_lr-0.025_bs-64_gpu-2_4x_no-scale-aug \
#--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup/summary_compressed_1000_resnet-50_lr-0.025_bs-64_gpu-2_1x_no-scale-aug \
#--output_filename_per_class TESTING_PER_CLASS.txt

#--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup/summary_compressed_1000_resnet-18_lr-0.025_bs-64_gpu-2_4x_no-scale-aug \
#--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup_bn-average/summary_bottleneck_1000_chopped-1_lr-0.025_bs-64_gpu-1_1x_bn-average_0.9_40 \
#--checkpoint_name model.ckpt-1600000
