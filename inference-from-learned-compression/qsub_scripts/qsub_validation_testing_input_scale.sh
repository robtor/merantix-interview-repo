#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

#BRANCH=master
#
##BRANCH=calc_batch_norm_average
#BASE=/scratch_net/tractor/robertto/git_repos_tmp_runs
#TEMPLATE="${TAG}_XXXXXX"
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE}  masters_thesis_repo_XXXXXX)
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/masters_thesis/ .
#pwd

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf_gpu

python -u run_multiple_validation.py \
--base_dir /srv/glusterfs/robertto/imagenet/summaries_backup_bn-average/ \
--pattern 'summary_image_1000_resnet-50_lr-0.025_bs-64_gpu-1_1x_no-scale-aug_resize-0.5_bn*' \
--output_dir /scratch_net/tractor/robertto/masters_thesis/output/output_input_scale_delete \
--steps 20000 \
--start_point 1000000 \
--end_point 1020000

#--base_dir /srv/glusterfs/robertto/imagenet/summaries_backup/ \
#python -u run_multiple_validation.py \
#--base_dir /home/robertto/masters_thesis/summaries/tmp_bn \
#--pattern 'summary_bottleneck_1000*' \
#--output_dir /scratch_net/tractor/robertto/masters_thesis/output \
#--model_basename model_start.ckpt \
#--steps 20000

#--base_dir /srv/glusterfs/robertto/imagenet/summaries_backup/ \
