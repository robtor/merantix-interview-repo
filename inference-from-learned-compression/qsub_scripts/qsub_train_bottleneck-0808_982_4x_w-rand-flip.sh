#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf_gpu


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""


python -u /scratch_net/tractor/robertto/masters_thesis/resnet_train.py \
-c0 /home/robertto/masters_thesis/configs/bottleneck.config \
-c1 /home/robertto/masters_thesis/configs/bottleneck_1000.config \
--summary_basedir /scratch_net/tractor/robertto/masters_thesis/summaries \
--directory /srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_bottleneck-0808_tfrecord \
--centers_filename /srv/glusterfs/mentzerf/roberto_train_0808_1054/centers.npy \
--operating_point 8 \
--epochs_decay 8 \
--num_gpus 1 \
--num_preprocess_threads 4 \
--data_format NCHW \
--batch_norm_decay 0.9 \
--network_name chopped-48 \
--rand_flip \
--suffix _w-rand-flip \
--from_scratch \
--num_classes 982 \
--synsets_filename /home/robertto/masters_thesis/input/synsets_2308.txt \
--input_type symbol-new \
--feat_vol_input_size 56 \
--overwrite
