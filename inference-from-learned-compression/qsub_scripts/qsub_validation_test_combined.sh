#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

#BRANCH=master
#
##BRANCH=calc_batch_norm_average
#BASE=/scratch_net/tractor/robertto/git_repos_tmp_runs
#TEMPLATE="${TAG}_XXXXXX"
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE}  masters_thesis_repo_XXXXXX)
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/masters_thesis/ .
#pwd

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf_gpu

python -u resnet_eval.py \
-c0 /scratch_net/tractor/robertto/masters_thesis/configs/bottleneck_1000_val.config \
--model_type combined \
--input_resize_factor 1 \
--output_filename /scratch_net/tractor/robertto/masters_thesis/output/output_tmp_combined/output.txt \
--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup_bn-average/summary_compressed_1000_resnet-50_lr-0.025_bs-64_gpu-2_1x_no-scale-aug_bn-average_0.9_40 \
--checkpoint_name model.ckpt-1900000
