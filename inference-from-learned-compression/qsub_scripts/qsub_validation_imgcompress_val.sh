#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf-gpu-3.6.2


python -u run_multiple_imgcompress_val.py \
--base_dir /srv/glusterfs/robertto/imagenet/summaries_backup/joint-training \
--pattern '*encoder-chopped*control*' \
--output_dir /scratch_net/tractor/robertto/masters_thesis/output/output_psnr \
--steps 10000

python -u run_multiple_imgcompress_val.py \
--base_dir /srv/glusterfs/robertto/imagenet/summaries_backup/joint-training_max-entropy-beta \
--pattern '*joint-network*' \
--output_dir /scratch_net/tractor/robertto/masters_thesis/output/output_psnr \
--steps 10000

