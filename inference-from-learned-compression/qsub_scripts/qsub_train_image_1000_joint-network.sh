#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


#echo "----------Info----------"
#current_host_name=$(uname -n)
#echo "Hostname: $current_host_name"
#echo "Job ID: $JOB_ID"
#echo "Arguments: $@"
#echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
#echo "------------------------"
#echo ""
#
#export CUDA_VISIBLE_DEVICES=7

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

pyenv activate tf-gpu-3.6.2

python3 -u /home/robertto/masters_thesis/resnet_train.py \
-c0 /home/robertto/masters_thesis/configs/image_compressed.config \
-c1 /home/robertto/masters_thesis/configs/image_1000.config \
-c2 /home/robertto/masters_thesis/configs/image_processing_encoder_decoder.config \
--encoder-decoder-dir '/srv/glusterfs/robertto/imagenet/compression_runs/0915_1533 pp:shared_centers_c12_nips_lr1e*3_bn' \
--encoder-decoder-checkpoint-name ckpt-748000 \
--checkpoint-dir '/home/robertto/glusterfs_summaries/frozen-encoder-decoder-non-fixed-bn/summary_image_1000_4x_encoder-chopped-match_lr-0.025_bs-64_no-scale-aug_key-0915-1533' \
--checkpoint-name model.ckpt-560000 \
--load-from-encoder-decoder-dir \
--summary-basedir /home/robertto/masters_thesis/summaries \
--epochs-decay 8 \
--num-gpus 1 \
--data-format NHWC \
--network-name joint-network \
--batch-norm-decay 0.9 \
--learning-rate 0.00025 \
--learning-rate-type constant \
--overwrite \
--add-joint-network-loss \
--loss-scale-factor 1000 \
--batch-size 32 \
--suffix _TEST_COREECT_GRAPH \
--add-histograms


#--loss-scale-factor 400

#--encoder-decoder-dir '/srv/glusterfs/mentzerf/out_tb_pushpsnr/1209_0941 pp:shared_centers_c12_nips' \
#--encoder-decoder-dir '/srv/glusterfs/mentzerf/out_tb_pushpsnr/1209_0941 pp:shared_centers_c12_nips' \
