#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

#BRANCH=master
#BASE=/scratch_net/tractor/robertto/git_repos_tmp_runs
#TEMPLATE="${TAG}_XXXXXX"
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE}  masters_thesis_repo_XXXXXX)
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/masters_thesis/ .
#pwd

pyenv activate tf_gpu

# If I ever need to grab the last index of a model
# ls -v model.ckpt*index | head -n 1 | grep -Eo [0-9]+

start_step=0
end_step=2000000
step_size=20000

base_folder=/srv/glusterfs/robertto/imagenet/summaries_backup

#for d in $base_folder/summary_*; do
for d in $base_folder/summary_bottleneck_200_chopped-2_lr-0.025_bs-64_gpu-4_2x_testing-gpu-4*; do
  if [[ -d $d ]]; then
    echo $d
    for i in $(eval echo {$start_step..$end_step..$step_size})
      do
        if [ -f $d/model.ckpt-$i.data-00000-of-00001 ] || \
           [ -f $d/model.ckpt-$i.index ] || \
           [ -f $d/model.ckpt-$i.meta ]; then

            python -u resnet_eval.py \
            -c0 $d/validation.config \
            --checkpoint_dir $d \
            --checkpoint_name model.ckpt-$i \
            --network_name chopped-2
        fi
      done
  fi
done
