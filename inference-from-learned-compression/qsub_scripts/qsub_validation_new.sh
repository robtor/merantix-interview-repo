#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

#BRANCH=master
#
##BRANCH=calc_batch_norm_average
#BASE=/scratch_net/tractor/robertto/git_repos_tmp_runs
#TEMPLATE="${TAG}_XXXXXX"
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE}  masters_thesis_repo_XXXXXX)
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/masters_thesis/ .
#pwd

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf-gpu-3.6.2

#python -u run_multiple_validation.py \
#--base-dir /srv/glusterfs/robertto/imagenet/summaries_backup/frozen-encoder-decoder \
#--pattern '*' \
#--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_classification_per-class \
#--steps 20000 \
#--start-point 560000 \
#--end-point 560000 \
#--save-per-class-accuracy

#python -u run_multiple_validation.py \
#--base-dir /srv/glusterfs/robertto/imagenet/summaries_backup \
#--pattern 'summary_image_1000_4x_resnet-50_lr-0.025_bs-64_gpu-1_no-scale-aug' \
#--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_classification_per-class \
#--start-point 560000 \
#--end-point 560000 \
#--steps 20000 \
#--save-per-class-accuracy

python -u run_multiple_validation.py \
--base-dir /srv/glusterfs/robertto/imagenet/summaries_backup/normal_runs \
--pattern '*' \
--output-dir /scratch_net/tractor/robertto/masters_thesis/tmp_output_test \
--steps 20000

python -u run_multiple_validation.py \
--base-dir /srv/glusterfs/robertto/imagenet/summaries_backup/normal_runs \
--pattern '*' \
--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_classification \
--steps 20000

python -u run_multiple_validation.py \
--base-dir /srv/glusterfs/robertto/imagenet/summaries_backup/frozen-encoder-decoder \
--pattern '*' \
--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_classification \
--steps 20000

python -u run_multiple_validation.py \
--base-dir /srv/glusterfs/robertto/imagenet/summaries_backup/joint-training \
--pattern '*' \
--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_classification \
--steps 20000

python -u run_multiple_validation.py \
--base-dir /srv/glusterfs/robertto/imagenet/summaries_backup/joint-training_max-entropy-beta \
--pattern '*' \
--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_classification \
--steps 20000




#
#python -u run_multiple_validation.py \
#--base-dir /srv/glusterfs/robertto/imagenet/summaries_backup \
#--pattern 'summary_image_1000_1x_resnet-50_lr-0.025_bs-64_gpu-4_no-scale-aug' \
#--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_classification_per-class \
#--steps 20000 \
#--start-point 560000 \
#--end-point 560000 \
#--save-per-class-accuracy
