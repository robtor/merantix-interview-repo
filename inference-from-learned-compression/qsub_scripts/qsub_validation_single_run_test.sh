#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

#BRANCH=combined_model
#
##BRANCH=calc_batch_norm_average
#BASE=/scratch_net/tractor/robertto/git_repos_tmp_runs
#TEMPLATE="${TAG}_XXXXXX"
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE}  masters_thesis_repo_XXXXXX)
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/masters_thesis/ .
#pwd

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf-gpu-3.6.2

python -u resnet_eval.py \
-c0 /scratch_net/tractor/robertto/masters_thesis/configs/image_1000_val.config \
--encoder-decoder-dir '/srv/glusterfs/robertto/imagenet/compression_runs/0919_0752 pp:shared_centers_c12_nips32_bn_H1.2' \
--encoder-decoder-dir '/srv/glusterfs/robertto/imagenet/compression_runs/0928_0918 pp:rob_x0.5' \
--encoder-decoder-checkpoint-name ckpt-109000 \
--output-filename /scratch_net/tractor/robertto/masters_thesis/output/tmp_output_delete.txt \
--network-name encoder-chopped-match \
--network-name encoder-decoder-resnet-50 \
--network-name encoder-resnet-6-match \
--checkpoint-dir /srv/glusterfs/robertto/imagenet/summaries_backup/frozen-encoder-decoder/summary_image_1000_4x_encoder-decoder-resnet-50_lr-0.025_bs-64_no-scale-aug_key-0928-0918_frozen \
--checkpoint-dir /srv/glusterfs/robertto/imagenet/summaries_backup/frozen-encoder-decoder/summary_image_1000_4x_encoder-resnet-6-match_lr-0.025_bs-64_no-scale-aug_key-0928-0918_frozen \
--checkpoint-name model.ckpt-120000 \
--no-subtract-mean \
--image-output-dim 256


#--network-name encoder-chopped-match \
#--checkpoint-dir /srv/glusterfs/robertto/imagenet/summaries_backup/frozen-encoder-decoder/summary_image_1000_4x_encoder-frozen-chopped-match_lr-0.025_bs-64_no-scale-aug_0915-1533 \
#--network-name encoder-decoder-resnet-50 \
#--checkpoint-dir /srv/glusterfs/robertto/imagenet/summaries_backup/frozen-encoder-decoder/summary_image_1000_4x_encoder-decoder-frozen-resnet-50_lr-0.025_bs-64_no-scale-aug_0919-0752 \
#python -u resnet_eval.py \
#-c0 /scratch_net/tractor/robertto/masters_thesis/configs/image_1000_val.config \
#--network_name encoder-decoder-frozen-chopped-match \
#--output_filename /scratch_net/tractor/robertto/masters_thesis/output/tmp_output_delete.txt \
#--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup/summary_image_1000_4x_frozen-encoder-decoder_lr-0.025_bs-64_no-scale-aug \
#--checkpoint_name model.ckpt-280000 \
#--image_dim 256

#--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup/summary_compressed_1000_resnet-50_lr-0.025_bs-64_gpu-2_1x_no-scale-aug \
#--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup/summary_compressed_1000_resnet-18_lr-0.025_bs-64_gpu-2_4x_no-scale-aug \
#--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup/summary_compressed_1000_resnet-50_lr-0.025_bs-64_gpu-2_1x_no-scale-aug \
#--output_filename_per_class TESTING_PER_CLASS.txt

#--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup/summary_compressed_1000_resnet-18_lr-0.025_bs-64_gpu-2_4x_no-scale-aug \
#--checkpoint_dir /srv/glusterfs/robertto/imagenet/summaries_backup_bn-average/summary_bottleneck_1000_chopped-1_lr-0.025_bs-64_gpu-1_1x_bn-average_0.9_40 \
#--checkpoint_name model.ckpt-1600000
