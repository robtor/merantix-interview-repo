COMPRESSION_BASEDIR=/home/robertto/glusterfs_robertto/imagenet/compression_runs

python -u run_imgcompress_val.py \
--ckpt-path ${COMPRESSION_BASEDIR}'/1016_1510 pp:shared_centers_c12_nips16_lr2e*6_bpp0.37_bn/ckpts/ckpt-959000' \
--encoder-decoder-dir ${COMPRESSION_BASEDIR}'/1016_1510 pp:shared_centers_c12_nips16_lr2e*6_bpp0.37_bn' \
--val-images-dir /srv/glusterfs/mentzerf/imagenet_256_train_val_128x128 \
--val-dataset-name roberto_val \
--output-filename tmp_test.txt

# python -u run_imgcompress_val.py \
# --ckpt-path ${COMPRESSION_BASEDIR}'/1014_1606 pp:shared_centers_c12_nips16_lr1e*5_bpp0.5_bn/ckpts/ckpt-1051000' \
# --encoder-decoder-dir ${COMPRESSION_BASEDIR}'/1014_1606 pp:shared_centers_c12_nips16_lr1e*5_bpp0.5_bn' \
# --val-images-dir /srv/glusterfs/mentzerf/imagenet_256_train_val_128x128 \
# --val-dataset-name roberto_val \
# --output-filename tmp_test.txt
#
# python -u run_imgcompress_val.py \
# --ckpt-path ${COMPRESSION_BASEDIR}'/1014_1605 pp:shared_centers_c12_nips16_lr1e*5_bpp0.37_bn/ckpts/ckpt-904000' \
# --encoder-decoder-dir ${COMPRESSION_BASEDIR}'/1014_1605 pp:shared_centers_c12_nips16_lr1e*5_bpp0.37_bn' \
# --val-images-dir /srv/glusterfs/mentzerf/imagenet_256_train_val_128x128 \
# --val-dataset-name roberto_val \
# --output-filename tmp_test.txt
#
# python -u run_imgcompress_val.py \
# --ckpt-path ${COMPRESSION_BASEDIR}'/1005_1344 pp:shared_centers_c12_nips_lr1e*3_bn/ckpts/ckpt-736000' \
# --encoder-decoder-dir ${COMPRESSION_BASEDIR}'/1005_1344 pp:shared_centers_c12_nips_lr1e*3_bn' \
# --val-images-dir /srv/glusterfs/mentzerf/imagenet_256_train_val_128x128 \
# --val-dataset-name roberto_val \
# --output-filename tmp_test.txt
#
# python -u run_imgcompress_val.py \
# --ckpt-path ${COMPRESSION_BASEDIR}'/1006_0749 pp:rob_x0.5/ckpts/ckpt-102000' \
# --encoder-decoder-dir ${COMPRESSION_BASEDIR}'/1006_0749 pp:rob_x0.5' \
# --val-images-dir /srv/glusterfs/mentzerf/imagenet_256_train_val_128x128 \
# --val-dataset-name roberto_val \
# --output-filename tmp_test.txt
