#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf-gpu-3.6.2


#python -u run_multiple_segmentation_val.py \
#--base-dir /scratch_net/tractor/robertto/masters_thesis/summaries/segmentation \
#--pattern 'segmentation-encoder-decoder-resnet-71_key-1005-1344_320x320_true-run' \
#--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_segmentation \
#--save-per-class-accuracy \
#--start-point 18000 \
#--steps 1000

python -u run_multiple_segmentation_val.py \
--base-dir /scratch_net/tractor/robertto/masters_thesis/summaries/segmentation \
--pattern 'segmentation*' \
--output-dir /scratch_net/tractor/robertto/masters_thesis/tmp_output_test \
--save-per-class-accuracy \
--steps 1000 \
--start-point 18000

python -u run_multiple_segmentation_val.py \
--base-dir /scratch_net/tractor/robertto/masters_thesis/summaries/segmentation \
--pattern 'segmentation*' \
--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_segmentation \
--save-per-class-accuracy \
--steps 1000 \
--start-point 18000

python -u run_multiple_segmentation_val.py \
--base-dir /scratch_net/tractor/robertto/masters_thesis/summaries/segmentation \
--pattern 'segmentation*' \
--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_segmentation \
--save-per-class-accuracy \
--steps 1000

#python -u run_multiple_segmentation_val.py \
#--base-dir /scratch_net/tractor/robertto/masters_thesis/summaries/segmentation \
#--pattern 'segmentation-*1005*' \
#--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_segmentation \
#--save-per-class-accuracy \
#--steps 2000
#
#python -u run_multiple_segmentation_val.py \
#--base-dir /scratch_net/tractor/robertto/masters_thesis/summaries/segmentation \
#--pattern 'segmentation-*resnet-50*main_320*' \
#--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_segmentation \
#--steps 2000 \
#--save-per-class-accuracy
#
#python -u run_multiple_segmentation_val.py \
#--base-dir /scratch_net/tractor/robertto/masters_thesis/summaries/segmentation \
#--pattern 'segmentation-*' \
#--output-dir /scratch_net/tractor/robertto/masters_thesis/output/output_segmentation \
#--steps 2000 \
#--save-per-class-accuracy

