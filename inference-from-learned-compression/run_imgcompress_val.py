import sys
sys.path.append('./vidcompress')
import vidcompress.imgcompress_val

parser = vidcompress.imgcompress_val.get_parser()
FLAGS = parser.parse_args()

vidcompress.imgcompress_val.main(FLAGS)
