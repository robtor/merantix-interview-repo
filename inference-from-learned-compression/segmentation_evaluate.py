"""Evaluation script for the DeepLab-ResNet network on the validation subset
   of PASCAL VOC dataset.

This script evaluates the model on 1449 validation images.
"""

from __future__ import print_function

import argparse
import os
import sys

sys.path.append('./vidcompress')

os.environ['CUDA_VISIBLE_DEVICES'] = os.environ['SGE_GPU']

import tensorflow as tf
import numpy as np

from deeplab_resnet import ImageReader
from networks import nets_class
from utils import metrics_wrapper

import utils


IMG_MEAN = np.array((122.67891434, 116.66876762, 104.00698793), dtype=np.float32)

DATA_DIRECTORY = '/home/VOCdevkit'
DATA_LIST_PATH = './dataset/val.txt'
IGNORE_LABEL = 255
NUM_CLASSES = 21
NUM_STEPS = 1449 # Number of images in the validation set.
RESTORE_FROM = './deeplab_resnet.ckpt'


def get_arguments():
    """Parse all the arguments provided from the CLI.
    
    Returns:
      A list of parsed arguments.
    """
    parser = argparse.ArgumentParser(description="DeepLabLFOV Network")
    parser.add_argument("--data-dir", type=str, default=DATA_DIRECTORY,
                        help="Path to the directory containing the PASCAL VOC dataset.")
    parser.add_argument("--data-list", type=str, default=DATA_LIST_PATH,
                        help="Path to the file listing the images in the dataset.")
    parser.add_argument("--ignore-label", type=int, default=IGNORE_LABEL,
                        help="The index of the label to ignore during the training.")
    parser.add_argument("--num-classes", type=int, default=NUM_CLASSES,
                        help="Number of classes to predict (including background).")
    parser.add_argument("--num-steps", type=int, default=NUM_STEPS,
                        help="Number of images in the validation set.")
    parser.add_argument("--restore-from", type=str, default=RESTORE_FROM,
                        help="Where restore model parameters from.")
    parser.add_argument("--network-name", type=str, default=None,
                        help="Name of the network to load")
    parser.add_argument("--model-scope", type=str, default='resnet',
                        help="Scope of the model. For backwards compatability")
    parser.add_argument("--encoder-decoder-dir", type=str, default=None,
                        help="Location of the information and checkpoints for encoders and decoders")
    parser.add_argument("--output-filename", type=str, default=None,
                        help="Path to the file that saves the results")
    parser.add_argument('--no-save-per-class-accuracy', dest='save_per_class_accuracy', action='store_false',
                        help="Not to save per class IoU")
    parser.add_argument('--save-per-class-accuracy', dest='save_per_class_accuracy', action='store_true',
                        help="Save per class IoU")
    parser.set_defaults(save_per_class_accuracy=False)

    return parser


def load(saver, sess, ckpt_path):
    """
    Load trained weights.
    
    Args:
      saver: TensorFlow saver object.
      sess: TensorFlow session.
      ckpt_path: path to checkpoint file with parameters.
    """
    saver.restore(sess, ckpt_path)
    print("Restored model parameters from {}".format(ckpt_path))


def main(args):
    """Create the model and start the evaluation process."""
    # Create queue coordinator.
    coord = tf.train.Coordinator()

    img_mean_true = IMG_MEAN
    if 'encoder' in args.network_name:
        # Don't subtract when passing through encoder-decoder network
        img_mean_true = np.array((0.0, 0.0, 0.0), dtype=np.float32)

    # Load reader.
    with tf.name_scope("create_inputs"):
        reader = ImageReader(
            args.data_dir,
            args.data_list,
            None,  # No defined input size.
            False,  # No random scale.
            False,  # No random mirror.
            args.ignore_label,
            img_mean_true,
            coord)
        image, label = reader.image, reader.label
    image_batch, label_batch = tf.expand_dims(image, dim=0), tf.expand_dims(label, dim=0) # Add one batch dimension.

    network_class = nets_class.get_network_by_name(args.network_name)
    network = network_class(num_classes=None,
                            is_training=False,
                            batch_norm_decay=0.9,
                            model_scope=args.model_scope,
                            data_format='NHWC',
                            input_network_log_dir=args.encoder_decoder_dir,
                            frozen_encoder_decoder=True,
                            network_name=args.network_name)

    output_dict = network.build(image_batch, reuse=None)
    raw_output = output_dict['logits']

    # Which variables to load.
    restore_var = tf.global_variables()
    
    # Predictions.
    raw_output = tf.image.resize_bilinear(raw_output, tf.shape(image_batch)[1:3,])
    raw_output = tf.argmax(raw_output, axis=3)
    pred = tf.expand_dims(raw_output, dim=3)  # Create 4-d tensor.
    
    # mIoU
    pred = tf.reshape(pred, [-1,])
    gt = tf.reshape(label_batch, [-1,])
    gt = tf.cast(gt, tf.int32)

    mask = tf.less_equal(gt, args.num_classes - 1)  # Ignoring all labels greater than or equal to n_classes.
    weights = tf.cast(mask, tf.int32)  # Ignoring all labels greater than or equal to n_classes.
    # Set all not valid to 0 and then mask them later when calling mean_iou
    gt = tf.multiply(gt, weights)

    # In newer version of tf settings weights on values above the allowed ones doesn't work so.
    # VERY IMPORTANT THAT THE WEIGHTS ARE CORRECT. otherwise we're adding to a class!
    # mIoU, update_op = tf.contrib.metrics.streaming_mean_iou(pred, gt, num_classes=args.num_classes, weights=weights)
    #mIoU, update_op = tf.metrics.mean_iou(gt, pred, num_classes=args.num_classes, weights=weights)
    (mIoU, IoU), update_op = metrics_wrapper.mean_iou(gt, pred, num_classes=args.num_classes, weights=weights)

    # Set up tf session and initialize variables.
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.allow_soft_placement = True
    sess = tf.Session(config=config)
    init = tf.global_variables_initializer()

    sess.run(init)
    sess.run(tf.local_variables_initializer())

    # Load weights.
    loader = tf.train.Saver(var_list=restore_var)
    if args.restore_from is not None:
        load(loader, sess, args.restore_from)

    # Start queue threads.
    threads = tf.train.start_queue_runners(coord=coord, sess=sess)

    # Iterate over training steps.
    for step in range(args.num_steps):
        preds, _ = sess.run([pred, update_op])
        if step % 100 == 0:
            print('step {:d}'.format(step))

    mIoU_val = mIoU.eval(session=sess)
    print('Mean IoU: {:.4f}'.format(mIoU_val))
    IoU_val = IoU.eval(session=sess)

    if args.output_filename:
        output_str = '%s,%.4f,%d' % (os.path.basename(args.restore_from), mIoU_val, args.num_steps)
        with open(args.output_filename, 'a') as f:
            f.write(output_str + '\n')

    if args.save_per_class_accuracy:
        with open(utils.append_to_filename(args.output_filename, suffix='21_classes'), 'a') as f:
            acc_per_class_str = ','.join(['%.5f' % num for num in IoU_val])
            output_str_per_class = os.path.basename(args.restore_from) + ',' + acc_per_class_str
            f.write(output_str_per_class + '\n')

    coord.request_stop()
    coord.join(threads)


if __name__ == '__main__':
    parser = get_arguments()
    args = parser.parse_args()

    main(args)
