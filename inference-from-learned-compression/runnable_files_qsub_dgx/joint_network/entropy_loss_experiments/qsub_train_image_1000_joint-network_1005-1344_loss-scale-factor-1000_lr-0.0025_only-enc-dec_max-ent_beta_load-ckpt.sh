#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

echo $HOSTNAME
if [[ "$HOSTNAME" = *"biwi"* ]]; then
    echo "On biwi"
    BASEFOLDER=/scratch_net/tractor/robertto/masters_thesis
    SUMMARIES_FOLDER=${BASEFOLDER}/summaries
    ENCODER_DECODER_BASEDIR=/home/robertto/glusterfs_robertto/imagenet/compression_runs/
    # data_format really depends on script, so this is not always best
    DATA_FORMAT=NHWC

    source ~/.pyenvrc
    export CUDA_VISIBLE_DEVICES=$SGE_GPU
    echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
    pyenv activate tf-gpu-3.6.2
else
    echo "Not on biwi"
    BASEFOLDER=/raid/robertto/masters_thesis
    SUMMARIES_FOLDER=/raid/robertto/summaries
    ENCODER_DECODER_BASEDIR=/raid/mentzerf/vidcompress/dgx_logs_tb/
    CONFIG_SUFFIX=_dgx1
    # data_format really depends on script, so this is not always best
    DATA_FORMAT=NCHW

    if [ -z "$1" ]
      then
        echo "No CUDA number supplied"
        echo "Exiting..."
        exit 1
    fi
    export CUDA_VISIBLE_DEVICES=$1
fi

echo $1

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

echo "BETA: ${2}"
echo "MAX_ENTROPY: ${3}"

python3 -u ${BASEFOLDER}/resnet_train.py \
-c0 ${BASEFOLDER}/configs/image_compressed.config \
-c1 ${BASEFOLDER}/configs/image_1000${CONFIG_SUFFIX}.config \
-c2 ${BASEFOLDER}/configs/image_processing_encoder_decoder.config \
--summary-basedir ${SUMMARIES_FOLDER} \
--encoder-decoder-dir ${ENCODER_DECODER_BASEDIR}'1005_1344 pp:shared_centers_c12_nips_lr1e*3_bn' \
--only-encoder-decoder \
--epochs-decay 3 \
--num-gpus 1 \
--data-format NHWC \
--batch-norm-decay 0.9 \
--network-name joint-network-chopped-match \
--suffix "_key-1005-1344_loss-scale-factor-1000_only-enc-dec_max-ent-${3}_beta-${2}" \
--learning-rate 0.0025 \
--loss-scale-factor 1000 \
--add-joint-network-loss \
--max-entropy ${3} \
--beta ${2} \
--save-checkpoint-steps 10000
