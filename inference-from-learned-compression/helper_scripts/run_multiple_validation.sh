#!/bin/bash

# If I ever need to grab the last index of a model
# ls -v model.ckpt*index | head -n 1 | grep -Eo [0-9]+

start_step=0
end_step=2000000
step_size=20000

base_folder=/srv/glusterfs/robertto/imagenet/summaries_backup

for d in $base_folder/summary_*; do
  if [[ -d $d ]]; then
    echo $d
    for i in $(eval echo {$start_step..$end_step..$step_size})
      do
        if [ -f $d/model.ckpt-$i.data-00000-of-00001 ] || \
           [ -f $d/model.ckpt-$i.index ] || \
           [ -f $d/model.ckpt-$i.meta ]; then

            python -u resnet_eval.py \
            -c0 $d/validation.config \
            --checkpoint_dir $d \
            --checkpoint_name model.ckpt-$i
        fi
      done
  fi
done
