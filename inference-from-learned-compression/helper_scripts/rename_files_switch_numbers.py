import os
import glob
import argparse

def reverse_numbers_in_filename(model_path):
    # Example:
    # 
    model_name = os.path.basename(model_path)
    m1, m2, m3 = model_name.split('.')
    
    name1 = m1.split('-')
    name2 = m2.split('-')
    
    name1[1], name2[1] = name2[1], name1[1]
    
    name1_str = '-'.join(name1) 
    name2_str = '-'.join(name2) 
    
    model_name_out = '.'.join([name1_str, name2_str, m3])

    return model_name_out

def main(FLAGS):
    """
    This script tranforms names such as
    model_bn-520000.ckpt-8000.index --> model_bn-8000.ckpt-520000.index

    This script has no checks, make sure it's doing the right thing.
    For example if the numbers were 20000 and 30000 and there is 
    another filename that has 30000 and 20000 on of them will be overwritten
    """
    model_paths = glob.glob(os.path.join(FLAGS.base_dir, FLAGS.pattern))
    model_paths.sort()

    for model_path in model_paths:
        model_name_out = reverse_numbers_in_filename(model_path)
        model_path_dst = os.path.join(FLAGS.base_dir, model_name_out)

        print('Renaming:')
        print(model_path)
        print('---------------->')
        print(model_path_dst)

        if FLAGS.rename:
            os.rename(model_path, model_path_dst) 
            print('Was renamed')
        else:
            print('Will be renamed, set flag --rename to rename files')
        print()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # TODO A bit hacky, find a way to pass a list
    parser.add_argument("--base_dir",
                        type=str,
                        help="Dir to search in")
    parser.add_argument("--pattern",
                        type=str,
                        default='*',
                        help="Glob pattern to search for")
    parser.add_argument('--rename', dest='rename', action='store_true',
                        help="This flags sets rename to on, default is to not rename, only print filenames")
    parser.set_defaults(rename=False)
    FLAGS = parser.parse_args()

    main(FLAGS)
