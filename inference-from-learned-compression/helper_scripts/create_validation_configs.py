import glob
import os
import shutil

BASEFOLDER_OUTPUT = '/scratch_net/tractor/robertto/masters_thesis/output/output_new'
BASEFOLDER_CONFIGS = '/scratch_net/tractor/robertto/masters_thesis/configs'

CONFIG_MAPPING = {'bottleneck_200': 'bottleneck_200_val.config',
                  'bottleneck_1000': 'bottleneck_1000_val.config',
                  'compressed_200': 'compressed_200_val.config',
                  'compressed_1000': 'compressed_1000_val.config',
                  'original_200': 'image_200_val.config',
                  'original_1000': 'image_1000_val.config',
                  'image_200': 'image_200_val.config',
                  'image_1000': 'image_1000_val.config',
                  }

def main():
    basedir = '/srv/glusterfs/robertto/imagenet/summaries_backup'
    pattern = basedir + '/*/'
    pattern = basedir + '/summary_bottleneck_200_chopped-2_lr-0.025_bs-64_gpu-4_2x_testing-gpu-4*'

    dir_names = glob.glob(pattern)

    for dir_name in dir_names:
        if 'summary_' in dir_name:
            folder_name = os.path.basename(os.path.normpath(dir_name))

            # This all depends on the exact format for the filename
            mapping_key = '_'.join((folder_name.split('_')[1:3]))

            config_path = os.path.join(BASEFOLDER_CONFIGS, CONFIG_MAPPING[mapping_key])

            output_validation_file_path = os.path.join(dir_name,'validation.config')
            shutil.copy2(config_path, output_validation_file_path)

            print('=' * 70)
            print('Copied\n{}\nto\n{}'.format(config_path, output_validation_file_path))
            
            print('With added lines:')

            with open(output_validation_file_path, 'a') as f:
                f.write('\n')
                output_filename_config = 'output_filename={}\n'.format(
                        os.path.join(BASEFOLDER_OUTPUT, folder_name + '_val.txt')) 

                f.write(output_filename_config)
                print(output_filename_config.strip())
                if any(x in folder_name for x in ['chopped_2', 'chopped-2']):
                    network_name = 'network_name={}\n'.format('chopped-2') 
                    f.write(network_name)
                    print(network_name.strip())
            print('=' * 70)

if __name__ == '__main__':
    main()
