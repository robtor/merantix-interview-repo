import os
import tensorflow as tf
import numpy as np
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import math_ops


BASEPATH = '/scratch_net/tractor/robertto/imagenet_256/roberto/n01924916'
filelist = ['ILSVRC2012_val_00033409_sym.npy']

#BASEPATH = '.'
#filelist = ['ILSVRC2012_val_00018111_sym.npy']

pathlist = []

for f in filelist:
    pathlist.append(os.path.join(BASEPATH, f))

filename_queue = tf.train.string_input_producer(pathlist)
reader = tf.WholeFileReader()

filename_val, np_file = reader.read(filename_queue)

numpy_tensor = tf.decode_raw(np_file, tf.int16)

numpy_tensor = tf.reshape(numpy_tensor, (5672, ))
numpy_tensor = numpy_tensor[40:]
numpy_tensor = tf.cast(numpy_tensor, tf.int32)

params = tf.constant(np.array(range(0, 1000)) * 10.0, tf.float32)
mapped = tf.nn.embedding_lookup(params, numpy_tensor)

mapped = tf.reshape(mapped, (16, 352))

init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
init_op = tf.group(*init_list)

with tf.Session() as sess:

    sess.run(init_op)
    #sess.run(init_op_table)

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    np_tensor1, np_tensor2 = sess.run([mapped, numpy_tensor])

    print(np_tensor1)
    print(np_tensor2)

    coord.request_stop()
    coord.join(threads)
