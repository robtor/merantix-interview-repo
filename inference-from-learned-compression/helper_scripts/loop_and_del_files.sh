#!/bin/bash

start_step=0
end_step=2500000
step_size=10000

base_folder=/srv/glusterfs/robertto/imagenet/summaries_backup

for f in $base_folder/*; do
    if [[ -d $f ]]; then
        echo $f
        for i in $(eval echo {$start_step..$end_step..$step_size})
            do
                if ((i % 20000)); then
                    echo Removing model.ckpt-$i
                    rm $f/model.ckpt-$i.*
                    #echo $f/model.ckpt-$i.*
                fi
            done
    fi
done


#original
#data_folder=/srv/glusterfs/robertto/imagenet/summaries_backup/summary_bottleneck_1000_1x_no-decay
#
#for i in $(eval echo {$start_step..$end_step..$step_size})
#do
#    if ((i % 20000)); then
#        echo Removing model.ckpt-$i
#        rm $data_folder/model.ckpt-$i.*
#        #echo $data_folder/model.ckpt-$i.*
#    fi
#done
