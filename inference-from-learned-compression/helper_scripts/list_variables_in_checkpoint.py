import tensorflow as tf
import argparse


def main(flags):
    for var_name, _ in tf.contrib.framework.list_variables(flags.directory):
        print(var_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--directory",
                        type=str,
                        help="Default directory to get data from")

    FLAGS = parser.parse_args()
    main(FLAGS)
