# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Converts MNIST data to TFRecords file format with Example protos."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os
import sys
import glob

import tensorflow as tf
import numpy as np

from PIL import Image
from tensorflow.contrib.learn.python.learn.datasets import mnist

FLAGS = None


def _int64_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def write_tfrecord():
  #filelist = glob.glob(os.path.join(FLAGS.directory, '*', '*.JPEG'))
  filelist = glob.glob(os.path.join(FLAGS.directory, '*.JPEG'))

  print(filelist)
  filename = os.path.join(FLAGS.directory, 'test_data.tfrecords')

  decode_jpeg_data = tf.placeholder(dtype=tf.string)
  decode_jpeg = tf.image.decode_jpeg(decode_jpeg_data, channels=3)

  with tf.Session() as sess:
    writer = tf.python_io.TFRecordWriter(filename)

    filename = os.path.join(FLAGS.directory, 'test_data.tfrecords')
    print('Writing', filename)
    writer = tf.python_io.TFRecordWriter(filename)

    for i, f_name in enumerate(filelist):
      with tf.gfile.FastGFile(f_name, 'r') as f:
        image_data = f.read()

      img = sess.run(decode_jpeg, feed_dict={decode_jpeg_data: image_data})
      rows, cols, depth = img.shape

      label = i % 10

      example = tf.train.Example(features=tf.train.Features(feature={
          'height': _int64_feature(rows),
          'width': _int64_feature(cols),
          'depth': _int64_feature(depth),
          'label': _int64_feature(label),
          'image_raw': _bytes_feature(image_data)}))
      writer.write(example.SerializeToString())

      if i % 100 == 0:
        print('step: {}'.format(i))

    writer.close()

def read_tfrecord():
  filename = os.path.join(FLAGS.directory, 'validation-00021-of-00128')
  filename_queue = tf.train.string_input_producer([filename])

  reader = tf.TFRecordReader()

  key, serialized_example = reader.read(filename_queue)

  features = tf.parse_single_example(
      serialized_example,
      features={
          'image_raw': tf.FixedLenFeature([], tf.string),
          'label': tf.FixedLenFeature([], tf.int64),
          'depth': tf.FixedLenFeature([], tf.int64)
      })

  image_jpeg = tf.image.decode_jpeg(features['image_raw'], channels=3)
  label = features['label']
  depth = features['depth']

  # TEST THE VALiDATION STUFF

  with tf.Session() as sess:
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    for i in range(0, 10):
      l, d, im = sess.run([label, depth, image_jpeg])

      #img = Image.fromarray(im, 'RGB')
      #img.show()
      print(im.shape)
  
    coord.request_stop()
    coord.join(threads, stop_grace_period_secs=10)

def main(unused_argv):
  #write_tfrecord()
  read_tfrecord()


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument(
      '--directory',
      type=str,
      default='/home/robert/image_net_samples/dataset/dogs/n02105855',
      help='Directory to download data files and write the converted result'
  )

  FLAGS, unparsed = parser.parse_known_args()
  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
