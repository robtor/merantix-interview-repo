import glob
import sys
import os
import argparse

# A bit of a hack
sys.path.insert(0, os.path.abspath('..'))

from masters_thesis import utils


FLAGS = None

#TODO Move into utils
def get_4_challenge_synsets():
    return ['n01910747', 'n03584829', 'n04429376', 'n12144580']

def main():
    filelist = []
    challenge_synsets = utils.get_challenge_synsets(FLAGS.synsets_filename)
    #challenge_synsets = get_4_challenge_synsets()

    print('Listing files across {} synsets'.format(len(challenge_synsets)))

    for i, synset in enumerate(challenge_synsets):
        image_file_pattern = '%s/%s/*.%s' % (FLAGS.directory,
                                             synset,
                                             FLAGS.extension)
    
        filelist.extend(glob.glob(image_file_pattern))

        if (i+1) % 100 == 0:
            print('Processed synset {} of {}'.format((i+1), len(challenge_synsets)))

    print('Found {} files across {} synsets'.format(len(filelist),
                                                    len(challenge_synsets)))

    with open(FLAGS.output_filename, 'w') as f:
        for f_name in filelist:
            f.write(f_name.strip() + '\n')

    print('Results saved to {}'.format(FLAGS.output_filename))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--directory",
                        type=str,
                        help="Default directory to get data from")
    parser.add_argument('--extension',
                        type=str,
                        default='png')
    parser.add_argument('--synsets_filename',
                        type=str,
                        default='input/imagenet_lsvrc_2015_synsets.txt')
    parser.add_argument('--output_filename',
                        type=str)

    FLAGS = parser.parse_args()

    main()
