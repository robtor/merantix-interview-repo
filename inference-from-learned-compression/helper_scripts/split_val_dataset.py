import os
import glob
import random
import math
import argparse


FLAGS = None

def write_to_file(filelist, out_filename):
    with open(out_filename, 'w') as f:
        for f_name in filelist:
            f.write(f_name + '\n')

def append_suffix_to_filename(filename, suffix=''):
    name, ext = os.path.splitext(filename)
    return "{name}_{suffix}{ext}".format(name=name, suffix=suffix, ext=ext)


def main():
    num_files_to_keep = math.ceil(FLAGS.fraction * FLAGS.num_files_per_folder)

    challenge_synsets = [l.strip() for l in open(FLAGS.synsets_file, 'r')]
    #assert(len(dir_paths) == 1000)

    val_set = []
    rest_set = []
    
    for synset in challenge_synsets:
        image_file_pattern = '%s/%s/*.%s' % (FLAGS.directory,
                                             synset,
                                             FLAGS.extension)

        matching_files = glob.glob(image_file_pattern)

        assert(len(matching_files) == FLAGS.num_files_per_folder)

        shuffled_index = list(range(len(matching_files)))
        #random.seed(12345)
        random.shuffle(shuffled_index)

        matching_files = [matching_files[i] for i in shuffled_index]

        val_set.extend(matching_files[0:num_files_to_keep])
        rest_set.extend(matching_files[num_files_to_keep:])
    
    result = set(val_set).intersection(set(rest_set))
    assert(len(result) == 0)
    assert((len(val_set) + len(rest_set)) == FLAGS.num_examples)

    if FLAGS.save_results:
        filename_val = append_suffix_to_filename(FLAGS.output_filename,
                                                 suffix='val')
        filename_rest = append_suffix_to_filename(FLAGS.output_filename,
                                                 suffix='rest')
        
        write_to_file(val_set, filename_val)
        write_to_file(rest_set, filename_rest)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--num_examples",
                        type=int,
                        default=50000,
                        help="Number of total examples expected")
    parser.add_argument("--num_files_per_folder",
                        type=int,
                        default=50,
                        help="Number of examples per folder expected")
    parser.add_argument("--fraction",
                        type=float,
                        default=1.0,
                        help="Fraction of the examples to output. Reduced validation.")
    parser.add_argument("--directory",
                        type=str,
                        default="/scratch_net/tractor/robertto/imagenet_256/roberto",
                        help="Default directory to get data from")
    parser.add_argument('--extension',
                        type=str,
                        default='png',
                        help="Extension of the image files to search for")
    parser.add_argument('--synsets_file',
                        type=str,
                        default='input/imagenet_lsvrc_2015_synsets.txt',
                        help="File that contains a list of all the synsets")
    parser.add_argument('--output_filename',
                        type=str,
                        default='set_filenames.txt',
                        help="Name of output file")

    parser.add_argument('--save_results', dest='save_results', action='store_true')
    parser.set_defaults(save_results=False)

    FLAGS = parser.parse_args()

    main()
