import argparse
import os
import glob
import time
import sys
import datetime

import numpy as np
from PIL import Image

FLAGS = None


def get_current_range(challenge_synsets, split_idx, num_splits=1):
    spacing = np.linspace(0, len(challenge_synsets), num_splits + 1).astype(np.int)

    idx_start = spacing[split_idx]
    idx_end = spacing[split_idx+1]

    return challenge_synsets[idx_start:idx_end]


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def append_suffix(filename, suffix=''):
    name, ext = os.path.splitext(filename)
    return "{name}{suffix}{ext}".format(name=name, suffix=suffix, ext=ext)


def get_filelist_list(synsets_filename, idx_splits, num_splits):
    """
    Gets a list of filelist. Each of these filelists contains all the filenames
    from one synset
    """
    filelist_list = []

    challenge_synsets = [l.strip() for l in open(synsets_filename, 'r')]
    challenge_synsets = get_current_range(challenge_synsets, idx_splits, num_splits=num_splits)
    #challenge_synsets = ['n01873310', 'n01877812']

    for i, synset in enumerate(challenge_synsets):
        image_file_pattern = os.path.join(FLAGS.directory, synset, '*.%s' % FLAGS.extension)

        #filelist.extend(glob.glob(image_file_pattern))
        filelist_list.append(glob.glob(image_file_pattern))
        if i % 100 == 0:
            print('Finished reading filenames from synset {} of {}'.format(i, len(challenge_synsets)))

    return filelist_list, challenge_synsets


def get_filelist_list_with_new_filenames(filelist_list, map_name_from='_sym.bin', map_name_to='.png'):

    """
    Replaces the '.png' in each filename of the filelists within
    filelist_list with '_sym.bin'
    """
    filelist_img_list = []
    for filelist in filelist_list:
        filelist_img_list.append([f_name.replace(map_name_from, map_name_to) for f_name in filelist])

    return filelist_img_list

def main():
    file_exists = os.path.exists(FLAGS.output_filename)
    if file_exists:
        question ='File {} already exists, are you sure you want to append the results to it?'.format(FLAGS.output_filename) 
        answer = query_yes_no(question, default='yes')

        if not answer:
            return 0

    print('Reading filenames start')

    start_time = time.time()

    filelist_list, challenge_synsets = get_filelist_list(FLAGS.synsets_filename, FLAGS.idx_splits, FLAGS.num_splits)
    filelist_img_list = get_filelist_list_with_new_filenames(filelist_list,
                                                             map_name_from=FLAGS.map_name_from,
                                                             map_name_to=FLAGS.map_name_to)

    num_files = sum([len(f_list) for f_list in filelist_list])

    print('Reading filenames stop. Took {:.2f} sec'.format(time.time() - start_time))
    print('Found {} files across {} synsets'.format(num_files, len(challenge_synsets)))

    if not file_exists:
        with open(FLAGS.output_filename, 'a') as f:
            f.write('num_files,{}\n'.format(num_files))
            f.write('name,height,width\n')

    print('Getting shapes start')
    start_time = time.time()

    finished_synsets = []

    for filelist_img, synset in zip(filelist_img_list, challenge_synsets):

        print(datetime.datetime.now())
        print('Reading shapes of files from synset {}'.format(synset))

        filelist_img_shape = []

        for f_name in filelist_img:
            im = Image.open(f_name)
            width, height = im.size

            filelist_img_shape.append((height, width))

        print('Writing shapes of files from synset {}'.format(synset))
        with open(FLAGS.output_filename, 'a') as f:
            filelist_img_reduced = [os.path.splitext(os.path.basename(f_name))[0] for f_name in filelist_img]

            for f_name_img, f_name_img_shape in zip(filelist_img_reduced, filelist_img_shape):
                f.write('{},{},{}\n'.format(f_name_img, *f_name_img_shape))

        finished_synsets.append(synset)

        with open(append_suffix(FLAGS.output_filename, '_synsets_finished'), 'a') as f:
            f.write(synset + '\n')
        


    print('Getting shapes stop. Took {:.2f} s'.format(time.time() - start_time))

    #d = dict(zip(filelist_img, filelist_img_shape))
    # TODO Append to file for each synset, so if it crashes I can resume
    # TODO Add sanity check in the end where all filenames are checked

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--directory",
                        type=str,
                        default="/scratch_net/tractor/robertto/imagenet_256/roberto",
                        help="Default directory to get data from")
    parser.add_argument('--extension',
                        type=str,
                        default='bin')
    parser.add_argument('--synsets_filename',
                        type=str,
                        default='input/imagenet_lsvrc_2015_synsets.txt')
    parser.add_argument('--output_filename',
                        type=str)
    parser.add_argument('--map_name_from',
                        type=str,
                        default='_sym.bin')
    parser.add_argument('--map_name_to',
                        type=str,
                        default='.png')
    parser.add_argument('--num_splits',
                        type=int,
                        default=1)
    parser.add_argument('--idx_splits',
                        type=int,
                        default=0)
    FLAGS = parser.parse_args()

    main()
