#!/bin/bash

start_step=0
end_step=1500000
step_size=10000

data_folder=/raid/robertto/summaries/summary_bottleneck_200_4x
target_folder=/home/robertto/glusterfs_r/imagenet/summaries_backup/summary_bottleneck_200_4x

mkdir -p $target_folder

rsync -avh $data_folder/events.out.tfevents.* $target_folder
rsync -avh $data_folder/checkpoint $target_folder

for i in $(eval echo {$start_step..$end_step..$step_size})
do
    echo Copying model.ckpt-$i
    rsync -avh $data_folder/model.ckpt-$i.* $target_folder
done
