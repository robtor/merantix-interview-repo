import glob
import argparse
import os


def main(FLAGS):
    filenames = glob.glob(os.path.join(FLAGS.directory, FLAGS.pattern))

    print('Found {} files'.format(len(filenames)))


    tot_counter = 0

    for filename in filenames:
        with open(filename, 'r') as f:
            num_files = int(f.readline().split(',')[1])
            header = f.readline()

            file_mappings = []
            counter = 0
            for line in f:
                file_mappings.append(line)
                counter += 1

            if counter != num_files:
                print('{} has a mismatch. Expected {} mappings but got {}'.format(filename, num_files, counter))
                print('Equal number')

        if not os.path.exists(FLAGS.output_filename):
            with open(FLAGS.output_filename, 'a') as f:
                f.write(header)

        with open(FLAGS.output_filename, 'a') as f:
            for file_mapping in file_mappings:
                f.write(file_mapping.replace(FLAGS.remove, ''))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--output_filename",
                        type=str)
    parser.add_argument("--pattern",
                        type=str,
                        default='*')
    parser.add_argument("--directory",
                        type=str,
                        help="Default directory to get data from")
    parser.add_argument("--remove",
                        type=str,
                        default='',
                        help='String to remove from each line, such as an extra \'.JPEG\'')
    FLAGS = parser.parse_args()

    main(FLAGS)
