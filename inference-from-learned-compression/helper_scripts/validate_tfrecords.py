import os
import glob
import tensorflow as tf

def main():
    filelist = glob.glob(os.path.join('/scratch_net/tractor/robertto/imagenet_256/images_train_compress_reduced_tfrecord', '*.tfrecord'))

    print(len(filelist))
    #print(filelist)

    filename_queue = tf.train.string_input_producer(
    filelist, num_epochs=1)

    reader = tf.TFRecordReader()

    _, serialized_example = reader.read(filename_queue)

    features_map = {
    'image/height': tf.FixedLenFeature([], tf.int64),
    'image/width': tf.FixedLenFeature([], tf.int64),
    'image/colorspace': tf.FixedLenFeature([], tf.string),
    'image/channels': tf.FixedLenFeature([], tf.int64),
    'image/class/label': tf.FixedLenFeature([], tf.int64),
    'image/class/synset': tf.FixedLenFeature([], tf.string),
    'image/class/text': tf.FixedLenFeature([], tf.string),
    'image/format': tf.FixedLenFeature([], tf.string),
    'image/filename': tf.FixedLenFeature([], tf.string),
    'image/encoded': tf.FixedLenFeature([], tf.string)
    }

    features = tf.parse_single_example(serialized_example,
                                       features=features_map)


    #image = tf.image.decode_png(features['image/encoded'], channels=3)
    feats_out = []
    for key in sorted(features_map.keys()):
        if not ('encoded' in key):
            feats_out.append(features[key])

    filename = features['image/filename']
    label_val = features['image/class/label']
    filenames = []

    with tf.Session() as sess:
        sess.run(tf.local_variables_initializer())
        sess.run(tf.initialize_all_variables())

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        try:

            images_list = []

            step = 0
            while True:
                #sess.run(filename)
                filenames.append(sess.run(filename))
                step += 1
                if step % 10000 == 0 or step > 1200000:
                    print(step)

        except Exception as e:  # pylint: disable=broad-except
            coord.request_stop(e)

        coord.request_stop()
        coord.join(threads, stop_grace_period_secs=10)

        print(step)
        name_set = set(filenames)
        print(len(name_set))

        #print(filenames)

        
        coord.request_stop()
        coord.join(threads, stop_grace_period_secs=10)
if __name__ == '__main__':
    main()
