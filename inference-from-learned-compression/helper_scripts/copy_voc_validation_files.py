import os
import shutil


basefolder = '/srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012'
output_folder = 'VOCValidation'

with open('dataset/val.txt', 'r') as f:
    for line in f:
        f_name, f_label= line.strip().split()

        f_path_img = basefolder + f_name
        f_path_label = basefolder + f_label

        print('Copying {} ----> {}'.format(f_path_img, output_folder))
        shutil.copy2(f_path_img, output_folder)

        print('Copying {} ----> {}'.format(f_path_label, output_folder))
        shutil.copy2(f_path_label, output_folder)
