import glob
import random
import shutil
import argparse


LABELS_FILE = '/raid/robertto/input/imagenet_lsvrc_2015_synsets.txt'


def get_challenge_synsets(labels_file):
    with open(labels_file, 'r') as f:
        challenge_synsets = [l.strip() for l in f]

    return challenge_synsets


def copy_random_images(data_dir,
                       output_dir,
                       num_files_per_folder=1,
                       extension='JPEG',
                       random_seed=None,
                       labels_file=LABELS_FILE):

    random.seed(random_seed)
    challenge_synsets = get_challenge_synsets(labels_file)

    filenames = []

    for i, synset in enumerate(challenge_synsets):
        jpeg_file_path = '{}/{}/*.{}'.format(data_dir, synset, extension)
        matching_files = glob.glob(jpeg_file_path)
        matching_files.sort()
        sampled_files = random.sample(matching_files, num_files_per_folder)

        filenames.extend(sampled_files)

    for f_name in filenames:
        shutil.copy2(f_name, output_dir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--image-dir",
                        type=str,
                        required=True,
                        help='')
    parser.add_argument("--output-dir",
                        type=str,
                        required=True,
                        help='')
    parser.add_argument("--num-files-per-folder",
                        type=int,
                        default=1,
                        help='')
    parser.add_argument("--extension",
                        type=str,
                        default='JPEG',
                        help='')
    parser.add_argument("--random-seed",
                        type=int,
                        default=None,
                        help='')
    FLAGS = parser.parse_args()

    copy_random_images(FLAGS.image_dir,
                       FLAGS.output_dir,
                       num_files_per_folder=FLAGS.num_files_per_folder,
                       extension=FLAGS.extension,
                       random_seed=FLAGS.random_seed)
