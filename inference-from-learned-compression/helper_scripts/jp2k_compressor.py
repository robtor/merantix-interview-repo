import glob

import os
import re
import sys
import subprocess
import argparse
from contextlib import contextmanager

import time
from PIL import Image
import numpy as np
import scipy.misc
import operator

from datetime import datetime


################################################################################


class BinarySearchFailedException(Exception):
    def __init__(self, discovered_values):
        self.discovered_values = discovered_values

    def first_x_yielding_y_greater_than(self, y_target):
        for x, y in sorted(self.discovered_values, key=operator.itemgetter(1)):
            if y > y_target:
                return x
        raise ValueError('No x found with y > {} in {}.'.format(y_target, self.discovered_values))


def binary_search(f, f_type, y_target, y_target_eps, x_min, x_max, x_eps, max_num_iter=1000):
    """ does binary search on f :: X -> Y by calculating y = f(x).
    f is assumed to be monotonically increasing iff f_tpye == 'increasing' and monotonically decreasing iff
    f_type == 'decreasing'.
    Returns first (x, y) for which |y_target - f(x)| < y_target_eps. x_min, x_max specifiy initial search interval for x.
    Stops if x_max - x_min < x_eps. Raises BinarySearchFailedException when x interval too small or if search takes
    more than max_num_iter iterations. The expection has a field `discovered_values` which is a list of checked
    (x, y) coordinates. """
    assert f_type in ('increasing', 'decreasing')
    cmp_op = {'increasing': operator.gt,
              'decreasing': operator.lt}[f_type]
    discovered_values = []
    #print_col_width = len(str(x_max)) + 3
    for _ in range(max_num_iter):
        x = x_min + (x_max - x_min) / 2
        y = f(x)
        discovered_values.append((x, y))
        #print('[{:{width}.2f}, {:{width}.2f}] -- g(f({:{width}.2f})) = {:.2f}'.format(
            #x_min, x_max, x, y, width=print_col_width))
        if abs(y_target - y) < y_target_eps:
            #print('y_target - y: {}'.format(y_target - y))
            return x, y
        if cmp_op(y, y_target):
            x_max = x
        else:
            x_min = x
        if x_max - x_min < x_eps:
            #print('Stopping, interval too close!')
            break
    sorted_discovered_values = sorted(discovered_values)
    first_y, last_y = sorted_discovered_values[0][1], sorted_discovered_values[-1][1]
    if (f_type == 'increasing' and first_y > last_y) or (f_type == 'decreasing' and first_y < last_y):
        raise ValueError('Got f_type == {}, but first_y, last_y = {}, {}'.format(
            f_type, first_y, last_y))
    raise BinarySearchFailedException(discovered_values)


################################################################################


ENV_VAR_KDU_COMPRESS = 'KDU_COMPRESS'
_KDU_COMPRESS_PATH = os.environ.get(ENV_VAR_KDU_COMPRESS, 'kdu_compress')
_KDU_RE_PAT = r'Compressed bytes \(excludes codestream headers\) = .*=\s(.*)\sbpp'

LABELS_FILE = '/raid/robertto/input/imagenet_lsvrc_2015_synsets.txt'


from skimage.measure import compare_psnr
import collections


def get_challenge_synsets(labels_file):
    with open(labels_file, 'r') as f:
        challenge_synsets = [l.strip() for l in f]

    return challenge_synsets


def get_list_of_files(data_dir, labels_file, extension):
    print('Determining list of input files and labels from %s.' % data_dir)
    challenge_synsets = get_challenge_synsets(labels_file)

    filenames = []
    filecounter = 0

    for i, synset in enumerate(challenge_synsets):
        jpeg_file_path = '{}/{}/*.{}'.format(data_dir, synset, extension)
        matching_files = glob.glob(jpeg_file_path)
        filenames.append((synset, matching_files))

        filecounter += len(matching_files)

        if i % 100 == 0:
            print('Listing files in synset {} of {}'.format(i, len(challenge_synsets)))

    print('Found {} files across {} synsets'.format(filecounter, len(challenge_synsets)))

    return filenames


def create_directory_structure(output_folder, labels_file):
    print('Creating synset directories in output folder: {}'.format(output_folder))
    challenge_synsets = get_challenge_synsets(labels_file)
    for synset in challenge_synsets:
        directory = os.path.join(output_folder, synset)

        if not os.path.exists(directory):
            os.makedirs(directory)


def convert_to_jpeg(img_compressed):
    im = Image.open(img_compressed)
    output_name_jpeg = os.path.splitext(img_compressed)[0] + '.JPEG'
    im.save(output_name_jpeg, optimize=False, quality=100)


def compress_imgs(img_dir_p,
                  target_bpp,
                  output_folder,
                  output_filename,
                  extension='JPEG',
                  labels_file=LABELS_FILE,
                  convert_to_jpeg_flag=True):
    filenames_synsets = get_list_of_files(img_dir_p, labels_file, extension)
    create_directory_structure(output_folder, labels_file)
    times = []

    for j, (synset, filenames) in enumerate(filenames_synsets):
        synset_sum = 0.0
        output_folder_synset = os.path.join(output_folder, synset)
        for i, img in enumerate(filenames):
            s = time.time()
            img_compressed, actual_bpp = compress_img_no_search(img, target_bpp, output_folder=output_folder_synset)
            #print('{} -> {}'.format(target_bpp, actual_bpp))
            synset_sum += actual_bpp

            if convert_to_jpeg_flag:
                convert_to_jpeg(img_compressed)

            times.append(time.time() - s)
            if i % 10 == 0:
                view_string = 'synset: {}/{}, {}, {}'.format(j,
                                                             len(filenames_synsets),
                                                             i,
                                                             np.round(np.mean(times), decimals=4))
                print(view_string, end='\r')

        synset_avg_bpp = synset_sum / float(len(filenames))
        output_str = '{},{},{}'.format(synset, synset_avg_bpp, len(filenames))

        print(' ' * 30, end='\r')
        if j % 5 == 0:
            print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        print(output_str)
        with open(output_filename, 'a') as f:
            f.write(output_str + '\n')


def _open_im(im_p):
    return scipy.misc.fromimage(Image.open(im_p))


def compress_img(input_image_p, target_bpp, no_weights=True):
    output_image_j2_p = os.path.splitext(input_image_p)[0] + '_out_jp2.jp2'
    # kdu can only work with "tif", "tiff", "bmp", "pgm", "ppm", "raw" and "rawl"
    with remove_file_after(convert_im_to('bmp', input_image_p)) as input_image_bmp_p:
        def compress_with_bpp(bpp):
            cmd = [_KDU_COMPRESS_PATH,
                   '-i', input_image_bmp_p, '-o', output_image_j2_p,
                   '-rate', str(bpp), '-no_weights']
            output = subprocess.check_output(cmd).decode()
            actual_bpp = float(re.search(_KDU_RE_PAT, output).group(1))
            return actual_bpp
        try:
            y_target_eps = 0.01
            x_max = 2 if target_bpp < 1 else 4
            _, output_bpp = binary_search(compress_with_bpp, 'increasing',
                                          y_target=target_bpp, y_target_eps=y_target_eps,
                                          x_min=0.05, x_max=x_max, x_eps=0.01)
            return output_image_j2_p, output_bpp
        except BinarySearchFailedException:
            print('ERROR: Cannot compress image to {}: {}'.format(target_bpp, input_image_p))
            return None, None


def compress_img_no_search(input_image_p, target_bpp, output_folder=None, no_weights=True):
    if output_folder:
        f_name = os.path.basename(input_image_p)
        f_name = os.path.splitext(f_name)[0] + '.jp2'
        output_image_j2_p = os.path.join(output_folder, f_name)
    else:
        output_image_j2_p = os.path.splitext(input_image_p)[0] + '_out_jp2.jp2'
    # kdu can only work with "tif", "tiff", "bmp", "pgm", "ppm", "raw" and "rawl"
    #with remove_file_after(convert_im_to('bmp', input_image_p)) as input_image_bmp_p:
    input_image_bmp_p = convert_im_to('bmp', input_image_p)
    cmd = [_KDU_COMPRESS_PATH,
           '-i', input_image_bmp_p, '-o', output_image_j2_p,
           '-rate', str(target_bpp), '-no_weights']
    output = subprocess.check_output(cmd).decode()
    actual_bpp = float(re.search(_KDU_RE_PAT, output).group(1))

    return output_image_j2_p, actual_bpp

@contextmanager
def remove_file_after(p):
    """ Yields p, removes path p after. """
    yield p
    os.remove(p)


def convert_im_to(ext, input_image_p):
    input_image_root_p, _ = os.path.splitext(input_image_p)
    input_image_ext_p = input_image_root_p + '__tmp.{}'.format(ext)

    if not os.path.exists(input_image_ext_p):
        im = Image.open(input_image_p)
        if im.mode != 'RGB':
            im = im.convert('RGB')

        im.save(input_image_ext_p)

    return input_image_ext_p


def check_if_kdu_compress_is_available():
    try:
        subprocess.call([_KDU_COMPRESS_PATH, '-v'],
                        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except FileNotFoundError:
        print('Invalid kdu_compress: {}'.format(_KDU_COMPRESS_PATH))
        print('Make sure kdu_compress is available in $PATH or at $KDU_COMPRESS.')
        sys.exit(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--image-dir",
                        type=str,
                        required=True,
                        help='')
    parser.add_argument("--target-bpp",
                        type=float,
                        required=True,
                        help='')
    parser.add_argument("--output-dir",
                        type=str,
                        required=True,
                        help='')
    parser.add_argument("--output-filename",
                        type=str,
                        required=True,
                        help='')
    parser.add_argument("--extension",
                        type=str,
                        default='JPEG',
                        help='')
    parser.add_argument('--convert-to-jpeg', dest='convert_to_jpeg', action='store_true',
                        help='Whether a jpeg file is created along with the j2k file. ~2x slower')
    parser.set_defaults(convert_to_jpeg=False)
    FLAGS = parser.parse_args()

    check_if_kdu_compress_is_available()

    print(FLAGS)

    compress_imgs(FLAGS.image_dir,
                  FLAGS.target_bpp,
                  FLAGS.output_dir,
                  FLAGS.output_filename,
                  convert_to_jpeg_flag=FLAGS.convert_to_jpeg,
                  extension=FLAGS.extension)
