#!/bin/bash

OUTPUT_FOLDER=/scratch/robertto/imagenet_256/tmp_bin
mkdir -p $OUTPUT_FOLDER

python build_imagenet_data.py \
--train_directory /srv/glusterfs/mentzerf/roberto_train \
--output_directory $OUTPUT_FOLDER \
--train_shards 16 \
--labels_file /scratch/robertto/masters_thesis/input/imagenet_10_class_synsets.txt \
--extension bin \
--num_threads 1 \
--num_channels 32

