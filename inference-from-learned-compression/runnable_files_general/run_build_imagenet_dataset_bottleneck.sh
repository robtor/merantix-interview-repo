#!/bin/bash

OUTPUT_DIR=/srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_bottleneck-0808_tfrecord
mkdir -p $OUTPUT_DIR

python build_imagenet_data.py \
--directory /srv/glusterfs/mentzerf/scalar/roberto_train_0808_1054 \
--output_directory $OUTPUT_DIR  \
--num_shards 1024 \
--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
--extension bin \
--num_threads 8 \
--dataset_type train \
--output_type symbol \
--input_dim_mapping_file input/filename_to_shape_map_train_0808.txt \
--num_channels 8


OUTPUT_DIR=/srv/glusterfs/robertto/imagenet/imagenet_256/images_val_256_bottleneck-0808_tfrecord
mkdir -p $OUTPUT_DIR

python build_imagenet_data.py \
--directory /srv/glusterfs/mentzerf/scalar/roberto_val_0808_1054 \
--output_directory $OUTPUT_DIR  \
--num_shards 128 \
--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
--extension bin \
--num_threads 8 \
--dataset_type validation \
--output_type symbol \
--input_dim_mapping_file input/filename_to_shape_map_val_0808.txt \
--num_channels 8


OUTPUT_DIR=/srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_bottleneck-2308_tfrecord
mkdir -p $OUTPUT_DIR

python build_imagenet_data.py \
--directory /srv/glusterfs/mentzerf/scalar/roberto_train_2308_1051 \
--output_directory $OUTPUT_DIR  \
--num_shards 1024 \
--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
--extension bin \
--num_threads 8 \
--dataset_type train \
--output_type symbol \
--input_dim_mapping_file input/filename_to_shape_map_train_2308.txt \
--num_channels 8


OUTPUT_DIR=/srv/glusterfs/robertto/imagenet/imagenet_256/images_val_256_bottleneck-2308_tfrecord
mkdir -p $OUTPUT_DIR

python build_imagenet_data.py \
--directory /srv/glusterfs/mentzerf/scalar/roberto_val_2308_1051 \
--output_directory $OUTPUT_DIR  \
--num_shards 128 \
--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
--extension bin \
--num_threads 8 \
--dataset_type validation \
--output_type symbol \
--input_dim_mapping_file input/filename_to_shape_map_val_2308.txt \
--num_channels 8


#--labels_file /scratch_net/tractor/robertto/masters_thesis/input/synsets_2308.txt \
#--input_dim_mapping_file input/filename_to_shape_map_train_0808.txt \
#--directory /srv/glusterfs/mentzerf/scalar/roberto_train_2308_1051 \
#python build_imagenet_data.py \
#--directory /srv/glusterfs/mentzerf/roberto_val \
#--output_directory $OUTPUT_DIR  \
#--num_shards 32 \
#--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
#--extension bin \
#--output_type symbol \
#--num_channels 32 \
#--num_threads 8 \
#--input_dim_mapping_file ./input/filename_shape_map_val.txt \
#--dataset_type validation

#--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_200_class_synsets.txt \
#--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
#--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
#--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_200_class_synsets.txt \
#--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_10_class_synsets.txt \
