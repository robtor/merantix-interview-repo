#!/bin/bash

BASEFOLDER=/raid/robertto/data

#OUTPUT_DIR=/srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_compressed-0808_tfrecord
#mkdir -p $OUTPUT_DIR
#
#python build_imagenet_data.py \
#--directory /srv/glusterfs/mentzerf/scalar/roberto_train_0808_1054 \
#--output_directory $OUTPUT_DIR  \
#--num_shards 1024 \
#--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
#--extension png \
#--num_threads 8 \
#--dataset_type train

export CUDA_VISIBLE_DEVICES=''

OUTPUT_DIR=${BASEFOLDER}/images_val_256_j2k_bpp-target-0.6625_tfrecord
mkdir -p $OUTPUT_DIR

python3 -u build_imagenet_data.py \
--directory ${BASEFOLDER}/images_val_256_j2k_bpp-target-0.6625 \
--output_directory $OUTPUT_DIR  \
--num_shards 128 \
--labels_file /raid/robertto/input/imagenet_lsvrc_2015_synsets.txt \
--extension JPEG \
--num_threads 8 \
--dataset_type validation


OUTPUT_DIR=${BASEFOLDER}/images_train_256_j2k_bpp-target-0.6625_tfrecord
mkdir -p $OUTPUT_DIR

python3 -u build_imagenet_data.py \
--directory ${BASEFOLDER}/images_train_256_j2k_bpp-target-0.6625 \
--output_directory $OUTPUT_DIR  \
--num_shards 1024 \
--labels_file /raid/robertto/input/imagenet_lsvrc_2015_synsets.txt \
--extension JPEG \
--num_threads 8 \
--dataset_type train

#
#OUTPUT_DIR=/srv/glusterfs/robertto/imagenet/imagenet_256/images_val_256_compressed-0808_tfrecord
#mkdir -p $OUTPUT_DIR
#
#python build_imagenet_data.py \
#--directory /srv/glusterfs/mentzerf/scalar/roberto_val_0808_1054 \
#--output_directory $OUTPUT_DIR  \
#--num_shards 128 \
#--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
#--extension png \
#--num_threads 8 \
#--dataset_type validation


#OUTPUT_DIR=/srv/glusterfs/robertto/imagenet/imagenet_256/images_train_256_compressed-2308_tfrecord
#mkdir -p $OUTPUT_DIR
#
#python build_imagenet_data.py \
#--directory /srv/glusterfs/mentzerf/scalar/roberto_train_2308_1051 \
#--output_directory $OUTPUT_DIR  \
#--num_shards 1024 \
#--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
#--extension png \
#--num_threads 8 \
#--dataset_type train
#
#
#OUTPUT_DIR=/srv/glusterfs/robertto/imagenet/imagenet_256/images_val_256_compressed-2308_tfrecord
#mkdir -p $OUTPUT_DIR
#
#python build_imagenet_data.py \
#--directory /srv/glusterfs/mentzerf/scalar/roberto_val_2308_1051 \
#--output_directory $OUTPUT_DIR  \
#--num_shards 128 \
#--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
#--extension png \
#--num_threads 8 \
#--dataset_type validation
#
##python build_imagenet_data.py \
##--directory /srv/glusterfs/mentzerf/roberto_val \
##--output_directory $OUTPUT_DIR  \
##--num_shards 32 \
##--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
##--extension bin \
##--output_type symbol \
##--num_channels 32 \
##--num_threads 8 \
##--input_dim_mapping_file ./input/filename_shape_map_val.txt \
##--dataset_type validation
#
##--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_200_class_synsets.txt \
##--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
##--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
##--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_200_class_synsets.txt \
##--labels_file /scratch_net/tractor/robertto/masters_thesis/input/imagenet_10_class_synsets.txt \
