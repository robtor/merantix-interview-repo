#!/bin/bash
DATASET_TYPE=$2
TARGET_BPP=$1

echo "Target bpp: ${TARGET_BPP}"
echo "Dataset type: ${DATASET_TYPE}"

python3 -u helper_scripts/jp2k_compressor.py \
--image-dir /raid/robertto/data/images_${DATASET_TYPE}_256 \
--output-dir /raid/robertto/data/images_${DATASET_TYPE}_256_j2k_bpp-target-${TARGET_BPP} \
--output-filename /raid/robertto/masters_thesis/bpp_dataset_values/images_256_${DATASET_TYPE}_bpp-target-${TARGET_BPP}.txt \
--target-bpp ${TARGET_BPP} \
--extension JPEG \
--convert-to-jpeg
