#!/bin/bash

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf-gpu-3.6.2

BASEDIR=/scratch_net/tractor/robertto/masters_thesis
ENCODER_DECODER_DIR=/srv/glusterfs/robertto/imagenet/compression_runs/

python -u ${BASEDIR}/segmentation_mask_output.py \
    --data-dir /srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012 \
    --data-list ${BASEDIR}/dataset/train.txt \
    --model-scope resnet \
    --encoder-decoder-dir ${ENCODER_DECODER_DIR}'1005_1344 pp:shared_centers_c12_nips_lr1e*3_bn' \
    --restore-from /srv/glusterfs/robertto/imagenet/summaries_backup/segmentation/segmentation-encoder-chopped-match_key-1005-1344_320x320/model.ckpt-20000 \
    --network-name segmentation-encoder-chopped-match \
    --output-dir images_test \
    --op-point-name 'low' \
    --filename 2007_000629.jpg


    #--encoder-decoder-dir ${ENCODER_DECODER_DIR}'1006_0749 pp:rob_x0.5' \
    #--restore-from /srv/glusterfs/robertto/imagenet/summaries_backup/segmentation/segmentation-encoder-chopped-match_key-1006-0749_320x320/model.ckpt-20000 \
