python -u /scratch_net/tractor/robertto/masters_thesis/helper_scripts/filename_to_shape_map.py \
--directory /srv/glusterfs/mentzerf/roberto_train_0808_1054 \
--extension bin \
--synsets_filename /scratch_net/tractor/robertto/masters_thesis/input/imagenet_10_class_synsets.txt \
--map_name_from _sym.bin \
--map_name_to .JPEG.png \
--output_filename /scratch_net/tractor/robertto/masters_thesis/input/filename_shape_map_train_2308.txt

#--synsets_filename /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
