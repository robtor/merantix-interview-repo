#!/bin/bash
#
## otherwise the default shell would be used
#$ -S /bin/bash
 
## Pass environment variables of workstation to GPU node 
#$ -V
 
## <= 1h is short queue, <= 6h is middle queue, <= 48 h is long queue
#$ -l h_rt=05:00:00
 
## the maximum memory usage of this job, (below 4G does not make much sense)
#$ -l h_vmem=4G
 
## stderr and stdout are merged together to stdout
#$ -j y
#
# logging directory. preferrably on your scratch
#$ -o /scratch_net/tractor/robertto/masters_thesis/logs
#
## send mail on job's end and abort
#$ -m a
 
# call your calculation executable, redirect output

## MAKE SURE THIS NUMBER IS THE SAME AS num_splits
## schedule 10 jobs with ids 1-10
#$ -t 1-100
 
# if this job is run in sge: take the sge task id
# otherwise, the first argument of this script is taken as task id (if you want 
# to try the script locally).
TASK_ID=${SGE_TASK_ID:-"$1"}
  
# call your calculation executable, redirect output
i=$(expr $SGE_TASK_ID - 1)

source ~/.pyenvrc
pyenv activate tf_cpu

OUTPUT_DIR=/scratch_net/tractor/robertto/masters_thesis/results_filename_map/train_0808
OUTPUT_FILENAME=filename_shape_map_train_0808.txt$i
INPUT_DIR=/srv/glusterfs/mentzerf/scalar/roberto_train_0808_1054
mkdir -p $OUTPUT_DIR

python -u /scratch_net/tractor/robertto/masters_thesis/helper_scripts/filename_to_shape_map.py \
--directory $INPUT_DIR \
--extension bin \
--synsets_filename /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
--map_name_from _sym.bin \
--map_name_to .JPEG.png \
--output_filename $OUTPUT_DIR/$OUTPUT_FILENAME \
--idx_splits $i \
--num_splits 100


OUTPUT_DIR=/scratch_net/tractor/robertto/masters_thesis/results_filename_map/val_0808
OUTPUT_FILENAME=filename_shape_map_val_0808.txt$i
INPUT_DIR=/srv/glusterfs/mentzerf/scalar/roberto_val_0808_1054
mkdir -p $OUTPUT_DIR

python -u /scratch_net/tractor/robertto/masters_thesis/helper_scripts/filename_to_shape_map.py \
--directory $INPUT_DIR \
--extension bin \
--synsets_filename /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
--map_name_from _sym.bin \
--map_name_to .JPEG.png \
--output_filename $OUTPUT_DIR/$OUTPUT_FILENAME \
--idx_splits $i \
--num_splits 100


OUTPUT_DIR=/scratch_net/tractor/robertto/masters_thesis/results_filename_map/train_2308
OUTPUT_FILENAME=filename_shape_map_train_2308.txt$i
INPUT_DIR=/srv/glusterfs/mentzerf/scalar/roberto_train_2308_1051
mkdir -p $OUTPUT_DIR

python -u /scratch_net/tractor/robertto/masters_thesis/helper_scripts/filename_to_shape_map.py \
--directory $INPUT_DIR \
--extension bin \
--synsets_filename /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
--map_name_from _sym.bin \
--map_name_to .JPEG.png \
--output_filename $OUTPUT_DIR/$OUTPUT_FILENAME \
--idx_splits $i \
--num_splits 100


OUTPUT_DIR=/scratch_net/tractor/robertto/masters_thesis/results_filename_map/val_2308
OUTPUT_FILENAME=filename_shape_map_val_2308.txt$i
INPUT_DIR=/srv/glusterfs/mentzerf/scalar/roberto_val_2308_1051
mkdir -p $OUTPUT_DIR

python -u /scratch_net/tractor/robertto/masters_thesis/helper_scripts/filename_to_shape_map.py \
--directory $INPUT_DIR \
--extension bin \
--synsets_filename /scratch_net/tractor/robertto/masters_thesis/input/imagenet_lsvrc_2015_synsets.txt \
--map_name_from _sym.bin \
--map_name_to .JPEG.png \
--output_filename $OUTPUT_DIR/$OUTPUT_FILENAME \
--idx_splits $i \
--num_splits 100
