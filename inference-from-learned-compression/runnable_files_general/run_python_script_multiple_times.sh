#!/bin/bash

for i in {2000..84000..8000}
do
    python resnet_eval.py \
    --directory /home/robertto/scratch_tractor/imagenet_256/images_val_256 \
    --extension JPEG \
    --checkpoint_dir ./summaries/from_scratch_learning_rate/run1 \
    --checkpoint_name model.ckpt-$i \
    --num_examples 50000 \
    --output_filename output_from_scratch_results_val_1.txt
done

for i in {2000..84000..8000}
do
    python resnet_eval.py \
    --directory /home/robertto/scratch_tractor/imagenet_256/images_val_256 \
    --extension JPEG \
    --checkpoint_dir ./summaries/from_scratch_learning_rate/run2 \
    --checkpoint_name model.ckpt-$i \
    --num_examples 50000 \
    --output_filename output_from_scratch_results_val_2.txt
done

for i in {2000..84000..8000}
do
    python resnet_eval.py \
    --directory /home/robertto/scratch_tractor/imagenet_256/images_val_256 \
    --extension JPEG \
    --checkpoint_dir ./summaries/from_scratch_learning_rate/run3 \
    --checkpoint_name model.ckpt-$i \
    --num_examples 50000 \
    --output_filename output_from_scratch_results_val_3.txt
done

for i in {218000..270000..8000}
do
    python resnet_eval.py \
    --directory /home/robertto/scratch_tractor/imagenet_256/roberto_val \
    --extension png \
    --checkpoint_dir ./summaries/finetune_32_channels \
    --checkpoint_name model.ckpt-$i \
    --num_examples 50000 \
    --output_filename output_finetune_results_val.txt
done

