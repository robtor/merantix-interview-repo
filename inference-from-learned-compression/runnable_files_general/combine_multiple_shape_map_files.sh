python combine_filename_shape_map_files.py \
--output_filename ./input/filename_to_shape_map_train_0808.txt \
--pattern filename_shape_map_train_0808.txt* \
--directory results_filename_map/train_0808 \
--remove .JPEG

python combine_filename_shape_map_files.py \
--output_filename ./input/filename_to_shape_map_val_0808.txt \
--pattern filename_shape_map_val_0808.txt* \
--directory results_filename_map/val_0808 \
--remove .JPEG

python combine_filename_shape_map_files.py \
--output_filename ./input/filename_to_shape_map_train_2308.txt \
--pattern filename_shape_map_train_2308.txt* \
--directory results_filename_map/train_2308 \
--remove .JPEG

python combine_filename_shape_map_files.py \
--output_filename ./input/filename_to_shape_map_val_2308.txt \
--pattern filename_shape_map_val_2308.txt* \
--directory results_filename_map/val_2308 \
--remove .JPEG
