#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=2

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/masters_thesis/configs/image_compressed.config \
-c1 /raid/robertto/masters_thesis/configs/image_1000_dgx1.config \
-c2 /raid/robertto/masters_thesis/configs/image_processing_encoder_decoder.config \
--summary-basedir /raid/robertto/summaries \
--encoder-decoder-dir '/raid/mentzerf/vidcompress/dgx_logs_tb/0919_0752 pp:shared_centers_c12_nips32_bn_H1.2' \
--encoder-decoder-checkpoint-name ckpt-437000 \
--load-from-encoder-decoder-dir \
--checkpoint-dir '/home/robertto/raid_r/summaries/summary_image_1000_4x_encoder-chopped-match_lr-0.025_bs-64_no-scale-aug_key-0919-0752_frozen' \
--checkpoint-name model.ckpt-560000 \
--epochs-decay 3 \
--num-gpus 1 \
--data-format NCHW \
--batch-norm-decay 0.9 \
--network-name encoder-chopped-match \
--suffix _0919-0752_train-both \
--learning-rate 0.0025 \
--learning-rate-type schedule \
--not-restore-global-step \
--add-histograms
