#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

#source ~/.pyenvrc
#export CUDA_VISIBLE_DEVICES=$SGE_GPU
#echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
#pyenv activate tf_gpu


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=4,5

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/masters_thesis/configs/bottleneck-0808.config \
-c1 /raid/robertto/masters_thesis/configs/bottleneck-0808_1000_dgx1.config \
--summary_dir /raid/robertto/summaries/summary_bottleneck-0808_982_chopped-48_lr-0.025_bs-64_gpu-2_4x_w-rand-flip \
--checkpoint_name model.ckpt-500000 \
--epochs_decay 8 \
--num_gpus 2 \
--num_preprocess_threads 8 \
--data_format NCHW \
--batch_norm_decay 0.9 \
--network_name chopped-48 \
--rand_flip \
--model_type bottleneck-0808 \
--suffix _w-rand-flip
