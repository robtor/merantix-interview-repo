#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=2,3

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/configs/image_compressed.config \
-c1 /raid/robertto/configs/compressed_200.config \
--summary_basedir /raid/robertto/summaries \
--epochs_decay 75 \
--scale_augmentation \
--batch_norm_decay 0.9 \
--num_gpus 2 \
--num_preprocess_threads 8 \
--suffix _bnd-0.9 \
--learning_rate 0.1 \
--batch_size 256 \
--save_checkpoint_steps 5000
