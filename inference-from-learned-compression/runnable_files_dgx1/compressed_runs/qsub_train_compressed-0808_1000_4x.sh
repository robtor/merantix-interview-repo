#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=4,5

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/masters_thesis/configs/image_compressed.config \
-c1 /raid/robertto/masters_thesis/configs/compressed-0808_1000_dgx1.config \
--summary_dir /raid/robertto/summaries/summary_compressed_982_resnet-50_lr-0.025_bs-64_gpu-4_4x_no-scale-aug_0808 \
--epochs_decay 8 \
--no_scale_augmentation \
--num_gpus 2 \
--num_preprocess_threads 8 \
--data_format NCHW \
--model_type compressed \
--batch_norm_decay 0.9 \
--save_checkpoint_steps 20000 \
--suffix _no-scale-aug_0808 \
--num_examples 1281167
