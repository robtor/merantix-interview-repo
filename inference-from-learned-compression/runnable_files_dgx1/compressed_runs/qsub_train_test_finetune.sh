#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=0,1

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/configs/image_compressed.config \
-c1 /raid/robertto/configs/compressed_200.config \
--summary_basedir /raid/robertto/summaries \
--learning_rate_type constant \
--learning_rate 0.00025 \
--scale_augmentation \
--num_gpus 2 \
--num_preprocess_threads 8 \
--batch_norm_decay 0.9 \
--suffix _bnd-0.9_test-finetune \
--checkpoint_dir /raid/robertto/input \
--checkpoint_name resnet_v1_50.ckpt \
--variables_to_train all \
--variables_to_restore finetune
