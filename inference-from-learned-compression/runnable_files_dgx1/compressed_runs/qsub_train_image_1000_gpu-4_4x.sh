#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=0,1,2,3

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/configs/image_compressed.config \
-c1 /raid/robertto/configs/image_1000.config \
--summary_basedir /raid/robertto/summaries \
--checkpoint_dir /raid/robertto/summaries/summary_image_1000_resnet-50_lr-0.025_bs-64_gpu-4_2x \
--checkpoint_name model.ckpt-150000 \
--epochs_decay 8 \
--scale_augmentation \
--num_gpus 4 \
--num_preprocess_threads 16 \
--data_format NCHW
