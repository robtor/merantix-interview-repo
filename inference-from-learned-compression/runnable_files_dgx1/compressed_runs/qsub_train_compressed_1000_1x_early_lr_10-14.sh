#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=6

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/configs/image_compressed.config \
-c1 /raid/robertto/configs/compressed_1000.config \
--summary_basedir /raid/robertto/summaries \
--checkpoint_dir /raid/robertto/summaries/summary_compressed_1000_resnet-50_lr-0.00025_bs-64_gpu-1_1x_no-scale-aug_early-lr-1000000 \
--checkpoint_name model.ckpt-1400000 \
--learning_rate 0.000025 \
--learning_rate_type constant \
--no_scale_augmentation \
--num_gpus 1 \
--num_preprocess_threads 4 \
--data_format NCHW \
--model_type compressed \
--batch_norm_decay 0.9 \
--save_checkpoint_steps 20000 \
--suffix _no-scale-aug_early-lr-10-14

#--epochs_decay 30 \
