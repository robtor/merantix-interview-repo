#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=7

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/configs/image_compressed.config \
-c1 /raid/robertto/configs/image_1000.config \
--summary_basedir /raid/robertto/summaries \
--epochs_decay 30 \
--no_scale_augmentation \
--num_gpus 1 \
--num_preprocess_threads 4 \
--data_format NCHW \
--no-nesterov \
--input_resize_factor 0.5 \
--batch_norm_decay 0.9 \
--suffix _no-scale-aug_resize-2

