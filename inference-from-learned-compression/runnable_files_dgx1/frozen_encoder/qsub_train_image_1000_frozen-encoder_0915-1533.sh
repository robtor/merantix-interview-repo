#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=2

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/masters_thesis/configs/image_compressed.config \
-c1 /raid/robertto/masters_thesis/configs/image_1000_dgx1.config \
-c2 /raid/robertto/masters_thesis/configs/image_processing_encoder_decoder.config \
--summary_basedir /raid/robertto/summaries \
--encoder-decoder-dir '/raid/mentzerf/vidcompress/dgx_logs_tb/0915_1533 pp:shared_centers_c12_nips_lr1e*3_bn' \
--encoder-decoder-checkpoint-name ckpt-748000 \
--load-from-encoder-decoder-dir \
--epochs_decay 8 \
--num_gpus 1 \
--data_format NCHW \
--batch_norm_decay 0.9 \
--network_name encoder-frozen-chopped-match \
--suffix _0915-1533 \
--from_scratch
