#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=7

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/masters_thesis/configs/image_compressed.config \
-c1 /raid/robertto/masters_thesis/configs/image_1000_dgx1.config \
-c2 /raid/robertto/masters_thesis/configs/image_processing_encoder_decoder.config \
--summary_basedir /raid/robertto/summaries \
--encoder-decoder-dir '/raid/mentzerf/vidcompress/dgx_logs_tb/0920_1151 pp:shared_centers_c12_nips_b0' \
--encoder-decoder-checkpoint-name ckpt-201000 \
--load-from-encoder-decoder-dir \
--epochs_decay 8 \
--num_gpus 1 \
--data_format NCHW \
--batch_norm_decay 0.9 \
--network_name encoder-decoder-frozen-resnet-50 \
--suffix _0920-1151 \
--from_scratch \
