#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=48:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

#source ~/.pyenvrc
#export CUDA_VISIBLE_DEVICES=$SGE_GPU

echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

#pyenv activate tf_gpu
export CUDA_VISIBLE_DEVICES=6

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/masters_thesis/configs/bottleneck_dgx1.config \
-c1 /raid/robertto/masters_thesis/configs/bottleneck_1000_dgx1.config \
--summary_basedir /raid/robertto/summaries \
--checkpoint_dir /home/robertto/glusterfs_r/imagenet/summaries_backup_bn-average/summary_compressed_1000_resnet-50_lr-0.025_bs-64_gpu-2_1x_no-scale-aug_bn-average_0.9_40 \
--checkpoint_name model.ckpt-1900000 \
--checkpoint_dir_decoder /home/robertto/glusterfs_m/output_printemps/models/arXivPlots/arXiv_32_b5e_4_prog_LR6e-6_imgnet/training \
--checkpoint_name_decoder ckpt-1162357 \
--num_gpus 1 \
--num_preprocess_threads 4 \
--data_format NHWC \
--network_name combined \
--model_type bottleneck \
--no-nesterov \
--batch_norm_decay 0.9 \
--suffix _finetune_input-size-32 \
--optimizer momentum \
--learning_rate_type constant \
--learning_rate 0.0000025 \
--feat_vol_input_size 32 \
--debug_summary

