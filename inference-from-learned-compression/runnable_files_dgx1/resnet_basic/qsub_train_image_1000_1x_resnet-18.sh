#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=2

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/masters_thesis/configs/image_compressed.config \
-c1 /raid/robertto/masters_thesis/configs/image_1000_dgx1.config \
--summary-basedir /raid/robertto/summaries \
--epochs-decay 30 \
--frozen-encoder-decoder \
--scale-augmentation \
--num-gpus 1 \
--num-preprocess-threads 4 \
--data-format NHWC \
--model-type image \
--network-name resnet-18 \
--batch-norm-decay 0.9 \
--save-checkpoint-steps 20000 \
--suffix _TEST-DEL \
--from-scratch \
--overwrite
