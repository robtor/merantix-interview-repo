#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/


echo "----------Info----------"
current_host_name=$(uname -n)
echo "Hostname: $current_host_name"
echo "Job ID: $JOB_ID"
echo "Arguments: $@"
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
echo "------------------------"
echo ""

export CUDA_VISIBLE_DEVICES=4,5

python3 -u /raid/robertto/masters_thesis/resnet_train.py \
-c0 /raid/robertto/configs/bottleneck.config \
-c1 /raid/robertto/configs/bottleneck_1000.config \
--summary_basedir /raid/robertto/summaries \
--checkpoint_dir /raid/robertto/summaries/summary_bottleneck_1000_chopped-1_lr-0.025_bs-64_gpu-1_2x_w-rand-flip \
--checkpoint_name model.ckpt-280000 \
--epochs_decay 30 \
--num_gpus 2 \
--num_preprocess_threads 8 \
--data_format NCHW \
--batch_norm_decay 0.9 \
--network_name chopped-1 \
--rand_flip \
--suffix _w-rand-flip
