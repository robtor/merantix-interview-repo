import sys
sys.path.append('./vidcompress')

import tensorflow as tf
import os
import shutil
import argparse

os.environ["CUDA_VISIBLE_DEVICES"] = os.environ['SGE_GPU']

from networks import nets_class, nets_loader
import utils
import time

from PIL import Image
import numpy as np

log_dir = '/srv/glusterfs/mentzerf/out_tb_pushpsnr/1209_0941 pp:shared_centers_c12_nips'


def get_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("--output-dir",
                        type=str,
                        required=True,
                        help='Output folder for the images')
    parser.add_argument("--filename",
                        type=str,
                        required=True,
                        help='Image to input')
    parser.add_argument("--op-point-name",
                        type=str,
                        help='Name of the operating point for output filename')
    parser.add_argument("--encoder-decoder-dir",
                        type=str,
                        help='Folder where the configs are stored')
    parser.add_argument("--checkpoint-path",
                        type=str,
                        help='Path to checkpoint')
    return parser


def main(FLAGS):
    image_in = tf.placeholder(tf.uint8, (1, None, None, 3))
    image_in = tf.to_float(image_in)

    network_name = 'encoder-decoder'
    network_class = nets_class.get_network_by_name(network_name)
    network = network_class(num_classes=1000,
                            is_training=False,
                            data_format='NHWC',
                            input_network_log_dir=FLAGS.encoder_decoder_dir,
                            network_name=network_name)

    output_dict = network.build(image_in, reuse=None)
    output_image = output_dict['output_image']

    if not FLAGS.checkpoint_path:
        raise ValueError('Must explicitly pass checkpoint path')

    checkpoint_path = FLAGS.checkpoint_path
    checkpoint_paths_dict = {'output': checkpoint_path}
    print(checkpoint_path)

    init_fn = nets_loader.get_init_fn(checkpoint_paths_dict,
                                      network.get_variables_to_restore())

    init_list = [tf.global_variables_initializer(), tf.tables_initializer()]
    init_op = tf.group(*init_list)

    with utils.get_session() as sess:
        sess.run(init_op)
        init_fn(sess)

        im = Image.open(FLAGS.filename)
        im_array = np.array(im)
        im_array = np.expand_dims(im_array, axis=0)

        print(im.size)
        print(im_array.shape)

        start_time = time.time()
        img_out = sess.run(output_image, feed_dict={image_in: im_array})

        print(img_out.shape)
        print(time.time() - start_time)

        im = Image.fromarray(np.uint8(np.squeeze(img_out)))

        output_name = os.path.splitext(os.path.basename(FLAGS.filename))[0]
        output_filename = '{}_{}.png'.format(output_name, FLAGS.op_point_name)

        im.save(os.path.join(FLAGS.output_dir, output_filename))

        shutil.copy2(FLAGS.filename, FLAGS.output_dir)

    print('finished')


if __name__ == '__main__':
    parser = get_parser()
    FLAGS = parser.parse_args()

    main(FLAGS)
