#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o /scratch_net/tractor/robertto/tensorflow-deeplab-resnet/logs/
#$ -e /scratch_net/tractor/robertto/tensorflow-deeplab-resnet/logs/

#BRANCH=master
#
##BRANCH=calc_batch_norm_average
#BASE=/scratch_net/tractor/robertto/git_repos_tmp_runs
#TEMPLATE="${TAG}_XXXXXX"
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE}  masters_thesis_repo_XXXXXX)
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/masters_thesis/ .
#pwd

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf-gpu-2.7.11

BASEDIR=/scratch_net/tractor/robertto/tensorflow-deeplab-resnet

python -u $BASEDIR/fine_tune.py \
    --batch-size 4 \
    --data-dir /srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012 \
    --data-list $BASEDIR/dataset/train.txt \
    --restore-from $BASEDIR/checkpoints/deeplab_resnet.ckpt \
    --save-pred-every 500 \
    --not-restore-last \
    --random-scale \
    --random-mirror \

