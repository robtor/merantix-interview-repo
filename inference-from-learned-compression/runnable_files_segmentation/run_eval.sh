#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o logs/
#$ -e logs/

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf-gpu-3.6.2

EVALFOLDER=/scratch_net/tractor/robertto/masters_thesis/summaries/segmentation
OUTPUT_BASEFOLDER=/scratch_net/tractor/robertto/masters_thesis/output_segmentation
COMPRESSION_BASE='/srv/glusterfs/robertto/imagenet/compression_runs'
COMPRESSION_FOLDER_LOW="1005_1344 pp:shared_centers_c12_nips_lr1e*3_bn"
COMPRESSION_FOLDER_MEDIUM="1006_0749 pp:rob_x0.5"

#ARCHITECTURE=segmentation-encoder-chopped-1
#DATA_FOLDER_NAME=encoder-chopped-1_key-1006-0749_320x320
#COMPRESSION_FOLDER="${COMPRESSION_BASE}/${COMPRESSION_FOLDER_MEDIUM}"

#ARCHITECTURE=segmentation-encoder-decoder-resnet-50
#DATA_FOLDER_NAME=encoder-decoder-resnet-50_key-1006-0749_320x320
#COMPRESSION_FOLDER="${COMPRESSION_BASE}/${COMPRESSION_FOLDER_MEDIUM}"

#DATA_FOLDER_NAME=encoder-chopped-match_key-1006-0749_320x320
#ARCHITECTURE=segmentation-encoder-chopped-match
#COMPRESSION_FOLDER="${COMPRESSION_BASE}/${COMPRESSION_FOLDER_MEDIUM}"

#DATA_FOLDER_NAME=encoder-chopped-match_key-1005-1344_320x320_TEST-RUN-2
#ARCHITECTURE=segmentation-encoder-chopped-match
#COMPRESSION_FOLDER="${COMPRESSION_BASE}/${COMPRESSION_FOLDER_LOW}"

#DATA_FOLDER_NAME=encoder-decoder-resnet-50_key-1005-1344_320x320_TEST-RUN-2
#ARCHITECTURE=segmentation-encoder-decoder-resnet-50
#COMPRESSION_FOLDER="${COMPRESSION_BASE}/${COMPRESSION_FOLDER_LOW}"

#ARCHITECTURE=segmentation-encoder-decoder-resnet-50
#DATA_FOLDER_NAME=encoder-decoder-resnet-50_key-1005-1344_320x320
#COMPRESSION_FOLDER="${COMPRESSION_BASE}/${COMPRESSION_FOLDER_LOW}"

ARCHITECTURE=segmentation-resnet-50
DATA_FOLDER_NAME=segmentation-resnet-50_1x_nesterov
COMPRESSION_FOLDER=''

DATA_FOLDER=${EVALFOLDER}/${DATA_FOLDER_NAME}
OUTPUT_FILENAME=${OUTPUT_BASEFOLDER}/${DATA_FOLDER_NAME}_seg_val.txt
OUTPUT_FILENAME=tmp_test.txt

echo ${DATA_FOLDER}

for i in {0..20000..2000}; do
    echo "Evaluating model: $i"
    python -u segmentation_evaluate.py \
    --data-dir /srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012 \
    --data-list dataset/val.txt \
    --network-name ${ARCHITECTURE} \
    --encoder-decoder-dir "${COMPRESSION_FOLDER}" \
    --restore-from ${DATA_FOLDER}/model.ckpt-$i \
    --model-scope resnet \
    --output-filename ${OUTPUT_FILENAME} \
    --num-steps 1449
done

