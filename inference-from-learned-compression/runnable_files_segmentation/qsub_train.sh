#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o /scratch_net/tractor/robertto/masters_thesis/logs/
#$ -e /scratch_net/tractor/robertto/masters_thesis/logs/

#BRANCH=master
#
##BRANCH=calc_batch_norm_average
#BASE=/scratch_net/tractor/robertto/git_repos_tmp_runs
#TEMPLATE="${TAG}_XXXXXX"
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE}  masters_thesis_repo_XXXXXX)
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/masters_thesis/ .
#pwd

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf-gpu-3.6.2

BASEDIR=/scratch_net/tractor/robertto/masters_thesis

python -u ${BASEDIR}/segmentation_train.py \
    --batch-size 10 \
    --data-dir /srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012 \
    --data-list ${BASEDIR}/dataset/train.txt \
    --save-pred-every 1000 \
    --not-restore-last \
    --random-mirror \
    --learning-rate  1.0e-3 \
    --snapshot-dir ${BASEDIR}/snapshot_test \
    --encoder-decoder-dir '/srv/glusterfs/robertto/imagenet/compression_runs/0928_0918 pp:rob_x0.5' \
    --restore-from /srv/glusterfs/robertto/imagenet/summaries_backup/frozen-encoder-decoder/summary_image_1000_4x_encoder-chopped-match_lr-0.025_bs-64_no-scale-aug_key-0928-0918_frozen/model.ckpt-560000 \
    --restore-from /srv/glusterfs/robertto/imagenet/summaries_backup/frozen-encoder-decoder/summary_image_1000_4x_encoder-decoder-resnet-50_lr-0.025_bs-64_no-scale-aug_key-0928-0918_frozen/model.ckpt-560000 \
    --network-name segmentation-encoder-decoder-resnet-50 \

    # network_class = nets_class.get_network_by_name(FLAGS.network_name)
    # network_name = 'segmentation-encoder-decoder-resnet-50'
    # network_name = 'segmentation-encoder-chopped-match'
    # network_name = 'segmentation-resnet-50'


