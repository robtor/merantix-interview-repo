#!/bin/bash
#$ -S /bin/bash
#$ -l h_rt=24:00:00
#$ -l h_vmem=25G
#$ -l gpu=1
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -o /scratch_net/tractor/robertto/masters_thesis/logs/
#$ -e /scratch_net/tractor/robertto/masters_thesis/logs/

#BRANCH=master
#
##BRANCH=calc_batch_norm_average
#BASE=/scratch_net/tractor/robertto/git_repos_tmp_runs
#TEMPLATE="${TAG}_XXXXXX"
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE}  masters_thesis_repo_XXXXXX)
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/masters_thesis/ .
#pwd

source ~/.pyenvrc
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES: $CUDA_VISIBLE_DEVICES"
pyenv activate tf-gpu-3.6.2

BASEDIR=/scratch_net/tractor/robertto/masters_thesis
ENCODER_DECODER_DIR=/srv/glusterfs/robertto/imagenet/compression_runs/
RESTORE_DIR=/srv/glusterfs/robertto/imagenet/summaries_backup/frozen-encoder-decoder

python -u ${BASEDIR}/segmentation_train.py \
    --batch-size 10 \
    --data-dir /srv/glusterfs/robertto/imagenet/VOCdevkit/VOC2012 \
    --data-list ${BASEDIR}/dataset/train.txt \
    --save-pred-every 1000 \
    --not-restore-last \
    --random-mirror \
    --learning-rate  1.0e-3 \
    --snapshot-dir ${BASEDIR}/summaries/segmentation/segmentation-encoder-decoder-resnet-50_key-1014-1605_320x320 \
    --input-size 320,320 \
    --encoder-decoder-dir ${ENCODER_DECODER_DIR}'1014_1605 pp:shared_centers_c12_nips16_lr1e*5_bpp0.37_bn' \
    --restore-from ${RESTORE_DIR}/summary_image_1000_4x_encoder-decoder-resnet-50_lr-0.025_bs-64_no-scale-aug_key-1014-1605_frozen/model.ckpt-560000 \
    --network-name segmentation-encoder-decoder-resnet-50
