"""Evaluation script for the DeepLab-ResNet network on the validation subset
   of PASCAL VOC dataset.

This script evaluates the model on 1449 validation images.
"""

from __future__ import print_function

import argparse
import os
import shutil
import sys

sys.path.append('./vidcompress')

os.environ['CUDA_VISIBLE_DEVICES'] = os.environ['SGE_GPU']

import tensorflow as tf
import numpy as np

from deeplab_resnet import decode_labels
from networks import nets_class
from PIL import Image


IMG_MEAN = np.array((122.67891434, 116.66876762, 104.00698793), dtype=np.float32)

DATA_DIRECTORY = '/home/VOCdevkit'
DATA_LIST_PATH = './dataset/val.txt'
IGNORE_LABEL = 255
NUM_CLASSES = 21
NUM_STEPS = 1449 # Number of images in the validation set.
RESTORE_FROM = './deeplab_resnet.ckpt'


def get_arguments():
    """Parse all the arguments provided from the CLI.
    
    Returns:
      A list of parsed arguments.
    """
    parser = argparse.ArgumentParser(description="DeepLabLFOV Network")
    parser.add_argument("--data-dir", type=str, default=DATA_DIRECTORY,
                        help="Path to the directory containing the PASCAL VOC dataset.")
    parser.add_argument("--ignore-label", type=int, default=IGNORE_LABEL,
                        help="The index of the label to ignore during the training.")
    parser.add_argument("--num-classes", type=int, default=NUM_CLASSES,
                        help="Number of classes to predict (including background).")
    parser.add_argument("--restore-from", type=str, default=RESTORE_FROM,
                        help="Where restore model parameters from.")
    parser.add_argument("--network-name", type=str, default=None,
                        help="Name of the network to load")
    parser.add_argument("--model-scope", type=str, default='resnet',
                        help="Scope of the model. For backwards compatability")
    parser.add_argument("--encoder-decoder-dir", type=str, default=None,
                        help="Location of the information and checkpoints for encoders and decoders")
    parser.add_argument("--output-dir", type=str, default=None,
                        help="Output directory for images")
    parser.add_argument("--op-point-name", type=str, default=None,
                        help="Name of the operating point")
    parser.add_argument("--filename", type=str, default=None,
                        help="Name of the input file")

    return parser


def load(saver, sess, ckpt_path):
    """
    Load trained weights.
    
    Args:
      saver: TensorFlow saver object.
      sess: TensorFlow session.
      ckpt_path: path to checkpoint file with parameters.
    """
    saver.restore(sess, ckpt_path)
    print("Restored model parameters from {}".format(ckpt_path))


def main(args):
    """Create the model and start the evaluation process."""
    # Create queue coordinator.
    coord = tf.train.Coordinator()

    # This was ok because all the experiments were done using enc-dec networks
    img_mean_true = IMG_MEAN
    if 'encoder' in args.network_name:
        # Don't subtract when passing through encoder-decoder network
        img_mean_true = np.array((0.0, 0.0, 0.0), dtype=np.float32)

    # # Load reader.
    # with tf.name_scope("create_inputs"):
    #     reader = ImageReader(
    #         args.data_dir,
    #         args.data_list,
    #         None,  # No defined input size.
    #         False,  # No random scale.
    #         False,  # No random mirror.
    #         args.ignore_label,
    #         img_mean_true,
    #         coord)
    #     image, label = reader.image, reader.label
    # image_batch, label_batch = tf.expand_dims(image, dim=0), tf.expand_dims(label, dim=0) # Add one batch dimension.

    label_input = tf.placeholder(tf.uint8, (None, None))
    label_in = tf.expand_dims(label_input, axis=0)
    label_in = tf.expand_dims(label_in, axis=-1)

    image_input = tf.placeholder(tf.uint8, (None, None, 3))
    image_in = tf.to_float(image_input)

    image_in = tf.expand_dims(image_in, axis=0)

    network_class = nets_class.get_network_by_name(args.network_name)
    network = network_class(num_classes=None,
                            is_training=False,
                            batch_norm_decay=0.9,
                            model_scope=args.model_scope,
                            data_format='NHWC',
                            input_network_log_dir=args.encoder_decoder_dir,
                            frozen_encoder_decoder=True,
                            network_name=args.network_name)

    output_dict = network.build(image_in, reuse=None)
    raw_output = output_dict['logits']

    # Which variables to load.
    restore_var = tf.global_variables()
    
    # Predictions.
    raw_output = tf.image.resize_bilinear(raw_output, tf.shape(image_in)[1:3,])
    raw_output = tf.argmax(raw_output, axis=3)
    pred = tf.expand_dims(raw_output, dim=3)  # Create 4-d tensor.

    labels_summary = tf.py_func(decode_labels, [label_in, 1, args.num_classes], tf.uint8)
    preds_summary = tf.py_func(decode_labels, [pred, 1, args.num_classes], tf.uint8)

    # Set up tf session and initialize variables.
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.allow_soft_placement = True
    sess = tf.Session(config=config)
    init = tf.global_variables_initializer()

    sess.run(init)
    sess.run(tf.local_variables_initializer())

    # Load weights.
    loader = tf.train.Saver(var_list=restore_var)
    if args.restore_from is not None:
        load(loader, sess, args.restore_from)

    im_org_filename = args.data_dir + '/JPEGImages/' + args.filename
    im_org = Image.open(im_org_filename)

    label_org_filename = args.data_dir + '/SegmentationClassAug/' + os.path.splitext(args.filename)[0] + '.png'
    label_org = Image.open(label_org_filename)

    im_org_array = np.array(im_org)
    label_org_array = np.array(label_org)

    # Iterate over training steps.
    labels_out, preds_out = sess.run([labels_summary, preds_summary], feed_dict={image_input: im_org_array,
                                                                                 label_input: label_org_array})
    output_name = os.path.splitext(os.path.basename(args.filename))[0]
    output_filename = os.path.join(args.output_dir, '{}_{}_label.png'.format(output_name, args.op_point_name))

    im = Image.fromarray(np.uint8(np.squeeze(labels_out)))
    im.save(output_filename)

    output_filename = os.path.join(args.output_dir, '{}_{}_preds.png'.format(output_name, args.op_point_name))
    im = Image.fromarray(np.uint8(np.squeeze(preds_out)))
    im.save(output_filename)

    shutil.copy2(im_org_filename, args.output_dir)


if __name__ == '__main__':
    parser = get_arguments()
    args = parser.parse_args()

    main(args)
