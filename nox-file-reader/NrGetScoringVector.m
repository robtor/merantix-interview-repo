function eventVec = NrGetScoringVector(scorings,sigInfo,eventType)
%% eventVec = NrGetScoringVector(scorings,sigInfo,eventType)
% Given a struct from NrImportScoring (see documentation), a signal
% from NrLoadFiles and an event type, returns a vector matching the
% signal length having the value 1 where there is an event of the
% given type and 0 where there is not
% 
% Input:
%   scorings:  struct from NrImportScoring (see documentation)
%   sigInfo:   signal struct from NrLoadFiles
%   eventType: The event to match. Note, the function matches substrings
%              so 'apnea' would match all types of apneas
%
% Output:
%   eventVec:  Vector that matches the length of the input signal. The
%              vector has the value 1 where there's an event and 0
%              where there's no event.
% 
% Example usage:
% f = NrLoadFiles('D:\Work\Data\recording');
% scoring = NrImportScoring('myscoring.txt');
% eventVec = NrGetScoringVector(scorings,f.Abdomen,'apnea-obstructive');
% eventVec => [0;0;0;0;0;0;0;1;1;1;1;0;0;0;.......]
%
% Nox Medical, 24/06/2015, Robert Torfason

eventIdx = NrGetEventType(scorings,eventType);

timeStartEvent = scorings.timeStart(eventIdx);
timeStopEvent = scorings.timeStop(eventIdx);

startTimeTicks = NrNoxtimeToTicks(sigInfo.startTime);

idxStartEvent = NrTicksToIndex(timeStartEvent,startTimeTicks,sigInfo.fs);
idxStopEvent = NrTicksToIndex(timeStopEvent,startTimeTicks,sigInfo.fs);

nEventVec = length(sigInfo.data);
eventVec = zeros(nEventVec,1);

for i = 1:length(timeStartEvent)
    currStartIdx = idxStartEvent(i);
    currStopIdx = idxStopEvent(i);
    eventVec(currStartIdx:currStopIdx) = 1;
end

% Lazy way
eventVec = eventVec(1:nEventVec);

end