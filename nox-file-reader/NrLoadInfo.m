function fInfoStruct = NrLoadInfo(filepath,varargin)
%filesStruct = NrLoadInfo(filepath,varargin)
%  
%  Loads noxturnal files from filepath.
%  
%  Input:
%    filepath: Filepath to a single Noxturnal file or a path contraining a
%              recording
%  Output:
%    filesStruct: A struct of the info for each of the signal in the
%                 recording
%
%  Variable input:
%    'preset':    A cell array of the signal names to be loaded. Also
%                 supports 'all' and 'noaudio'. Default is 'noaudio'
%   
%  Example usage:
%    fInfoStruct = NrLoadInfo('D:\Work\Data\recording')
%    fInfoStruct = NrLoadFiles('D:\Work\Data\recording','preset',{'abdomen','thorax'})
%    abdomenStruct = NrLoadFiles('D:\Work\Data\recording\abdomen.ndf')
%
% Note: It is recommended to input full paths. Exists also checks for files
%       in matlab path
%
% Nox Medical 19/05/2015, R�bert Torfason

if exist(filepath,'file') == 2
% Single file. Only loads the file. No options
    fInfo = NrGetNdfFileInfo(filepath);
    fInfoRed = NrSelectMainInfoSignal(fInfo);
    
    fInfoRed.startTime = fInfoRed.sigStartTime;

    fInfoStruct = fInfoRed;
    
elseif exist(filepath,'file') == 7
    % Folder

    fInfo = NrGetNdfFilesInPath(filepath);
    [startTime,stopTime] = NrCalcStartAndStopTime(fInfo);
    
    p = inputParser;
    addParamValue(p,'preset','all');
    
    parse(p,varargin{:});
    preset = p.Results.preset;

    fInfoRed = NrRemoveSignalsNotInPreset(fInfo,preset);
    
    fInfoRedRed = NrSelectMainInfo(fInfoRed);
    fInfoStruct = NrAddStartAndStopTime(fInfoRedRed,startTime,stopTime);
else
    msg = ['File does not exist: ' filepath];
    error(msg)
end
    
end