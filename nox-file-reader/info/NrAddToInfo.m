function info = NrAddToInfo(infoRaw,sIn,filename)

info = infoRaw;

info.isAudio = strcmp(info.Format,'ByteMuLaw');
info.format = NrGetFormat(info.Format);
info.readSize = NrGetReadSize(info.format);

if ~isempty(sIn.sessions)
    info.startTime = sIn.sessions(1).time;
    info.stopTime = NrGetStopTime(sIn,info);
end
info.filepath = filename;

end