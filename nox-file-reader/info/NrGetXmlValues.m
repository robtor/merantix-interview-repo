function s = NrGetXmlValues(xmlString,typeToLabel)

import javax.xml.xpath.*
factory = XPathFactory.newInstance;
xpath = factory.newXPath;

docNode = NrXmlReadString(xmlString);

values = {'SamplingRate','Label', 'Unit', 'Format','Scale','Type'};

s = struct;

mappingStr = '';

for i = 1:length(values)
    
    % compile and evaluate the XPath Expression
    xpathString = ['//Channel//' values{i}];
    expression = xpath.compile(xpathString);
    valueNode = expression.evaluate(docNode, XPathConstants.NODE);

    currentValue = char(valueNode.getTextContent);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % This is shitty temporary code
    if strcmp(values{i},'Type') || strcmp(values{i},'Label')
       mappingStr = [currentValue ';' mappingStr];
    end
    %%%%%%%%%%%%%%%%%%%%%%%%
    
    
    if strcmp(values{i},'SamplingRate') || strcmp(values{i},'Scale')
        s.(values{i}) = str2double(currentValue);
    elseif strcmp(values{i},'Type')
        cleanTypeValue = NrCleanLabelName(currentValue);
        try
            s.Label = typeToLabel.(cleanTypeValue);
        catch ME
           error(['Add to type_to_label_mappings.txt: ' mappingStr]) 
        end
    else
        s.(values{i}) = currentValue;
    end
    
end

end