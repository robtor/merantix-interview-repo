function infoStr = NrGetRawInfoData(fid,sIn)

labelNum = cell2mat({sIn.raw(:).labelNum});
idxInfo = find(labelNum == 256);

assert(length(idxInfo) == 1);

offset = sIn.raw(idxInfo).absOffset;
size = sIn.raw(idxInfo).size/2;

fseek(fid,offset,'bof');
infoData = fread(fid,size,'*uint16','l')';

% The XML reader can't encode this, so we replace. This is the � character
%infoData(infoData == 176) = [];

infoData(infoData > 128) = [];
infoData(infoData == 0) = [];
infoStr = native2unicode(infoData,'latin1');

end