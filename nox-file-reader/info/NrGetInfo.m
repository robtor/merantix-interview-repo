function info = NrGetInfo(fid,sIn,filename,typeToLabel)
% Information: 256

infoStr = NrGetRawInfoData(fid,sIn);
info = NrGetXmlValues(infoStr,typeToLabel);
info = NrAddToInfo(info,sIn,filename);

end