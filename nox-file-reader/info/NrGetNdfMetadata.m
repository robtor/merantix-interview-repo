function sRawData = NrGetNdfMetadata(fid)

labelDict = NrGetLabelDict();

labelCell = {};
labelNumList = {};
sizeList = {};
absOffsetList = {};
absOffset = 4;

% Ensure that it finds the true EOF
fread(fid,1);

while ~feof(fid)
    fseek(fid, -1, 'cof');
    
    labelNum = fread(fid,1,'*uint16','l');
    labelNumList{end+1} = labelNum;
    labelCell{end+1} = labelDict(labelNum);
    
    nextSize = fread(fid,1,'*uint32','l');
    
    absOffset = absOffset + 6;
    absOffsetList{end+1} = absOffset;
    
    absOffset = absOffset + nextSize;
    sizeList{end+1} = nextSize;
    fseek(fid, nextSize, 'cof');
    
    % Check for EOF
    fread(fid,1);    
end

sRawData = struct('absOffset', absOffsetList, 'size', sizeList,...
           'labelNum', labelNumList, 'label', labelCell);

end