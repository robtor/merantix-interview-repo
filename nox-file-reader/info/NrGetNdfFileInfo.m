function s = NrGetNdfFileInfo(filename,typeToLabel)

fid = fopen(filename,'r');

if NrIsNoxturnalFileFormat(fid) == 0
   error('Not a Noxturnal file')
end

s = struct;
s.raw = NrGetNdfMetadata(fid);
s.sessions = NrGetSessions(fid,s);
s.info = NrGetInfo(fid,s,filename,typeToLabel);

fclose(fid);

end