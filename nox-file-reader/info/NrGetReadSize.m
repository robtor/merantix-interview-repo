function size = NrGetReadSize(formatStr)

if(~isempty(strfind(formatStr,'8')))
    size = 1;
elseif(~isempty(strfind(formatStr,'16')))
    size = 2;
elseif(~isempty(strfind(formatStr,'32')))
    size = 4;
else
    error('Unrecognized format')
end