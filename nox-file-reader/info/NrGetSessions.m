function sOut = NrGetSessions(fid,sIn)
% Time: 512
% Data: 513

labelNum = cell2mat({sIn.raw(:).labelNum});

iTime = find(labelNum == 512);
iData = find(labelNum == 513);

assert(length(iTime) == length(iData));
nSessions = length(iTime);

sOut = struct('offset', {}, 'size', {}, 'time', {});

for i = 1:length(iTime) 
    
    offset = sIn.raw(iTime(i)).absOffset;
    size = sIn.raw(iTime(i)).size/2;
    
    fseek(fid,offset,'bof');
    timeData = fread(fid,size,'*uint16','l')';
    
    sessionTimeStr = native2unicode(timeData,'UTF-8');
    sessionSize = sIn.raw(iData(i)).size;
    sessionOffset = sIn.raw(iData(i)).absOffset;
    
    sOut(i).offset = sessionOffset;
    sOut(i).size = sessionSize;
    sOut(i).time = sessionTimeStr;    
end

end