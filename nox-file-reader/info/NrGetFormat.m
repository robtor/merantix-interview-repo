function formatOut = NrGetFormat(formatStr)

% Matlab version of a dict...
s = struct;
s.Int16 = '*int16';
s.Int32 = '*int32';
s.ByteMuLaw = '*uint8';
s.Byte = '*uint8';

formatOut = s.(formatStr);

end