function s = NrGetNdfFilesInPath(path)

fileListing = dir(fullfile(path,'*.ndf'));

typeToLabel = NrGetSigTypeToLabelMapping();
s = struct;

for i = 1:length(fileListing)
    currentFilename = fileListing(i).name;
    currentFilepath = fullfile(path,fileListing(i).name);
    
    
%      try
        tmpInfo = NrGetNdfFileInfo(currentFilepath,typeToLabel);
%      catch ME
        %close file here?
%          warning(['Could not open file: ' currentFilename]);
%          continue
%      end
    
    fclose('all');
    
    structLabel = NrCleanLabelName(tmpInfo.info.Label);
    s.(structLabel) = tmpInfo;
   
end



end