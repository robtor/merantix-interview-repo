function sOut = NrSelectMainInfoSignal(sIn)

sOut.fsTrue = sIn.info.SamplingRate;
sOut.fs = round(sIn.info.SamplingRate);
sOut.unit = sIn.info.Unit;
sOut.sigStartTime = sIn.info.startTime;
sOut.sigStopTime = sIn.info.stopTime;

end