function data = NrGetSignalData(s,varargin)

p = inputParser;
signalStartTime = s.info.startTime;
signalStopTime = s.info.stopTime;

addParamValue(p,'StartTime',signalStartTime);
addParamValue(p,'StopTime',signalStopTime);

parse(p,varargin{:});

startTime = p.Results.StartTime;
stopTime = p.Results.StopTime;

samplingRate = round(s.info.SamplingRate);

[allocateSize,absoluteStartTime] = NrGetTimeLimitExtremes(startTime,stopTime,...
                                    signalStartTime,signalStopTime,...
                                    samplingRate);
data = zeros(allocateSize,1);

readSize = s.info.readSize;
readFormat = s.info.format;
scale = s.info.Scale;
fid = fopen(s.info.filepath);

for i = 1:length(s.sessions)

currentSize = s.sessions(i).size/readSize;
currentOffset = s.sessions(i).offset;
currentTime = s.sessions(i).time;
currentIndex = NrTimeToIndex(absoluteStartTime,currentTime,samplingRate);

fseek(fid,currentOffset,'bof');
currentData = fread(fid,currentSize,readFormat);
currentData = double(currentData)*scale;

% This is what the EDF export does. Maybe use the sampling
% rate and plot as is? Or perhaps interpolate to the true
% sampling rate?
currentData = NrDropSamples(currentData,s.info.SamplingRate);

data(currentIndex:currentIndex+length(currentData)-1) = currentData;

end

% This is not memory efficient, but by far the most readable!
data = data(NrTimeToIndex(absoluteStartTime,startTime,samplingRate):...
            NrTimeToIndex(absoluteStartTime,stopTime,samplingRate));

fclose(fid);

end