function [allocateSize,absoluteStartTime] = NrGetTimeLimitExtremes(startTime,stopTime,...
                                            signalStartTime,signalStopTime,...
                                            samplingRate)

startDiff = NrTimeStringDiff(signalStartTime,startTime);
stopDiff = NrTimeStringDiff(signalStopTime,stopTime);

if startDiff > 0
    if stopDiff > 0
        allocateSize = NrTimeToIndex(signalStartTime,stopTime,samplingRate);
        absoluteStartTime = signalStartTime;
    else
        allocateSize = NrTimeToIndex(signalStartTime,signalStopTime,samplingRate);
        absoluteStartTime = signalStartTime;
    end
else
   if stopDiff > 0
       allocateSize = NrTimeToIndex(startTime,stopTime,samplingRate);
       absoluteStartTime = startTime;
   else
       allocateSize = NrTimeToIndex(startTime,signalStopTime,samplingRate);
       absoluteStartTime = startTime;
   end
end


end