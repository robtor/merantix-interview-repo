function s = NrGetPresets()

s.RIP = {'abdomen','abdomenCal','thorax','RIPSum','RIPFlow','k'};
s.all = 'all';
s.noaudio = 'noaudio';
s.robert = {'abdomen','thorax','c3','k'};

end