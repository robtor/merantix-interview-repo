function info = NrSelectMainInfo(s)

fields = fieldnames(s);
info = struct;

for i = 1:length(fields)

info.(fields{i}) = NrSelectMainInfoSignal(s.(fields{i}));

end


end