function fInfoOut = NrRemoveSignalsNotInPreset(fInfo,preset)

fields = fieldnames(fInfo);
fInfoOut = struct;

% We match label names so the preset strings must match the altered label
% names
if iscell(preset)
    for k = 1:length(preset)
        preset{k} = NrCleanLabelName(preset{k}); 
    end
end

for i = 1:length(fields)
    if NrIsSigInPreset(preset,fields{i})
        fInfoOut.(fields{i}) = fInfo.(fields{i});
    end
end

end