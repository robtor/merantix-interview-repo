function info = NrAddStartAndStopTime(s,startTime,stopTime)

fields = fieldnames(s);
info = struct;

for i = 1:length(fields)

info.(fields{i}) = s.(fields{i});
info.(fields{i}).startTime = startTime;
%info.(fields{i}).stopTime = stopTime;

end


end