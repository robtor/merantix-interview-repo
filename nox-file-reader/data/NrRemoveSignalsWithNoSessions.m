function fInfoOut = NrRemoveSignalsWithNoSessions(fInfoIn)

fields = fieldnames(fInfoIn);
fInfoOut = struct;

for i = 1:length(fields)

if(isfield(fInfoIn.(fields{i}),'sessions'))
    if(~isempty(fInfoIn.(fields{i}).sessions))
       fInfoOut.(fields{i}) = fInfoIn.(fields{i});
    end
end

end

end