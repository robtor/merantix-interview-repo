function data = NrDropSamples(dataIn,samplingRate)

samplingRateOrg = floor(samplingRate);
residue = samplingRate - samplingRateOrg;

if residue == 0
    data = dataIn;
else
    intervalSec = round(1/residue);
    intervalSamples = round(intervalSec*samplingRate);

    indexRemove = 1:intervalSamples:length(dataIn);
    data = dataIn;
    data(indexRemove) = [];
end

end