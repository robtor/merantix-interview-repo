function data = NrZeroFillData(fid,dataStruct,Fs)

I = find(dataStruct.labelNum == 512);

if length(I) > 1
    fseek(fid,dataStruct.absOffset(I(1)),'bof');
    timeData = fread(fid,dataStruct.size(I(1))/2,'*uint16','l')';
    firstSessionStartTime = native2unicode(timeData,'UTF-8');
    
    fseek(fid,dataStruct.absOffset(I(end)),'bof');
    timeData = fread(fid,dataStruct.size(I(end))/2,'*uint16','l')';
    lastSessionStartTime = native2unicode(timeData,'UTF-8');
    
    timeInd = timeToIndex(firstSessionStartTime,lastSessionStartTime,Fs);
    
    data = zeros(timeInd,1);
    
    for i = 1:length(I)
        fseek(fid,dataStruct.absOffset(I(i)),'bof');
        timeData = fread(fid,dataStruct.size(I(i))/2,'*uint16','l')';
        currentSessionStartTime = native2unicode(timeData,'UTF-8');
        
        % Banker's rounding?
        timeToIndex(firstSessionStartTime,currentSessionStartTime,Fs);
    end
    
else
    iData = find(labelNumList == 513);
    
    fseek(fid,absOffsetList(I(1)),'bof');
    rawData = fread(fid,sizeList(iData(1))/2,'*uint16','l')';
    
    data = double(rawData);








end