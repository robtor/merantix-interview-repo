function logVal = NrIsSigInPreset(preset,label)
% For a given preset is the signal contained?
label = lower(label);
preset = lower(preset);

if iscellstr(preset)
    if sum(strcmp(label,preset)) > 0
        logVal = 1;
    else
        logVal = 0;
    end
    
elseif ischar(preset)
    
    if strcmp(preset,'all')
        logVal = 1;
    elseif strcmp(preset,'noaudio')
        if strcmp(label,'audio')
            logVal = 0;
        else
            logVal = 1;
        end
    else
        error('Unknown preset')
    end
else
    error('Preset must be a string or a cell array of strings')    
end

end