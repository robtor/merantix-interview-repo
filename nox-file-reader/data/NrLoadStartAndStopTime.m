function [recStartTime,recStopTime] = NrLoadStartAndStopTime(filepath)

% This is the Noxturnal database file
dataFilepath = fullfile(filepath,'Data.ndb')

dbid = mksqlite('open', dataFilepath);

sqlQuery1 = 'select value from internal_property where key = "RecordingStart"';
sqlQuery2 = 'select value from internal_property where key = "RecordingStop"';

recStartInfo = mksqlite(sqlQuery1);
recStopInfo = mksqlite(sqlQuery2);

mksqlite('close');

recStartTicks = str2double(recStartInfo.value);
recStopTicks = str2double(recStopInfo.value);

recStartTime = NrTicksToNoxtime(recStartTicks);
recStopTime = NrTicksToNoxtime(recStopTicks);

end