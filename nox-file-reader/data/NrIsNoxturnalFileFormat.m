function b = NrIsNoxturnalFileFormat(fid)

ident = fread(fid, 3,'*char')';
num = fread(fid, 1,'*uint8')';

if strcmp(ident,'NOX')
    b = 1;
else
    b = 0;
end

end