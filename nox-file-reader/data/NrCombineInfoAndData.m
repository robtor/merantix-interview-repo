function sOut = NrCombineInfoAndData(fInfo,data)


fields = fieldnames(fInfo);
sData = struct;

sOut = struct;

for i = 1:length(fields)
    sOut.(fields{i}) = fInfo.(fields{i});
    sOut.(fields{i}).data = data.(fields{i});
end

end