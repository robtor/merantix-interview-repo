function [recStartTime,recStopTime] = NrCalcStartAndStopTime(fInfo)

fields = fieldnames(fInfo);

recStartTime = fInfo.(fields{1}).info.startTime;
recStopTime = fInfo.(fields{1}).info.stopTime;

for i = 1:length(fields)
    try
        currStartTime = fInfo.(fields{i}).info.startTime;
        currStopTime = fInfo.(fields{i}).info.stopTime;
    catch ME
        continue;
    end
    
    
    if NrTimeStringDiff(currStartTime,recStartTime) > 0
        recStartTime = currStartTime;
    end
    
    if NrTimeStringDiff(currStopTime,recStopTime) < 0
        recStopTime = currStopTime;
    end    
end


end