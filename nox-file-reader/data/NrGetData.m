function sData = NrGetData(s,varargin)

%p = inputParser;
%defaultLoadSignals = {'all'};

%addParamValue(p,'LoadSignals',defaultLoadSignals);
%addParamValue(p,'StartTime',defaultLoadSignals);
%addParamValue(p,'StopTime',defaultLoadSignals);

%parse(p,varargin{:});

%loadSignals = p.Results.LoadSignals;

fields = fieldnames(s);
sData = struct;

for i = 1:length(fields)
    if ~isempty(s.(fields{i}).sessions)
        sData.(fields{i}) = NrGetSignalData(s.(fields{i}),varargin{:});
    end
end


end