function [startTime, stopTime] = FindAnalysisPeriod(scoringStruct)

startPos = strfind(scoringStruct.label, 'period-analysisstart');
stopPos = strfind(scoringStruct.label, 'period-analysisstop');

startTicks = scoringStruct.timeStart(~cellfun(@isempty, startPos));
stopTicks = scoringStruct.timeStop(~cellfun(@isempty, stopPos));

if isempty(startTicks)
   startTicks = min(scoringStruct.timeStart); 
end
if isempty(stopTicks)
    stopTicks = max(scoringStruct.timeStop);
end

reStartTicks = repmat(startTicks, 1, length(stopTicks));
reStopTicks = repmat(stopTicks', length(startTicks), 1);

tickDiff = reStopTicks - reStartTicks;
tickDiff(tickDiff <= 0) = nan;

[minTickDiff, tickRowIdx] = min(tickDiff);
[~, tickColIdx] = max(minTickDiff);

tickStartIdx = tickRowIdx(tickColIdx);
tickStopIdx = tickColIdx;

startTime = NrTicksToNoxtime(startTicks(tickStartIdx));
stopTime = NrTicksToNoxtime(stopTicks(tickStopIdx));