function [filesStruct, scoringStruct] = TruncateByScoring(filesStruct,...
    scoringStruct)

[startTime, stopTime] = FindAnalysisPeriod(scoringStruct);
startTick = NrNoxtimeToTicks(startTime);
stopTick = NrNoxtimeToTicks(stopTime);

fileFields = fields(filesStruct);

for fieldIdx = 1:numel(fileFields)
    measStartTime = filesStruct.(fileFields{fieldIdx}).startTime;
    Fs = filesStruct.(fileFields{fieldIdx}).fs;
    startIdx = NrTimeToIndex(measStartTime,startTime(1,:),Fs);
    stopIdx =  NrTimeToIndex(measStartTime,stopTime(end,:),Fs);
    
    filesStruct.(fileFields{fieldIdx}).data(stopIdx:end) = [];
    filesStruct.(fileFields{fieldIdx}).data(1:startIdx) = [];
end

scoringFields = fields(scoringStruct);
eventIdx = (scoringStruct.timeStart <= min(startTick)) | ...
    (scoringStruct.timeStop) >= min(stopTick);

for fieldIdx = 1:numel(scoringFields)
   scoringStruct.(scoringFields{fieldIdx})(eventIdx) = [];
end

