function [startTime,stopTime] = NrGetAnalysisTimeFromScoring(scoring)

startIdx = NrGetEventType(scoring,'period-analysisstart');
stopIdx = NrGetEventType(scoring,'period-analysisstop');

if isempty(startIdx) || isempty(stopIdx)
    startTime = [];
    stopTime = [];
else
    startTimeTicks = scoring.timeStart(startIdx(1));
    stopTimeTicks = scoring.timeStart(stopIdx(1));

    startTime = NrTicksToNoxtime(startTimeTicks);
    stopTime = NrTicksToNoxtime(stopTimeTicks); 
end

end