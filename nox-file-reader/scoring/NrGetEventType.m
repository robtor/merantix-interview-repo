function eventIdx = NrGetEventType(scorings,eventSubstring)
% eventSubstring is the input. Matches a substring of the event type
% Returns logical indices of the events

findSubstr = strfind(scorings.label,eventSubstring);
eventIdxLog = ~(cellfun(@isempty,findSubstr));
eventIdx = find(eventIdxLog);

end