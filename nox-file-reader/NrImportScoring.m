function s = NrImportScoring(filename)
%% s = NrImportScoring(filename)
% Given a path to a text file containing scorings, imports them 
% and outputs as a struct of arrays. The struct contains the fields
% label, timeStart, timeStop
%
% The format of the text file is 
% Column 1: Label for the event type
% Column 4: Start of event (.NET ticks)
% Column 5: End of event (.NET ticks)
%
% Example usage:
% 
% s = NrImportScoring('.\myscoring.txt')
% s.label(1198)     => 'apnea-obstructive'
% s.timeStart(1198) => 6.346211611060100e+17
% s.timeStop(1198)  => 6.346211613441600e+17
%
% Nox Medical 24/06/2015, Robert Torfason

fid = fopen(filename);
C = textscan(fid,'%s','delimiter','\r\n');
fclose(fid);

C = cellfun(@(x) textscan(x,'%s','Delimiter',';'),C{:});

s = struct;

s.label= cellfun(@(x) x(1),C);
s.timeStart = cellfun(@(x) str2double(x(4)),C);
s.timeStop = cellfun(@(x) str2double(x(5)),C);

end