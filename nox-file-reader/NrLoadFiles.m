function filesStruct = NrLoadFiles(filepath,varargin)
%filesStruct = NrLoadFiles(filepath,varargin)
%  
%  Loads noxturnal files from filepath.
%  
%  Input:
%    filepath: Filepath to a single Noxturnal file or a path contraining a
%              recording
%  Output:
%    filesStruct: A struct of the signal data and associated info for each
%                 of the signal that was loaded
%
%  Variable input:
%    'StartTime': The start time from when the signals are to be loaded. If
%                 this time is before the signals start they are zero
%                 padded. If it is after they start the signal are
%                 truncated. Default is the earliest starttime of all the
%                 signal that are loaded.
%                 Format: Noxturnal timestamp, '20150121T223330.377000'
%
%    'StopTime':  The stop time of the signals. If this time is after the
%                 signals stop they are zero padded. If it is before they
%                 start the signal are truncated. Default is the last
%                 stoptime of all the signal that are loaded.
%                 Format: Noxturnal timestamp, '20150121T223330.377000'
%
%    'preset':    A cell array of the signal names to be loaded. Also
%                 supports 'all' and 'noaudio'. Default is 'noaudio'
%   
%  Example usage:
%    filesStruct = NrLoadFiles('D:\Work\Data\recording')
%    filesStruct = NrLoadFiles('D:\Work\Data\recording',...
%                              'StartTime','20150121T223330.377000',...
%                              'StopTime','20150122T090120.120000')
%    filesStruct = NrLoadFiles('D:\Work\Data\recording','preset',{'abdomen','thorax'})
%    abdomenStruct = NrLoadFiles('D:\Work\Data\recording\abdomen.ndf')
%
% Note: It is recommended to input full paths. Exists also checks for files
%       in matlab path
%
% Nox Medical 19/05/2015, R�bert Torfason

if exist(filepath,'file') == 2
% Single file. Only loads the file. No options
    fInfo = NrGetNdfFileInfo(filepath);
    data = NrGetSignalData(fInfo);
    fInfoRed = NrSelectMainInfoSignal(fInfo);
    
    fInfoRed.startTime = fInfoRed.sigStartTime;
    
    fInfoRed.data = data;
    filesStruct = fInfoRed;
    
elseif exist(filepath,'file') == 7
    % Folder

    fInfo = NrGetNdfFilesInPath(filepath);
    fInfo = NrRemoveSignalsWithNoSessions(fInfo);
    
    [startTime,stopTime] = NrCalcStartAndStopTime(fInfo);
    
    p = inputParser;
    addParamValue(p,'preset','noaudio');
    addParamValue(p,'StartTime',startTime);
    addParamValue(p,'StopTime',stopTime);
    
    parse(p,varargin{:});
    
    startTime = p.Results.StartTime;
    stopTime = p.Results.StopTime;
    preset = p.Results.preset;
    
    argumentsIn = {'StartTime',startTime,...
                   'StopTime',stopTime};

    fInfoRed = NrRemoveSignalsNotInPreset(fInfo,preset);
    data = NrGetData(fInfoRed,argumentsIn{:});
    
    fInfoRedRed = NrSelectMainInfo(fInfoRed);
    fInfoRedRed = NrAddStartAndStopTime(fInfoRedRed,startTime,stopTime);
    
    filesStruct = NrCombineInfoAndData(fInfoRedRed,data);
    % Make a unified struct with everything
    % Set start time as recording start or analysis start

else
    msg = ['File does not exist: ' filepath];
    error(msg)
end
    
end