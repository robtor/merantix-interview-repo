function [datestrNox,currDatenum] = NrTicksToNoxtime(ticks)
%[datestrNox,currDatenum] = NrNoxtimeToTicks(ticks)
%
%  Converts .NET ticks to a Noxturnal timestamp and a datenum value
%  
%  Input:
%    ticks: .NET ticks
%  Output:
%    datestrNox: Noxturnal timestamp corresponding to the input ticks
%
%  Example usage:
%    [datestrNox,~] = NrTicksToNoxtime(6.355747641037700e+17)
%    datestrNox  =>  '20150121T223330.377000'
% 
%  Nox Medical 19/05/2015, R�bert Torfason


formatIn = 'yyyymmddTHHMMSS';
datenumRef = datenum('19700101T000000',formatIn);
ticksRef = 621355968000000000;

secsInDay = 3600*24;

tickDiff = ticks-ticksRef;
daysDiff = NrTicksToSeconds(tickDiff)/secsInDay;

currDatenum = datenumRef + daysDiff;

formatOut = 'yyyymmddTHHMMSS.FFF000';
datestrNox = datestr(currDatenum,formatOut);

end