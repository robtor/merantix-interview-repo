function ticks = NrSecondsToTicks(secs)
%secs = NrTicksToSeconds(secs)
%
%  Converts seconds to .NET ticks 
%  
%  Input:
%    secs: Time in seconds 
%  Output:
%    ticks: .NET ticks corresponding to the input time
%
%  Example usage:
%   
% 
%  Nox Medical 16/06/2015, R�bert Torfason

ticksInSec = 10000000;
ticks = secs*ticksInSec;

end