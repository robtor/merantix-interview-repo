function ticks = NrNoxtimeToTicks(t)
%ticks = NrNoxtimeToTicks(t)
%
%  Exports Noxturnal time format to .NET ticks
%  
%  Input:
%    t: Noxturnal timestamp
%  Output:
%    ticks: .NET ticks corresponding to the input timestamp
%
%  Example usage:
%    ticks = NrNoxtimeToTicks('20150121T223330.377000')
%    ticks  =>  6.355747641037700e+17
% 
%  Nox Medical 19/05/2015, R�bert Torfason

formatIn = 'yyyymmddTHHMMSS';
datenumRef = datenum('19700101T000000',formatIn);
ticksRef = 621355968000000000;

[tOut,tPrecicion] = NrNoxtimeToDatenum(t);

datenumDiff = tOut - datenumRef;

ticksInSec = 10000000;
secsInDay = 3600*24;

ticks = ticksRef + datenumDiff*secsInDay*ticksInSec + tPrecicion*ticksInSec;
ticks = round(ticks);

end