function time = NrIndexToNoxtime(idx,refTime,fs)
% refTime: Noxturnal timestamp format or ticks
% idx: The index from the starting time
% fs: Sampling rate
% 
% time: The (Noxturnal) timestamp of the given index in relation to the
%       given reference timestamp

if ischar(refTime)
    refTimeTicks = NrNoxtimeToTicks(refTime);
else
    refTimeTicks = refTime;    
end

absTicks = NrIndexToTicks(idx,refTimeTicks,fs);
time = NrTicksToNoxtime(absTicks);


end