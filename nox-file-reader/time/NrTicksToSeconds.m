function secs = NrTicksToSeconds(ticks)
%secs = NrTicksToSeconds(ticks)
%
%  Converts .NET ticks to seconds
%  
%  Input:
%    ticks: .NET ticks
%  Output:
%    secs: Seconds corresponding to the input ticks
%
%  Example usage:
%    secs = NrTicksToSeconds(6712000000)
%    secs => 671.2 
% 
%  Nox Medical 19/05/2015, R�bert Torfason

ticksInSec = 10000000;
secs = ticks/ticksInSec;

end