function ticks = NrIndexToTicks(idx,refTicks,fs)

% 1-based indexing
secs = (idx-1)/fs;
tickDiff = NrSecondsToTicks(secs);

ticks = refTicks+tickDiff;

end