function timestamp = NrAddTime(startTimeStamp,tAddSecs)
%timestamp = NrAddTime(startTimeStamp,tAddSecs)
%
%  Adds seconds to Noxturnal timestamp and outputs a new Noxturnal
%  timestamp with the added seconds
%  
%  Input:
%    startTimeStamp: Noxturnal timestamp
%    tAddSecs: Seconds to add to startTimeStamp
%  Output:
%    timestamp: startTimeStamp + tAddSecs. In Noxturnal timestamp
%               format.
% 
%  Note: Only assume positive, might work for negative, have to 
%  check the validity
%
% Nox Medical 19/05/2015, R�bert Torfason

[startDatenum,startPrec] = NrNoxtimeToDatenum(startTimeStamp);

addSecs = floor(tAddSecs);

residue = tAddSecs - addSecs;
totalPrecicion = startPrec + residue;

addResidueSec = floor(totalPrecicion);
finalPrecicion = mod(totalPrecicion,1);

addSecs = addSecs + addResidueSec;

finalDatenum = startDatenum + addSecs/(60*60*24);

formatOut = 'yyyymmddTHHMMSS';
timestamp = datestr(finalDatenum,formatOut);

precStr = num2str(round(10^6*finalPrecicion));
precStr = [repmat('0',1,6-length(precStr)) precStr];

timestamp = [timestamp '.' precStr];

end