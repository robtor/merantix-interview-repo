function ind = NrTimeToIndex(startTime,endTime,Fs)

timeDiff = NrTimeStringDiff(startTime,endTime);
ind = timeDiff*Fs + 1;

% This should be looked into. Banker's rounding? Round? Calculate the
% cumulated residue and use that?
ind = floor(ind);
    
end