function ind = NrTicksToIndex(idxTime,refTime,Fs)

tickDiff = idxTime - refTime;
timeDiff = NrTicksToSeconds(tickDiff);
ind = timeDiff*Fs + 1;

% This should be looked into. Banker's rounding? Round? Calculate the
% cumulated residue and use that?
ind = floor(ind);

end