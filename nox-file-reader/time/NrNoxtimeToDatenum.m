function [tOut,tPrecicion] = NrNoxtimeToDatenum(t)
%[tOut,tPrecicion] = NrNoxtimeToDatenum(t)
%
%  Exports Noxturnal time format to a datetime value and a precicion 
%  value.
%  
%  Input:
%    t: Noxturnal timestamp
%  Output:
%    tOut: Datetime value for the seconds, hours, days, months, years
%    tPrecicion: A floating point number representing the values 
%                "behind the dot/comma"
%
%  Example usage:
%    [tOut,tPrecicion] = NrNoxtimeToDatenum('20150121T223330.377000')
%    
%    tOut        =>  7.359859399305555e+05
%    tPrecicion  =>  0.3770
%    
% 
% Nox Medical 19/05/2015, R�bert Torfason
tSplit = textscan(t, '%s', 'delimiter', '.');

tDatetime = tSplit{1}{1};
tPrecicion = tSplit{1}{2};

formatIn = 'yyyymmddTHHMMSS';
tOut = datenum(tDatetime,formatIn);

% Look into the last significant letter. Possible floating point rounding
% error. See the python script for the HR/PES sync-ing
tPrecicion = str2double(tPrecicion)/10^6;

end