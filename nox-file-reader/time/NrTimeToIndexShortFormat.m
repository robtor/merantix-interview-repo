function ind = NrTimeToIndexShortFormat(startTime,endTime,Fs)
% startTime: Noxturnal format. Example: 20140401T033201.100023
% endTime: HHmmss or HHmmss.pppppp
% Fs: Signal sampling rate
%
% Example usage:
%
% st = '20150323T004525.050000'
% fs = 20
%
% idx = NrTimeToIndexShortFormat(st,'011509',fs)
% idx => 35680
% idx = NrTimeToIndexShortFormat(st,'011509.100000',fs)
% idx => 35682

startTimeCell = textscan(startTime,'%s','Delimiter','T');
startTimeCell = startTimeCell{1};

date = startTimeCell{1};
val = endTime;
delim = 'T';

C = textscan(endTime,'%s','Delimiter','.');
C = C{1};

if length(C) == 2
    trueEndTime = strcat(date,delim,val);
else
    precicion = '.000000';
    trueEndTime = strcat(date,delim,val,precicion);
end

if ~IsAfterMidnight(startTime) && IsAfterMidnight(trueEndTime)
    trueEndTime = AddOneDay(trueEndTime);
end

ind = NrTimeToIndex(startTime,trueEndTime,Fs);

end

function b = IsAfterMidnight(time)
% input: Noxturnal timestamp
% Before midnight: 12:00-23:59
% After midnight: 00:00-11:59

C = textscan(time,'%s','Delimiter','.');
dateString = C{1}{1};
formatIn = NrGetNoxtimeStringFormat();

%[y,m,d,h,mn,s]
[~,~,~,h,~,~] = datevec(dateString,formatIn);

if h<12
    b = 1;
else
    b = 0;
end
end


function time = AddOneDay(timeIn)
% Add one day to a timestamp
secsAdd = 60*60*24;
time = NrAddTime(timeIn,secsAdd);
end