function timestring = NrGetStopTime(s,info)
% TEST
readSize = info.readSize;
% Naive stoptime. Does not assume sample dropping
% This will lead to trailing zeroes
samplingRate = double(round(info.SamplingRate));

startTime = s.sessions(end).time;
numOfSamples = double(s.sessions(end).size/readSize);
numOfSecs = numOfSamples/samplingRate;

timestring = NrAddTime(startTime,numOfSecs);

end