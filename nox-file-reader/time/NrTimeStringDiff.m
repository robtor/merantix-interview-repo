function secs = NrTimeStringDiff(t1,t2)
% secs = timeStringDiff(t1,t2)
% t1 and t2 are strings on the Noxturnal timestamp format
%
% Returns t2-t1 in seconds

[t1Datenum,t1Precicion] = NrNoxtimeToDatenum(t1);
[t2Datenum,t2Precicion] = NrNoxtimeToDatenum(t2);

timediff = t2Datenum - t1Datenum;

secs = round(timediff * 24 * 3600);
precicion = t2Precicion - t1Precicion;

secs = secs + precicion;

end