function labelClean = NrCleanLabelName(labelIn)
%labelClean = NrCleanLabelName(labelIn)
%  Removes the characters ' ,.:;!-()' from a string and for strings that
%  start with a number, adds an 'X' in front
%
%  Input:
%    labelIn: string
%  Output:
%    labelClean: Cleaned input string whose illegal characters have been
%                removed
%
%  Reason: All signals are kept in a struct. Certain names and characters
%  are illegal. This function cleans the labels in accordance with legal
%  field names for matlab structs
%
% Nox Medical 19/05/2015, R�bert Torfason

labelClean = labelIn;

% Remove illegal characters
labelClean(ismember(labelIn,' ,.:;!-()')) = [];

% Illegal to start with num, add an X in front
[~,status] = str2num(labelClean(1));
if(status)
    labelClean = ['X' labelClean];
end



end