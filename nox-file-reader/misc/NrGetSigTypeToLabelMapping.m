function s = NrGetSigTypeToLabelMapping()

fid = fopen('\misc\type_to_label_mapping.txt');
C = textscan(fid,'%s','delimiter','\r\n');
fclose(fid);

C = cellfun(@(x) textscan(x,'%s','Delimiter',';'),C{:});

s = struct;

for i = 1:length(C)
    currTypeName = NrCleanLabelName(C{i}{1});
    currLabelName = NrCleanLabelName(C{i}{2});
    
    s.(currTypeName) = currLabelName;    
end


end