function scoringStruct = BatchNrImportScoring(filePath, varargin)

%scoringStruct = BatchNrImportScoring(filePath, varargin)
%   Calls the NrImportScoring function multiple times. The function can
%   handle multiple scoring files for each measurement session, either
%   reading more than one scoring per measurement or only choosing a
%   limited number of scorings for each measurement.
%
%   If multiple scoring files are read for one measurement the scoring
%   events are concatinated into a single array of events.
%
%   Input
%   filePath        A path leading to a folder containing multiple folders
%                   each containing a sinlge measurement session and one or
%                   more scoring data files.
%                   Example:
%                       filePath = 'D:\measurements\';
%
%                       The folder D:\measurements\ contains the folders
%                       D:\measurements\meas01
%                       D:\measurements\meas02 ...
%   varargin
%       'include'     A cellarray containing strings with names of
%                     measurements to be included. The strings can match
%                     partially the names of the scorings and can candle
%                     regular expressions using the regexpi Matlab function.
%       'exclude'     A cellarray containing strings with names of
%                     measurements to be excluded. The strings can match
%                     partially the names of the scorings and can candle
%                     regular expressions using the regexpi Matlab function.
%
%                     If exclude contains a value include will be set to be
%                     empty. The logic is that there is a redundancy in
%                     including spesific measurements while excluding other
%                     measurements.
%       'scoringFile' A string containing the name or a common part of the
%                     name of the scoring files.
%
%   Output
%   scoringStruct   An array of structs where each element contains the
%                   scorings from each measurement. The struct has the
%                   fields:
%                   label       Labeling the event
%                   timeStart   The starting time of the event
%                   timeStop    The ending time of the event
%                   name        The name of the persons scoring the data
%                   folder      The folder the scoring files were read from
%
%   Example
%   scoringStruct = BatchNrImportScoring('D:\Measurements\',...
%       'exclude', {'sigr[u�]n'});
%
% Nox Medical 16/06/2015, J�n Sk�rnir �g�stsson

p = inputParser;
addParamValue(p,'include',{''});
addParamValue(p,'exclude',{''});
addParamValue(p,'scoringFile','scoring*');

parse(p,varargin{:});
include = p.Results.include;
exclude = p.Results.exclude;
scoringFile = p.Results.scoringFile;

if ~iscell(include)
    warning(['Names of scorers to include are not in a cell. '...
        'Input moved to a cell container.'])
    include = {include};
end

if ~iscell(exclude)
    warning(['Names of scorers to exclude are not in a cell. '...
        'Input moved to a cell container.'])
    exclude = {exclude};
end

if ~isempty([include{:}]) && ~isempty([exclude{:}])
    warning('Include and exclude parameters both contain values. Include term removed')
    include = {''};
end

fileNames = DirNoFiles(filePath);

if matlabpool('size') == 0
    matlabpool
end

parfor i = 1:length(fileNames)
    scoringFiles = dir([filePath fileNames(i).name '\' scoringFile]);
    idxExclude = zeros(1,numel(scoringFiles));
    for j = 1:numel(scoringFiles)
        if ~isempty(cell2mat(regexpi(scoringFiles(j).name, exclude)))
            idxExclude(j) = 1;
        end
        
        if isempty(cell2mat(regexpi(scoringFiles(j).name, include))) ...
                && ~isempty([include{:}])
            idxExclude(j) = 1;
        end
    end
    scoringFiles(idxExclude==1) = [];
    scoringName{i} = {scoringFiles(:).name};
    
    tmpScoringStruct = struct('label', [], 'timeStart', [], 'timeStop', []);
    for k = 1:numel(scoringFiles)
        % read scoring file
        tmpScoringStruct(k) =...
            NrImportScoring([filePath fileNames(i).name '\' scoringFiles(k).name]);
    end
    
    if numel(scoringFiles) > 1
        scoringStruct(i) = MergeStructs(tmpScoringStruct);
    else
        scoringStruct(i) = tmpScoringStruct;
    end
end

parfor i = 1:length(fileNames)
   scoringStruct(i).name = [cell2mat(scoringName{i})];
   scoringStruct(i).folder = fileNames(i).name;
end