function filesStruct = BatchNrLoadFiles(filePath,varargin)

%filesStruct = BatchNrLoadFiles(filePath,varargin)
%  Calls the NrLoadFiles function repeatedly. The file path points to a
%  folder containing a collection of folders, which each contains a single
%  measurement session.
%
%   Example:
%   filePath = 'D:\measurements\';
%
%   The folder D:\measurements\ contains the folders
%   D:\measurements\meas01
%   D:\measurements\meas02 ...
%
%   Input
%   filePath      Path to a folder containing multiple folders each
%                 containing a single measurement
%   varargin        
%    'StartTime': The start time from when the signals are to be loaded. If
%                 this time is before the signals start they are zero
%                 padded. If it is after they start the signal are
%                 truncated. Default is the earliest starttime of all the
%                 signal that are loaded.
%                 Format: Noxturnal timestamp, '20150121T223330.377000'
%
%    'StopTime':  The stop time of the signals. If this time is after the
%                 signals stop they are zero padded. If it is before they
%                 start the signal are truncated. Default is the last
%                 stoptime of all the signal that are loaded.
%                 Format: Noxturnal timestamp, '20150121T223330.377000'
%
%    'preset':    A cell array of the signal names to be loaded. Also
%                 supports 'all' and 'noaudio'. Default is 'noaudio'
%   
%   Output
%   filesStruct   An array of structs of the signal data and associated 
%                 info for each of the signal that was loaded
%  Example usage:
%    filesStruct = NrLoadFiles('D:\Work\Data\recording')
%    filesStruct = NrLoadFiles('D:\Work\Data\recording',...
%                              'StartTime','20150121T223330.377000',...
%                              'StopTime','20150122T090120.120000')
%    filesStruct = NrLoadFiles('D:\Work\Data\recording','preset',{'abdomen','thorax'})
%    abdomenStruct = NrLoadFiles('D:\Work\Data\recording\abdomen.ndf')
%
% Note: It is recommended to input full paths. Exists also checks for files
%       in matlab path
%
% Nox Medical 16/06/2015, J�n Sk�rnir �g�stsson

fileNames = DirNoFiles(filePath);

if matlabpool('size') == 0
    matlabpool
end

parfor i = 1:length(fileNames)
    % read Nox files    
    filesStruct(i) = NrLoadFiles([filePath fileNames(i).name], varargin{:});
end
