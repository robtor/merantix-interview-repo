This utility reads signals from the Noxturnal software for sleep research into Matlab.

These signals are recordings from sleep research devices and record various biomedical signals such as
ECG, respiratory signals, EEG, audio etc. from sleeping patients and the utility loads them up as needed.

Care has to be taken that all signals, that can be split into multiple non-continuous sessions, line up according
to their respective sampling rates (which can be non-integer), so that analysis between signals
(correlations, spectral cohesion etc.) make sense and are not offset by drift.
