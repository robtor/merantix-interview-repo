#include <opencv2/core/core.hpp>
#include <random>
#include "init_data.h"
#include "idx_helpers.h"
#include "message_passing.h"
#include "msg_edge.h"
#include "edge_connections.h"

void initializeOrgData(const cv::Mat& img, MsgEdge& org, double w)
{
	// Note the vector is passed by reference and is changed
	// inside this function
	// Assume grayscale image input

	// Add init on wOrgUp
	double NORM_CONST = 255.0;
	for (int i = 0; i < img.rows; i++)
	{
		for (int j = 0; j < img.cols; j++)
		{
			int idx = i*img.cols + j;
			org.eUp[idx] = w * ((double)img.at<uchar>(i, j)) / NORM_CONST;
			org.wUp[idx] = w;
		}
	}
}

void copyRandomEdges(const ColorGroup& ref, ColorGroup& dest)
{
	// Kind of a hacky way of doing this.
	// Think about changing this
	dest.left.eDown = std::vector<double>(ref.left.eDown);
	dest.left.eUp = std::vector<double>(ref.left.eUp);
	dest.left.wDown = std::vector<double>(ref.left.wDown);
	dest.left.wUp = std::vector<double>(ref.left.wUp);

	dest.right.eDown = std::vector<double>(ref.right.eDown);
	dest.right.eUp = std::vector<double>(ref.right.eUp);
	dest.right.wDown = std::vector<double>(ref.right.wDown);
	dest.right.wUp = std::vector<double>(ref.right.wUp);

	dest.org.eDown = std::vector<double>(ref.org.eDown);
	dest.org.wDown = std::vector<double>(ref.org.wDown);
}

void initializeRandomizedEdges(
	MsgEdge& left,
	MsgEdge& right,
	MsgEdge& org,
	EdgeConnections& edgeConnections,
	double sigmaZ)
{

	std::random_device randDev;
	std::mt19937 gen(randDev());
	std::uniform_real_distribution<double> dis(0.0, 1.0);

	double maxVal = 1 / sigmaZ;

	for (int i = 0; i < left.wUp.size(); i++)
	{
		left.wUp[i] = dis(gen) * maxVal;
		left.wDown[i] = dis(gen) * maxVal;

		right.wUp[i] = dis(gen) * maxVal;
		right.wDown[i] = dis(gen) * maxVal;

		// Random init of means, maybe not needed/not feasable?
		left.eUp[i] = dis(gen) * left.wUp[i];
		left.eDown[i] = dis(gen) * left.wDown[i];

		right.eUp[i] = dis(gen) * right.wUp[i];
		right.eDown[i] = dis(gen) * right.wDown[i];
	}

	messagePass3(left, right, org, edgeConnections, 1.0);
}
