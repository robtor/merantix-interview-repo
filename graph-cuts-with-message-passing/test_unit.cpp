#include <iostream>
#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "test_unit.h"
#include "img_helpers.h"
#include "idx_helpers.h"

int testStuff()
{
	// TEST STUFF START
	std::cout << isEven(102) << std::endl;
	std::cout << !isEven(12901) << std::endl;
	std::cout << (idxToCol(5, 5) == 0) << std::endl;
	std::cout << (idxToCol(5, 1) == 0) << std::endl;
	std::cout << (idxToCol(11, 3) == 2) << std::endl;
	std::cout << (idxToRow(5, 5) == 1) << std::endl;
	std::cout << (idxToRow(3, 5) == 0) << std::endl;
	std::cout << (idxToRow(10, 1) == 10) << std::endl;
	std::cout << (!isInSameRow(5, 3, 5)) << std::endl;
	std::cout << (!isInSameRow(5, 4, 5)) << std::endl;
	std::cout << (isInSameRow(6, 9, 5)) << std::endl;
	std::cout << (isLegalIdx(0, 3, 5)) << std::endl;
	std::cout << (isLegalIdx(14, 3, 5)) << std::endl;
	std::cout << (!isLegalIdx(15, 3, 5)) << std::endl;
	std::cout << (!isLegalIdx(-1, 3, 5)) << std::endl;

	/*
	std::vector<int> idxVec = getEdgeIdxConnectedToIdxLeft(0, 3, 5);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	std::cout << "" << std::endl;
	idxVec = getEdgeIdxConnectedToIdxLeft(14, 3, 5);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	std::cout << "" << std::endl;
	idxVec = getEdgeIdxConnectedToIdxRight(14, 3, 5);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	std::cout << "" << std::endl;
	idxVec = getEdgeIdxConnectedToIdxLeft(12, 3, 5);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	idxVec = getEdgeIdxConnectedToIdxRight(12, 3, 5);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	std::cout << "" << std::endl;
	idxVec = getEdgeIdxConnectedToIdxLeft(5, 1, 10);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	std::cout << "" << std::endl;
	idxVec = getEdgeIdxConnectedToIdxLeft(4, 3, 5);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	idxVec = getEdgeIdxConnectedToIdxRight(4, 3, 5);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	std::cout << "" << std::endl;
	idxVec = getEdgeIdxConnectedToIdxLeft(2, 3, 5);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	idxVec = getEdgeIdxConnectedToIdxRight(2, 3, 5);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	std::cout << "" << std::endl;
	idxVec = getEdgeIdxConnectedToIdxLeft(5, 10, 1);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';


	std::cout << "" << std::endl;
	idxVec = getEdgeIdxConnectedToIdxLeft(5, 4, 2);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	std::cout << "" << std::endl;
	idxVec = getEdgeIdxConnectedToIdxLeft(7, 3, 5);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';

	idxVec = getEdgeIdxConnectedToIdxRight(7, 3, 5);
	for (std::vector<int>::const_iterator i = idxVec.begin(); i != idxVec.end(); ++i)
		std::cout << *i << ' ';
	*/

	std::cout << "" << std::endl;
	std::cout << edgeIdxToPixelIdx(7, 5, true) << std::endl;
	std::cout << edgeIdxToPixelIdx(7, 5, false) << std::endl;
	std::cout << edgeIdxToPixelIdx(6, 5, true) << std::endl;
	std::cout << edgeIdxToPixelIdx(6, 5, false) << std::endl;
	std::cout << isLegalEdge(28, 3, 5) << std::endl;
	std::cout << edgeIdxToPixelIdx(28, 5, true) << std::endl;

	//cv::Mat x = loadImg("data/trump.jpeg");
	// TEST STUFF END

	return 1;
}