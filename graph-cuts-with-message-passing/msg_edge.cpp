#include <vector>
#include "msg_edge.h"

MsgEdge::MsgEdge(int initSize, double initValue)
{
	wUp = std::vector<double>(initSize, initValue);
	eUp = std::vector<double>(initSize, initValue);
	wDown = std::vector<double>(initSize, initValue);
	eDown = std::vector<double>(initSize, initValue);
}

ColorGroup::ColorGroup(int imgSize, double initValue)
{
	left = MsgEdge(2 * imgSize, initValue);
	right = MsgEdge(2 * imgSize, initValue);
	org = MsgEdge(imgSize, initValue);
}