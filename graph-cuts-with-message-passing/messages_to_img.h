#ifndef MESSAGE_TO_IMG_H_
#define MESSAGE_TO_IMG_H_

void getPosteriorImgVector(std::vector<double>& mxPosterior, MsgEdge org);

void getPosteriorEdgeVector(
	std::vector<double>& muPosterior,
	std::vector<double>& uPosterior,
	const MsgEdge& left,
	const MsgEdge& right,
	const std::vector<double>& sigmaEdge,
	double sigmaEps);

cv::Mat eOrgToMat(
	const std::vector<double>& eOrg,
	const std::vector<double>& wOrg,
	int nRows, int nCols);

cv::Mat vectorToImg(const std::vector<double>& mxPosterior, int nRows, int nCols);
cv::Mat vectorToMat(const std::vector<int>& vec, int nRows, int nCols);
cv::Mat vectorToMatUchar(const std::vector<int>& vec, int nRows, int nCols);
cv::Mat edgeVectorToImg(const std::vector<double>& sigmaEdge, bool isVert, bool isThresh, int nRows, int nCols);
void multiEdgeImage(
	cv::Mat& edgeImgMultiTot,
	const std::vector<double> sigmaEdge,
	cv::Scalar backgroundColor,
	cv::Scalar foregroundColor,
	bool isVert,
	int nRows,
	int nCols);
cv::Mat getPosteriorColorImg(const std::vector<ColorGroup>& colors, int nRows, int nCols);
cv::Mat getMultipleLevelEdgeImg(
	const std::vector<double>& sigmaEdge1,
	const std::vector<double>& sigmaEdge2,
	bool isVert, bool isThresh,
	int nRows, int nCols);
cv::Mat getConnCompsImgAverage(const cv::Mat& input, const cv::Mat& levels, int nLevels);
void saveEdgeImgs(const std::vector<double> edgeVector, int nRows, int nCols, bool isVert, int runIdx, std::string outputFilepath);


#endif // MESSAGE_TO_IMG_H_