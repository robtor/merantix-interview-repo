#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include "message_passing.h"
#include "idx_helpers.h"
#include "msg_edge.h"
#include "edge_connections.h"

void messagePass1(MsgEdge& left, MsgEdge& right, MsgEdge& org, EdgeConnections& edgeConnections, int nRows, int nCols)
{
	// Assert eLeftUp.size() == eRightUp.size() ?
	for (int i = 0; i < left.eUp.size(); i++)
	{
		if (edgeConnections.isActiveEdge(i))
		{
			// Do stuff for left edge
			bool isRightVar = false;
			int idxConnLeft = edgeIdxToPixelIdx(i, nCols, isRightVar);
			left.wUp[i] = org.wUp[idxConnLeft] + org.wDown[idxConnLeft] - left.wDown[i];
			left.eUp[i] = org.eUp[idxConnLeft] + org.eDown[idxConnLeft] - left.eDown[i];

			// Do stuff for right edge
			isRightVar = true;
			int idxConnRight = edgeIdxToPixelIdx(i, nCols, isRightVar);
			right.wUp[i] = org.wUp[idxConnRight] + org.wDown[idxConnRight] - right.wDown[i];
			right.eUp[i] = org.eUp[idxConnRight] + org.eDown[idxConnRight] - right.eDown[i];
		}
	}
}


void messagePass2(
	MsgEdge& left, MsgEdge& right,
	const std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	double sigmaEps, int nRows, int nCols)
{
	// Assert eLeftUp.size() == eRightUp.size() ?
	for (int i = 0; i < left.eUp.size(); i++)
	{
		if (edgeConnections.isActiveEdge(i))
		{
			// Do stuff for left edge
			double cLeft = 1 + (sigmaEps + sigmaEdge[i]) * right.wUp[i];
			left.wDown[i] = right.wUp[i] / cLeft;
			left.eDown[i] = right.eUp[i] / cLeft;

			// Do stuff for right edge
			double cRight = 1 + (sigmaEps + sigmaEdge[i]) * left.wUp[i];
			right.wDown[i] = left.wUp[i] / cRight;
			right.eDown[i] = left.eUp[i] / cRight;
		}
	}
}

void messagePass2(
	MsgEdge& left, MsgEdge& right,
	const std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	std::vector<double>& sigmaEpsVec, int nRows, int nCols)
{
	// Assert eLeftUp.size() == eRightUp.size() ?
	for (int i = 0; i < left.eUp.size(); i++)
	{
		if (edgeConnections.isActiveEdge(i))
		{
			// Do stuff for left edge
			double cLeft = 1 + (sigmaEpsVec[i] + sigmaEdge[i]) * right.wUp[i];
			left.wDown[i] = right.wUp[i] / cLeft;
			left.eDown[i] = right.eUp[i] / cLeft;

			// Do stuff for right edge
			double cRight = 1 + (sigmaEpsVec[i] + sigmaEdge[i]) * left.wUp[i];
			right.wDown[i] = left.wUp[i] / cRight;
			right.eDown[i] = left.eUp[i] / cRight;
		}
	}
}

bool messagePass3(MsgEdge& left, MsgEdge& right, MsgEdge& org, EdgeConnections& edgeConnections, double convergeEps)
{
	// Performs the 3rd message passing step. 
	// Also checks for convergence of the posterior image
	// The function returns true if convergence has been achieved

	// Note that since the posterior image is a sum eUp + eDown and wUp + wDown
	// where the eUp and wUp are constants (original image), checking for the 
	// convergence of the posterior image is equivalent to checking for the 
	// convergence of wOrgDown and eOrgDown

	bool hasConverged = true;

	for (int i = 0; i < org.eDown.size(); i++)
	{
		double wSum = 0.0;
		double eSum = 0.0;

		for (int j = 0; j < edgeConnections.idxLeft[i].size(); j++)
		{
			wSum = wSum + left.wDown[edgeConnections.idxLeft[i][j]];
			eSum = eSum + left.eDown[edgeConnections.idxLeft[i][j]];
		}

		for (int j = 0; j < edgeConnections.idxRight[i].size(); j++)
		{
			wSum = wSum + right.wDown[edgeConnections.idxRight[i][j]];
			eSum = eSum + right.eDown[edgeConnections.idxRight[i][j]];
		}

		hasConverged = hasConverged && (abs(org.wDown[i] - wSum) < convergeEps);
		hasConverged = hasConverged && (abs(org.eDown[i] - eSum) < convergeEps);

		org.wDown[i] = wSum;
		org.eDown[i] = eSum;
	}

	return hasConverged;
}

double calcWu(double sigmaEdge, double sigmaEps, double leftwUp, double rightwUp)
{
	// Set a guard for dividing by zero?
	return (1 / (sigmaEdge + sigmaEps)) + (leftwUp * rightwUp / (leftwUp + rightwUp));
}
double calcMu(double sigmaU, double rightwUp, double righteUp, double leftwUp, double lefteUp)
{
	return sigmaU * (leftwUp * righteUp - rightwUp * lefteUp) / (leftwUp + rightwUp);
}

std::vector<double> calcExpValColor(
	std::vector<ColorGroup>& colors,
	std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	double sigmaEps)
{
	std::vector<double> expVals = std::vector<double>(sigmaEdge.size());

	for (int i = 0; i < sigmaEdge.size(); i++)
	{
		if (edgeConnections.isActiveEdge(i))
		{
			double expVal = 0.0;
			for (int j = 0; j < colors.size(); j++)
			{
				// Refactor to use the expVals function
				double sigmaU = 1 / calcWu(sigmaEdge[i], sigmaEps, colors[j].left.wUp[i], colors[j].right.wUp[i]);
				double mU = calcMu(sigmaU, colors[j].right.wUp[i], colors[j].right.eUp[i], colors[j].left.wUp[i], colors[j].left.eUp[i]);
				expVal = expVal + std::pow(mU, 2) + sigmaU;
			}
			expVals[i] = expVal / 3.0;
		}
	}

	return expVals;
}

std::vector<double> calcExpValColor(
	const std::vector<ColorGroup>& colors,
	const std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	const std::vector<double>& sigmaEpsVec)
{
	std::vector<double> expVals = std::vector<double>(sigmaEdge.size());

	for (int i = 0; i < sigmaEdge.size(); i++)
	{
		if (edgeConnections.isActiveEdge(i))
		{
			double expVal = 0.0;
			for (int j = 0; j < colors.size(); j++)
			{
				// Refactor to use the expVals function
				double sigmaU = 1 / calcWu(sigmaEdge[i], sigmaEpsVec[i], colors[j].left.wUp[i], colors[j].right.wUp[i]);
				double mU = calcMu(sigmaU, colors[j].right.wUp[i], colors[j].right.eUp[i], colors[j].left.wUp[i], colors[j].left.eUp[i]);
				expVal = expVal + std::pow(mU, 2) + sigmaU;
			}
			expVals[i] = expVal / 3.0;
		}
	}

	return expVals;
}

void emStep(
	MsgEdge& left, MsgEdge& right, std::vector<double>& sigmaEdge, EdgeConnections& edgeConnections,
	double sigmaEps, int nRows, int nCols)
{
	for (int i = 0; i < sigmaEdge.size(); i++)
	{
		if (edgeConnections.isActiveEdge(i))
		{
			// Refactor using the calcMu and calcWu, make sure it works with grayscale
			double wU = (1 / (sigmaEdge[i] + sigmaEps)) + (left.wUp[i] * right.wUp[i] / (left.wUp[i] + right.wUp[i]));
			double sigmaU = 1 / wU;

			double mU = sigmaU * (left.wUp[i] * right.eUp[i] - right.wUp[i] * left.eUp[i]) / (left.wUp[i] + right.wUp[i]);

			sigmaEdge[i] = std::max(std::pow(mU, 2) + sigmaU - sigmaEps, 0.0);
		}
	}
}

void emStepColor(
	std::vector<ColorGroup>& colors, std::vector<double>& sigmaEdge, EdgeConnections& edgeConnections,
	double sigmaEps)
{
	// Estimate sigmall
	std::vector<double> expVals = calcExpValColor(colors, sigmaEdge, edgeConnections, sigmaEps);

	for (int i = 0; i < sigmaEdge.size(); i++)
	{
		if (edgeConnections.isActiveEdge(i))
		{
			sigmaEdge[i] = std::max(expVals[i] - sigmaEps, 0.0);
		}
	}
} 

void emStepColor(
	const std::vector<ColorGroup>& colors,
	std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	const std::vector<double>& sigmaEpsVec)
{
	// Estimate sigmall
	std::vector<double> expVals = calcExpValColor(colors, sigmaEdge, edgeConnections, sigmaEpsVec);

	for (int i = 0; i < sigmaEdge.size(); i++)
	{
		if (edgeConnections.isActiveEdge(i))
		{
			sigmaEdge[i] = std::max(expVals[i] - sigmaEpsVec[i], 0.0);
		}
	}
}

double estimateSigmaEps(
	std::vector<ColorGroup>& colors,
	std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	double sigmaEpsOld)
{
	// Relies on the correct state of edgeConnections. If all
	// edges have been removed, it only needs to calculate the
	// average over the all "active" edges

	
	std::vector<double> expVals = calcExpValColor(colors, sigmaEdge, edgeConnections, sigmaEpsOld);

	int totNumOfEdges = 0;
	double totExpSum = 0.0;
	for (int i = 0; i < expVals.size(); i++)
	{
		if (edgeConnections.isActiveEdge(i))
		{
			totNumOfEdges += 1;
			totExpSum += expVals[i];
		}
	}

	return totExpSum / ((double)totNumOfEdges);
}


void estimateSigmaEpsPerConnectedComponent(
	const std::vector<ColorGroup>& colors,
	const std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	std::vector<double>& sigmaEpsVec,
	const std::vector<int>& connCompsEdge)
{
	// Relies on the correct state of edgeConnections. If all
	// edges have been removed, it only needs to calculate the
	// average over the all "active" edges

	int maxVal = *std::max_element(connCompsEdge.begin(), connCompsEdge.end());
	std::vector<double> expVals = calcExpValColor(colors, sigmaEdge, edgeConnections, sigmaEpsVec);
	std::vector<double> connCompsExpValsSum = std::vector<double>(maxVal + 1, 0.0);
	std::vector<double> connCompsCount = std::vector<double>(maxVal + 1, 0.0);

	for (int i = 0; i < expVals.size(); i++)
	{
		connCompsExpValsSum[connCompsEdge[i]] += expVals[i];
		connCompsCount[connCompsEdge[i]] += 1.0;
	}

	for (int i = 0; i < sigmaEpsVec.size(); i++)
	{
		sigmaEpsVec[i] = connCompsExpValsSum[connCompsEdge[i]] / connCompsCount[connCompsEdge[i]];
	}
}