#ifndef IDX_HELPERS_H_
#define IDX_HELPERS_H_

//std::vector<int> getEdgeIdxConnectedToIdxRight(int idx, int rows, int cols);
//std::vector<int> getEdgeIdxConnectedToIdxLeft(int idx, int rows, int cols);
bool isLegalEdge(int edgeIdx, int rows, int cols);
int idxToCol(int idx, int cols);
int idxToRow(int idx, int cols);
bool isInSameRow(int refIdx, int targetIdx, int cols);
bool isLegalIdx(int refIdx, int rows, int cols);
bool isEven(int num);
int edgeIdxToPixelIdx(int edgeIdx, int cols, bool toRight);

#endif // IDX_HELPERS_H_
