#ifndef GRADIENT_DESCENT_H_
#define GRADIENT_DESCENT_H_

#include <cmath>
#include <vector>
#include "edge_connections.h"

double funcVal(
	const std::vector<double>& sigmaEdge,
	const std::vector<double>& expVals,
	EdgeConnections& edgeConnections,
	double xVal);

double funcDerivativeVal(
	const std::vector<double>& sigmaEdge,
	const std::vector<double>& expVals,
	EdgeConnections& edgeConnections,
	double xVal);

double findXStart(
	const std::vector<double>& sigmaEdge,
	const std::vector<double>& expVals,
	EdgeConnections& edgeConnections);

double stepwiseDescent(
	const std::vector<double>& sigmaEdge,
	const std::vector<double>& expVals,
	EdgeConnections& edgeConnections);

#endif // GRADIENT_DESCENT_H_