import os
import sys
import itertools as it

def get_procs_number(filename):
  num_procs = -1

  with open(filename, 'r') as f:
    for line in f:
      try:
        if 'procs' == line.split()[0]:
          num_procs = line.strip().split()[-1]
      except:
         pass

  num_procs = int(num_procs)

  return num_procs

def rounddown(x, num):
  return x - (x % num)

def create_folder(directory):
  if not os.path.exists(directory):
    os.makedirs(directory)

def create_folders(num_procs, start_point, runs_per_folder):
  level_0 = ['output']
  level_11 = ['img']
  level_12 = ['txt']
  level_111 = list(set([str(rounddown(x, runs_per_folder)) for x in range(start_point, start_point+num_procs)]))

  folder_master_list = []
  folder_master_list.append(it.product(level_0, level_11, level_111))
  folder_master_list.append(it.product(level_0, level_12))

  for folder_list in folder_master_list:
    for folder in folder_list:
      create_folder(os.path.join(*folder))

def main(argv):
  condor_filename = argv[0]
  runs_per_folder = int(argv[1])
  start_point = int(argv[2])
  num_procs = get_procs_number(condor_filename) 

  if num_procs == -1:
    raise ValueError('No process number found')

  basepath = 'output'
  create_folders(num_procs, start_point, runs_per_folder)

if __name__ == '__main__':
  main(sys.argv[1:])
