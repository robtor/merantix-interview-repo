#include <vector>
#include "idx_helpers.h"

bool isEven(int num)
{
	return num % 2 == 0;
}

int idxToCol(int idx, int cols)
{
	return idx % cols;
}

int idxToRow(int idx, int cols)
{
	// Note, this is integer division so 4/5 = 0, 8/5 = 1 etc
	return idx / cols;
}

bool isInSameRow(int refIdx, int targetIdx, int cols)
{
	return idxToRow(refIdx, cols) == idxToRow(targetIdx, cols);
}

bool isLegalIdx(int refIdx, int rows, int cols)
{
	return (refIdx >= 0) && (refIdx < rows*cols);
}

bool isLegalEdge(int edgeIdx, int rows, int cols)
{
	// Note that this check is due to a redundancy in the
	// data structure for simplified calculations
	//
	// Due to the way the data is constructed we only 
	// need to check for right edges and down edges.
	// If edgeIdx is even it is a right edge of a
	// pixel with idx, and we check if the next idx
	// is in the same row
	// If edgeIdx is odd it is a down edge and we
	// only need to check if the idx below (idx + cols)
	// is "outside" the image

	bool flag = false;

	if (isEven(edgeIdx))
	{
		int idx = edgeIdx / 2;
		flag = isInSameRow(idx, idx + 1, cols);
	}
	else
	{
		int idx = (edgeIdx - 1) / 2;
		flag = isLegalIdx(idx + cols, rows, cols);
	}
	return flag;
}

int edgeIdxToPixelIdx(int edgeIdx, int cols, bool toRight)
{
	// Maybe redundant? Already performed this calculation in the isLegal
	// function that is always called before this one. However, this improves
	// readability. Keep it this way at first.
	int idx;
	if (isEven(edgeIdx))
	{
		idx = edgeIdx / 2;
		if (toRight)
		{
			idx = idx + 1;
		}
	}
	else
	{
		idx = (edgeIdx - 1) / 2;
		if (toRight)
		{
			idx = idx + cols;
		}
	}

	return idx;
}