#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include "img_helpers.h"

cv::Mat loadImg(std::string img_name)
{
	cv::Mat img;
	img = cv::imread(img_name, CV_LOAD_IMAGE_COLOR);

	return img;
}

void displayImg(cv::Mat img)
{
	cv::namedWindow("Display window", cv::WINDOW_AUTOSIZE);
	cv::imshow("Display window", img);

	cv::waitKey(0);
}


bool imgEqual(cv::Mat img1, cv::Mat img2)
{
	cv::Mat diff = (img1 != img2);
	int imgCount = cv::countNonZero(diff);

	return imgCount == 0;
}

double getMinMaxVal(cv::Mat img, bool getMax)
{
	double minVal;
	double maxVal;
	cv::Point minLoc;
	cv::Point maxLoc;

	minMaxLoc(img, &minVal, &maxVal, &minLoc, &maxLoc);

	double val = minVal;
	if (getMax)
	{
		val = maxVal;
	}

	return val;
}
