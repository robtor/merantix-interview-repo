#ifndef GENERAL_HELPERS_H_
#define GENERAL_HELPERS_H_

#include <opencv2/core/core.hpp>

void scaleVectorByMaxVal(std::vector<double>& vec);
std::string getOutputImgFilepath(std::string basepath, std::string suffix, std::string fileExtension, int idx);
void strSplit(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> strSplit(const std::string &s, char delim);
std::string getFilenameWoExtension(std::string filepath);
void getAbsVector(std::vector<double>& muPosterior);
cv::Scalar getDisjointColors(int i);
int rounddown(int runIdx, int num);

#endif // GENERAL_HELPERS_H_