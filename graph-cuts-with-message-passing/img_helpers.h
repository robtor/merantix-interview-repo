#ifndef IMG_HELPERS_H_
#define IMG_HELPERS_H_

cv::Mat loadImg(std::string img_name);
void displayImg(cv::Mat img);
bool imgEqual(cv::Mat img1, cv::Mat img2);

#endif // IMG_HELPERS_H_
