#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include "general_helpers.h"
#include "parameter_grid.h"

ParameterGrid::ParameterGrid()
{
	
	imgPaths = std::vector<std::string> { "data/church.jpg"};

	nEmItersVec = std::vector<int>{ 20 };
	nMsgItersVec = std::vector<int>{ 20 };

	//sigmaZsVec = std::vector<double>{ 1.00 * std::pow(10, -2), 1.00 * std::pow(10, -3), 1.00 * std::pow(10, -4),1.00 * std::pow(10, -5) };
	//sigmaEpsVec = std::vector<double>{ 1.00 * std::pow(10, -2), 1.00 * std::pow(10, -3), 1.00 * std::pow(10, -4),1.00 * std::pow(10, -5) };

	sigmaZsVec = std::vector<double>{ 5.00 * std::pow(10, -2), 2.50 * std::pow(10, -2), 1.00 * std::pow(10, -2), 0.75 * std::pow(10, -2), 0.50 * std::pow(10, -2) };
	sigmaEpsVec = std::vector<double>{ 5.00 * std::pow(10, -3), 2.50 * std::pow(10, -3), 1.00 * std::pow(10, -3), 0.75 * std::pow(10, -3), 0.50 * std::pow(10, -3) };

	resetMessageVec = std::vector<bool>{ true };
	
	/*
	imgPaths = std::vector<std::string> { "data/yellow.jpg", "data/horses.jpg", "data/swimmer.jpg", "data/ski.jpg", "data/woman.jpg", "data/tiger.jpg" };

	nEmItersVec = std::vector<int> { 20, 50 };
	nMsgItersVec = std::vector<int> { 20, 50 };

	sigmaZsVec = std::vector<double>{
		1.00 * std::pow(10, -2),
		6.67 * std::pow(10, -3), 3.33 * std::pow(10, -3), 1.00 * std::pow(10, -3),
		6.67 * std::pow(10, -4), 3.33 * std::pow(10, -4), 1.00 * std::pow(10, -4),
		6.67 * std::pow(10, -5), 3.33 * std::pow(10, -5), 1.00 * std::pow(10, -5),
                1.00 * std::pow(10, -6) };
	sigmaEpsVec = std::vector<double>{
		1.00 * std::pow(10, -2),
		6.67 * std::pow(10, -3), 3.33 * std::pow(10, -3), 1.00 * std::pow(10, -3),
		6.67 * std::pow(10, -4), 3.33 * std::pow(10, -4), 1.00 * std::pow(10, -4),
		6.67 * std::pow(10, -5), 3.33 * std::pow(10, -5), 1.00 * std::pow(10, -5),
                1.00 * std::pow(10, -6) };

	resetMessageVec = std::vector<bool>{ false, true };
	*/
	vecLens.push_back((int)imgPaths.size());
	vecLens.push_back((int)nEmItersVec.size());
	vecLens.push_back((int)nMsgItersVec.size());
	vecLens.push_back((int)sigmaZsVec.size());
	vecLens.push_back((int)sigmaEpsVec.size());
	vecLens.push_back((int)resetMessageVec.size());
}

std::string ParameterGrid::getOutputString(int idx)
{
	std::stringstream outputSs;
	outputSs << idx << "," <<
		getFilenameWoExtension(imgPaths[getIdxIntoObject(idx, 0)]) << "," <<
		nEmItersVec[getIdxIntoObject(idx, 1)] << "," <<
		nMsgItersVec[getIdxIntoObject(idx, 2)] << "," <<
		sigmaZsVec[getIdxIntoObject(idx, 3)] << "," <<
		sigmaEpsVec[getIdxIntoObject(idx, 4)] << "," <<
		resetMessageVec[getIdxIntoObject(idx, 5)];

	return outputSs.str();
}

std::string ParameterGrid::getOutputHeader()
{
	std::stringstream outputSs;
	outputSs << "idx" << "," <<
		"name" << "," <<
		"nEmIter" << "," <<
		"nMsgIter" << "," <<
		"sigmaZ" << "," <<
		"sigmaEps" << "," << 
		"resetMsg";

	return outputSs.str();
}

void ParameterGrid::writeOutputStringToFile(std::string filename, int idx)
{
	std::ofstream myfile;
	myfile.open(filename, std::ios_base::app);
	myfile << getOutputString(idx) + "\n";
	myfile.close();
}
void ParameterGrid::setParameters(
	int idx,
	std::string& imgPath,
	int& nEmIters,
	int& nMsgIter,
	double& sigmaZ,
	double& sigmaEps,
	bool& resetMsg)
{
	imgPath = imgPaths[getIdxIntoObject(idx, 0)];
	nEmIters = nEmItersVec[getIdxIntoObject(idx, 1)];
	nMsgIter = nMsgItersVec[getIdxIntoObject(idx, 2)];
	sigmaZ = sigmaZsVec[getIdxIntoObject(idx, 3)];
	sigmaEps = sigmaEpsVec[getIdxIntoObject(idx, 4)];
	resetMsg = resetMessageVec[getIdxIntoObject(idx, 5)];
}

int ParameterGrid::getNumOfPermutations()
{
	int result = 1;

	for (int i = 0; i < vecLens.size(); i++)
	{
		result = result * vecLens[i];
	}

	return result;
}

std::string ParameterGrid::getImgName(int idx)
{
	std::string imgPath = imgPaths[getIdxIntoObject(idx, 0)];
	return getFilenameWoExtension(imgPath);
}

int ParameterGrid::getIdxIntoObject(int idx, int objectIdx)
{
	int vecLenSize = (int)vecLens.size();

	for (int i = vecLenSize - 1; i >= objectIdx; i--)
	{
		if (i == objectIdx) { idx = idx % vecLens[i]; }
		else { idx = idx / vecLens[i]; }
	}

	return idx;
}
