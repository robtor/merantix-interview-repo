Semester project 2
Signal and Information Processing Laboratory (ISI) 

Graph Cuts with NUV-EM and Gaussian message passing

In this project, we investigate a method for cutting edges in a large graph and compare it to standard graph cut algorithms. The proposed method is based on a statistical model where possible neigboors are linked via a random variable allowing a sharp or a smooth transition.

We test our proposed algorithm for segmenting images and/or on large graph datasets.
