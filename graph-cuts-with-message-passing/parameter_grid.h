#ifndef PARAMETER_GRID_H_
#define PARAMETER_GRID_H_

#include <vector>

class ParameterGrid {
public:
	ParameterGrid();
	void setParameters(
		int idx,
		std::string& imgName,
		int& nEmIters,
		int& nMsgIter,
		double& sigmaZ,
		double& sigmaEps,
		bool& resetMsg);

	int getNumOfPermutations();
	std::string getOutputString(int idx);
	void writeOutputStringToFile(std::string filename, int idx);
	std::string getImgName(int idx);
	std::string getOutputHeader();

private:
	std::vector<std::string> imgPaths;

	std::vector<int> nEmItersVec;
	std::vector<int> nMsgItersVec;

	std::vector<double> sigmaZsVec;
	std::vector<double> sigmaEpsVec;

	std::vector<bool> resetMessageVec;

	std::vector<int> vecLens;

	int getIdxIntoObject(int idx, int objectIdx);
};

#endif // PARAMETER_GRID_H_
