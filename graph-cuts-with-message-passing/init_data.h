#ifndef INIT_DATA_H_
#define INIT_DATA_H_

#include "msg_edge.h"
#include "edge_connections.h"

void initializeOrgData(const cv::Mat& img, MsgEdge& org, double w);
void copyRandomEdges(const ColorGroup& ref, ColorGroup& dest);
void initializeRandomizedEdges(
	MsgEdge& left,
	MsgEdge& right,
	MsgEdge& org,
	EdgeConnections& edgeConnections,
	double sigmaZ);

#endif // INIT_DATA_H_