#ifndef EDGE_CONNECTIONS_H_
#define EDGE_CONNECTIONS_H_

class EdgeConnections {
public:
	std::vector<std::vector<int> > idxRight;
	std::vector<std::vector<int> > idxLeft;

	EdgeConnections(int nRows, int nCols);
	void removeNonZeroEdges(const std::vector<double>& sigmaEdge);
	bool isActiveEdge(int edgeIdx);
	int getNumOfActiveEdges();

private:
	std::vector<bool> isActiveEdgeVec;
	std::vector<int> getEdgeIdxConnectedToIdxLeft(int idx, int rows, int cols);
	std::vector<int> getEdgeIdxConnectedToIdxRight(int idx, int rows, int cols);
};

#endif // EDGE_CONNECTIONS_H_
