#ifndef MESSAGE_PASSING_H_
#define MESSAGE_PASSING_H_

#include "msg_edge.h"
#include "edge_connections.h"

void messagePass1(MsgEdge& left, MsgEdge& right, MsgEdge& org, EdgeConnections& edgeConnections, int nRows, int nCols);

void messagePass2(
	MsgEdge& left, MsgEdge& right,
	const std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	std::vector<double>& sigmaEpsVec, int nRows, int nCols);
void messagePass2(
	MsgEdge& left, MsgEdge& right,
	const std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	double sigmaEps, int nRows, int nCols);

bool messagePass3(MsgEdge& left, MsgEdge& right, MsgEdge& org, EdgeConnections& edgeConnections, double convergeEps);
void emStep(MsgEdge& left, MsgEdge& right, std::vector<double>& sigmaEdge, EdgeConnections& edgeConnections, double sigmaEps, int nRows, int nCols);

void emStepColor(
	std::vector<ColorGroup>& colors,
	std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	double sigmaEps);
void emStepColor(
	const std::vector<ColorGroup>& colors,
	std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	const std::vector<double>& sigmaEpsVec);

std::vector<double> calcExpValColor(
	std::vector<ColorGroup>& colors,
	std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	double sigmaEps);
std::vector<double> calcExpValColor(
	const std::vector<ColorGroup>& colors,
	const std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	const std::vector<double>& sigmaEpsVec);

double estimateSigmaEps(
	std::vector<ColorGroup>& colors,
	std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	double sigmaEpsOld);

void estimateSigmaEpsPerConnectedComponent(
	const std::vector<ColorGroup>& colors,
	const std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	std::vector<double>& sigmaEpsVec,
	const std::vector<int>& connCompsEdge);


#endif // MESSAGE_PASSING_H_