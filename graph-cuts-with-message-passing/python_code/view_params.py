import os
import glob
import collections
import pandas as pd
import numpy as np
import cv2
import matplotlib.pyplot as plt

def show_image(img, img_caption):
    cv2.imshow('image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def parse_filepath(pattern):
    filepaths = glob.glob(pattern)
    filenames = [os.path.basename(filepath) for filepath in filepaths]
    indices = [int(filename.split('_')[0]) for filename in filenames]
    filenames_no_ext = [filename.split('.')[0] for filename in filenames]
    img_ids = ['_'.join(filename.split('_')[2:]) for filename in filenames_no_ext]

    d = collections.defaultdict(dict)
    for idx, img_id, filepath in zip(indices, img_ids, filepaths):
        d[idx][img_id] = filepath

    return d

def get_conditioned_df(df, d_conditions):
     bool_cond = (df[df.columns[0]] == df[df.columns[0]])

     for key in d_conditions:
	 bool_cond = (bool_cond) & (df[key] == d_conditions[key])
     
     return df[bool_cond]

def get_img_with_border(img, borderwidth=1):
    img_new = np.zeros((img.shape[0] + 2 * borderwidth, img.shape[1] + 2 * borderwidth, img.shape[2]), dtype='uint8')
    img_new[borderwidth:-borderwidth,borderwidth:-borderwidth] = img

    return img_new

def get_2d_collage(key, df, d_filepaths, groupby_list, params, allowed_values):
    # 2d plot, can only have 2 varying parameters
    assert(len(params) == 2)
    groupby_list_loc = list(groupby_list)

    for param in params:
        groupby_list_loc.remove(param)
    
    dfs = df.groupby(groupby_list_loc)

    for vals, df in dfs:
        print(vals)
        print(df)
        sort_vals = params
        sort_order = [False, False] 

        mydf = df.sort_values(params, ascending=sort_order)

        n_cols = mydf[params[1]].unique().shape[0]
        n_rows = mydf[params[0]].unique().shape[0]

        print(n_cols)
        print(n_rows)

        img_index = mydf['idx'].iloc[0]
        #print(d_filepaths[img_index].keys())
        img = cv2.imread(d_filepaths[img_index][key], cv2.IMREAD_COLOR)
        borderwidth = 1
        img = get_img_with_border(img, borderwidth=borderwidth)

        row_len = img.shape[0]
        col_len = img.shape[1]
        color_depth = img.shape[2]
        print(row_len)
        print(col_len)

        border_thickness = 10
        n_row_pixels = row_len*n_rows + (n_rows-1)*border_thickness
        n_col_pixels = col_len*n_cols + (n_cols-1)*border_thickness

        #black
        img_tot = np.zeros((n_row_pixels, n_col_pixels, color_depth), dtype=img.dtype)
        #white:
        img_tot = np.ones((n_row_pixels, n_col_pixels, color_depth), dtype=img.dtype) * 255

        for i in range(n_rows):
            for j in range(n_cols):
                img_index = mydf['idx'].iloc[i*n_cols+j]
                if 'multiedge' in key:
                    key1 = 'multiedge_vert_thresh_' + key.split('_')[-1]
                    key2 = 'multiedge_hori_thresh_' + key.split('_')[-1]

                    img1 = cv2.imread(d_filepaths[img_index][key1], cv2.IMREAD_COLOR)
                    img2 = cv2.imread(d_filepaths[img_index][key2], cv2.IMREAD_COLOR)

                    change_color(img1, (0,0,0), (255, 255, 255))
                    change_color(img1, (0,255,0), (0, 0, 0))
                    change_color(img1, (255,0,0), (0, 0, 0))

                    change_color(img2, (0,0,0), (255, 255, 255))
                    change_color(img2, (0,255,0), (0, 0, 0))
                    change_color(img2, (255,0,0), (0, 0, 0))

                    img = (img1.astype(np.float32) + img2.astype(np.float32))/2
                    img = img.astype(np.uint8)


                else: 
                    img = cv2.imread(d_filepaths[img_index][key], cv2.IMREAD_COLOR)

                img = get_img_with_border(img, borderwidth=borderwidth)

                row_start = i*row_len + i*border_thickness
                row_end = (i+1)*row_len + i*border_thickness 
                col_start = j*col_len + j*border_thickness
                col_end = (j+1)*col_len + j*border_thickness

                img_tot[row_start:row_end,col_start:col_end,:] = img

        print(img_tot.shape)
        #img_tot = cv2.resize(img_tot, (img_tot.shape[1]/2, img_tot.shape[0]/2), interpolation=cv2.INTER_CUBIC)

        output_filename = mydf['name'].iloc[0] + '_' + key + '_grid.png'
        output_filepath = os.path.join('results', output_filename)
        cv2.imwrite(output_filepath, img_tot)

def create_graph_cut_img(df, d_filepaths, img_index, keys1, keys2, suffix):
    # 2d plot, can only have 2 varying parameters
    assert(len(keys1) == len(keys2))

    n_cols = len(keys1)
    n_rows = 2

    keys = keys1 + keys2

    for index, row in df.iterrows():
       img_index = row['idx']

       key_init = keys[0]

       img = cv2.imread(d_filepaths[img_index][key_init], cv2.IMREAD_COLOR)
       img1_prev_mask_black = np.zeros((img.shape[0], img.shape[1]), dtype=bool)
       img2_prev_mask_black = np.zeros((img.shape[0], img.shape[1]), dtype=bool)

       img1_prev_mask_green = np.zeros((img.shape[0], img.shape[1]), dtype=bool)
       img2_prev_mask_green = np.zeros((img.shape[0], img.shape[1]), dtype=bool)
 
       borderwidth = 1
       img = get_img_with_border(img, borderwidth=borderwidth)

       row_len = img.shape[0]
       col_len = img.shape[1]
       color_depth = img.shape[2]

       border_thickness = 10
       n_row_pixels = row_len*n_rows + (n_rows-1)*border_thickness
       n_col_pixels = col_len*n_cols + (n_cols-1)*border_thickness

       img_tot = np.ones((n_row_pixels, n_col_pixels, color_depth), dtype=img.dtype) * 255
       for i in range(n_rows):
           for j in range(n_cols):
               key = keys[i*n_cols + j]

               if 'multiedge' in key:
                   key1 = 'multiedge_vert_thresh_' + key.split('_')[-1]
                   key2 = 'multiedge_hori_thresh_' + key.split('_')[-1]

                   img1 = cv2.imread(d_filepaths[img_index][key1], cv2.IMREAD_COLOR)
                   img2 = cv2.imread(d_filepaths[img_index][key2], cv2.IMREAD_COLOR)

                   #change_color(img1, (0,0,0), (255, 255, 255))
                   #change_color(img1, (0,255,0), (0, 0, 0))
                   #change_color(img1, (255,0,0), (0, 254, 0))

                   #change_color(img2, (0,0,0), (255, 255, 255))
                   #change_color(img2, (0,255,0), (0, 0, 0))
                   #change_color(img2, (255,0,0), (0, 254, 0))

                   change_color(img1, (0,0,0), (255, 255, 255))
                   change_color(img1, (0,255,0), (0, 0, 0))
                   change_color(img1, (255,0,0), (0, 0, 0))

                   change_color(img2, (0,0,0), (255, 255, 255))
                   change_color(img2, (0,255,0), (0, 0, 0))
                   change_color(img2, (255,0,0), (0, 0, 0))

                   img1[img1_prev_mask_black] = np.array([0,255,0])
                   img2[img2_prev_mask_black] = np.array([0,255,0])

                   img1[img1_prev_mask_green] = np.array([0,255,0])
                   img2[img2_prev_mask_green] = np.array([0,255,0])

                   img1_prev_mask_black = np.where((img1 == [0,0,0]).all(axis = 2))
                   img2_prev_mask_black = np.where((img2 == [0,0,0]).all(axis = 2))

                   img1_prev_mask_green = np.where((img1 == [0,255,0]).all(axis = 2))
                   img2_prev_mask_green = np.where((img2 == [0,255,0]).all(axis = 2))

                   img = (img1.astype(np.float32) + img2.astype(np.float32))/2
                   img = img.astype(np.uint8)
               else: 
                   img = cv2.imread(d_filepaths[img_index][key], cv2.IMREAD_COLOR)

               img = get_img_with_border(img, borderwidth=borderwidth)

               row_start = i*row_len + i*border_thickness
               row_end = (i+1)*row_len + i*border_thickness 
               col_start = j*col_len + j*border_thickness
               col_end = (j+1)*col_len + j*border_thickness

               img_tot[row_start:row_end,col_start:col_end,:] = img

       print(img_tot.shape)
       #img_tot = cv2.resize(img_tot, (img_tot.shape[1]/2, img_tot.shape[0]/2), interpolation=cv2.INTER_CUBIC)

       output_filename = df.loc[img_index]['name'] + '_sz_' + str(row['sigmaZ']) + '_seps_' + str(row['sigmaEps']) + suffix + '_gradual.png'
       output_filepath = os.path.join('results', 'gradual', output_filename)
       cv2.imwrite(output_filepath, img_tot)
    
    
def inspect_element(df, d, groupby_list, element):
    groupby_list_loc = list(groupby_list)
    groupby_list_loc.remove(element)

    dfs = df.groupby(groupby_list_loc, sort=False)
    for val, df_val in dfs:
        print(groupby_list_loc)
        print(val)
        for index, row in df_val.iterrows():
            print('idx: {}, {}: {}'.format(row['idx'], element, row[element]))
            img = cv2.imread(d[row['idx']]['conncomp_20'], cv2.IMREAD_COLOR)
            show_image(img, 'conncomp_20')

def change_color(img, old_color, new_color):
    r1, g1, b1 = old_color # Original value
    r2, g2, b2 = new_color # Value that we want to replace it with

    red, green, blue = img[:,:,0], img[:,:,1], img[:,:,2]
    mask = (red == r1) & (green == g1) & (blue == b1)
    img[:,:,:3][mask] = [r2, g2, b2]


def main():
    basepath ='C:\Work\semester-project-2-data\output' 
    pattern = os.path.join(basepath, 'img', '*', '*.*')
    d = parse_filepath(pattern)

    df = pd.read_csv(os.path.join(basepath, 'txt', 'output_total.txt'), sep=',', header=None, index_col=False)
    df.columns = ['idx', 'name',
                  'nEmIter', 'nMsgIter',
                  'sigmaZ', 'sigmaEps', 'resetMsg']

    sort_vals =['name', 'nEmIter', 'nMsgIter', 'sigmaEps', 'sigmaZ', 'resetMsg'] 
    sort_order = [True, True, True, False, False, True] 
    df = df.sort_values(sort_vals, ascending=sort_order)

    #d_conditions = {'name': 'ski', 'nMsgIter': 50, 'nEmIter': 50, 'resetMsg': 0}
    d_conditions = {'nMsgIter': 50, 'nEmIter': 50, 'resetMsg': 1}
    df = get_conditioned_df(df, d_conditions)

    #print(df['sigmaZ'].unique())
    allowed_values_eps = [10**(-3), 10**(-4), 5*10**(-5), 10**(-5)]
    #allowed_values_eps = [10**(-3), 5*10**(-4), 10**(-4), 5*10**(-5), 10**(-5)]
    #allowed_values_z = [10**(-2), 5*10**(-3), 2.5*10**(-3), 10**(-3), 5*10**(-4)]
    allowed_values_z = [10**(-2), 5*10**(-3), 2.5*10**(-3), 7.5*10**(-4)]

    df = df[df['sigmaZ'].isin(allowed_values_z)]
    df = df[df['sigmaEps'].isin(allowed_values_eps)]

    df = df[~df['name'].isin(['det_headslice75_inv'])]
    
    groupby_list = ['name', 'nEmIter', 'nMsgIter', 'sigmaZ', 'sigmaEps', 'resetMsg']
    
    #key = 'conncomp_0'
    #key = 'posterior_0'
    #key = 'conncomp_20'
    #key = 'multiedge_vert_thresh_0'
    key = 'multiedge_hori_thresh_20'
    #get_2d_collage(key, df, d, groupby_list, ['sigmaZ', 'sigmaEps'], allowed_values_z)

    #inspect_element(df, d, groupby_list, 'sigmaEps')
    #get_2d_collage(df, d, groupby_list, )
    #keys1 = ['conncomp_0', 'conncomp_1', 'conncomp_2', 'conncomp_3','conncomp_8']
    #keys2 = ['multiedge_vert_thresh_0', 'multiedge_vert_thresh_1', 'multiedge_vert_thresh_2', 'multiedge_vert_thresh_3','multiedge_vert_thresh_8']

    keys1 = ['conncomp_0', 'conncomp_2', 'conncomp_4', 'conncomp_8','conncomp_20']
    keys2 = ['multiedge_vert_thresh_0', 'multiedge_vert_thresh_2', 'multiedge_vert_thresh_4', 'multiedge_vert_thresh_8','multiedge_vert_thresh_20']

    suffix = '_multi_comp'
    create_graph_cut_img(df, d, 0, keys1, keys2, suffix)

    keys1 = ['conncomp_0', 'conncomp_20']
    keys2 = ['multiedge_vert_thresh_0', 'multiedge_vert_thresh_20']

    suffix = '_2_comp'
    create_graph_cut_img(df, d, 0, keys1, keys2, suffix)
if __name__ == '__main__':
    main()
