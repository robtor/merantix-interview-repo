from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt

def calc_y(x, sigma_ll, u_ll):
    result = 0.0

    for sigma, u in zip(sigma_ll, u_ll):
        result += np.log(sigma + x)
        result += u/(sigma + x)

    return result

def calc_y_der(x, sigma_ll, u_ll):
    result = 0.0

    for sigma, u in zip(sigma_ll, u_ll):
        result += 1/(sigma + x)
        result -= u/(sigma + x)**2

    return result

def main():
    x = np.logspace(-6, 2, 100)
    #x = np.arange(-2, 1, 200)
    
    mu, sigma_noise = 0, 0.002

    np.random.seed(100)
    n = 100
    sigma_ll = np.abs(np.random.normal(0, 0.1, n))*np.random.binomial(1, 0.1, n)
    u_ll = sigma_ll + np.abs(np.random.normal(mu, sigma_noise, sigma_ll.shape[0]))

    print('jo1')
    y = np.zeros(x.shape)
    y_der = np.zeros(x.shape)
    for i, val in enumerate(x):
        if i % 10 == 0:
            print('Round: ' + str(i))
        y[i] = calc_y(val, sigma_ll, u_ll)


    for i in range(sigma_ll.shape[0]):
        if i % 10 == 0:
            print('')
        print(sigma_ll[i], end=', ')
    print('')
    for i in range(u_ll.shape[0]):
        if i % 10 == 0:
            print('')
        print(u_ll[i], end=', ')
    
    
    #print(y)
    h1, = plt.semilogx(x, y, label='y')
    #h2, = plt.semilogx(x, y_der, label='y der')
    plt.grid(True)
    #plt.legend(handles=[h1, h2])
    plt.ylim([-0.3*10e3, 0.3*10e3])
    plt.show()

if __name__ == '__main__':
    main()

