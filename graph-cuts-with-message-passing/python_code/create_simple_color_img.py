import sys
import numpy as np
import cv2

def split_colors(color1, color2, img_size):
    img = np.zeros((img_size, img_size, 3), np.uint8)

    img[:,:int(img_size/2)] = color1
    img[:,int(img_size/2):] = color2

    return img

def main():
    # Colors are BGR
    color1 = np.array((0, 179, 0))
    color2 = np.array((255, 0, 255))

    # To gray formula is Y = 0.299 R + 0.587 G + 0.114 B

    img = split_colors(color1, color2, 150)
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    cv2.imwrite('colors2.png', img)
    #cv2.imshow('myimg', img)
    #cv2.waitKey(0)
    #cv2.imshow('myimg', gray_img)
    #cv2.waitKey(0)

    #print(gray_img[140, 140])
    #print(gray_img[1, 1])

if __name__ == '__main__':
    main()
