import os
import glob
import collections
import pandas as pd
import numpy as np
import cv2
import matplotlib.pyplot as plt

def main():
    basepath = 'C:\\Work\\semester-project-2-data\\output\\txt'
    tot_list = [] 

    for input_file in glob.glob(os.path.join(basepath, '*.txt')):
        with open(input_file, 'r') as f_in:
            for line in f_in:
                tot_list.append(line.strip())

    idx_list = [int(s.split(',')[0]) for s in tot_list]
    idx_and_tot_list = sorted(zip(idx_list, tot_list), key=lambda x: x[0])
    tot_list_sorted = [s[1] for s in idx_and_tot_list]

    output_file = os.path.join(basepath, 'output_total.txt')
    with open(output_file, 'w') as f_out:
        for line in tot_list_sorted:
            f_out.write(line + '\n')

if __name__ == '__main__':
    main()
