import itertools as it
import os
import numpy as np
import pandas as pd

def main():
    img_names = ['shepp256.png', 'grad_box.png', 'trump.jpeg']
    img_names = [os.path.join('data', img_name) for img_name in img_names]
    n_em_iter = [1, 5, 10, 20, 40]
    sigma_z = np.logspace(-1, -5, 5)
    sigma_eps = np.logspace(-1, -5, 5)

    
    params_list = list(it.product(img_names, n_em_iter, sigma_z, sigma_eps))
    df = pd.DataFrame(params_list)
    df.columns = ['img_name', 'n_em_iter', 'sigma_z', 'sigma_eps']
    df.to_csv('output_params.csv', sep=',', index=False, mode='w')


if __name__ == '__main__':
    main()
