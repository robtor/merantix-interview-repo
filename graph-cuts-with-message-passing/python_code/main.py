import os
import numpy as np
import cv2
import matplotlib.pyplot as plt


def show_image(img):
    cv2.imshow('image', img)
    cv2.waitKey(0) # wait for closing
    cv2.destroyAllWindows() # Ok, destroy the window

def noisy_img(img, sigma=0.01, mode='sat'):
    # Only 1D-case first
    # Add noise and then saturate, don't know
    # if that's the correct way. Other way would be
    # to re-normalize values

    if len(img.shape) > 2:
        raise ValueError('only works for grayscale images. Shape is {}'.format(image.shape))
    else:
       noise = sigma*np.random.randn(*img.shape)

    img_noise = img + noise

    if mode == 'sat':
        img_noise[img_noise > 1.0] = 1.0
        img_noise[img_noise < 0.0] = 0.0
    else:
        img_noise = img_noise/np.amax(img_noise)

    return img_noise

def normalize_gray_img(gray, max_val=255.0):
    max_val = float(max_val)
    return gray/max_val

def main():
    img_name = 'Shepp_logan.png'
    img_path = os.path.join('data', img_name)
    img = cv2.imread(img_path)
    rows, cols, _ = img.shape

    sigma = 0.3
    max_val = 255.0

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray_norm = normalize_gray_img(gray, max_val=max_val)
    gray_noisy = noisy_img(gray_norm, sigma=sigma, mode='none')

    show_image(gray_norm)
    show_image(gray_noisy)

    #print(np.amax(gray_noisy))
    #print(np.amin(gray_noisy))
    #print(gray.shape)
    #print(img.shape)

    #show_image(gray_norm) 
    #show_image(img) 

if __name__ == '__main__':
    main()
