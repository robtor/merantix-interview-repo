import os
import numpy as np
import matplotlib.pyplot as plt

def get_1d_picewise_const():
    x1 = np.ones(100)*0.1
    x2 = np.ones(300)*0.8
    x3 = np.ones(200)*0.65
    x4 = np.ones(100)*0.3
    x5 = np.ones(120)*1.0
    x6 = np.ones(100)*0.45

    return np.concatenate((x1, x2, x3, x4, x5, x6,), axis=0)

def get_noise_1d(x, sigma=0.1):
    return x + sigma*np.random.randn(*x.shape)

def msg_forward_single(m_x_prev, sigma_x_prev, sigma_u, sigma_z, y):
    sigma_ratio = (sigma_u + sigma_x_prev)/(sigma_z + (sigma_u + sigma_x_prev))

    m_x = m_x_prev + sigma_ratio*(y - m_x_prev)
    sigma_x = sigma_ratio * sigma_z

    return m_x, sigma_x

def msg_backwards_single(ex, Wx, sigma_u, sigma_z, my, mx, sigma_x):
    # Not correct stuff
    sigma_y = sigma_z
    F = (1 - sigma_x/sigma_y)
    Wx_prev = F*Wx*F + 1/sigma_y*F
    ex_prev = F*ex + 1/sigma_y*(mx - my)

    return ex_prev, Wx_prev

def msg_forward_pass(sigma_u, sigma_z, y_noise, m_0, sigma_0):
    mx = np.zeros(y_noise.shape)
    sigma_x = np.zeros(y_noise.shape)

    for i in range(0,len(y_noise)):
        if i == 0:
            mx_prev = m_0
            sigma_x_prev = sigma_0
        else:
            mx_prev = mx[i-1]
            sigma_x_prev = sigma_x[i-1]

        mx[i], sigma_x[i] = msg_forward_single(mx_prev,
                                               sigma_x_prev,
                                               sigma_u[i],
                                               sigma_z[i],
                                               y_noise[i])

    return mx, sigma_x


def msg_backward_pass(sigma_u, sigma_z, y_noise, mx, sigma_x):
    ex = np.zeros(y_noise.shape)
    Wx = np.zeros(y_noise.shape)

    for i in reversed(range(1,len(y_noise))):
        ex[i-1], Wx[i-1] = msg_backwards_single(ex[i],
                                                Wx[i],
                                                sigma_u[i],
                                                sigma_z[i],
                                                y_noise[i],
                                                mx[i],
                                                sigma_x[i])

    return ex, Wx
        
def msg_backward_pass_v2(sigma_u, sigma_z, y_noise, mx, sigma_x):
    mx_back = np.zeros(y_noise.shape)
    Vx_back = np.zeros(y_noise.shape)
    Vx_back[-1] = 100

    for i in reversed(range(1,len(y_noise))):
        mx_back[i-1], Vx_back[i-1] = msg_backward_single_v2(mx[i],
                                                            sigma_x[i],
                                                            sigma_u[i],
                                                            sigma_z[i],
                                                            y_noise[i])

    return mx_back, Vx_back

def msg_backward_single_v2(mx, sigma_x, sigma_u, sigma_z, y):
    sigma_ratio = sigma_x/(sigma_z + sigma_x)
    mx_prev = mx + sigma_ratio*(y - mx)
    sigma_x_prev = sigma_u + sigma_ratio*sigma_z

    return mx_prev, sigma_x_prev

def msg_posterior_v2(mx_forw, sigma_x_forw, mx_back, sigma_x_back, sigma_u, sigma_z, y):
    sigma_u_post = np.zeros(y.shape)
    mu_post = np.zeros(y.shape)
    mx_post = np.zeros(y.shape)
    sigma_u_new = np.zeros(y.shape)

    for i in range(1, len(y)):
        sigma_u_back = sigma_x_forw[i-1] + sigma_x_back[i]*sigma_z[i]/(sigma_x_back[i] + sigma_z[i])
        sigma_ratio = sigma_u[i]/(sigma_u[i] + sigma_u_back)

        muk_back = mx_back[i-1] - mx_forw[i-1]

        mu_post[i] = muk_back * sigma_ratio
        sigma_u_post[i] = sigma_u_back * sigma_ratio

        mx_post[i] = mx_forw[i] + sigma_x_forw[i]/(sigma_x_forw[i] + sigma_x_back[i])*(mx_back[i] - mx_forw[i])

        sigma_u_new[i] = max(muk_back**2 - sigma_u_back, 0)

    return mu_post, sigma_u_post, mx_post, sigma_u_new

def msg_posterior(mx, sigma_x, ex, Wx, sigma_u, y_noise):
    mx_post = np.zeros(y_noise.shape)
    sigma_x_post = np.zeros(y_noise.shape)
    muk = np.zeros(y_noise.shape)
    sigma_uk = np.zeros(y_noise.shape)

    for i in range(0, len(y_noise)):
        mx_post[i] = mx[i] - sigma_x[i]*ex[i]
        sigma_x_post[i] = sigma_x[i] - sigma_x[i]*Wx[i]*sigma_x[i]

        sigma_uk[i] = sigma_u[i] - sigma_u[i]*Wx[i]*sigma_u[i]
        muk[i] = sigma_u[i]*Wx[i]*y_noise[i]

    return sigma_uk, muk, mx_post, sigma_x_post 


def plot_stuff(x, x_noise):
    plt.plot(x)
    plt.plot(x_noise)
    #plt.ylim([-0.5, 10.5])
    plt.show()
    print(x.shape)

def main():
    sigma_z_val = 0.04
    sigma_u_val = 0.05

    y_true = get_1d_picewise_const()
    y_noise = get_noise_1d(y_true, sigma=0.08)

    sigma_z = np.ones(y_noise.shape)*sigma_z_val
    sigma_u = np.ones(y_noise.shape)*sigma_u_val

    mx = np.zeros(y_noise.shape)
    sigma_x = np.zeros(y_noise.shape)

    muk_list = []

    for i in range(0,3000):
        m_0 = mx[0]
        sigma_0 = sigma_x[0]

        mx, sigma_x = msg_forward_pass(sigma_u, sigma_z, y_noise, m_0, sigma_0)
        #ex, Wx = msg_backward_pass(sigma_u, sigma_z, y_noise, mx, sigma_x)
        #sigma_u_post, mu_post, mx_post, _ = msg_posterior(mx, sigma_x, ex, Wx, sigma_u, y_noise)
        mx_back, sigma_x_back = msg_backward_pass_v2(sigma_u, sigma_z, y_noise, mx, sigma_x)
        mu_post, sigma_u_post, mx_post, sigma_u_new = msg_posterior_v2(mx, sigma_x, mx_back, sigma_x_back, sigma_u, sigma_z, y_noise)
        #plt.plot(sigma_u_post)
        #plt.show()

        #sigma_u = sigma_u_new
        #sigma_u = np.zeros(sigma_u)
        for i in range(0, len(y_noise)):
            sigma_u[i] = mu_post[i]**2 + sigma_u_post[i]
            if sigma_u[i] < 0.001:
                sigma_u[i] = 0
            #sigma_u[i] = muk[i]**2/(1-sigma_uk[i]/sigma_u[i])


    print(np.amin(sigma_u))

    plt.plot(sigma_u)
    plt.show()

    plt.plot(mu_post)
    plt.show()

    plt.plot(mx_post)
    plt.plot(mx)
    plt.plot(y_noise)
    plt.show()

    plt.plot(mx)
    plt.plot(mx_post[1] + np.cumsum(mu_post))
    plt.plot(y_noise)
    plt.show()
    #plot_stuff(mx_post, y_noise)

    #print(m_x)
    #print(sigma_x)
    #plot_stuff(mx, y_noise)
    #plot_stuff(ex, y_noise)
    #plot_stuff(sigma_x, sigma_u)


    #plt.plot(y_noise)
    #plt.plot(mx_post)
    #plt.plot(mx)
    #plt.show()
        
    #plt.plot(sigma_x_post)
    #plt.show()

    #print(sigma_x_post[595:])
    print(Wx)

if __name__ == '__main__':
    main()
