def state_space(delta):
    A = np.array([[1, delta],[0, 1]])
    C = np.array([[1, 0]])

    return A, C

def fwd_update_complex(mx_prev, Vx_prev, sigma_u, my, sigma_z, A, C):
    A_t = A.transpose()
    C_t = C.transpose()

    mx_mid = np.dot(A, mx_prev)
    Vx_mid = np.dot(np.dot(A, Vx_prev), A_t) + sigma_u*np.identity(2)

    mx, Vx = fwd_update_block_complex(mx_mid, Vx_mid, sigma_z, my, A, C)

    return mx, Vx

def fwd_update_block_complex(mx_prev, Vx_prev, sigma_z, my, A, C):
    C_t = C.transpose()

    G = np.dot(np.dot(C,Vx_prev), C_t) + np.array([sigma_z])
    G = G[0][0]

    H = np.dot(Vx_prev, C_t)*G

    mx = mx_prev + H*(my - np.dot(C, mx_prev))
    Vx = Vx_prev - np.dot(H, np.dot(C, Vx_prev))

    return mx, Vx

def run_complex():
    delta = 0.01
    A, C = state_space(delta)

    sigma_z_val = 0.1
    sigma_u_val = 0.001

    #y_true = np.array(range(0,1000))
    #y_true = y_true*0.01 + 5
    y_true = get_1d_picewise_const()
    y_noise = get_noise_1d(y_true, sigma=sigma_z_val)

    sigma_z = np.ones(y_noise.shape)*sigma_z_val
    sigma_u = np.ones(y_noise.shape)*sigma_u_val

    mx = np.zeros((y_noise.shape[0], 2, 1))
    Vx = np.zeros((y_noise.shape[0], 2, 2))

    m_0 = 0
    sigma_0 = 0.01
    for i in range(0,len(y_noise)):
        if i == 0:
            mx_prev = 1.0*np.ones((2, 1))
            Vx_prev = sigma_0*np.identity(2)
        else:
            mx_prev = mx[i-1]
            Vx_prev = Vx[i-1,:,:]

        mx[i,:,:], Vx[i,:,:] = fwd_update_complex(mx_prev, Vx_prev, sigma_u[i], y_noise[i], sigma_z[i], A, C)


    plot_stuff(mx[:,0,0], y_noise)

def main():
    """
    y_noise = np.array([75, 71, 70, 74])

    sigma_z = np.ones(y_noise.shape)*4.0
    sigma_u = np.ones(y_noise.shape)*0.0

    m_x = np.zeros(y_noise.shape)
    sigma_x = np.zeros(y_noise.shape)

    m_0 = 68
    sigma_0 = 2
    """

