import os
import itertools as it
import time
import numpy as np
import matplotlib.pyplot as plt
import cv2

def show_image(img):
    cv2.imshow('image', img)
    cv2.waitKey(0) # wait for closing
    cv2.destroyAllWindows() # Ok, destroy the window

def normalize_gray_img(gray, max_val=255.0):
    max_val = float(max_val)
    return gray/max_val

def load_image(img_path):
    img = cv2.imread(img_path)

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray_norm = normalize_gray_img(gray)

    return gray_norm

def yx_to_num(yx, img_shape):
    # Note num is 0-indexed
    rows, cols = img_shape
    y, x = yx

    if y >= rows or x >= cols:
        raise ValueError('{} is outside image bounds {}'.format(xy, img_shape))

    return x*cols + y

def num_to_yx(num, img_shape):
    # Note, num is 0-indexed
    rows, cols = img_shape
    if num >= rows*cols:
        raise ValueError('Num ({}) can not be larger than image size ({}).'.format(num, rows*cols))
    y, x = divmod(num, cols)

    return y, x

def img_pairs(img_shape):
    rows, cols = img_shape

    pair_list = []
    ref_list = [[] for i in range(rows*cols)]

    for i in range(rows):
        for j in range(cols):
            curr = (i, j)
            next_row = (i+1, j)
            next_col = (i, j+1)

            curr_num = i*cols + j
            next_row_num = (i+1)*cols + j
            next_col_num = i*cols + (j+1)

            if j+1 < cols:
                #pair_list.append((curr, next_col))
                pair_list.append((curr_num, next_col_num))

                curr_pos = len(pair_list) - 1
                ref_list[curr_num].append(len(pair_list) - 1)
                ref_list[next_col_num].append(len(pair_list) - 1)
            if i+1 < rows:
                #pair_list.append((curr, next_row))
                pair_list.append((curr_num, next_row_num))

                curr_pos = len(pair_list) - 1
                ref_list[curr_num].append(curr_pos)
                ref_list[next_row_num].append(curr_pos)

    return pair_list, ref_list

def initialize_data(img, sigma_z=10**(-3), sigma_ll=10**(-5)):
    pairs, refs = img_pairs(img.shape)
    # Ravel perhaps? Less memory overhead
    img_flat = img.flatten()

    pairs_msg = []
    org = []

    # TODO: Look into the range of random numbers. Can not be bigger than
    # 1/sigma_z because then you can get a negative W in the first
    # msg-passing iteration
    init_rand = False
    for i, ref in enumerate(refs):
        if init_rand:
            w_init = np.random.rand()/sigma_z
        else:
            w_init = 0
        org.append({'msg': {'up': {'e': img_flat[i]/sigma_z, 'w': 1/sigma_z},
                            'down': {'e': 0, 'w': w_init}},
                    'refs': ref})

    #print(sigma_ll)
    #print(sigma_z)
    for i, pair in enumerate(pairs):
        if init_rand:
            w_init = np.random.rand(4)/sigma_z
        else:
            w_init = np.zeros(4)

        pair_dict = {'msg': {pair[0]: {'up': {'e': 0, 'w': w_init[0]},
                                       'down': {'e': 0, 'w': w_init[1]}},
                             pair[1]: {'up': {'e': 0, 'w': w_init[2]},
                                       'down': {'e': 0, 'w': w_init[3]}}},
                     'sigma_ll': sigma_ll,
                     'mu_post': 0,
                     'sigma_ll_post': 0}
        pairs_msg.append(pair_dict)

    return pairs_msg, org


def calculate_posteriors(pairs_msg, sigma_eps=0.001):
    for i in range(len(pairs_msg)):
        # Assume only 2 keys, should always hold
        # Assumption is that key0 is the one with a lower
        # index. Only matters in the mu_post calculation
        key0, key1 = sorted(pairs_msg[i]['msg'].keys())

        w_up0 = pairs_msg[i]['msg'][key0]['up']['w']
        w_up1 = pairs_msg[i]['msg'][key1]['up']['w']
        
        e_up0 = pairs_msg[i]['msg'][key0]['up']['e']
        e_up1 = pairs_msg[i]['msg'][key1]['up']['e']
        
        wu_down = 1/(sigma_eps + pairs_msg[i]['sigma_ll'])
        wu_up = w_up0*w_up1/(w_up0 + w_up1)

        wu = wu_down + wu_up
        
        pairs_msg[i]['sigma_ll_post'] = 1/wu
        pairs_msg[i]['mu_post'] = 1/wu*(w_up0*e_up1 - w_up1*e_up0)/(w_up0 + w_up1)

    return pairs_msg

def calculate_em(pairs_msg, sigma_eps=0.001):
    for i in range(len(pairs_msg)):
        pairs_msg[i]['sigma_ll'] = max(0, pairs_msg[i]['mu_post']**2 + pairs_msg[i]['sigma_ll_post'] - sigma_eps)

    return pairs_msg

def msg_pass1(org, pairs_msg):
    for i in range(len(pairs_msg)):
        for key in pairs_msg[i]['msg']:
            pairs_msg[i]['msg'][key]['up']['w'] = (org[key]['msg']['up']['w']
                                                    + org[key]['msg']['down']['w']
                                                    - pairs_msg[i]['msg'][key]['down']['w'])
            pairs_msg[i]['msg'][key]['up']['e'] = (org[key]['msg']['up']['e']
                                                    + org[key]['msg']['down']['e']
                                                    - pairs_msg[i]['msg'][key]['down']['e'])

    return pairs_msg

def msg_pass2(pairs_msg, sigma_eps=0.001):
    for i in range(len(pairs_msg)):
        key0, key1 = pairs_msg[i]['msg'].keys()
        # Redundant code, just for safety
        mult0 = 1 + (sigma_eps + pairs_msg[i]['sigma_ll'])*pairs_msg[i]['msg'][key1]['up']['w']
        pairs_msg[i]['msg'][key0]['down']['w'] =  pairs_msg[i]['msg'][key1]['up']['w']/mult0
        pairs_msg[i]['msg'][key0]['down']['e'] =  pairs_msg[i]['msg'][key1]['up']['e']/mult0

        mult1 = 1 + (sigma_eps + pairs_msg[i]['sigma_ll'])*pairs_msg[i]['msg'][key0]['up']['w']
        pairs_msg[i]['msg'][key1]['down']['w'] =  pairs_msg[i]['msg'][key0]['up']['w']/mult1
        pairs_msg[i]['msg'][key1]['down']['e'] =  pairs_msg[i]['msg'][key0]['up']['e']/mult1

    return pairs_msg

def msg_pass3(org, pairs_msg):
    for i, d in enumerate(org):
        sum_w = 0
        sum_e = 0
        for ref in d['refs']:
            sum_w += pairs_msg[ref]['msg'][i]['down']['w']
            sum_e += pairs_msg[ref]['msg'][i]['down']['e']

        org[i]['msg']['down']['w'] = sum_w
        org[i]['msg']['down']['e'] = sum_e

    return org

def draw_edges(pairs_msg, img):
    edge_hori = np.zeros(img.shape)
    edge_vert = np.zeros(img.shape)

    for pair in pairs_msg:
        key0, key1 = sorted(pair['msg'].keys())
        y, x = num_to_yx(key0, img.shape)

        if key1 - key0 == 1:
            # If the difference is 1, then it's a vertical edge
            # since they are numbered in row-major order (row first)
            #edge_vert[y][x] = abs(pair['mu_post'])
            edge_vert[y][x] = pair['sigma_ll']
        else:
            #edge_hori[y][x] = abs(pair['mu_post'])
            edge_hori[y][x] = pair['sigma_ll']
            
    return edge_vert, edge_hori

def draw_img_post(org, img_shape):
    img_post = np.zeros(len(org))

    for i in range(len(org)):
        w_post = org[i]['msg']['down']['w'] + org[i]['msg']['up']['w']
        e_post = org[i]['msg']['down']['e'] + org[i]['msg']['up']['e']
        img_post[i] = e_post/w_post

    show_image(img_post.reshape(img_shape))
    print(img_post)

    return img_post

def run_em(img,
           sigma_eps=10**(-3),
           sigma_z=10**(-4),
           em_num_iter=15,
           msg_num_iter=15):

    sigma_ll = 0.1*sigma_eps

    pairs_msg, org = initialize_data(img, sigma_z=sigma_z, sigma_ll=sigma_ll)

    for _x in range(em_num_iter):
        print('EM: {}'.format(_x))
        #monitor(pairs_msg, org)
        for _xx in range(msg_num_iter):
            pairs_msg = msg_pass1(org, pairs_msg)
            #print('1st part')
            #monitor(pairs_msg, org)
            pairs_msg = msg_pass2(pairs_msg, sigma_eps=sigma_eps)
            #print('2nd part')
            #monitor(pairs_msg, org)
            org = msg_pass3(org, pairs_msg)     
            #print('3rd part')
            #monitor(pairs_msg, org)

        # Note, side effects, pairs_msg changes inside the function
        pairs_msg = calculate_posteriors(pairs_msg, sigma_eps=sigma_eps)
        pairs_msg = calculate_em(pairs_msg, sigma_eps=sigma_eps)

    return pairs_msg, org

def monitor(pairs_msg, org):
    for i, pair in enumerate(pairs_msg):
        keys_list = sorted(pair['msg'].keys())
        for key in keys_list:
            print('Pair {}, key {}: w_up: {}, w_dwn: {} '.format(i,
                                                                 key,
                                                                 pair['msg'][key]['up']['w'],
                                                                 pair['msg'][key]['down']['w']))
            
            print('Pair {}, key {}: e_up: {}, e_dwn: {} '.format(i,
                                                                 key,
                                                                 pair['msg'][key]['up']['e'],
                                                                 pair['msg'][key]['down']['e']))

    print('')
    for i, pix in enumerate(org):
        print('Edge {}, w_up: {}, w_dwn: {}'.format(i,
                                                    pix['msg']['up']['w'],
                                                    pix['msg']['down']['w']))
        print('Edge {}, e_up: {}, e_dwn: {}'.format(i,
                                                    pix['msg']['up']['e'],
                                                    pix['msg']['down']['e']))

    print('')

def stringify_list(l):
    return [str(a) for a in l]

def output_files(i, param, elap_time, basepath, edges_vert, edges_hori):
    # This should be cleaned up
    output_params = [i] + list(param) + [elap_time]
    csv_file_out = os.path.join(basepath, 'output_img.csv')

    fileexist = os.path.exists(csv_file_out)
    with open(csv_file_out, 'a') as f:
        if not fileexist:
            header = ('num, img, sigma_eps, sigma_z,'
                      'em_iter, msg_iter, time\n')
            f.write(header)
       
        f.write(','.join(stringify_list(output_params)))
        f.write('\n')

    output_img_name_list = [os.path.split(param[0])[-1]] + \
                           list(param[1:])

    img_vert_name = str(i) + \
                    '_vert_' + \
                    '_'.join(stringify_list(output_img_name_list))
    img_hori_name = str(i) + \
                    '_hori_' + \
                    '_'.join(stringify_list(output_img_name_list))

    cv2.imwrite(os.path.join(basepath, img_vert_name) + '.png', edges_vert*255.0)
    cv2.imwrite(os.path.join(basepath, img_hori_name) + '.png', edges_hori*255.0)

def single_run():
    img = np.zeros((2,2))
    img[0,0] = 1.0
    print(img)

    em_num_iter = 1
    msg_num_iter = 50
    sigma_eps = 0.001
    sigma_z = 0.01

    pairs_msg, org = run_em(img,
                            sigma_eps=sigma_eps,
                            sigma_z=sigma_z,
                            em_num_iter=em_num_iter,
                            msg_num_iter=msg_num_iter)      

def get_noisy_img(x, sigma=0.1):
    return x + sigma*np.random.randn(*x.shape)

def main():
    #single_run()
    #1/0
    #img_list = ['shepp256.png', 'grad_box.png', 'grad_box_inv.png', 'zurich_buildings.jpeg']
    #img_list = ['zurich_buildings.jpeg']
    img_list = ['trump.jpeg']
    path_list = [os.path.join('..','data', img_name) for img_name in img_list]

    #sigma_eps_list = np.logspace(-1, -9, 5)
    #sigma_z_list = np.logspace(-1, -5, 3)
    sigma_eps_list = [10**(-3)]
    sigma_z_list = [10**(-3)]

    #em_num_iter_list = [1, 2, 5, 10]
    #msg_num_iter_list = [1, 2, 5, 10]
    em_num_iter_list = [1]
    msg_num_iter_list = [50]

    params = it.product(path_list,
               sigma_eps_list,
               sigma_z_list,
               em_num_iter_list,
               msg_num_iter_list)

    for i, param in enumerate(params):
        img_path, sigma_eps, sigma_z, em_num_iter, msg_num_iter = param
        output_str = ('img: {}\n'
                      'sigma_eps: {}\n'
                      'sigma_z: {}\n'
                      'em_iter: {}\n'
                      'msg_iter: {}\n')
        output_str = output_str.format(img_path,
                                       sigma_eps,
                                       sigma_z,
                                       em_num_iter,
                                       msg_num_iter)
        print(output_str)
                                     
        start = time.clock()
        img = load_image(img_path)
        #show_image(img)
        #img = get_noisy_img(img, sigma=0.03)
        #show_image(img)
        pairs_msg, org = run_em(img,
                                sigma_eps=sigma_eps,
                                sigma_z=sigma_z,
                                em_num_iter=em_num_iter,
                                msg_num_iter=msg_num_iter)      

        end = time.clock()
        elap_time = end-start
        print('Elapsed time: {}'.format((elap_time)))

        #show_image(img)
        #draw_img_post(org, img.shape)
        #edges_vert, edges_hori = draw_edges(pairs_msg, img)
        #show_image(edges_vert)
        #show_image((edges_vert > 0.0001)*1.0)

        #output_files(i, param, elap_time, 'output',
        #             edges_vert, edges_hori)

if __name__ == '__main__':
    main()

