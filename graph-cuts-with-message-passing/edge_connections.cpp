#include <vector>
#include <algorithm>
#include "idx_helpers.h"
#include "edge_connections.h"

EdgeConnections::EdgeConnections(int nRows, int nCols)
{
	idxRight = std::vector<std::vector<int> >(nRows*nCols);
	idxLeft = std::vector<std::vector<int> >(nRows*nCols);
	isActiveEdgeVec = std::vector<bool>(2*nRows*nCols, true);

	for (int i = 0; i < idxLeft.size(); i++)
	{
		idxLeft[i] = getEdgeIdxConnectedToIdxLeft(i, nRows, nCols);
		idxRight[i] = getEdgeIdxConnectedToIdxRight(i, nRows, nCols);
	}

	for (int i = 0; i < isActiveEdgeVec.size(); i++)
	{
		isActiveEdgeVec[i] = isLegalEdge(i, nRows, nCols);
	}
}

std::vector<int> getNewConnections(const std::vector<double>& sigmaEdge, const std::vector<int>& idxList)
{
	std::vector<int> newIdxList;
	for (int k = 0; k < idxList.size(); k++)
	{
		int currEdgeIdx = idxList[k];

		if (!(sigmaEdge[currEdgeIdx] > 0))
		{
			newIdxList.push_back(currEdgeIdx);
		}
	}

	return newIdxList;
}

bool EdgeConnections::isActiveEdge(int edgeIdx)
{
	return isActiveEdgeVec[edgeIdx];
}

void EdgeConnections::removeNonZeroEdges(const std::vector<double>& sigmaEdge)
{
	for (int i = 0; i < idxLeft.size(); i++)
	{
		idxLeft[i] = getNewConnections(sigmaEdge, idxLeft[i]);
		idxRight[i] = getNewConnections(sigmaEdge, idxRight[i]);
	}

	for (int i = 0; i < isActiveEdgeVec.size(); i++)
	{
		if (sigmaEdge[i] > 0)
		{
			isActiveEdgeVec[i] = false;
		}
	}
}

std::vector<int> EdgeConnections::getEdgeIdxConnectedToIdxLeft(int idx, int rows, int cols)
{
	int right = idx + 1;
	int down = idx + cols;

	// In increasing order of size
	// Rather use array here because of 
	// fixed size?
	std::vector<int> idxs;

	if (isInSameRow(idx, right, cols) && isLegalIdx(right, rows, cols))
	{
		idxs.push_back(2 * idx);
	}
	if (isLegalIdx(down, rows, cols))
	{
		idxs.push_back(2 * idx + 1);
	}

	return idxs;
}

std::vector<int> EdgeConnections::getEdgeIdxConnectedToIdxRight(int idx, int rows, int cols)
{
	int left = idx - 1;
	int up = idx - cols;

	// In increasing order of size
	// Rather use array here because of 
	// fixed size?
	std::vector<int> idxs;

	if (isLegalIdx(up, rows, cols))
	{
		idxs.push_back(2 * up + 1);
	}
	if (isInSameRow(idx, left, cols) && isLegalIdx(left, rows, cols))
	{
		idxs.push_back(2 * left);
	}

	return idxs;
}

int EdgeConnections::getNumOfActiveEdges()
{
	return std::count(isActiveEdgeVec.begin(), isActiveEdgeVec.end(), true);
}
