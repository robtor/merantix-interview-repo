#include <algorithm>
#include <opencv2/core/core.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "general_helpers.h"

double getVectorMaxVal(std::vector<double> vec)
{
	return *std::max_element(vec.begin(), vec.end());
}

void scaleVectorByMaxVal(std::vector<double>& vec)
{
	double maxVal = getVectorMaxVal(vec);

	for (int i = 0; i < vec.size(); i++)
	{
		vec[i] = vec[i] / maxVal;
	}
}

std::string getOutputImgFilepath(std::string basepath, std::string suffix, std::string fileExtension, int idx)
{
	std::stringstream ss;
	ss << basepath << "/" << idx << suffix << "." << fileExtension;

	return ss.str();
}

void strSplit(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
}

std::vector<std::string> strSplit(const std::string &s, char delim) {
	std::vector<std::string> elems;
	strSplit(s, delim, elems);
	return elems;
}

std::string getFilenameWoExtension(std::string filepath)
{
	// Note that this is very "heuristicy" and doesn't account
	// for any corner cases. Assumes that the input has the
	// form .../somepath/filename.extension

	std::vector<std::string> pathSplit = strSplit(filepath, '/');
	std::string filenameWithExtension = pathSplit[pathSplit.size() - 1];

	return strSplit(filenameWithExtension, '.')[0];
}

void getAbsVector(std::vector<double>& muPosterior)
{
	for (int i = 0; i < muPosterior.size(); i++)
	{
		muPosterior[i] = abs(muPosterior[i]);
	}
}

cv::Scalar getDisjointColors(int i)
{
	std::vector<cv::Scalar> colors = {
		cv::Scalar(0, 0, 255),      //Blue
		cv::Scalar(255, 0, 0),      //Red
		cv::Scalar(0, 255, 0),      //Green
		cv::Scalar(255, 255, 0),    //Yellow
		cv::Scalar(255, 0, 255),    //Magenta
		cv::Scalar(255, 128, 128),  //Pink
		cv::Scalar(128, 128, 128),  //Gray
		cv::Scalar(128, 0, 0),      //Brown
		cv::Scalar(255, 128, 0)    //Orange
	};
	
	return colors[i];
	/*
	private static readonly List<Color> _boyntonOptimized = new List<Color>
	{

	};

	kelly_colors = dict(vivid_yellow=(255, 179, 0),
	strong_purple=(128, 62, 117),
	vivid_orange=(255, 104, 0),
	very_light_blue=(166, 189, 215),
	vivid_red=(193, 0, 32),
	grayish_yellow=(206, 162, 98),
	medium_gray=(129, 112, 102),

	# these aren't good for people with defective color vision:
	vivid_green=(0, 125, 52),
	strong_purplish_pink=(246, 118, 142),
	strong_blue=(0, 83, 138),
	strong_yellowish_pink=(255, 122, 92),
	strong_violet=(83, 55, 122),
	vivid_orange_yellow=(255, 142, 0),
	strong_purplish_red=(179, 40, 81),
	vivid_greenish_yellow=(244, 200, 0),
	strong_reddish_brown=(127, 24, 13),
	vivid_yellowish_green=(147, 170, 0),
	deep_yellowish_brown=(89, 51, 21),
	vivid_reddish_orange=(241, 58, 19),
	dark_olive_green=(35, 44, 22))
	*/
}

int rounddown(int runIdx, int num)
{
	return runIdx - (runIdx % num);
}