#ifndef MSG_EDGE_H_
#define MSG_EDGE_H_

struct MsgEdge {
	std::vector<double> wUp;
	std::vector<double> eUp;
	std::vector<double> wDown;
	std::vector<double> eDown;

	MsgEdge(int initSize, double initValue);
};

struct ColorGroup{
	MsgEdge left = MsgEdge(0, 0);
	MsgEdge right = MsgEdge(0, 0);
	MsgEdge org = MsgEdge(0, 0);

	ColorGroup(int initSize = 0, double initValue = 0.0);
};

#endif // MSG_EDGE_H_