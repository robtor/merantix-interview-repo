#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <ctime>
#include <vector>
#include "general_helpers.h"
#include "img_helpers.h"
#include "idx_helpers.h"
#include "init_data.h"
#include "message_passing.h"
#include "messages_to_img.h"
#include "msg_edge.h"
#include "edge_connections.h"
#include "connected_components.h"
#include "parameter_grid.h"
#include "stepwise_descent.h"

bool POSTERIOR_OUTPUT = true;
int NUM_RUNS_PER_FOLDER = 50;

void initializeData(std::vector<ColorGroup>& colors, EdgeConnections& edgeConnections, const cv::Mat img[], double sigmaZ)
{
	for (int i = 0; i < colors.size(); i++)
	{
		initializeOrgData(img[i], colors[i].org, 1.0 / sigmaZ);
	}

	// Fix this hardcode?
	// See what happens with different initializations?
	initializeRandomizedEdges(colors[0].left, colors[0].right, colors[0].org, edgeConnections, sigmaZ);
	copyRandomEdges(colors[0], colors[1]);
	copyRandomEdges(colors[0], colors[2]);
}

void runMessagePassing(
	std::vector<ColorGroup>& colors,
	std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	int nEmIter, int nMsgIter,
	double sigmaZ, std::vector<double> sigmaEpsVec,
	double convergeEps,
	int nRows, int nCols)
{
	for (int i = 0; i < nEmIter; i++)
	{
		for (int k = 0; k < colors.size(); k++)
		{
			int convergenceNum = nMsgIter + 1;
			for (int j = 0; j < nMsgIter; j++)
			{
				bool hasConverged = false;

				messagePass1(colors[k].left, colors[k].right, colors[k].org, edgeConnections, nRows, nCols);
				messagePass2(colors[k].left, colors[k].right, sigmaEdge, edgeConnections, sigmaEpsVec, nRows, nCols);
				hasConverged = messagePass3(colors[k].left, colors[k].right, colors[k].org, edgeConnections, convergeEps);

				if (hasConverged) { convergenceNum = j;	break; }
			}
			//std::cout << "Conv. num: " << convergenceNum << std::endl;
		}

		if (POSTERIOR_OUTPUT)
		{
			std::stringstream ssPost;
			ssPost << "output/posterior_sp/" << i << "_sp_post.png";
			cv::imwrite(ssPost.str(), getPosteriorColorImg(colors, nRows, nCols));

			cv::Mat imgEdgeHori = edgeVectorToImg(sigmaEdge, false, false, nRows, nCols);
			cv::Mat imgEdgeHoriThresh = edgeVectorToImg(sigmaEdge, false, true, nRows, nCols);;

			cv::Mat imgEdgeVert = edgeVectorToImg(sigmaEdge, true, false, nRows, nCols);
			cv::Mat imgEdgeVertThresh = edgeVectorToImg(sigmaEdge, true, true, nRows, nCols);;

			std::stringstream ssEdgeHori;
			ssEdgeHori << "output/posterior_sp/" << i << "_sp_edge_hori.png";
			cv::imwrite(ssEdgeHori.str(), imgEdgeHoriThresh);

			std::stringstream ssEdgeVert;
			ssEdgeVert << "output/posterior_sp/" << i << "_sp_edge_vert.png";
			cv::imwrite(ssEdgeVert.str(), imgEdgeVertThresh);
		}

		//std::cout << "EM: " << i << std::endl;
		emStepColor(colors, sigmaEdge, edgeConnections, sigmaEpsVec);
		//emStep(left, right, sigmaEdge, sigmaEps);
		//if (convergenceNum == 0) { break; }
	}
}

void runMessagePassing(
	std::vector<ColorGroup>& colors,
	std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	int nEmIter, int nMsgIter,
	double sigmaZ, double sigmaEps,
	double convergeEps,
	int nRows, int nCols)
{
	for (int i = 0; i < nEmIter; i++)
	{
		for (int k = 0; k < colors.size(); k++)
		{
			int convergenceNum = nMsgIter + 1;
			for (int j = 0; j < nMsgIter; j++)
			{
				bool hasConverged = false;

				messagePass1(colors[k].left, colors[k].right, colors[k].org, edgeConnections, nRows, nCols);
				messagePass2(colors[k].left, colors[k].right, sigmaEdge, edgeConnections, sigmaEps, nRows, nCols);
				hasConverged = messagePass3(colors[k].left, colors[k].right, colors[k].org, edgeConnections, convergeEps);

				if (hasConverged) { convergenceNum = j;	break; }
			}
			//std::cout << "Conv. num: " << convergenceNum << std::endl;
		}

		//std::cout << "EM: " << i << std::endl;
		emStepColor(colors, sigmaEdge, edgeConnections, sigmaEps);
		//if (convergenceNum == 0) { break; }
	}
}

void outputStuff(
	const cv::Mat& img,
	const std::vector<ColorGroup>& colors,
	const std::vector<double>& sigmaEdge,
	EdgeConnections& edgeConnections,
	const std::vector<int> connComps,
	cv::Mat& edgeImgMultiTotVert,
	cv::Mat& edgeImgMultiTotHori,
	int nRows,
	int nCols,
	int runIdx,
	ParameterGrid paramGrid,
	std::string suffix)
{
	int maxVal = *std::max_element(connComps.begin(), connComps.end());
	cv::Mat connCompMask = vectorToMat(connComps, nRows, nCols);

	cv::Mat connCompImg = getConnCompsImgAverage(img, connCompMask, maxVal);

	// Have already removed non-zero edges, so I can't see if next to a "true" edge
	// need to have all connections intact, therefore I make a new EdgeConnection object without removal

	EdgeConnections orgConnection = EdgeConnections(nRows, nCols);

	std::vector<int> connCompEdge = getEdgePixelsOfConnectedComponents(connComps, orgConnection, nRows, nCols);
	cv::Mat connCompEdgeMask = vectorToMatUchar(connCompEdge, nRows, nCols);

	cv::Mat colorImgPost = getPosteriorColorImg(colors, nRows, nCols);

	//cv::Mat imgEdgeHori = edgeVectorToImg(sigmaEdge, false, false, nRows, nCols);
	cv::Mat imgEdgeHoriThresh = edgeVectorToImg(sigmaEdge, false, true, nRows, nCols);

	//cv::Mat imgEdgeVert = edgeVectorToImg(sigmaEdge, true, false, nRows, nCols);
	cv::Mat imgEdgeVertThresh = edgeVectorToImg(sigmaEdge, true, true, nRows, nCols);

	multiEdgeImage(edgeImgMultiTotVert, sigmaEdge, cv::Scalar(255, 0, 0), cv::Scalar(0, 255, 0), true, nRows, nCols);
	multiEdgeImage(edgeImgMultiTotHori, sigmaEdge, cv::Scalar(255, 0, 0), cv::Scalar(0, 255, 0), false, nRows, nCols);
	
	cvtColor(connCompImg, connCompImg, CV_Lab2BGR);
	cvtColor(colorImgPost, colorImgPost, CV_Lab2BGR);

	// Perform in BGR, easier
	connCompImg.setTo(cv::Scalar(255, 20, 147), connCompEdgeMask);
	
	//displayImg(connCompImg);
	//displayImg(colorImgPost);
	//displayImg(imgEdgeVert);
	//displayImg(imgEdgeVertThresh);
	//displayImg(imgEdgeHori);
	//displayImg(imgEdgeHoriThresh);

	std::string basepath = "output/img/" + std::to_string(rounddown(runIdx, NUM_RUNS_PER_FOLDER)) + "/";
	std::string runIdxStr = std::to_string(runIdx);

	cv::imwrite(
		basepath + runIdxStr + "_" + paramGrid.getImgName(runIdx) + "_posterior" + suffix + ".jpg",
		colorImgPost);

	cv::imwrite(
		basepath + runIdxStr + "_" + paramGrid.getImgName(runIdx) + "_conncomp" + suffix + ".jpg",
		connCompImg);
	/*
	cv::imwrite(
		basepath + runIdxStr + "_" + paramGrid.getImgName(runIdx) + "_sigmall_hori" + suffix + ".jpg",
		imgEdgeHori);
	*/

	/*
	cv::imwrite(
		basepath + runIdxStr + "_" + paramGrid.getImgName(runIdx) + "_sigmall_hori_thresh" + suffix + ".png",
		imgEdgeHoriThresh);
	*/

	/*
	cv::imwrite(
		basepath + runIdxStr + "_" + paramGrid.getImgName(runIdx) + "_sigmall_vert" + suffix + ".png",
		imgEdgeVert);
	*/

	/*
	cv::imwrite(
		basepath + runIdxStr + "_" + paramGrid.getImgName(runIdx) + "_sigmall_vert_thresh" + suffix + ".png",
		imgEdgeVertThresh);
	*/

	cv::imwrite(
		basepath + runIdxStr + "_" + paramGrid.getImgName(runIdx) + "_multiedge_vert_thresh" + suffix + ".png",
		edgeImgMultiTotVert);

	cv::imwrite(
		basepath + runIdxStr + "_" + paramGrid.getImgName(runIdx) + "_multiedge_hori_thresh" + suffix + ".png",
		edgeImgMultiTotHori);
}

void setNonActiveEdgesToZero(std::vector<double>& sigmaEdge, EdgeConnections& edgeConnections)
{
	for (int i = 0; i < sigmaEdge.size(); i++)
	{
		if (!edgeConnections.isActiveEdge(i))
		{
			sigmaEdge[i] = 0.0;
		}
	}
}

int main(int argc, char* argv[])
{
	int runIdx;
 	std::string imgPath;

	double sigmaZ;
	double sigmaEps;

	int nEmIter;
	int nMsgIter;

	bool resetMsg;

	double convergeEps = 0.001;

	ParameterGrid paramGrid = ParameterGrid();
        std::cout << "Num of permutations" << std::endl;
        std::cout << paramGrid.getNumOfPermutations() << std::endl;

	if (argc < 2)
	{
		// Incorperate into the ParameterGrid?
		runIdx = 22;
		imgPath = "data/tiger.jpg";
		
		sigmaZ = 2*std::pow(10, -3);
		sigmaEps = std::pow(10, -4);

		nEmIter = 10;
		nMsgIter = 30;

		resetMsg = false;
	}
	else if(argc == 2)
	{
		runIdx = std::atoi(argv[1]);

		if (runIdx > paramGrid.getNumOfPermutations() - 1)
		{
			std::cout << "Index " << runIdx << " too large";
			return 0;
		}

		paramGrid.setParameters(runIdx, imgPath, nEmIter, nMsgIter, sigmaZ, sigmaEps, resetMsg);
	}
	else if(argc == 3)
	{
		runIdx = std::atoi(argv[1]) + std::atoi(argv[2]);

		if (runIdx > paramGrid.getNumOfPermutations() - 1)
		{
			std::cout << "Index " << runIdx << " too large";
			return 0;
		}

		paramGrid.setParameters(runIdx, imgPath, nEmIter, nMsgIter, sigmaZ, sigmaEps, resetMsg);
	}
        else
        {
		std::cout << "Not correct number of input parameters" << std::endl;
		return 0;
        }

	cv::Mat img = loadImg(imgPath);
	if (!img.data) { std::cout << "No img found\n"; return -1; }

	std::cout << "runIdx" << std::endl;
	std::cout << runIdx << std::endl;

	std::cout << paramGrid.getOutputHeader() << std::endl;
	std::cout << paramGrid.getOutputString(runIdx) << std::endl;
	
	cvtColor(img, img, CV_BGR2Lab);

	int nCols = img.cols;
	int nRows = img.rows;
	int imgSize = nCols*nRows;
	
	cv::Mat channels[3];
	cv::split(img, channels);

	std::vector<ColorGroup> colors;
	colors.push_back(ColorGroup(imgSize, 0.0));
	colors.push_back(ColorGroup(imgSize, 0.0));
	colors.push_back(ColorGroup(imgSize, 0.0));

	EdgeConnections edgeConnections = EdgeConnections(nRows, nCols);
	//std::vector<double> sigmaEpsVec = std::vector<double>(2 * imgSize, sigmaEps);

	cv::Mat edgeImgMultiTotVert = cv::Mat::zeros(nRows, nCols, CV_8UC3);
	cv::Mat edgeImgMultiTotHori = cv::Mat::zeros(nRows, nCols, CV_8UC3);

	// If this run has already been performed, we stop. 
	// This is for the parallel condor stuff
	std::string prefix = std::to_string(runIdx) + "_";
	std::string outputFilename = "output/txt/" + prefix + "output.txt";

	if (std::ifstream(outputFilename))
	{
		std::cout << "File already exists" << std::endl;
		return 0;
	}

	for (int i = 0; i < 21; i++)
	{
		if (resetMsg || i == 0)
		{
			colors[0] = ColorGroup(imgSize, 0.0);
			colors[1] = ColorGroup(imgSize, 0.0);
			colors[2] = ColorGroup(imgSize, 0.0);

			initializeData(colors, edgeConnections, channels, sigmaZ);
		}

		std::vector<double> sigmaEdge = std::vector<double>(2 * imgSize, 0.1 * sigmaEps);
		setNonActiveEdgesToZero(sigmaEdge, edgeConnections);

		runMessagePassing(
			colors,
			sigmaEdge, edgeConnections,
			nEmIter, nMsgIter,
			sigmaZ, sigmaEps,
			convergeEps,
			nRows, nCols);

		edgeConnections.removeNonZeroEdges(sigmaEdge);

		std::vector<int> connComps = connectedComponents(edgeConnections, nRows, nCols);
		//std::vector<int> connCompsEdge = assignEdgesToConnectedComponents(connComps, edgeConnections);
		//estimateSigmaEpsPerConnectedComponent(colors, sigmaEdge, edgeConnections, sigmaEpsVec, connCompsEdge);

		std::string suffix = "_" + std::to_string(i);
		if (i % 4 == 0 || i < 5)
		{
			outputStuff(img, colors, sigmaEdge, edgeConnections, connComps, edgeImgMultiTotVert, edgeImgMultiTotHori, nRows, nCols, runIdx, paramGrid, suffix);
		}
	}

	paramGrid.writeOutputStringToFile(outputFilename, runIdx);

	return 0;
}
