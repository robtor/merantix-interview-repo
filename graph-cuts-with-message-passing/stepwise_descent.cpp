#include <cmath>
#include <vector>
#include <assert.h>
#include "edge_connections.h"
#include "stepwise_descent.h"

double funcVal(
	const std::vector<double>& sigmaEdge,
	const std::vector<double>& expVals,
	EdgeConnections& edgeConnections,
	double xVal)
{
	double yVal = 0.0;

	for (int i = 0; i < sigmaEdge.size(); i++)
	{
		//if (edgeConnections.isActiveEdge(i))
		if (true)
		{
			yVal = yVal + log(sigmaEdge[i] + xVal) + expVals[i] / (sigmaEdge[i] + xVal);
		}
	}

	return yVal;
}

double funcDerivativeVal(
	const std::vector<double>& sigmaEdge,
	const std::vector<double>& expVals,
	EdgeConnections& edgeConnections,
	double xVal)
{
	double yVal = 0.0;

	for (int i = 0; i < sigmaEdge.size(); i++)
	{
		if (edgeConnections.isActiveEdge(i))
		{
			yVal = yVal + 1/(sigmaEdge[i] + xVal) - expVals[i] / std::pow(sigmaEdge[i] + xVal, 2.0);
		}
	}

	return yVal;
}

double findXStart(
	const std::vector<double>& sigmaEdge,
	const std::vector<double>& expVals,
	EdgeConnections& edgeConnections)
{
	// This function is spefic to this mathematical function we are trying to 
	// minimize. Sum of functions that all have only one global minum and therefore
	// the "maximum minumum" that is the function that has it's minimum at the highest
	// value of x must be the upper bound for the global minimum of the sum

	double xStart = 0.0;

	for (int i = 0; i < sigmaEdge.size(); i++)
	{
		if (edgeConnections.isActiveEdge(i))
		{
			double currVal = expVals[i] - sigmaEdge[i];
			if (currVal > xStart) { xStart = currVal; }
		}
	}

	return xStart;
}

double stepwiseDescentNaive(
	const std::vector<double>& sigmaEdge,
	const std::vector<double>& expVals,
	EdgeConnections& edgeConnections)
{
        // This might overshoot the minimum by at most xStart/startStepSize
	double x = findXStart(sigmaEdge, expVals, edgeConnections);
	double y = funcVal(sigmaEdge, expVals, edgeConnections, x);

	double yLast;

	// The value to find must be larger than 0.0
	// Starting stepsize is (x - 0)/10
        double startStepSize = 10.0;
	double nuMax = x / startStepSize;
	double nuMin = 10e-10;
	double nuCurr = nuMax;

	while (nuCurr > nuMin)
	{
		// Not optimized so if it goes into the if-statement it calculates the 
		// gradient for the same point again

		yLast = y;
		y = funcVal(sigmaEdge, expVals, edgeConnections, x);

		if (y > yLast)
		{
			x = x + nuCurr;
			nuCurr = nuCurr / 10;
		}
		else { x = x - nuCurr; }
	}

	return x;
}

double stepwiseDescent(
	const std::vector<double>& sigmaEdge,
	const std::vector<double>& expVals,
	EdgeConnections& edgeConnections)
{
	// This essentially searches for the zero of the derivative
	// Comes in from the right and always moves left
	double x = findXStart(sigmaEdge, expVals, edgeConnections);
	double y = funcDerivativeVal(sigmaEdge, expVals, edgeConnections, x);
	
	assert(y > 0.0);

	// The value to find must be larger than 0.0
	// Starting stepsize is (x - 0)/10
	double startStepSize = 10.0;
	double nuMax = x / startStepSize;
	double nuMin = 10e-10;
	double nuCurr = nuMax;

	while (nuCurr > nuMin)
	{
		// Not optimized so if it goes into the if-statement it calculates the 
		// gradient for the same point again

		y = funcDerivativeVal(sigmaEdge, expVals, edgeConnections, x);

		if (y < 0.0)
		{
			x = x + nuCurr;
			nuCurr = nuCurr / 10;
		}

		x = x - nuCurr;
		assert(x > 0.0);
	}

	return x;
}
