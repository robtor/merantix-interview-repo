#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include "edge_connections.h"
#include "idx_helpers.h"
#include "connected_components.h"

void printQueue(std::queue<int> q)
{
	// Should be passed by value (copying) so emptying it here should be fine
	while (!q.empty())
	{
		std::cout << q.front() << ",";
		q.pop();
	}
	std::cout << "\n";
}

void addCurrentConnectionsToQueue(
	std::queue<int>& idxQueue,
	int currIdx,
	std::vector<bool>& hasBeenVisited,
	const EdgeConnections& edgeConnections,
	int nCols
	)
{
	for (int k = 0; k < edgeConnections.idxLeft[currIdx].size(); k++)
	{
		int currEdgeIdx = edgeConnections.idxLeft[currIdx][k];
		int currAddIdx = edgeIdxToPixelIdx(currEdgeIdx, nCols, true);
		
		if (!hasBeenVisited[currAddIdx])
		{
			idxQueue.push(currAddIdx);
			hasBeenVisited[currAddIdx] = true;
		}
	}
	for (int k = 0; k < edgeConnections.idxRight[currIdx].size(); k++)
	{
		int currEdgeIdx = edgeConnections.idxRight[currIdx][k];
		int currAddIdx = edgeIdxToPixelIdx(currEdgeIdx, nCols, false);

		if (!hasBeenVisited[currAddIdx])
		{
			idxQueue.push(currAddIdx);
			hasBeenVisited[currAddIdx] = true;
		}
	}
}

std::vector<int> getEdgePixelsOfConnectedComponents(
	const std::vector<int>& connectedComponentsVec,
	EdgeConnections& edgeConnections,
	int nRows,
	int nCols)
{
	std::vector<int> connCompEdgeMask = std::vector<int>(connectedComponentsVec.size(), 0);
	std::vector<bool> paintedEdges = std::vector<bool>(2 * nRows * nCols, false);

	for (int i = 0; i < connectedComponentsVec.size(); i++)
	{
		bool isEdge = false;
		int pixelComponentVal = connectedComponentsVec[i];

		std::vector<int> connectedPixelEdges;
		
		for (int j = 0; j < edgeConnections.idxLeft[i].size(); j++)
		{
			int pixelIdx = edgeIdxToPixelIdx(edgeConnections.idxLeft[i][j], nCols, true);
			int connPixelComponentVal = connectedComponentsVec[pixelIdx];

			if (pixelComponentVal != connPixelComponentVal)
			{
				connectedPixelEdges.push_back(edgeConnections.idxLeft[i][j]);
				isEdge = true;
			}
		}
		for (int j = 0; j < edgeConnections.idxRight[i].size(); j++)
		{
			int pixelIdx = edgeIdxToPixelIdx(edgeConnections.idxRight[i][j], nCols, false);
			int connPixelComponentVal = connectedComponentsVec[pixelIdx];

			if ((!isEdge) && (pixelComponentVal != connPixelComponentVal))
			{
				connectedPixelEdges.push_back(edgeConnections.idxRight[i][j]);
				isEdge = true;
			}
		}

		if (isEdge)
		{
			bool shouldBePainted = false;

			for (size_t t = 0; t < connectedPixelEdges.size(); t++)
			{
				shouldBePainted = (!paintedEdges[connectedPixelEdges[t]]) || shouldBePainted;
			}

			if (shouldBePainted)
			{
				for (size_t t = 0; t < connectedPixelEdges.size(); t++)
				{
					paintedEdges[connectedPixelEdges[t]] = true;
				}

				connCompEdgeMask[i] = 1;
			}
			

		}
	}

	return connCompEdgeMask;
}

std::vector<int> connectedComponents(
	const EdgeConnections& edgeConnections,
	int nRows, int nCols)
{
	std::vector<int> connComps = std::vector<int>(nRows*nCols, 0);
	std::vector<bool> hasBeenVisited = std::vector<bool>(nRows*nCols, false);
	std::queue<int> idxQueue;

	int currComponentNum = 1;

	for (int i = 0; i < connComps.size(); i++)
	{
		if (!hasBeenVisited[i])
		{
			idxQueue.push(i);
			hasBeenVisited[i] = true;

			while (!idxQueue.empty())
			{
				int currIdx = idxQueue.front();
				idxQueue.pop();
				connComps[currIdx] = currComponentNum;

				addCurrentConnectionsToQueue(idxQueue, currIdx, hasBeenVisited, edgeConnections, nCols);
			}

			currComponentNum = currComponentNum + 1;
		}
	}

	return connComps;
}

std::vector<int> countNumInComponents(std::vector<int> connComps)
{
	std::vector<int> connCompsCount = std::vector<int>(*std::max_element(connComps.begin(), connComps.end()) + 1, 0);

	for (int i = 0; i < connComps.size(); i++)
	{
		connCompsCount[connComps[i]] += 1;
	}

	return connCompsCount;
}

std::vector<int> assignEdgesToConnectedComponents(const std::vector<int>& connComps, EdgeConnections& edgeConnections)
{
	std::vector<int> connCompsEdge = std::vector<int>(2 * connComps.size(), 0);

	for (int i = 0; i < connComps.size(); i++)
	{
		for (int j = 0; j < edgeConnections.idxLeft[i].size(); j++)
		{
			connCompsEdge[edgeConnections.idxLeft[i][j]] = connComps[i];
		}
		
		for (int j = 0; j < edgeConnections.idxRight[i].size(); j++)
		{
			connCompsEdge[edgeConnections.idxRight[i][j]] = connComps[i];
		}
	}

	return connCompsEdge;
}
