#!/bin/bash
start_point=$2
num_runs=$1

rsync -r --remove-source-files /home/robertto/semester-project-2/output/img/ /scratch_net/isitux22/robertto/output/img/ &&
rsync -r /home/robertto/semester-project-2/output/txt/ /scratch_net/isitux22/robertto/output/txt/ &&
sed -e "s/\${num_runs}/$num_runs/" -e "s/\${start_point}/$start_point/" run.condor.template > run.condor &&
python create_output_folder_structure.py run.condor 50 $start_point &&
condor_submit run.condor
