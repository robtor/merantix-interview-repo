#ifndef CONNECTED_COMPONENTS_H_
#define CONNECTED_COMPONENTS_H_

#include <queue>
#include "edge_connections.h"

void addCurrentConnectionsToQueue(
	std::queue<int>& idxQueue,
	int currIdx,
	std::vector<bool>& hasBeenVisited,
	const EdgeConnections& edgeConnections,
	int nCols
);
std::vector<int> connectedComponents(
	const EdgeConnections& edgeConnections,
	int nRows,
	int nCols);

std::vector<int> getEdgePixelsOfConnectedComponents(
	const std::vector<int>& connectedComponentsVec,
	EdgeConnections& edgeConnections,
	int nRows,
	int nCols);

std::vector<int> countNumInComponents(std::vector<int> connComps);
std::vector<int> assignEdgesToConnectedComponents(const std::vector<int>& connComps, EdgeConnections& edgeConnections);


#endif // CONNECTED_COMPONENTS_H_