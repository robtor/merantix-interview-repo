#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "idx_helpers.h"
#include "img_helpers.h"
#include "msg_edge.h"
#include "messages_to_img.h"

void getPosteriorImgVector(std::vector<double>& mxPosterior, MsgEdge org)
{
	for (int i = 0; i < org.eDown.size(); i++)
	{
		double ePost = org.eUp[i] + org.eDown[i];
		double wPost = org.wUp[i] + org.wDown[i];

		mxPosterior[i] = ePost / wPost;
	}
}

void getPosteriorEdgeVector(
	std::vector<double>& muPosterior,
	std::vector<double>& uPosterior,
	const MsgEdge& left,
	const MsgEdge& right,
	const std::vector<double>& sigmaEdge,
	double sigmaEps)
{
	for (int i = 0; i < sigmaEdge.size(); i++)
	{
		double wU = (1 / (sigmaEdge[i] + sigmaEps)) + (left.wUp[i] * right.wUp[i] / (left.wUp[i] + right.wUp[i]));
		uPosterior[i] = 1 / wU;

		muPosterior[i] = 1 / wU * (left.wUp[i] * right.eUp[i] - right.wUp[i] * left.eUp[i]) / (left.wUp[i] + right.wUp[i]);
	}
}

cv::Mat eOrgToMat(
	const std::vector<double>& eOrg,
	const std::vector<double>& wOrg,
	int nRows, int nCols)
{
	cv::Mat outputShow = cv::Mat(nRows, nCols, CV_8UC1);
	for (int i = 0; i < nRows; i++)
	{
		for (int j = 0; j < nCols; j++)
		{
			outputShow.at<uchar>(i, j) = (int)(255.0*eOrg[i*nCols + j] / wOrg[i*nCols + j]);
		}
	}

	return outputShow;
}

cv::Mat vectorToImg(const std::vector<double>& mxPosterior, int nRows, int nCols)
{
	cv::Mat img = cv::Mat(nRows, nCols, CV_8UC1);

	for (int i = 0; i < nRows; i++)
	{
		for (int j = 0; j < nCols; j++)
		{
			img.at<uchar>(i, j) = (int)(255.0 * mxPosterior[i*nCols + j]);
		}
	}

	return img;
}

cv::Mat vectorToMat(const std::vector<int>& vec, int nRows, int nCols)
{
	cv::Mat img = cv::Mat(nRows, nCols, CV_32SC1);

	for (int i = 0; i < nRows; i++)
	{
		for (int j = 0; j < nCols; j++)
		{
			img.at<int>(i, j) = vec[i*nCols + j];
		}
	}

	return img;
}

cv::Mat vectorToMatUchar(const std::vector<int>& vec, int nRows, int nCols)
{
	cv::Mat img = cv::Mat(nRows, nCols, CV_8UC1);

	for (int i = 0; i < nRows; i++)
	{
		for (int j = 0; j < nCols; j++)
		{
			img.at<uchar>(i, j) = vec[i*nCols + j];
		}
	}

	return img;
}

cv::Mat edgeVectorToImg(const std::vector<double>& sigmaEdge, bool isVert, bool isThresh, int nRows, int nCols)
{
	cv::Mat imgEdge = cv::Mat(nRows, nCols, CV_8UC1, double(0));

	for (int i = 0; i < nRows; i++)
	{
		for (int j = 0; j < nCols; j++)
		{
			int currIdx = i*nCols + j;
			int edgeIdx = 2 * currIdx;

			if (!isVert)
			{
				edgeIdx = edgeIdx + 1;
			}

			if (isLegalEdge(edgeIdx, nRows, nCols))
			{
				if (isThresh)
				{
					int val = (int)255.0*(sigmaEdge[edgeIdx] > 0.0);
					imgEdge.at<uchar>(i, j) = val;
				}
				else
				{
					imgEdge.at<uchar>(i, j) = (int)(255.0*sigmaEdge[edgeIdx]);
				}
			}
		}
	}

	return imgEdge;
}

void multiEdgeImage(
	cv::Mat& edgeImgMultiTot,
	const std::vector<double> sigmaEdge,
	cv::Scalar backgroundColor,
	cv::Scalar foregroundColor,
	bool isVert,
	int nRows,
	int nCols)
{
	cv::Mat mask;
	inRange(edgeImgMultiTot, foregroundColor, foregroundColor, mask);
	edgeImgMultiTot.setTo(backgroundColor, mask);

	edgeImgMultiTot.setTo(foregroundColor, edgeVectorToImg(sigmaEdge, isVert, true, nRows, nCols));
}

cv::Mat getPosteriorColorImg(const std::vector<ColorGroup>& colors, int nRows, int nCols)
{
	std::vector<cv::Mat> colorImgVec;

	for (int i = 0; i < colors.size(); i++)
	{
		std::vector<double> mxPosterior(nRows*nCols, 0.0);

		getPosteriorImgVector(mxPosterior, colors[i].org);
		//displayImg(vectorToImg(mxPosterior, nRows, nCols));
		colorImgVec.push_back(vectorToImg(mxPosterior, nRows, nCols));
	}

	cv::Mat colorImg;
	cv::merge(colorImgVec, colorImg);

	return colorImg;
}

cv::Mat getMultipleLevelEdgeImg(
	const std::vector<double>& sigmaEdge1,
	const std::vector<double>& sigmaEdge2,
	bool isVert, bool isThresh,
	int nRows, int nCols)
{
	std::vector<cv::Mat> edgeImgVec;
	edgeImgVec.push_back(edgeVectorToImg(sigmaEdge1, isVert, isThresh, nRows, nCols));
	edgeImgVec.push_back(edgeVectorToImg(sigmaEdge2, isVert, isThresh, nRows, nCols));
	edgeImgVec.push_back(cv::Mat::zeros(nRows, nCols, CV_8UC1));

	cv::Mat outputImg;
	cv::merge(edgeImgVec, outputImg);

	return outputImg;
}

cv::Mat getConnCompsImgAverage(const cv::Mat& input, const cv::Mat& levels, int nLevels)
{
	assert(levels.channels() == 1);
	cv::Mat output(input.size(), input.type());

	for (int level = 0; level <= nLevels; level++)
	{
		const cv::Mat mask = levels == level;
		const cv::Scalar avg = cv::mean(input, mask);

		output.setTo(avg, mask);
	}

	return output;
}

void saveEdgeImgs(const std::vector<double> edgeVector, int nRows, int nCols, bool isVert, int runIdx, std::string outputFilepath)
{
	cv::Mat imgEdge = edgeVectorToImg(edgeVector, isVert, false, nRows, nCols);
	cv::Mat imgEdgeThresh;

	cv::threshold(imgEdge, imgEdgeThresh, 1, 255, cv::THRESH_BINARY_INV);
	bitwise_not(imgEdge, imgEdge);

	cv::imwrite(outputFilepath + ".png", imgEdge);
	cv::imwrite(outputFilepath + "_thresh.png", imgEdgeThresh);
}
